# JBPEL2ISPL

A BPEL to ISPL (MCMAS) converter.



## To Do

- pick
- flow

## Get it running

### Release

#### Build the release
  
    ./gradlew clean build

The release *.zip and *.tar can be found in `build/distributions/`.
Use the `jbpel2ispl-boot-*` archive. The other most likely doesn't work.

#### Run

1. Extract `unzip build/distributions/jbpel2ispl-boot-*.zip`
2. Change into `cd ./jbpel2ispl-boot-*` directory
3. Launch `bin/jbpel2ispl <path-to-bpel-file>`
4. See the `<path-to-bpel-file>.ispl` for the output

Commandline options:

    usage: jbpel2ispl [-D <arg>] [-h] [-o <path-to-output-file>] <path-to-bpel-file>
    
    Converts an BPEL input file into an ISPL file for MCMAS
    
     -D <arg>                            spring options
     -h,--help                           Hilfe
     -o,--output <path-to-output-file>   Name der Ausgabedatei
    
    Please report issues at
    https://gitlab.com/bpel-verification/JBPEL2ISPL/issues

### For debug:

Run neo4j [docker image](https://hub.docker.com/_/neo4j/) and browse the model via the neo4j browser

1. Run the docker image (mounts a volume from `<currentdir>/tmp/neo4j.data` as data volume into container)

        docker run \
            --publish=7474:7474 --publish=7687:7687 \
            --volume=$(pwd)/tmp/neo4j.data:/data \
            --env=NEO4J_AUTH=none \
            --name neo4j \
            --env=NEO4J_dbms_memory_pagecache_size=4G \
            --env=NEO4J_dbms_memory_heap_maxSize=2G \
            neo4j:3.4.5

2. Change the password for neo4j instance
    1. Go to http://localhost:7474/browser/ in a browser
    2. Do the `:server connect` and first time you will ask to change the password to i.e. `test`
    3. Enter the password and remember
3. set it in src/resources/application.properties or as environment variable

4. Check SonarQube

    1. Start SonarQube in Docker

            docker run -d --name sonarqube -p 9900:9000 -p 9992:9092 sonarqube
    2. Generate access-token at sonarqube
        1. Browse to <http://localhost:9900>
        2. Log in with `admin` and `admin`

    3. run gradle

            ./gradlew sonarqube -Dsonar.host.url=http://localhost:9900 -Dsonar.login=admin -Dsonar.password=admin

## Assumptions

* Initial states are named (`name` attribute in BPEL-activity) with something that contains `initial(i[sz]ation)?`
* Variables that _\<assign\>_ ed from an output variable of an \<invoke\> activity are used in an follow-up \<if\> condition


## Used Libraries

* Apache ODE
* Spring Boot (<https://docs.spring.io/spring-boot/docs/2.0.0.RELEASE/reference/htmlsingle/#getting-started-first-application>)
* JGraph
* neo4j

### install sources

    mvn deploy:deploy-file \
        -DgroupId=org.apache.ode \
        -DartifactId=ode-bpel-compiler \
        -Dversion=1.3.8 \
        -Dfile=ode-bpel-compiler-1.3.8.jar \
        -Dsources=ode-bpel-compiler-1.3.8-sources.jar \
        -Durl=file:///

## Links

* MCMAS original download <http://vas.doc.ic.ac.uk/software/mcmas/documentation/>
* MCMAS-C <http://users.encs.concordia.ca/~bentahar/MCMAS-C/>
