package de.hu_berlin.informatik.svt.jbepl2ispl.ispl;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplAgentsBuilder;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentIdentifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.*;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVariable;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.*;
import lombok.NonNull;
import org.apache.commons.lang3.NotImplementedException;
import org.eclipse.emf.common.util.EList;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Christoph Graupner on 2018-06-20.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
class IsplAgentWriter extends IsplSubWriterBase {

  IsplAgentWriter(
      final IsplWriter isplWriter, final ISPLFactory factory
  ) {
    super(factory, isplWriter);
  }

  void buildAgents(final IsplAgentsBuilder builder) {
    final List<? extends AgentAutomaton> agents = isplWriter.model
        .getAllAgentAutomatons()
        .stream()
        .sorted(Comparator.comparing(AgentAutomaton::getName))
        .collect(Collectors.toList());

    for (final AgentAutomaton agent: agents) {
      if (FlowService.ENV.equals(agent.getName())) {
        continue;
      }

      currentAgent = agent;
      builder.beginAgent()
             .lobsVars(createAgentLobVars(agent))
             .name(agent.getIsplName())
             .variables(createAgentVarDef(agent))
             .actions(createAgentActionDefinition(agent))
             .redStates(createAgentRedStatesDefinition(agent))
             .protocol(createAgentProtocolDefinition(agent))
             .evolution(createAgentEvolutionDefinition(agent))
             .end();
    }
  }

  private void appendAgentProtocolOther(
      final AgentProtocolDef agentProtocolDef, final Collection<String> actionOthers
  ) {
    if (actionOthers != null && false == actionOthers.isEmpty()) {
      final OtherBranch otherBranch = factory.createOtherBranch();
      otherBranch.getIds().addAll(actionOthers);
      agentProtocolDef.setOtherbranch(otherBranch);
    }
  }

  private AgentArithmeticExpressionAddition asAgentArithmeticExpressionAddition(
      final LinkedList<AgentBitExpression> results, final AdditionOp additionOp
  ) {
    if (results.size() <= 1) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }
    AgentArithmeticExpressionAddition rootMult = factory.createAgentArithmeticExpressionAddition();
    rootMult.setOperator(additionOp);
    AgentArithmeticExpressionAddition iterMult = rootMult;

    iterMult.setLeft((AgentArithmeticExpression) results.removeFirst());
    AgentBitExpression last = results.removeLast();

    for (AgentBitExpression isBoolCondition: results) {
      AgentArithmeticExpressionAddition tmp = factory.createAgentArithmeticExpressionAddition();
      tmp.setOperator(additionOp);
      tmp.setLeft((AgentArithmeticExpression) isBoolCondition);

      AgentArithmeticExpressionParenthesized parenthesizedMult = asParenthesized(tmp);

      iterMult.setRight(parenthesizedMult);
      iterMult = tmp;
    }
    iterMult.setRight((AgentArithmeticExpression) last);
    return rootMult;
  }

  private AgentArithmeticExpressionMultiplication asAgentArithmeticExpressionMult(
      final LinkedList<AgentBitExpression> results, final MultiplicationOp multiplicationOp
  ) {
    if (results.size() <= 1) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }
    AgentArithmeticExpressionMultiplication rootMult = factory.createAgentArithmeticExpressionMultiplication();
    rootMult.setOperator(multiplicationOp);
    AgentArithmeticExpressionMultiplication iterMult = rootMult;

    iterMult.setLeft((AgentArithmeticExpression) results.removeFirst());
    AgentBitExpression last = results.removeLast();

    for (AgentBitExpression isBoolCondition: results) {
      AgentArithmeticExpressionMultiplication tmp = factory.createAgentArithmeticExpressionMultiplication();
      tmp.setOperator(multiplicationOp);
      tmp.setLeft((AgentArithmeticExpression) isBoolCondition);

      AgentArithmeticExpressionParenthesized parenthesizedMult = asParenthesized(tmp);

      iterMult.setRight(parenthesizedMult);
      iterMult = tmp;
    }
    iterMult.setRight((AgentArithmeticExpression) last);
    return rootMult;
  }

  private AgentBitExpression asAgentBitExpressionAnd(final LinkedList<AgentBitExpression> results) {
    if (results.size() == 1) {
      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }
    AgentBitExpressionAnd rootAnd = factory.createAgentBitExpressionAnd();
    AgentBitExpressionAnd iterAnd = rootAnd;

    iterAnd.setLeft(results.removeFirst());
    AgentBitExpression last = results.removeLast();

    for (AgentBitExpression isBoolCondition: results) {
      AgentBitExpressionAnd tmp = factory.createAgentBitExpressionAnd();
      tmp.setLeft(isBoolCondition);

      AgentBitExpressionParenthesized parenthesizedAnd =
          asParenthesized(tmp);

      iterAnd.setRight(parenthesizedAnd);
      iterAnd = tmp;
    }
    iterAnd.setRight(last);
    return rootAnd;
  }

  private AgentBitExpression asAgentBitExpressionOr(final LinkedList<AgentBitExpression> results) {
    if (results.size() == 1) {
      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }
    AgentBitExpressionOr rootOr = factory.createAgentBitExpressionOr();
    AgentBitExpressionOr iterOr = rootOr;

    iterOr.setLeft(results.removeFirst());
    AgentBitExpression last = results.removeLast();

    for (AgentBitExpression isBoolCondition: results) {
      AgentBitExpressionOr tmp = factory.createAgentBitExpressionOr();
      tmp.setLeft(isBoolCondition);

      AgentBitExpressionParenthesized parenthesizedOr = asParenthesized(tmp);

      iterOr.setRight(parenthesizedOr);
      iterOr = tmp;
    }
    iterOr.setRight(last);
    return rootOr;
  }

  private AgentEvoBoolCondition asAgentEvoBoolConditionAnd(final LinkedList<AgentEvoBoolCondition> results) {
    if (results.size() == 1) {

      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }
    AgentEvoBoolConditionAnd rootAnd = factory.createAgentEvoBoolConditionAnd();
    AgentEvoBoolConditionAnd iterAnd = rootAnd;

    iterAnd.setLeft(results.removeFirst());
    AgentEvoBoolCondition last = results.removeLast();

    for (AgentEvoBoolCondition isBoolCondition: results) {
      AgentEvoBoolConditionAnd tmp = factory.createAgentEvoBoolConditionAnd();
      tmp.setLeft(isBoolCondition);

      AgentEvoBoolConditionParenthesized parenthesizedAnd =
          asParenthesized(tmp);

      iterAnd.setRight(parenthesizedAnd);
      iterAnd = tmp;
    }
    iterAnd.setRight(last);
    return rootAnd;
  }

  private AgentEvoBoolCondition asAgentEvoBoolConditionOr(final LinkedList<AgentEvoBoolCondition> results) {
    if (results.size() == 1) {
      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }

    AgentEvoBoolConditionOr rootOr = factory.createAgentEvoBoolConditionOr();
    AgentEvoBoolConditionOr iterOr = rootOr;

    iterOr.setLeft(results.removeFirst());
    AgentEvoBoolCondition lastOr = results.removeLast();

    for (AgentEvoBoolCondition isBoolCondition: results) {
      AgentEvoBoolConditionOr tmp = factory.createAgentEvoBoolConditionOr();
      tmp.setLeft(isBoolCondition);

      AgentEvoBoolConditionParenthesized parenthesizedOr =
          asParenthesized(tmp);

      iterOr.setRight(parenthesizedOr);
      iterOr = tmp;
    }
    iterOr.setRight(lastOr);
    return rootOr;
  }

  private AgentLiteral asAgentLiteral(@NonNull final Object obj) {
    if (obj instanceof QName) {
      final QName qName = (QName) obj;
      if (qName.isInEnvironment()) {
        final EnvironmentRef environmentRef = factory.createEnvironmentRef();
        environmentRef.setVar(isplWriter.asIdLiteral(qName.getName()));
        return environmentRef;
      } else if (qName.belongsToAgent(currentAgent.getName())) {
        return isplWriter.asIdLiteral(qName.getName());
      }
      throw new IllegalStateException("Houston we have a problematic obj: " + obj.toString());
    }
    if (obj instanceof B2IExpression) {
      switch (((B2IExpression) obj).getType()) {
        case ACTION_COMPARE:
          QName qName = ((B2IActionExpression) obj).getTerm();
          if (qName.isInEnvironment()) {
            final EnvironmentRef environmentRef = factory.createEnvironmentRef();
            environmentRef.setVar(isplWriter.asIdLiteral(qName.getName()));
            return environmentRef;
          }
          break;
        case VARIABLE_REF:
          qName = ((B2IVarRefExpression) obj).getTerm();
          if (qName.isInEnvironment()) {
            final EnvironmentRef environmentRef = factory.createEnvironmentRef();
            environmentRef.setVar(isplWriter.asIdLiteral(qName.getName()));
            return environmentRef;
          } else if (qName.getAgent() == null) {
            logger.error(
                LogMarker.TODO_EXCEPTION,
                "IsplAgentWriter.asAgentLiteral: agent is <null> <- prevent it!"
            ); //FIXME agent is <null> <- prevent it!
            final EnvironmentRef environmentRef = factory.createEnvironmentRef();
            environmentRef.setVar(isplWriter.asIdLiteral(qName.getName()));
            return environmentRef;
          }
          break;
      }

      return asAgentLiteral(((B2IExpressionTerm) obj).getTerm());
    }
    return isplWriter.getLiteral(obj);
  }

  private AgentBoolCondition asAgentProtocolBoolConditionAnd(final LinkedList<AgentBoolConditionCompare> results) {
    if (results.size() == 1) {

      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }
    AgentBoolConditionAnd rootAnd = factory.createAgentBoolConditionAnd();
    AgentBoolConditionAnd iterAnd = rootAnd;

    iterAnd.setLeft(results.removeFirst());
    AgentBoolConditionCompare last = results.removeLast();

    for (AgentBoolConditionCompare isBoolCondition: results) {
      AgentBoolConditionAnd tmp = factory.createAgentBoolConditionAnd();
      tmp.setLeft(isBoolCondition);

      AgentBoolConditionParenthesized parenthesizedAnd = asParenthesized(tmp);

      iterAnd.setRight(parenthesizedAnd);
      iterAnd = tmp;
    }
    iterAnd.setRight(last);
    return rootAnd;
  }

  private AgentBoolConditionParenthesized asParenthesized(final AgentBoolCondition condition) {
    if (condition instanceof AgentBoolConditionParenthesized) {
      return (AgentBoolConditionParenthesized) condition;
    }
    final AgentBoolConditionParenthesized parenthesized = factory.createAgentBoolConditionParenthesized();
    parenthesized.setCondition(condition);
    return parenthesized;
  }

  private AgentEvoBoolResultParenthesized asParenthesized(final AgentEvoBoolResult result) {
    if (result instanceof AgentEvoBoolResultParenthesized) {
      return (AgentEvoBoolResultParenthesized) result;
    }
    AgentEvoBoolResultParenthesized parenthesized = factory.createAgentEvoBoolResultParenthesized();
    parenthesized.setResult(result);

    return parenthesized;
  }

  private AgentEvoBoolConditionParenthesized asParenthesized(final AgentEvoBoolCondition condition) {
    if (condition instanceof AgentEvoBoolConditionParenthesized) {
      return (AgentEvoBoolConditionParenthesized) condition;
    }
    AgentEvoBoolConditionParenthesized parenthesizedAnd = factory.createAgentEvoBoolConditionParenthesized();
    parenthesizedAnd.setCondition(condition);
    return parenthesizedAnd;
  }

  private AgentBitExpressionParenthesized asParenthesized(final AgentBitExpression expression) {
    if (expression instanceof AgentBitExpressionParenthesized) {
      return (AgentBitExpressionParenthesized) expression;
    }
    AgentBitExpressionParenthesized parenthesizedAnd = factory.createAgentBitExpressionParenthesized();
    parenthesizedAnd.setExpression(expression);
    return parenthesizedAnd;
  }

  private AgentArithmeticExpressionParenthesized asParenthesized(final AgentArithmeticExpression expression) {

    if (expression instanceof AgentArithmeticExpressionParenthesized) {
      return (AgentArithmeticExpressionParenthesized) expression;
    }
    AgentArithmeticExpressionParenthesized parenthesizedAnd = factory.createAgentArithmeticExpressionParenthesized();
    parenthesizedAnd.setExpression(expression);
    return parenthesizedAnd;
  }

  private AgentActionDef createAgentActionDefinition(final AgentAutomaton agent) {
    final AgentActionDef agentActionDef = factory.createAgentActionDef();
    agentActionDef.getActionNames().add(Consts.ISPL_ACTION_NAME_NONE);
    agent.getActions()
         .stream()
         .filter(qName -> qName.belongsToAgent(agent.getName()))
         .sorted()
         .forEachOrdered(
             qName -> agentActionDef.getActionNames().add(qName.getName())
         );
    return agentActionDef;
  }

  private AgentEvolutionDef createAgentEvolutionDefinition(final AgentAutomaton agent) {
    final AgentEvolutionDef agentEvolutionDef = factory.createAgentEvolutionDef();

    agent.getStates()
         .stream()
         .filter(agentState -> agentState.getCausingActivity() != Activity.__INITIAL_STATE)
         .sorted(Comparator.comparingInt(AgentIdentifiable::getElementId))
         .forEachOrdered(
             agentState -> {
               final AgentEvolutionStatement evolutionStatement = factory.createAgentEvolutionStatement();

               final B2IExpression             stateEntryConditions = agentState.getStateEntryConditions();
               final Map<QName, B2IExpression> assignmentMap        = agentState.getAssignmentMap();
               if (assignmentMap.isEmpty()) {
                 return; //skip empty
               }

               logger.debug(LogMarker.DEBUG_VALUES, "ISPL: {}.Evolution {}:\n {}\n -> {}",
                            agent.getName(),
                            agentState,
                            stateEntryConditions,
                            assignmentMap
               );

               evolutionStatement.setCondition(
                   asParenthesized(encodeConditionTermForEvolution(stateEntryConditions))
               );
               evolutionStatement.setResult(
                   asParenthesized(encodeAssignments(assignmentMap))
               );

               agentEvolutionDef.getEvolutionStatements().add(evolutionStatement);
             }
         );

    return agentEvolutionDef;
  }

  private AgentLobsVarDef createAgentLobVars(final AgentAutomaton agent) {
    final AgentLobsVarDef ret = factory.createAgentLobsVarDef();

    final AgentAutomaton envAgent = isplWriter.model
        .getAgentAutomaton(FlowService.ENV);
    envAgent
        .getVariables()
        .stream()
        .sorted()
        .forEachOrdered(
            varName -> {
              final FlowVariable.PurposeType type = envAgent.getVariableDefinition(varName).getPurposeType();
              if (type != null) {
                switch (type) {
                  case CONTAINER:
                  case COMM_CHANNEL:
                    ret.getLobVars().add(varName);
                    break;
                }
              }
            }
        );
    ret.getLobVars().add(Consts.ISPL_VAR_NAME_STATEVAR_ENV);
    ret.getLobVars().add(agent.getTraceLabelName().getName());
    return ret;
  }

  private AgentProtocolDef createAgentProtocolDefinition(@NonNull final AgentAutomaton agent) {
    final AgentProtocolDef agentProtocolDef = factory.createAgentProtocolDef();
    appendAgentProtocolOther(agentProtocolDef, agent.getActionOthers());

    final Set<B2IExpressionActionPair> actionActivationConditions = agent.getProtocolConditions();
    if (actionActivationConditions.isEmpty()) {
      logger.warn(LogMarker.ISPL, "No actions for {}", agent);
      appendAgentProtocolOther(agentProtocolDef, Arrays.asList("nothing"));
    } else {
      int entries = 0;
      for (final B2IExpressionActionPair actionPair: actionActivationConditions) {
        //FIXME how to handle things, there no action is for condition?
        if (actionPair.getActions().isEmpty()) {
          logger.warn(LogMarker.ISPL, "Empty actions for {}: {}", agent, actionPair);
          continue;
        }
        final B2IExpression condition = actionPair.getExpression();

        AgentProtocolStatement agentProtocolStatement = factory.createAgentProtocolStatement();
        agentProtocolStatement.setCondition(
            asParenthesized(
                getAgentProtocolCondition(
                    (B2INaryExpression) condition
                )
            )
        );
        final EnabledIdList enabledIdList = factory.createEnabledIdList();
        for (final QName actionName: actionPair.getActions()) {
          if (actionName == null) {
            logger.error(LogMarker.ISPL, "actionName is null");
            continue;
          }

          if (!actionName.belongsToAgent(currentAgent.getName())) {
            continue;
          }

          enabledIdList.getIds().add(actionName.getName()); //strip agent name //TODO: check agent name
        }
        if (!enabledIdList.getIds().isEmpty()) { // only add if there are actions for the state
          agentProtocolStatement.setActionEnabledIdList(enabledIdList);

          agentProtocolDef.getProtocolStatements().add(agentProtocolStatement);
          entries++;
        }
      }

      if (entries == 0) {
        logger.warn(LogMarker.ISPL, "No actions for {} although there transitions", agent);
        appendAgentProtocolOther(agentProtocolDef, Arrays.asList(Consts.ISPL_ACTION_NAME_NONE));
      }
    }

    return agentProtocolDef;
  }

  private AgentRedStatesDef createAgentRedStatesDefinition(final AgentAutomaton agent) {

    final AgentRedStatesDef ret = factory.createAgentRedStatesDef();
    agent.getUnDesiredStates().forEach(
        s -> ret.getCondition().add(getAgentProtocolCondition(agent.getUnDesiredStateDefinition(s)))
    );
    return ret;
  }

  private AgentVarDef createAgentVarDef(final AgentAutomaton agent) {
    final AgentVarDef vd = factory.createAgentVarDef();

    final EList<VariableDefinition> varDefs = vd.getVarDefs();

    //noinspection Duplicates
    final LinkedList<String> variables = new LinkedList<>(agent.getVariables());
    variables.sort(String::compareToIgnoreCase);

    createVarDefs(agent, varDefs, variables);

    return vd;
  }

  private AgentBitExpression encodeAssignmentValue(final B2IExpression assignment) {
    logger.debug(LogMarker.ISPL, "Assigment: {}", assignment);
    LinkedList<AgentBitExpression> results = new LinkedList<>();
    switch (assignment.getType()) {
      case PARENTHESIZED:
        B2IUnaryExpression unaryExpression = (B2IUnaryExpression) assignment;
        AgentBitExpressionParenthesized parenthesized = factory.createAgentBitExpressionParenthesized();
        parenthesized.setExpression(encodeAssignmentValue(unaryExpression.getTerm()));
        results.add(parenthesized);
        break;
      case NUM_ADD:
        B2INaryExpression xExpression = (B2INaryExpression) assignment;
        results = Arrays.stream(xExpression.getTerms())
                        .sorted()
                        .map(this::encodeAssignmentValue)
                        .collect(Collectors.toCollection(LinkedList::new));
        return asAgentArithmeticExpressionAddition(results, AdditionOp.PLUS);
      case NUM_SUB:
        xExpression = (B2INaryExpression) assignment;
        results = Arrays.stream(xExpression.getTerms())
                        .map(this::encodeAssignmentValue)
                        .collect(Collectors.toCollection(LinkedList::new));
        return asAgentArithmeticExpressionAddition(results, AdditionOp.MINUS);
      case NUM_MUL:
        xExpression = (B2INaryExpression) assignment;
        results = Arrays.stream(xExpression.getTerms())
                        .sorted()
                        .map(this::encodeAssignmentValue)
                        .collect(Collectors.toCollection(LinkedList::new));
        return asAgentArithmeticExpressionMult(results, MultiplicationOp.MULTI);
      case NUM_DIV:
        xExpression = (B2INaryExpression) assignment;
        results = Arrays.stream(xExpression.getTerms())
                        .map(this::encodeAssignmentValue)
                        .collect(Collectors.toCollection(LinkedList::new));
        return asAgentArithmeticExpressionMult(results, MultiplicationOp.DIV);
      case BIT_NEGATE:
        AgentBitExpressionNegated neg = factory.createAgentBitExpressionNegated();
        neg.setOperand(encodeAssignmentValue(((B2IUnaryExpression) assignment).getTerm()));
        return neg;
      case BIT_AND:
        xExpression = (B2INaryExpression) assignment;
        results = Arrays.stream(xExpression.getTerms())
                        .sorted()
                        .map(this::encodeAssignmentValue)
                        .collect(Collectors.toCollection(LinkedList::new));
        return asAgentBitExpressionAnd(results);
      case BIT_OR:
        xExpression = (B2INaryExpression) assignment;
        results = Arrays.stream(xExpression.getTerms())
                        .sorted()
                        .map(this::encodeAssignmentValue)
                        .collect(Collectors.toCollection(LinkedList::new));
        return asAgentBitExpressionOr(results);
      case VALUE:
      case TERM:
        return asAgentLiteral(assignment);
      case VARIABLE_REF:
        return asAgentLiteral(assignment); //the varName is a QName, so it will be a EnvironmentRef here
      default:
        throw new NotImplementedException("unhandled Assignment.Type: " + assignment.getType());
    }
    return null;
  }

  private AgentEvoBoolResult encodeAssignments(final Map<QName, B2IExpression> assignments) {
    final AgentEvoBoolResult agentBoolResult;

    final LinkedList<AgentEvoBoolResult> results = new LinkedList<>();
    if (assignments.isEmpty()) {
      throw new IllegalStateException("StateEvolutionStatement.assignments should never be empty.");
    }
    assignments.forEach(
        (varName, assignment) -> {
          AgentEvoBoolResultAssignment resultAssignment = factory.createAgentEvoBoolResultAssignment();

          if (false == varName.belongsToAgent(currentAgent.getName())) {
            throw new IllegalStateException(varName + " should be variable of agent " + currentAgent.getName());
          }

          resultAssignment.setVarName(varName.getName());
          resultAssignment.setValue(encodeAssignmentValue(assignment));

          results.add(resultAssignment);
        }
    );

    if (results.size() == 1) {
      agentBoolResult = results.get(0);
    } else {

      AgentEvoBoolResultAnd rootAnd = factory.createAgentEvoBoolResultAnd();
      AgentEvoBoolResultAnd iter    = rootAnd;
      iter.setLeft(results.removeFirst());
      AgentEvoBoolResult last = results.removeLast();

      for (AgentEvoBoolResult isBoolCondition: results) {
        AgentEvoBoolResultAnd tmp = factory.createAgentEvoBoolResultAnd();
        tmp.setLeft(isBoolCondition);

        iter.setRight(asParenthesized(tmp));
        iter = tmp;
      }
      iter.setRight(last);

      agentBoolResult = rootAnd;
    }

    return agentBoolResult;
  }

  private AgentEvoBoolCondition encodeCompareConditionForEvolution(final B2ICompareExpression compareExpression) {
    switch (compareExpression.getType()) {
      case COMPARE:
      default:
        final AgentEvoBoolConditionCompare agentEvoBoolConditionCompare = factory.createAgentEvoBoolConditionCompare();
        agentEvoBoolConditionCompare.setLogicop(isplWriter.getLogicOp(compareExpression.getOperator()));

        agentEvoBoolConditionCompare.setLeft(asAgentLiteral(compareExpression.getLeft()));
        agentEvoBoolConditionCompare.setRight(asAgentLiteral(compareExpression.getRight()));
        return agentEvoBoolConditionCompare;
    }
  }

  private AgentEvoBoolCondition encodeConditionTermForEvolution(@NonNull final B2IExpression o) {
    final LinkedList<AgentEvoBoolCondition> results = new LinkedList<>();
    switch (o.getType()) {
      case AND:
        for (final B2IExpression term: ((B2INaryExpression) o).getTerms()) {
          results.add(encodeConditionTermForEvolution(term));
        }

        AgentEvoBoolCondition rootAnd = asAgentEvoBoolConditionAnd(results);

        return asParenthesized(rootAnd);
      case OR:
        for (final B2IExpression term: ((B2INaryExpression) o).getTerms()) {
          results.add(encodeConditionTermForEvolution(term));
        }

        AgentEvoBoolCondition rootOr = asAgentEvoBoolConditionOr(results);

        return asParenthesized(rootOr);
      case NOT:
        AgentEvoBoolConditionNegated negated = factory.createAgentEvoBoolConditionNegated();
        negated.setCondition(encodeConditionTermForEvolution(((B2IUnaryExpression) o).getTerm()));

        return negated;
      case PARENTHESIZED:
        return asParenthesized(encodeConditionTermForEvolution(((B2IUnaryExpression) o).getTerm()));
      case TERM:
      case NUM_ADD:
      case NUM_SUB:
      case NUM_MUL:
      case NUM_DIV:
      case VALUE:
      case BIT_NEGATE:
      case BIT_AND:
      case BIT_OR:
      case VARIABLE_REF:
        throw new NotImplementedException("Not Implement yet");
      case ACTION_COMPARE:
        final B2IActionExpression actionExpression = (B2IActionExpression) o;
        final QName action = actionExpression.getTerm();

        final AgentEvoBoolConditionAction agentEvoBoolConditionAction = factory.createAgentEvoBoolConditionAction();

        agentEvoBoolConditionAction.setActionName(isplWriter.translateToIsplName(action.getName()));
        if (null != action.getAgent()) {
          agentEvoBoolConditionAction.setAgentName(isplWriter.translateToIsplName(action.getAgent()));
        }
        return agentEvoBoolConditionAction;
      case COMPARE:
        if (o instanceof B2ICompareExpression) {
          return encodeCompareConditionForEvolution(((B2ICompareExpression) o));
        }
      default:
        throw new IllegalStateException("unhandled B2IExpression.type: " + o.getType());
    }
  }

  private AgentBoolConditionCompare getAgentBoolConditionCompare(final B2ICompareExpression compare) {
    final AgentBoolConditionCompare conditionCompare = factory.createAgentBoolConditionCompare();
    conditionCompare.setLogicop(isplWriter.getLogicOp(compare.getOperator()));

    conditionCompare.setLeft(asAgentLiteral(compare.getLeft()));
    conditionCompare.setRight(asAgentLiteral(compare.getRight()));

    return conditionCompare;
  }

  private AgentBoolCondition getAgentProtocolCondition(
      final B2INaryExpression actionConditions
  ) {

    final AgentBoolConditionParenthesized ret   = factory.createAgentBoolConditionParenthesized();
    int                                   count = actionConditions.getTerms().length;
    logger.debug(LogMarker.ISPL, "Agent Protocol Condition");

    if (count == 0) {
      return null;
    } else if (count == 1) {
      return asParenthesized(getAgentBoolConditionCompare((B2ICompareExpression) actionConditions.getTerms()[0]));
    } else {
      LinkedList<AgentBoolConditionCompare> results = new LinkedList<>();

      for (final B2IExpression expression: actionConditions.getTerms()) {
        if (expression instanceof B2ICompareExpression) {
          final AgentBoolConditionCompare conditionCompare = getAgentBoolConditionCompare(
              (B2ICompareExpression) expression);
          results.add(conditionCompare);
        }
        //FIXME right condition of AND and recursive
        logger.error(LogMarker.TODO_EXCEPTION, "FIXME right condition of AND and recursive");
      }

      logger.debug(LogMarker.ISPL, "ProtocolConditions: {}", ret.getCondition());
      ret.setCondition(asAgentProtocolBoolConditionAnd(results));
    }
    return ret;
  }
}
