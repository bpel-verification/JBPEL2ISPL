package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.MainApp;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentAutomatonModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentTransitionModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph.AnalysisGraph;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph.FlowStateWrapper;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph.FlowTransitionWrapper;
import de.hu_berlin.informatik.svt.jbepl2ispl.visualize.graph.GraphOutput;
import org.apache.commons.lang3.StringUtils;
import org.jgrapht.Graph;
import org.jgrapht.io.ExportException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
@Service
public class StatesCommonExtractor extends CommonExtractorBase {
  protected StatesCommonExtractor(
      final FlowModel flowModel,
      final AutomatonModel model,
      final StateInformationResolver stateResolver,
      final VariableTargetServiceResolver resolver
  ) {
    super(flowModel, model, stateResolver, resolver);
  }

  @Override
  public void extract() {
    logger.trace(LogMarker.EXTRACTION, "Extracting states for Environment");
    envExtractStates(
        (AgentAutomatonModifiable) model.getAgentAutomaton(FlowService.ENV),
        flowModel.findServiceByName(FlowService.ENV)
    );
    logger.trace(LogMarker.EXTRACTION, "DONE Extracting states for Environment");

    flowModel.getServices()
             .stream()
             .filter(flowService -> !FlowService.ENV.equals(flowService.getName()))
             .forEach(
                 flowService -> {
                   logger.trace(LogMarker.EXTRACTION, "Extracting states for Agent '{}'", flowService.getName());
                   agentExtractStates(
                       (AgentAutomatonModifiable) model.getAgentAutomaton(flowService.getName()),
                       flowService
                   );
                   logger.trace(LogMarker.EXTRACTION, "DONE Extracting states for Agent '{}'", flowService.getName());
                 }
             );
  }

  private void agentExtractStates(
      final AgentAutomatonModifiable agent,
      final FlowService flowService
  ) {
    final AnalysisGraph analysisGraph = new AnalysisGraph(flowModel);

    final Collection<FlowState> allInvolvedStates =
        flowModel.findAllStatesConnectedByInvolvingLinkService(flowService);
    allInvolvedStates.addAll(flowModel.findAllInvokeStatesWithService(flowService));
    final Collection<FlowState> allWithTargetInAssignState = flowModel.findAllWithSourceInAssignState(flowService);
    allWithTargetInAssignState.forEach(flowState -> {
      if (logger.isDebugEnabled()) {
        logger.debug(LogMarker.DEBUG_VALUES, "INVOLVED FILTER: {}, {}", flowState.getInputVariable(),
                     flowState.getInputVariable() != null ? flowModel.findVariableByName(flowState.getInputVariable())
                                                                     .getProperty(
                                                                         Consts.PROPERTY_VAR_IS_EXTERNAL_BPEL_CLIENT_INPUT)
                                                          : "<null>"
        );
      }
      if (
        // skip initial states and there the value is <receive>d (also marked as initial) from the process caller
          false == (flowState.isInitialState())
      ) {
        allInvolvedStates.add(flowState);
      } else {
        logger.debug(LogMarker.DEBUG_VALUES, "SKIPPED");
      }
    });

//    allInvolvedStates.addAll(allWithTargetInAssignState);
    logger.debug(LogMarker.EXTRACTION, "involved States: {}", StringUtils.join(allInvolvedStates, "\n -- "));

    new HashSet<>(analysisGraph.getGraph().vertexSet()).forEach(stateWrapper -> {
      switch (stateWrapper.getCausingActivity()) {
        case __INITIAL_STATE:
          break;
        case SEQUENCE:
        case FLOW_BRANCH:
        case IFCASE_ELSE:
          analysisGraph.removeState(stateWrapper);
          return;
        default:
          if (false == allInvolvedStates.contains(stateWrapper.wrapped())
              && stateWrapper.getCausingActivity() != Activity.__INITIAL_STATE)
          {

            analysisGraph.removeState(stateWrapper);
          }
          break;
      }
    });

    removeActivityPaths(analysisGraph);

    saveToAgentAutomation(analysisGraph, agent, flowService);
  }

  private void envExtractStates(
      final AgentAutomatonModifiable agent,
      final FlowService flowService
  ) {
    final AnalysisGraph analysisGraph = new AnalysisGraph(flowModel);

    logger.trace(LogMarker.EXTRACTION, "Stripping useless states off...");

    new HashSet<>(analysisGraph.getGraph().vertexSet()).forEach(stateWrapper -> {
      switch (stateWrapper.getCausingActivity()) {
        case __INITIAL_STATE:
          break;
        case SEQUENCE:
        case FLOW_BRANCH:
        case IFCASE_ELSE:
          analysisGraph.removeState(stateWrapper);
          break;
        default:
          if (stateWrapper.isInitialState()) {
            analysisGraph.removeState(stateWrapper);
          }
          break;
      }
    });

    removeActivityPaths(analysisGraph);
    logger.trace(LogMarker.EXTRACTION, "DONE Stripping useless states off...");

    saveToAgentAutomation(analysisGraph, agent, flowService);
  }

  private void removeActivityPaths(final AnalysisGraph analysisGraph) {
    removeUselessActivityPaths(analysisGraph, BpelPath.ELEMENT_IFCASE_ELSE);
    removeUselessActivityPaths(analysisGraph, BpelPath.ELEMENT_IF);
    removeUselessActivityPaths(analysisGraph, BpelPath.ELEMENT_FLOW);
    removeUselessActivityPaths(analysisGraph, BpelPath.ELEMENT_WHILE);
    removeEmptyFlows(analysisGraph);
  }

  private void removeEmptyFlows(final AnalysisGraph analysisGraph) {
    Map<BpelPath, List<FlowStateWrapper>> wrapperList          = new HashMap<>();
    final Graph<FlowStateWrapper, FlowTransitionWrapper> graph = analysisGraph.getGraph();
    graph.vertexSet().forEach(flowStateWrapper -> {
      final BpelPath bpelPath = flowStateWrapper.getBpelPath();
      if (flowStateWrapper.getCausingActivity() == Activity.FLOW) {
        if (false == wrapperList.containsKey(bpelPath)) {
          wrapperList.put(bpelPath, new LinkedList<>());
        }
        wrapperList.get(bpelPath).add(flowStateWrapper);
      }
    });
    wrapperList.forEach(
        (bpelPath, values) -> {
          values.sort(Comparator.comparingInt(BpelIdentifiable::getElementId));
          final FlowStateWrapper sourceVertex = values.get(0);
          final FlowStateWrapper targetVertex = values.get(1);
          while (null != graph.getEdge(sourceVertex, targetVertex)) {
            //there is an direct edge from begin to end
            graph.removeEdge(sourceVertex, targetVertex);
          }

          if (MainApp.getConfig().isDebugRemoveSingleFlows()) {
            logger.warn(LogMarker.TODO_EXCEPTION, "TODO is this correct?");
            // if there is just one flow branch left, remove flow encapsulation
            if (graph.outDegreeOf(sourceVertex) == 1 && graph.inDegreeOf(targetVertex) == 1) {
              analysisGraph.removeState(sourceVertex);
              analysisGraph.removeState(targetVertex);
            }
          }
        }
    );
  }

  private void removeUselessActivityPaths(final AnalysisGraph analysisGraph, final String element) {
    final Map<BpelPath, List<FlowStateWrapper>> activityAnalysis = new HashMap<>();
    analysisGraph.getGraph().vertexSet().forEach(
        stateWrapper -> {
          final BpelPath nearestElement = stateWrapper.getBpelPath().getNearestElement(element);
          if (false == activityAnalysis.containsKey(nearestElement)) {
            activityAnalysis.put(nearestElement, new LinkedList<>());
          }
          if (false == activityAnalysis.get(nearestElement).contains(stateWrapper)) {
            activityAnalysis.get(nearestElement).add(stateWrapper);
          }
        }
    );

    activityAnalysis.forEach((key, value) -> {
      if (logger.isDebugEnabled() && MainApp.getConfig().isDebugPrintRemoveUselessInspection()) {
        logger.debug(LogMarker.EXTRACTION, "IFfer {} {}:\n ## {}", element, key, StringUtils.join(value, "\n ## "));
      }
      boolean okay = true;
      LL:
      for (final FlowStateWrapper flowStateWrapper: value) {
        switch (flowStateWrapper.getCausingActivity()) {
          case IF:
          case IFCASE_ELSE:
          case IFCASE_ELSEIF:
          case FLOW:
          case FLOW_BRANCH:
          case SEQUENCE:
          case WHILE:
            break;
          default:
            okay = false;
            break LL;
        }
      }
      if (okay) {
        value.forEach(analysisGraph::removeState);
      }
    });
  }

  private void saveToAgentAutomation(
      final AnalysisGraph analysisGraph,
      final AgentAutomatonModifiable agent,
      final FlowService flowService
  ) {
    logger.trace(LogMarker.EXTRACTION, "Storing AnalysisGraph in GraphDB for Agent {}...", agent.getName());

    final Map<FlowState, AgentState> mapFlowAgentState = new HashMap<>();

    final Set<FlowStateWrapper> flowStateWrappers = analysisGraph.getGraph().vertexSet();
    logger.trace(LogMarker.EXTRACTION, "AnalysisGraph: Storing {} nodes in GraphDB...", flowStateWrappers.size());
    flowStateWrappers.forEach(
        stateWrapper -> {
          AgentState agentState = factory.createAgentState(stateWrapper.wrapped());
          agent.addState(agentState);
          mapFlowAgentState.put(stateWrapper.wrapped(), agentState);
        }
    );

//    logger.debug(LogMarker.DEBUG_VALUES, "MAP: {}", StringUtils.join(mapFlowAgentState.keySet(), "\n ~~ "));

    final Set<FlowTransitionWrapper> flowTransitionWrappers = analysisGraph.getGraph().edgeSet();
    logger.trace(
        LogMarker.EXTRACTION, "AnalysisGraph: Storing {} relations in GraphDB...", flowTransitionWrappers.size());
    flowTransitionWrappers.forEach(
        edgeWrapper -> {
          AgentTransitionModifiable agentTransition = factory.createAgentTransition(
              mapFlowAgentState.get(edgeWrapper.getSourceState()),
              mapFlowAgentState.get(edgeWrapper.getTargetState()),
              edgeWrapper.getGuard(),
              edgeWrapper.getName()
          );
          agentTransition.setAction(edgeWrapper.getIsplActionName());
          agentTransition.setInvertGuard(edgeWrapper.invertGuard());

          agent.save(agentTransition);
        }
    );

    if (MainApp.getConfig().isPrintAgentGraphs()) {
      final GraphOutput go = new GraphOutput();
      try {
        go.render(
            analysisGraph,
            MainApp.getConfig().getDebugPathForAgentGraphs() + "/" + flowService.getName()
        );
      } catch (IOException | ExportException e) {
        logger.error(LogMarker.EXCEPTION, e.getLocalizedMessage(), e);
      }
    }

    logger.trace(LogMarker.EXTRACTION, "DONE Storing AnalysisGraph in GraphDB for Agent {}...", agent.getName());
  }
}
