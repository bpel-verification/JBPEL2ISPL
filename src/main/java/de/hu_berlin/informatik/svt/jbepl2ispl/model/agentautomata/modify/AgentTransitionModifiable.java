package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentTransition;

/**
 * Created by Christoph Graupner on 2018-04-21.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AgentTransitionModifiable extends AgentTransition {
}
