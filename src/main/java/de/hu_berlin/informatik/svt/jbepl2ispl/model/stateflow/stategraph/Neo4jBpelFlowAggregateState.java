package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.BpelFlowAggregateState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.BpelPath;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christoph Graupner on 2018-06-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NodeEntity(label = "FlowAggregateState")
@NoArgsConstructor
public class Neo4jBpelFlowAggregateState implements BpelFlowAggregateState {
  @Relationship(type = "FLOW_AGGREGATED_WITH")
  private List<State> aggregatedStates = new ArrayList<>();
  private String      bpelPathAsString;
  @Relationship(type = "AN_AGGREGATE_FOR")
  private State       flowToAggregateState;
  @Getter
  @Id
  @GeneratedValue
  private Long        id;

  public Neo4jBpelFlowAggregateState(@NonNull final State flowBeginState) {
    if (flowBeginState.getCausingActivity() != Activity.FLOW
        || flowBeginState.getBeginEndType() != BeginEndType.BEGIN)
    {
      throw new IllegalStateException(
          "[flowBeginState] needs to be Activity.FLOW and BeginEndType.BEGIN but was: " + flowBeginState
      );
    }
    flowToAggregateState = flowBeginState;
    bpelPathAsString = flowBeginState.getBpelPathAsString();
  }

  @Override
  public void addState(final FlowState state) {
    if (false == state.getBpelPath().isInside(BpelPath.ELEMENT_FLOW)) {
      throw new IllegalArgumentException("[state] is not inside a <flow> branch: " + state);
    }
    aggregatedStates.add((State) state);
  }

  @Override
  public List<? extends FlowState> getAggregatedStates() {
    return aggregatedStates;
  }
}
