package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import lombok.Getter;

import java.util.Objects;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class FlowId {
  @Getter
  int alternateId;
  @Getter
  int elementId;

  public FlowId(final int elementId, final int alternativeCount) {
    alternateId = alternativeCount;
    this.elementId = elementId;
  }

  public FlowId(final FlowId flowId) {
    alternateId = flowId.alternateId;
    elementId = flowId.elementId;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof FlowId)) {
      return false;
    }
    final FlowId flowId = (FlowId) o;
    return getAlternateId() == flowId.getAlternateId() &&
           getElementId() == flowId.getElementId();
  }

  @Override
  public int hashCode() {
    return Objects.hash(getAlternateId(), getElementId());
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("FlowId{");
    sb.append(elementId);
    sb.append(":").append(alternateId);
    sb.append('}');
    return sb.toString();
  }
}
