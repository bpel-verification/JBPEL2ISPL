package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpression;

import java.util.Map;

/**
 * Created by Christoph Graupner on 2018-04-21.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AgentStateModifiable extends AgentState {

  void setAgentAutomaton(
      AgentAutomaton agentAutomaton
  );

  void setAssignmentMap(Map<QName, B2IExpression> assignmentMap);

  void setName(String name);
}
