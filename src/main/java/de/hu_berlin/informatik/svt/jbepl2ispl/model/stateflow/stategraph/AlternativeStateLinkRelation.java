package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowAlternativeStateLink;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.*;

/**
 * Created by Christoph Graupner on 2018-06-14.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@RelationshipEntity(type = Naming.RELATION_ALTERNATIVE_STATE)
@NoArgsConstructor
public class AlternativeStateLinkRelation implements FlowAlternativeStateLink {
  @Getter
  @EndNode
  State alternative;
  @Getter
  @StartNode
  State origin;
  @Id
  @GeneratedValue
  private Long relationshipId;

  public AlternativeStateLinkRelation(final State originState, final State altState) {
    alternative = altState;
    origin = originState;
  }
}
