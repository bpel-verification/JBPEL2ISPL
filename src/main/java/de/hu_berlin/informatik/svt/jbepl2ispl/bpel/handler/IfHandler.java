package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowTransition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.StateFlowFactory;
import org.apache.ode.bpel.compiler.bom.Bpel20QNames;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.apache.ode.bpel.compiler.bom.IfActivity;

import java.util.*;

/**
 * Created by Christoph Graupner on 2018-03-04.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public class IfHandler extends HandlerBase {

  private Map<Long, Set<FlowState>> mapIf2Case = new HashMap<>();
  private Deque<FlowState>          stack      = new LinkedList<>();

  public IfHandler() {
    super(Arrays.asList(IfActivity.class, IfActivity.Case.class));
  }

  @Override
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    if (currentBpelObject instanceof IfActivity) {
      handleIfMainBegin((IfActivity) currentBpelObject, compilerState);
    } else if (currentBpelObject instanceof IfActivity.Case) {
      if (Bpel20QNames.ELSEIF.getLocalPart().equals(currentBpelObject.getType().getLocalPart())) {
        handleIfCaseBegin((IfActivity.Case) currentBpelObject, compilerState);
      } else {
        handleElseBegin((IfActivity.Case) currentBpelObject, compilerState);
      }
    }
    return super.handleBegin(currentBpelObject, compilerState);
  }

  @Override
  public <T extends HandlerBase> T handleEnd(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    if (currentBpelObject instanceof IfActivity) {
      handleIfMainEnd((IfActivity) currentBpelObject, compilerState);
    } else if (currentBpelObject instanceof IfActivity.Case) {
      if (Bpel20QNames.ELSEIF.getLocalPart().equals(currentBpelObject.getType().getLocalPart())) {
        handleIfCaseEnd((IfActivity.Case) currentBpelObject, compilerState);
      } else {
        handleElseEnd((IfActivity.Case) currentBpelObject, compilerState);
      }
    }
    return super.handleEnd(currentBpelObject, compilerState);
  }

  private String getIfStateName(final IfActivity ifActivity) {
    return "IF__" + ifActivity.getName()
           + "__" + (ifActivity.getCondition() == null
                     ? "STRANGE"
                     : ifActivity.getCondition().getTextValue().trim()) + "__BEGIN";
  }

  private FlowState getLastMainIf() {
    for (final FlowState next: stack) {
      if (Activity.IF == next.getCausingActivity()) {
        return next;
      }
    }
    return null;
  }

  private void handleElseBegin(final IfActivity.Case caseActivity, final CompilerState aCompilerState) {
    final FlowModel        model   = aCompilerState.getFlowModel();
    final StateFlowFactory factory = model.factory();

    final String name = "ELSE";

    FlowState state = factory.createState(
        name,
        "IFCASE__" + name + "__BEGIN",
        Activity.IFCASE_ELSE,
        BeginEndType.BEGIN,
        aCompilerState.getCurrentBpelPath()
    );
    state = model.save(state);

    mapIf2Case.get(getLastMainIf().getId()).add(model.findPreviousState(state));

    //inbound transition
    final FlowState ifBeginState = model.refresh(stack.getFirst(), 3);
    FlowTransition  transition;
    //<else> branch
    transition = factory.createTransition(
        ifBeginState,
        state,
        ifBeginState.getProperty(Consts.PROPERTY_IF_CONDITION),
        name,
        aCompilerState.getCurrentBpelPath()
    );
    transition.setInvertGuard(true);
    model.save(transition);
  }

  private void handleElseEnd(final IfActivity.Case caseActivity, final CompilerState aCompilerState) {
    final FlowModel        model   = aCompilerState.getFlowModel();
    final StateFlowFactory factory = model.factory();
    final String           name    = "ELSE";
    FlowState caseEndState = factory.createState(
        name,
        "IFCASE__" + name + "__END",
        Activity.IFCASE_ELSE,
        BeginEndType.END,
        aCompilerState.getCurrentBpelPath()
    );
    caseEndState = model.save(caseEndState);

    final FlowState previousCaseState = model.findPreviousState(caseEndState);
    assert previousCaseState != null;
    FlowTransition transition = factory.createTransition(
        previousCaseState,
        caseEndState,
        null,
        String.format("Prev(ELSE)[%s]->ELSE[%s]", previousCaseState.getFlowId(), caseEndState.getFlowId()),
        aCompilerState.getCurrentBpelPath()
    );
    model.save(transition);

    mapIf2Case.get(getLastMainIf().getId()).add(caseEndState);
  }

  private void handleIfCaseBegin(final IfActivity.Case caseActivity, final CompilerState aCompilerState) {
    final FlowModel        model   = aCompilerState.getFlowModel();
    final StateFlowFactory factory = model.factory();

    final String name = caseActivity.getCondition().getTextValue().trim();

    FlowState state = factory.createState(
        name,
        "IFCASE__" + name + "__BEGIN",
        Activity.IFCASE_ELSEIF,
        BeginEndType.BEGIN,
        aCompilerState.getCurrentBpelPath()
    );
    state.setProperty(Consts.PROPERTY_IF_CONDITION, caseActivity.getCondition().getTextValue().trim());

    state = model.save(state);

    //for connecting ends of <if>-branches with ENDIF
    mapIf2Case.get(getLastMainIf().getId()).add(model.findPreviousState(state));

    //inbound transition
    final FlowState ifBeginState = model.refresh(stack.getFirst(), 3);
    FlowTransition  transition;
    transition = factory.createTransition(
        ifBeginState,
        state,
        ifBeginState.getProperty(Consts.PROPERTY_IF_CONDITION),
        String.format("IF(CASE)[%s]->IFCASE_BEGIN[%s]", ifBeginState.getFlowId(), state.getFlowId()),
        aCompilerState.getCurrentBpelPath()
    );
    transition.setInvertGuard(true);
    model.save(transition);

    if (caseActivity.getCondition() != null) {
      createReadAccessForCondition(
          caseActivity.getCondition().getTextValue(),
          state,
          model,
          aCompilerState
      );
    } else {
      logger.error(
          LogMarker.MODEL_PROBLEM,
          "There is an <elseif> with no condition. Should not be possible. {}",
          caseActivity
      );
    }
    stack.push(state);
    mapIf2Case.put(state.getId(), new HashSet<>());
  }

  private void handleIfCaseEnd(final IfActivity.Case caseActivity, final CompilerState aCompilerState) {
    final FlowModel        model   = aCompilerState.getFlowModel();
    final StateFlowFactory factory = model.factory();
    final String           name    = caseActivity.getCondition().getTextValue().trim();
    final FlowState caseEndState = model.save(factory.createState(
        name,
        "IFCASE__" + name + "__END",
        Activity.IFCASE_ELSEIF,
        BeginEndType.END,
        aCompilerState.getCurrentBpelPath()
                                              )
    );

    final FlowState previousCaseState = model.findPreviousState(caseEndState);
    assert previousCaseState != null;
    FlowTransition transition = factory.createTransition(
        previousCaseState,
        caseEndState,
        null,
        String.format("Prev(IFCASE)[%s]->IFCASE_END[%s]", previousCaseState.getFlowId(), caseEndState.getFlowId()),
        aCompilerState.getCurrentBpelPath()
    );
    model.save(transition);

    final FlowState beginState = stack.peekFirst();
    //outbound transition for CONDITION == TRUE
    final FlowState followingState = model.findFollowingState(beginState);
    transition = factory.createTransition(
        beginState,
        followingState,
        (null == caseActivity.getCondition()
         ? null
         : caseActivity.getCondition().getTextValue().trim()),
        String.format("IFELSE[%s]->Next(IFELSE)[%s]", beginState.getFlowId(), followingState.getFlowId()),
        aCompilerState.getCurrentBpelPath()
    );
    model.save(transition);

    mapIf2Case.get(getLastMainIf().getId()).add(caseEndState);
  }

  private void handleIfMainBegin(final IfActivity ifActivity, final CompilerState aCompilerState) {
    final FlowModel        model   = aCompilerState.getFlowModel();
    final StateFlowFactory factory = model.factory();
    FlowState beginState = factory.createState(
        ifActivity.getName(),
        getIfStateName(ifActivity),
        Activity.IF,
        BeginEndType.BEGIN,
        aCompilerState.getCurrentBpelPath()
    );
    beginState.setProperty(Consts.PROPERTY_IF_CONDITION, ifActivity.getCondition().getTextValue().trim());

    beginState = model.save(beginState);

    if (ifActivity.getCondition() != null) {
      createReadAccessForCondition(
          ifActivity.getCondition().getTextValue(),
          beginState,
          model,
          aCompilerState
      );
    } else {
      logger.error(
          LogMarker.MODEL_PROBLEM,
          "There is an <if> with no condition. Should not be possible. {}",
          ifActivity
      );
    }

    stack.push(beginState);
    mapIf2Case.put(beginState.getId(), new HashSet<>());
  }

  private void handleIfMainEnd(final IfActivity ifActivity, final CompilerState aCompilerState) {
    final FlowModel        model   = aCompilerState.getFlowModel();
    final StateFlowFactory factory = model.factory();
    final FlowState endState = model.save(
        factory.createState(
            ifActivity.getName(),
            "IF__" + ifActivity.getName() + "__END",
            Activity.IF,
            BeginEndType.END,
            aCompilerState.getCurrentBpelPath()
        )
    );
    // was there no <else> branch? -> transition from IF to IFEND
    if (ifActivity.getCases().size() == 0) {
      final FlowState ifBeginState = model.refresh(stack.peekFirst(), 3);
      //else transition
      FlowTransition transition = factory.createTransition(
          ifBeginState,
          endState,
          (null == ifActivity.getCondition()
           ? null
           : ifActivity.getCondition().getTextValue()),
          String.format("IF[%s]->ENDIF[%s]", ifBeginState.getFlowId(), endState.getFlowId()),
          aCompilerState.getCurrentBpelPath()
      );
      transition.setInvertGuard(true); //this marks it as <else> transition
      model.save(transition);

      //connect state before endState with endState
      final FlowState previousState = model.findPreviousState(endState);
      transition = factory.createTransition(
          previousState,
          endState,
          null,
          String.format("Prev(ENDIF)[%s]->ENDIF[%s]", previousState.getFlowId(), endState.getFlowId()),
          aCompilerState.getCurrentBpelPath()
      );
      model.save(transition);
      //outbound transition for CONDITION == TRUE
      final FlowState followingState = model.findFollowingState(ifBeginState);
      transition = factory.createTransition(
          ifBeginState,
          followingState,
          (null == ifActivity.getCondition()
           ? null
           : ifActivity.getCondition().getTextValue().trim()),
          String.format("IF[%s]->Next(IF)[%s]", ifBeginState.getFlowId(), followingState.getFlowId()),
          aCompilerState.getCurrentBpelPath()
      );
      model.save(transition);
    } else {
      //outbound transition for CONDITION == TRUE
      final FlowState ifBeginState   = model.refresh(getLastMainIf(), 3);
      final FlowState followingState = model.findFollowingState(ifBeginState);
      assert followingState != null;
      FlowTransition transition = factory.createTransition(
          ifBeginState,
          followingState,
          (null == ifActivity.getCondition()
           ? null
           : ifActivity.getCondition().getTextValue().trim()),
          String.format("IF[%s]->Next(IF)[%s]", ifBeginState.getFlowId(), followingState.getFlowId()),
          aCompilerState.getCurrentBpelPath()
      );
      model.save(transition);
      //connect the endState before the <else> endState with the end endState
      for (final FlowState caseState: mapIf2Case.getOrDefault(ifBeginState.getId(), null)) {
        transition = factory.createTransition(
            caseState,
            endState,
            null,
            String.format("Prev(IFCASE)[%s]->ENDIF[%s]", caseState.getFlowId(), endState.getFlowId()),
            aCompilerState.getCurrentBpelPath()
        );
        model.save(transition);
      }
    }
    while (!stack.isEmpty() && stack.peekFirst().getCausingActivity() != Activity.IF) {
      mapIf2Case.remove(stack.peekFirst().getId());
      stack.pop();
    }
    mapIf2Case.remove(stack.peekFirst().getId());
    stack.pop();
    //just a second <if> is directly inside the enclosing <if>
    if (!stack.isEmpty()
        && (endState.getBpelPath().isDirectChildOf(stack.peekFirst().getBpelPath()))
    )
    {
      mapIf2Case.get(stack.peekFirst().getId()).add(endState);
    }
  }
}
