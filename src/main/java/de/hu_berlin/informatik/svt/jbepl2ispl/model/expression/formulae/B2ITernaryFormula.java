package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public interface B2ITernaryFormula extends B2IFormula {
  B2IFormula getTerm1();

  B2IFormula getTerm2();

  B2IFormula getTerm3();
}
