package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.repository;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.Neo4jFlowReadAccessRelation;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
public interface VarReadAccessRepository extends Neo4jRepository<Neo4jFlowReadAccessRelation, Long> {
  @Query(
      "MATCH (s:State)<-[r:FLOW_READS_FROM]-(v:Variable) WHERE id(s) = {eId} RETURN DISTINCT r,s,v ORDER BY r.elementId")
  Collection<Neo4jFlowReadAccessRelation> findAllByState(@Param("eId") final long state);

  @Query(
      "MATCH (s:State)<-[r:FLOW_READS_FROM]-(v:Variable) WHERE id(s) = {eId} AND v.name = {varName} RETURN DISTINCT r,s,v"
  )
  Neo4jFlowReadAccessRelation findAllByStateAndVariable(
      @Param("eId") final long state, @Param("varName") final String variableName
  );

  @Query(
      "MATCH (s:State)<-[r:FLOW_READS_FROM]-(v:Variable) WHERE v.name = {varName} RETURN DISTINCT r,s,v ORDER BY r.elementId")
  List<Neo4jFlowReadAccessRelation> findAllByVariable(@Param("varName") final String variableName);
}
