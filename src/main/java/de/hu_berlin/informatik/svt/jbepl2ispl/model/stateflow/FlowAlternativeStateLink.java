package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

/**
 * Created by Christoph Graupner on 2018-06-14.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface FlowAlternativeStateLink {
  FlowState getAlternative();


  FlowState getOrigin();

}
