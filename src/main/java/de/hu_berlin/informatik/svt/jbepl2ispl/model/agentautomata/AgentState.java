package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.ExtraInformation;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.MessagingType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2INaryExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowId;
import lombok.NonNull;

import java.util.Map;

/**
 * Created by Christoph Graupner on 2018-04-21.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AgentState extends AgentIdentifiable, ExtraInformation {
  AgentAutomaton getAgentAutomaton();

  B2IExpression getAssignment(String variableName);

  Map<QName, B2IExpression> getAssignmentMap();

  Activity getCausingActivity();

  MessagingType getMessagingType();

  String getName();

  default String getNameAsIsplVarValue() {
    return getName();
//    return "S" + getElementId();
  }

  FlowId getOriginStateId();

  /**
   * If you append something to the expression use {@link #setStateEntryConditions(B2IExpression)} to
   * save it otherwise it is lost.
   *
   * @return an empty B2INaryExpression(AND) expression if nothing has been set before,
   * or an filled B2INaryExpression(AND) if something has be set.
   */
  B2INaryExpression getStateEntryConditions();

  void setStateEntryConditions(B2IExpression condition);

  String getVariableValue(String variableName);

  boolean isInitialState();

  void setAssignment(QName variableName, B2IExpression expression);

  /**
   * Sets the value for the variable varName, that is valid for that variable after the state has been reach.
   *
   * @param varName
   * @param variableValue
   */
  void setVariableValue(@NonNull QName varName, @NonNull String variableValue);
}
