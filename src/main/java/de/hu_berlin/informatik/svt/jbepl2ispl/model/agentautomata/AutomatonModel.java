package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentAutomatonModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentTransitionModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowId;

import java.util.Collection;
import java.util.Set;

/**
 * Created by Christoph Graupner on 2018-04-28.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AutomatonModel extends AutomatonModelEvaluation {

  void addAgentAutomaton(AgentAutomaton agentAutomaton);

  AgentAutomatonFactory factory();

  <T extends AgentState> Collection<T> findAllStatesWithActionForAgent(
      String actionName, AgentAutomaton agentAutomaton
  );

  Set<String> findAllVariableValuesFor(QName varName);

  AgentState findStateForAgentByOriginFlowId(
      final AgentAutomaton agentAutomaton,
      final FlowId flowStateId
  );

  AgentAutomaton getAgentAutomaton(String agentName);

  <T extends AgentAutomaton> Collection<T> getAllAgentAutomatons();

  <T extends AgentTransition> Collection<T> getAllInboundTransitions(AgentState state);

  <T extends AgentTransition> Collection<T> getAllOutboundTransitions(AgentState state);

  <T extends AgentState> Collection<T> getAllStatesForAutomaton(AgentAutomaton agentAutomaton);

  <T extends AgentTransition> Collection<T> getAllTransitions();

  <T extends AgentTransition> Collection<T> getAllTransitionsForAutomaton(String agentAutomatonName);

  AgentState getLastStateForAgent(AgentAutomaton agentAutomaton);

  AgentVariableDefinition refresh(AgentVariableDefinition variableDefinition, int depth);

  <T extends AgentAutomaton> T refresh(T agent, int depth);

  AgentState refresh(AgentState agent, int depth);

  AgentState save(AgentState state);

  AgentAutomatonModifiable save(AgentAutomaton automaton);

  AgentVariableDefinition save(AgentVariableDefinition variableDefinition);

  AgentTransitionModifiable save(AgentTransition agentTransition);
}
