package de.hu_berlin.informatik.svt.jbepl2ispl.model;

/**
 * Created by Christoph Graupner on 2018-06-23.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface Identifiable {
  /**
   * Per class (nearly) unique (except for clones)
   *
   * @return
   */
  int getElementId();

  /**
   * DB wide unique
   *
   * @return
   */
  Long getId();

  /**
   * Human identifiable name of the object.
   *
   * @return
   */
  String getName();
}
