package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor;

import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentVariableDefinition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentAutomatonModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVariable;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
@Service
public class VariableCommonAgentExtractor extends CommonExtractorBase {
  public VariableCommonAgentExtractor(
      final FlowModel flowModel, final AutomatonModel automatonModel,
      final StateInformationResolver stateInformationResolver,
      final VariableTargetServiceResolver variableTargetServiceResolver
  ) {
    super(flowModel, automatonModel, stateInformationResolver, variableTargetServiceResolver);
  }

  @Override
  public void extract() {

    for (final AgentAutomaton agentAutomaton: model.getAllAgentAutomatons()) {
      logger.trace(LogMarker.EXTRACTION, "Extracting variables for Agent '{}'", agentAutomaton.getName());
      if (agentAutomaton.isEnvironment()) {
        envExtractVariables((AgentAutomatonModifiable) agentAutomaton);
        envExtractTraceLabelVariables((AgentAutomatonModifiable) agentAutomaton);
      } else {
        agentExtractBufferVariables((AgentAutomatonModifiable) agentAutomaton);
      }
      logger.trace(LogMarker.EXTRACTION, "DONE Extracting variables for Agent '{}'", agentAutomaton.getName());
    }
  }

  private void agentExtractBufferVariables(
      @NonNull final AgentAutomatonModifiable agent
  ) {

    final FlowService flowService = flowModel.findServiceByName(agent.getName());
    final Collection<FlowService> connectedServices =
        flowModel.findAllServicesItTalksToForService(flowService.getName());

    if (logger.isDebugEnabled()) {
      logger.debug(LogMarker.EXTRACTION, "Create BufferVar for {} \n+++++\n {}",
                   flowService.getName(),
                   StringUtils.join(connectedServices, "\n--++  ")
      );
    }

    for (FlowService otherService: connectedServices) {
      final String         agentName      = otherService.getName();
      final AgentAutomaton agentAutomaton = model.getAgentAutomaton(agentName);
      agent.addTalksTo(agentAutomaton);

      final String bufferName = Naming.getBufferName(flowService.getName(), agentName);
      //create a bufferVar only if it does not exists
      if (agent.getVariableDefinition(bufferName) == null) {
        AgentVariableDefinition varDef = factory.createAgentVariableDefinition(bufferName);
        varDef.setValueType(AgentVariableDefinition.ValueType.ENUM);
        varDef.setPurposeType(FlowVariable.PurposeType.SHARED_BUFFER);
        varDef.setBpelDeclared(false);

        agent.save(varDef);

        logger.debug(LogMarker.EXTRACTION, "Create BufferVar [{}] in {}",
                     varDef,
                     agent.getName()
        );
      }
      model.save(agent);

      // this should not be skipped
      if (agentAutomaton.getVariableDefinition(bufferName) == null) {
        AgentVariableDefinition oppositeVarDef = factory.createAgentVariableDefinition(bufferName);
        oppositeVarDef.setValueType(AgentVariableDefinition.ValueType.ENUM);
        oppositeVarDef.setPurposeType(FlowVariable.PurposeType.SHARED_BUFFER);
        oppositeVarDef.setBpelDeclared(false);
        logger.debug(LogMarker.EXTRACTION, "Create opposite BufferVar [{}] in {}",
                     oppositeVarDef,
                     agentAutomaton.getName()
        );

        ((AgentAutomatonModifiable) agentAutomaton).save(oppositeVarDef);
        model.save(agentAutomaton);
      }
    }
  }

  private void envExtractTraceLabelVariables(
      final AgentAutomatonModifiable agent
  ) {
    model.getAllAgentAutomatons().forEach(
        otherAgent -> {
          if (otherAgent.isEnvironment()) {
            return;
          }
          AgentVariableDefinition varDef = factory.createAgentVariableDefinition(
              otherAgent.getTraceLabelName().getName()
          );
          varDef.setPurposeType(FlowVariable.PurposeType.TRACE_LABEL);
          varDef.setValueType(AgentVariableDefinition.ValueType.BOOLEAN);
          varDef.setBpelDeclared(false);
          agent.save(varDef);
          model.save(agent);
        }
    );
  }

  private void envExtractVariables(final AgentAutomatonModifiable agent) {
    for (final FlowVariable flowVariable: flowModel.getVariables()) {
      if (flowVariable.isInternalOnly()) {
        continue;
      }
      logger.debug(LogMarker.EXTRACTION, "VARI {}", flowVariable);

      final AgentVariableDefinition varDef = factory.createAgentVariableDefinition(flowVariable.getName());
      varDef.setBpelDeclared(!flowVariable.isVirtual());
      varDef.setOriginVariable(flowVariable);

      if (flowVariable.getPurposeType() == null) {
        throw new IllegalStateException("purposeType is null: " + flowVariable);
      }

      switch (flowVariable.getPurposeType()) {
        case SHARED_BUFFER:
          //should not be used
          throw new IllegalStateException(
              FlowVariable.PurposeType.SHARED_BUFFER + " should not be used in " + FlowService.ENV
          );
        case COMM_CHANNEL:
          varDef.setValueType(AgentVariableDefinition.ValueType.ENUM);
          break;
        case OPERATIONAL:
        case CONTAINER:
          final AgentVariableDefinition.ValueType variableType = getVariableType(flowVariable.getValueType());

          if (variableType == null) {
            logger.error(LogMarker.MODEL_PROBLEM, "{} is an {} vartype, so it must be boolean or integer, got {}",
                         flowVariable, flowVariable.getPurposeType(), flowVariable.getVariableType()
            );
          }
          varDef.setValueType(variableType);
          if (variableType == AgentVariableDefinition.ValueType.ENUM) {
            logger.error(LogMarker.TODO_EXCEPTION, "Define OPERATIONAL values [{}]", varDef);
          }

          break;
        case MESSAGE:
          varDef.setValueType(AgentVariableDefinition.ValueType.ENUM);
          break;
      }
      if (varDef.getValueType() == null) {
        logger.error(LogMarker.MODEL_PROBLEM, "{} is an {} vartype, so something should be done to handle {}",
                     flowVariable, flowVariable.getPurposeType(), flowVariable.getVariableType()
        );
      }

      agent.save(varDef);
    }
  }

  private AgentVariableDefinition.ValueType getVariableType(final FlowVariable.ValueType valueType) {
    switch (valueType) {
      case STRING:
        return AgentVariableDefinition.ValueType.ENUM;
      case BOOLEAN:
        return AgentVariableDefinition.ValueType.BOOLEAN;
      case INT:
        return AgentVariableDefinition.ValueType.INT;
      case UNKNOWN:
      default:
        return null;
    }
  }
}
