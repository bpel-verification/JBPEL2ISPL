package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae;

import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class DefaultB2INaryFormula implements B2INaryFormula {
  private static final long     serialVersionUID = 1L;
  @Getter final        Function function;
  @Getter
  B2IFormula[] terms;

  protected DefaultB2INaryFormula(
      final Function function, @NonNull B2IFormula... terms
  ) {
    this.function = function;
    this.terms = terms;
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends B2IFormula> T addTerm(B2IFormula expression) {
    this.terms = Arrays.copyOf(terms, terms.length + 1);
    terms[terms.length - 1] = expression;
    return (T) this;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof B2INaryFormula)) {
      return false;
    }
    final B2INaryFormula that = (B2INaryFormula) o;
    return getFunction() == that.getFunction() &&
           Arrays.equals(getTerms(), that.getTerms());
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(getFunction());
    result = 31 * result + Arrays.hashCode(getTerms());
    return result;
  }

  @Override
  public boolean isEmpty() {
    return terms.length == 0;
  }

  @Override
  public int size() {
    return terms.length;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder(getFunction().toString());
    sb.append("(")
      .append(StringUtils.join(terms, ", "));
    sb.append(')');
    return sb.toString();
  }
}
