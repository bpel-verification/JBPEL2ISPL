package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVarReadAccessRelation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.ogm.annotation.*;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@RelationshipEntity(type = Naming.RELATION_FLOW_VAR_READ_ACCESS)
@NoArgsConstructor
public class Neo4jFlowReadAccessRelation extends Neo4jFlowAccessRelation implements FlowVarReadAccessRelation {
  @Id
  @GeneratedValue
  @Getter
  Long     id;
  @EndNode
  @Getter
  @Setter
  State    state;
  @StartNode
  @Getter
  @Setter
  Variable variable;

  Neo4jFlowReadAccessRelation(
      final State state,
      final Variable variable,
      final VarValueSourceType varValueSourceType,
      final String value,
      final String valueSource,
      final int elementId,
      final String bpelPathAsString
  ) {
    super(
        state.getDisplayName() + ">" + variable.getName(), state, variable, varValueSourceType, value, valueSource,
        elementId, bpelPathAsString,
        0
    );
  }
}
