package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.repository;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.AlternativeStateLinkRelation;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
public interface AlternativeStateLinkRepository extends Neo4jRepository<AlternativeStateLinkRelation, Long> {
}
