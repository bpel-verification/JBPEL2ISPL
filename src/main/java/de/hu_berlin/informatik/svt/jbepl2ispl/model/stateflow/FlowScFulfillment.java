package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.ExtraInformation;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface FlowScFulfillment extends Identifiable, ExtraInformation {
  FlowScCommitment getCommitment();

  int getFulfillmentId();

  FlowState getTargetState();
}
