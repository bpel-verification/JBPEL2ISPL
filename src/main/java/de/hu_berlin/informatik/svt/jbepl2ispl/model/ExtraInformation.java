package de.hu_berlin.informatik.svt.jbepl2ispl.model;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface ExtraInformation {
  String getProperty(String propertyName);

  /**
   * @param identifier
   *
   * @return <b>true</b> if property is set and is <em>true</em>
   * <p>
   * <b>false</b> otherwise
   *
   * @see de.hu_berlin.informatik.svt.jbepl2ispl.Consts Consts.PROPERTY_* values
   */
  boolean isFlagged(String identifier);

  void setFlagged(String identifier, boolean state);

  void setProperty(String propertyName, String value);
}
