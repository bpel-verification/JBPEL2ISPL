package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.apache.ode.bpel.compiler.bom.ReceiveActivity;

import java.util.Arrays;

/**
 * Created by Christoph Graupner on 2018-05-11.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class ReceiveHandler extends HandlerBase {
  public ReceiveHandler() {
    super(Arrays.asList(ReceiveActivity.class));
  }

  @Override
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    if (currentBpelObject instanceof ReceiveActivity) {
      handleReceive((ReceiveActivity) currentBpelObject, compilerState);
    }
    return (T) this;
  }

  private void handleReceive(final ReceiveActivity receiveActivity, final CompilerState compilerState) {
    final FlowModel        model       = compilerState.getFlowModel();
    final StateFlowFactory factory     = model.factory();
    final FlowService      flowService = model.findServiceByName(receiveActivity.getPartnerLink());

    final String inputVariableName = receiveActivity.getVariable();
    assert inputVariableName != null;

    final FlowState state = factory.createState(
        receiveActivity.getName(), "RECEIVE__" + receiveActivity.getName() + "__" + inputVariableName,
        Activity.RECEIVE,
        BeginEndType.BOTH,
        compilerState.getCurrentBpelPath()
    );
    FlowVariable flowVariable = model.findVariableByName(inputVariableName);
    flowVariable.setProperty(Consts.PROPERTY_VAR_IS_EXTERNAL_BPEL_CLIENT_INPUT, Boolean.toString(true));
    flowVariable = model.save(flowVariable);


    state.setOutputVariable(flowVariable.getName());
    model.save(state);

    // this is a replacement for FlowVariableValue
    final FlowVarWriteAccessRelation writeValue = factory.createVarWriteAccessRelation(
        state,
        flowVariable,
        "RECEIVED_INPUT__" + receiveActivity.getName(),
        "CALL_VALUE__" + receiveActivity.getPartnerLink() + '#' + receiveActivity.getOperation(),
        VarValueSourceType.RECEIVE,
        compilerState.getCurrentBpelPath()
    );

    model.save(writeValue);

    FlowServiceOperation agentOperation = factory.createServiceOperation(
        flowService,
        receiveActivity.getOperation(),
        "CALL_VALUE",
        inputVariableName,
        compilerState.getCurrentBpelPath()
    );
    agentOperation = model.save(agentOperation);

    flowService.addOperation(agentOperation);
    flowService.addVariable(flowVariable.getName());
    model.save(flowService);

    state.setTargetServiceOperation(agentOperation);
    model.save(state);
  }
}
