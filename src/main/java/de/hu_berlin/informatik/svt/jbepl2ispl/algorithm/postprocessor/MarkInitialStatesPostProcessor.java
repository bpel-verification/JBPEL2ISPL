package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.EnumSet;
import java.util.stream.Collectors;

/**
 * Created by Christoph Graupner on 2018-06-27.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
class MarkInitialStatesPostProcessor extends FlowModelPostProcessor {

  protected MarkInitialStatesPostProcessor(final FlowModel model) {
    super(model);
  }

  @Override
  public void finish() {
    markInitializingStates();
  }

  private void markInitializingStates() {
    logger.trace(LogMarker.POSTPROCESSING, "Marking states as initial...");
    final EnumSet<Activity> statesDontBreak = EnumSet.of(Activity.SEQUENCE, Activity.__INITIAL_STATE);
    for (final FlowState flowState: flowModel.getStates()
                                             .stream()
                                             .sorted(Comparator.comparingInt(Identifiable::getElementId))
                                             .collect(Collectors.toList())) {
      //as long as an initial or non-initialization-breaking state, mark states, as the first other state comes, stop marking
      if (flowState.getCausingActivity() == Activity.RECEIVE
          || flowState.getCausingActivity() == Activity.ASSIGN &&
             flowState.getName().matches(Consts.INITIAL_STATE_NAME_IDENTIFIER_REGEX))
      {
        flowState.setInitialState(true);
        flowModel.save(flowState);
        logger.info(LogMarker.POSTPROCESSING, "Marked {} as initial...", flowState);
      } else if (false == statesDontBreak.contains(flowState.getCausingActivity())) {
        logger.trace(LogMarker.POSTPROCESSING, "First non initial state found: {}. Stopping...", flowState);
        break;
      }
    }
    logger.trace(LogMarker.POSTPROCESSING, "DONE Marking states as initial...");
  }
}
