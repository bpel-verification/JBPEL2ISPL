package de.hu_berlin.informatik.svt.jbepl2ispl;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by Christoph Graupner on 2018-06-23.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
public class Config {
  @Value("${debug.path-for-agent-graphs:/tmp}")
  @Getter
  String  debugPathForAgentGraphs;
  @Value("${debug.print-remove-useless-inspection:false}")
  @Getter
  boolean debugPrintRemoveUselessInspection;
  @Value("${debug.remove-single-flows:false}")
  @Getter
  boolean debugRemoveSingleFlows;
  @Value("${print.agent-graphs:false}")
  @Getter
  boolean printAgentGraphs;
  @Value("${print.flow-model-graph:false}")
  @Getter
  boolean printModelGraph;
  @Value("${print.variable-access-graph:false}")
  @Getter
  boolean printVariableAccessGraph;
  @Value("${commitment.activity-name:commitment-label}")
  @Getter
  String  scCommitmentActivityName;
  @Value("${fulfillment.activity-name:fulfillment-label}")
  @Getter
  String  scFulfillmentActivityName;
  @Value("${recovery.activity-name:recovery-label}")
  @Getter
  String  scRecoveryLabelActivityName;
  @Value("${trace-label.activity-name:trace-label}")
  @Getter
  String  scTraceLabelActivityName;
}
