package de.hu_berlin.informatik.svt.jbepl2ispl.utils;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowId;
import org.neo4j.ogm.typeconversion.AttributeConverter;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public class FlowIdNeo4jConverter implements AttributeConverter<FlowId, String> {
  @Override
  public FlowId toEntityAttribute(final String value) {
    final String[] x = value.split("[.]");
    assert x.length == 2;
    return new FlowId(Integer.valueOf(x[0]), Integer.valueOf(x[1]));
  }

  @Override
  public String toGraphProperty(final FlowId value) {
    return value == null ? null : value.getElementId() + "." + value.getAlternateId();
  }
}
