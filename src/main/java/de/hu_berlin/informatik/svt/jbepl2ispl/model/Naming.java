package de.hu_berlin.informatik.svt.jbepl2ispl.model;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowScCommitment;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowScFulfillment;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Christoph Graupner on 2018-03-18.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface Naming {
  String AGENT_AUTOMATON                           = "AgentAutomaton";
  String AGENT_VARIABLE_DEFINITION                 = "AgentVariableDefinition";
  String FLOW_MESSAGING                            = "Messaging";
  String RELATION_ALTERNATIVE_STATE                = "ALTERNATIVE";
  String RELATION_AUTOMATON_STATE                  = "AUTOMATON_STATE";
  String RELATION_AGENT_BELONGS_TO                 = "BELONGS_TO";
  String RELATION_DEFINES                          = "DEFINES";
  String RELATION_ENTITY_AGENT_TRANSITION          = "AGENT_TRANSITION";
  String RELATION_FLOW_BELONGS_TO                  = "FL_BELONGS_TO";
  String RELATION_FLOW_MSG_INTERMEDIATE_STATE_FROM = "FL_MSG_INTERMEDIATE_FROM";
  String RELATION_FLOW_MSG_INTERMEDIATE_STATE_TO   = "FL_MSG_INTERMEDIATE_TO";
  String RELATION_FLOW_MSG_ORIGIN_STATE            = "FL_MSG_ORIGIN";
  String RELATION_FLOW_MSG_RECEIVER                = "FL_RECEIVER";
  String RELATION_FLOW_MSG_SENDER                  = "FL_SENDER";
  String RELATION_FLOW_MSG_VALUE_FROM              = "FL_VALUE_FROM";
  String RELATION_FLOW_MSG_VALUE_TO                = "FL_VALUE_TO";
  String RELATION_FLOW_SC_CREDITOR                 = "SC_CREDITOR";
  String RELATION_FLOW_SC_DEBTOR                   = "SC_DEBTOR";
  String RELATION_FLOW_SC_FULFILLMENT_OF           = "SC_FULFILLMENT_OF";
  String RELATION_FLOW_SC_TARGET                   = "SC_TARGET_STATE";
  String RELATION_FLOW_SC_TRACED_SERVICE           = "SC_TRACED";
  String RELATION_FLOW_VAR_READ_ACCESS             = "FLOW_READS_FROM";
  String RELATION_FLOW_VAR_WRITE_ACCESS            = "FLOW_WRITE_TO";
  String RELATION_FOLLOWED_BY                      = "FOLLOWED_BY";
  String RELATION_FOLLOWED_BY_LABEL_PREV           = "<<PREV";
  String RELATION_ORIGIN_SERVICE                   = "ORIGIN_SERVICE";
  String RELATION_PROVIDES_OP                      = "PROVIDES_OP";
  String RELATION_TALKS_TO                         = "TALKS_TO";

  static QName createActionName(@NonNull final String agentName, @NonNull final String... names) {
    return new QName(agentName, StringUtils.join(names, Consts.ISPL_ACTION_NAME_DELEMITTER));
  }

  static String getBufferName(final String sourceService, final String targetService) {
    if (sourceService.compareToIgnoreCase(targetService) < 0) {
      return Consts.ISPL_VAR_PREFIX_BUFFER
             + Consts.ISPL_VAR_NAME_SEPARATOR + sourceService
             + Consts.ISPL_VAR_NAME_SEPARATOR + targetService;
    } else { //swapped name positions
      return Consts.ISPL_VAR_PREFIX_BUFFER
             + Consts.ISPL_VAR_NAME_SEPARATOR + targetService
             + Consts.ISPL_VAR_NAME_SEPARATOR + sourceService;
    }
  }

  static String getCommChannelName(final int channelNumber) {
    return Consts.VAR_NAME_PREFIX_COMM_CHANNEL + channelNumber;
  }

  static String getCommitmentConditionName(@NonNull final FlowScCommitment commitment) {
    return commitment.getName() + commitment.getCommitmentId() + Consts.ISPL_VAR_NAME_SEPARATOR + "Condition";
  }

  static String getCommitmentContentName(@NonNull final FlowScCommitment commitment) {
    return commitment.getName() + commitment.getCommitmentId() + Consts.ISPL_VAR_NAME_SEPARATOR + "Content";
  }

  static String getDesiredStateProposition(final String agentName) {
    return agentName + Consts.ISPL_VAR_NAME_SEPARATOR + Consts.PROPOSITION_DESIRED_STATE_SUFFIX;
  }

  static String getEnumVariableValueName(final String variableName, final int symbolicValue) {
    return variableName + Consts.ISPL_VAR_NAME_SEPARATOR + Consts.VAR_VALUE_ENUM_PREFIX_MSG + symbolicValue;
  }

  static String getFulfillmentConditionName(@NonNull final FlowScFulfillment fulfillment) {
    return fulfillment.getName() + fulfillment.getFulfillmentId() + Consts.ISPL_VAR_NAME_SEPARATOR + "Condition";
  }

  static QName getTraceLabelName(@NonNull final String agentName) {
    return new QName(
        FlowService.ENV,
        Consts.ISPL_VAR_PREFIX_TRACE_LABEL + Consts.ISPL_VAR_NAME_SEPARATOR + agentName
    );
  }

  static String getUnDesiredStateProposition(@NonNull String agentName) {
    return agentName + Consts.ISPL_VAR_NAME_SEPARATOR + Consts.PROPOSITION_UN_DESIRED_STATE_SUFFIX;
  }
}
