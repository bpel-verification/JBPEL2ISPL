package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j.repository;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j.Neo4jAgentTransition;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Christoph Graupner on 2018-05-29.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
public interface Neo4jAgentTransitionRepository
    extends Neo4jRepository<Neo4jAgentTransition, Long>, CustomNeo4jAgentTransitionRepository
{

  @Query("MATCH (s1:AgentState)-[r:AGENT_TRANSITION]->(s2:AgentState) WHERE  id(s1) = {stateId} RETURN r,s1,s2")
  List<Neo4jAgentTransition> findAllBySourceState(@Param("stateId") Long state);

  @Query("MATCH (s1:AgentState)-[r:AGENT_TRANSITION]->(s2:AgentState) WHERE  id(s2) = {stateId} RETURN r,s1,s2")
  List<Neo4jAgentTransition> findAllByTargetState(@Param("stateId") Long state);

  @Query("MATCH (n:AgentAutomaton {name: {agentName} })-[r:AUTOMATON_STATE]->(s:AgentState)" +
         " MATCH (s)-[r2:AGENT_TRANSITION]->(s2:AgentState) RETURN r2,s,s2")
  List<Neo4jAgentTransition> findAllForAutomaton(@Param("agentName") String agentAutomaton);
}
