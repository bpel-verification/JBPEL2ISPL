package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.apache.ode.bpel.compiler.bom.InvokeActivity;

import java.util.Arrays;

/**
 * Created by Christoph Graupner on 2018-02-28.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public class InvokeHandler extends HandlerBase {
  public InvokeHandler() {
    super(Arrays.asList(InvokeActivity.class));
  }

  @Override
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    if (currentBpelObject instanceof InvokeActivity) {
      handleInvoke((InvokeActivity) currentBpelObject, compilerState);
    }
    return (T) this;
  }

  protected void handleInvoke(final InvokeActivity invokeActivity, final CompilerState compilerState) {
    final FlowModel        model            = compilerState.getFlowModel();
    final StateFlowFactory factory          = model.factory();
    final String           sendVariable     = invokeActivity.getInputVar();
    final String           outputVariable   = invokeActivity.getOutputVar();
    final String           action           = invokeActivity.getOperation();
    final String           invokedAgentName = invokeActivity.getPartnerLink();

    FlowService flowService = model.findServiceByName(invokedAgentName);
    FlowService env         = model.findServiceByName(FlowService.ENV);

    FlowState state = factory.createState(
        invokeActivity.getName(),
        getInvokeStateName(invokeActivity),
        Activity.INVOKE,
        BeginEndType.BOTH,
        compilerState.getCurrentBpelPath()
    );
    state.setInputVariable(model.findVariableByName(sendVariable).getName());
    flowService.addVariable(sendVariable);
    if (outputVariable != null) {
      state.setOutputVariable(model.findVariableByName(outputVariable).getName());
      flowService.addVariable(outputVariable);
    }

    state = model.save(state);

    FlowServiceOperation agentOperation = model.findServiceOperation(flowService, action, sendVariable, outputVariable);
    if (agentOperation == null) {
      agentOperation = factory.createServiceOperation(
          flowService,
          action,
          sendVariable,
          outputVariable,
          compilerState.getCurrentBpelPath()
      );
      agentOperation = model.save(agentOperation);
    }

    flowService.addOperation(agentOperation);
    flowService = model.save(flowService);

    state.setSourceService(env);
    state.setTargetService(flowService);
    state.setTargetServiceOperation(agentOperation);

    model.save(state);

    // first read relation then write relation (the generated elementId keeps the order of value-flow)
    final FlowVarReadAccessRelation readAccess = factory.createVarReadAccessRelation(
        state,
        model.findVariableByName(sendVariable),
        null,
        sendVariable,
        VarValueSourceType.INVOKE,
        compilerState.getCurrentBpelPath()
    );
    model.save(readAccess);

    if (outputVariable != null) {
      final FlowVarWriteAccessRelation writeAccess = factory.createVarWriteAccessRelation(
          state,
          model.findVariableByName(outputVariable),
          "INVK_RET(" + invokedAgentName + "#" + action + "," + sendVariable + ")",
          sendVariable,
          VarValueSourceType.INVOKE,
          compilerState.getCurrentBpelPath()
      );
      model.save(writeAccess);
    }
  }

  private String getInvokeStateName(final InvokeActivity invokeActivity) {
    return "INVOKE__" + invokeActivity.getName() + Consts.ISPL_STATE_NAME_DELEMITTER
           + "IN" + Consts.ISPL_STATE_NAME_DELEMITTER + invokeActivity.getInputVar()
           + Consts.ISPL_STATE_NAME_DELEMITTER + "OUT" + Consts.ISPL_STATE_NAME_DELEMITTER +
           invokeActivity.getOutputVar();
  }
}
