package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import lombok.NonNull;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class B2IActionExpression extends B2IExpressionTerm<QName> {

  private static final long serialVersionUID = 1L;

  B2IActionExpression(@NonNull final QName qName) {
    super(Type.ACTION_COMPARE, qName);
  }
}
