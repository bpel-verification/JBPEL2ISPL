package de.hu_berlin.informatik.svt.jbepl2ispl.model;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public enum VarValueSourceType {
  /**
   * if the value was assigned from another variable
   **/
  VARIABLE,
  LITERAL, LITERAL_XML, TEXT, RECEIVE, INVOKE, REPLY,
  CONDITION, XPATH
}
