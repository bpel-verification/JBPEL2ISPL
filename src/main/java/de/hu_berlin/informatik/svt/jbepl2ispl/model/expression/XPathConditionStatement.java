package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import lombok.Getter;
import lombok.NonNull;
import net.sf.saxon.expr.*;
import net.sf.saxon.expr.parser.Token;
import net.sf.saxon.om.Item;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.AtomicValue;
import net.sf.saxon.value.BooleanValue;
import net.sf.saxon.value.IntegerValue;
import net.sf.saxon.xpath.JAXPVariableReference;
import net.sf.saxon.xpath.XPathEvaluator;
import net.sf.saxon.xpath.XPathExpressionImpl;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.xml.SimpleNamespaceContext;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Christoph Graupner on 2018-06-12.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class XPathConditionStatement implements ConditionStatement {

  @Getter
  private final String      conditionRaw;
  private final Expression  expression;
  private final Logger      logger = LoggerFactory.getLogger(getClass());
  private       Set<String> variables;

  public XPathConditionStatement(@NonNull final String condition) throws XPathExpressionException {
    conditionRaw = condition;
    XPathEvaluator c = new XPathEvaluator();
    c.setXPathVariableResolver(variableName -> null);
    c.setNamespaceContext(new SimpleNamespaceContext());

    final XPathExpression expr            = c.compile(conditionRaw);
    XPathExpressionImpl   xPathExpression = ((XPathExpressionImpl) expr);
    expression = xPathExpression.getInternalExpression();
  }

  /**
   * Determines if an XPath expression is capable to handled within JBPEL2ISPL
   * <p>
   * For now: just "<",">","="
   *
   * @return
   */
  public boolean ableToHandle() {
    return ableToHandle(expression);
  }

  /**
   * Saxon simplyfies the expression during compilation, so is
   * not necessarily the original expression you get here back, but an equivalent.
   *
   * @return
   */
  public B2IExpression asCondition() {
    return asCondition(expression);
  }

  public String getConditionForVariable(final String varName) {
    logger.error(LogMarker.TODO_EXCEPTION, "XPathConditionStatement.getConditionForVariable: IMPLEMENT for {}",
                 varName
    ); //FIXME IMPLEMENT
    return "unknown";
  }

  @Override
  public Set<String> getVariables() {
    if (variables == null) {
      variables = new HashSet<>();
      scanExpression(expression);
    }
    return variables;
  }

  private boolean ableToHandle(final Expression expr) {
    boolean ret = true;
    if (false == (expr instanceof GeneralComparison
                  || expr instanceof BooleanExpression
                  || expr instanceof Literal
                  || expr instanceof Atomizer
                  || expr instanceof JAXPVariableReference
    ))
    {
      return false;
    }

    for (final Operand operand: expr.operands()) {
      ret = ret && ableToHandle(operand.getChildExpression());
    }
    return ret;
  }

  private B2IExpression asCondition(final Expression expr) {
    if (expr instanceof Literal) {
      return asConditionLiteral(expr);
    } else if (expr instanceof JAXPVariableReference) {
      return asConditionLiteral(expr);
    } else if (expr instanceof GeneralComparison) {
      final Expression lhsExpression = ((GeneralComparison) expr).getLhsExpression();
      final Expression rhsExpression = ((GeneralComparison) expr).getRhsExpression();

      return B2IExpression.compare(
          getCompareOpForExpressionOp(((GeneralComparison) expr).getOperator()),
          Objects.requireNonNull(asConditionLiteral(lhsExpression), "lhsExpress is null"),
          Objects.requireNonNull(asConditionLiteral(rhsExpression), "rhsExpress is null")
      );
    } else if (expr instanceof ArithmeticExpression) {
      final Expression lhsExpression = ((ArithmeticExpression) expr).getLhsExpression();
      final Expression rhsExpression = ((ArithmeticExpression) expr).getRhsExpression();
      return B2IExpression.arithmetic(
          getArithmeticOpForExpressionOp(((ArithmeticExpression) expr).getOperator()),
          asCondition(lhsExpression),
          asCondition(rhsExpression)
      );
    } else if (expr instanceof SlashExpression) {
      final Expression lhsExpression = ((SlashExpression) expr).getLhsExpression();
      final Expression rhsExpression = ((SlashExpression) expr).getRhsExpression();
      return B2IExpression.arithmetic(
          getArithmeticOpForExpressionOp(((SlashExpression) expr).getOperator()),
          asCondition(lhsExpression),
          asCondition(rhsExpression)
      );
    } else if (expr instanceof UnaryExpression) {
      final UnaryExpression atomicSequenceConverter = (UnaryExpression) expr;
      return asCondition(atomicSequenceConverter.getOperand().getChildExpression());
    } else {
      throw new NotImplementedException(expr.getExpressionName() + "(" + expr.getClass() + ") is not yet implemented");
    }
  }

  private B2IExpression asConditionLiteral(final Expression expr) {
    if (expr instanceof Literal) {
      final Literal literal = (Literal) expr;
      try {
        final Item head = literal.getValue().head();
        if (head instanceof AtomicValue) {
          if (head instanceof BooleanValue) {
            return B2IExpression.term(((BooleanValue) head).getBooleanValue());
          } else if (head instanceof IntegerValue) {
            return B2IExpression.term(Math.toIntExact(((IntegerValue) head).longValue()));
          }
          return B2IExpression.term(literal.getValue().getStringValue());
        }
      } catch (XPathException e) {
        logger.error(LogMarker.EXCEPTION, e.getLocalizedMessage(), e);
        throw new IllegalArgumentException(e);
      }
    } else if (expr instanceof JAXPVariableReference) {
      return B2IExpression.variableReference(getVariableNameOf((JAXPVariableReference) expr));
    }

    for (final Operand operand: expr.operands()) {
      return asCondition(operand.getChildExpression());
    }

    return null;
  }

  private B2IExpression.Type getArithmeticOpForExpressionOp(final int operator) {
    switch (operator) {
      case Token.PLUS:
        return B2IExpression.Type.NUM_ADD;
      case Token.MINUS:
      case Token.NEGATE:
        return B2IExpression.Type.NUM_SUB;
      case Token.DIV:
      case Token.IDIV:
      case Token.SLASH:
        return B2IExpression.Type.NUM_DIV;
      case Token.MULT:
        return B2IExpression.Type.NUM_MUL;
      default:
        throw new NotImplementedException("Token." + operator + " operator code is not yet implemented.");
    }
  }

  private B2ICompareExpression.CompareType getCompareOpForExpressionOp(final int operator) {
    switch (operator) {
      case Token.LE:
        return B2ICompareExpression.CompareType.LE;
      case Token.GE:
        return B2ICompareExpression.CompareType.GE;
      case Token.GT:
        return B2ICompareExpression.CompareType.GT;
      case Token.LT:
        return B2ICompareExpression.CompareType.LT;
      case Token.EQUALS:
        return B2ICompareExpression.CompareType.EQ;
    }
    return null;
  }

  private String getVariableNameOf(final JAXPVariableReference expr) {
    String name = expr.getExpressionName();
    if (name.startsWith("$")) {
      name = name.substring(1);
    }
    if (name.contains(".")) {
      name = name.substring(0, name.indexOf('.'));
    }
    return name;
  }

  private boolean isTransparentExpression(final Expression expr) {
    return expr instanceof AtomicSequenceConverter;
  }

  private void scanExpression(Expression expr) {
    if (expr instanceof JAXPVariableReference) {
      String name = getVariableNameOf((JAXPVariableReference) expr);
      variables.add(name);
    }
    for (final Operand operand: expr.operands()) {
      scanExpression(operand.getChildExpression());
    }
  }
}
