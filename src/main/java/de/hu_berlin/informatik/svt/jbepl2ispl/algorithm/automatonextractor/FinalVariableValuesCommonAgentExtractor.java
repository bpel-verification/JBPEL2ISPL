package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentVariableDefinition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentAutomatonModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Set;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
@Service
public class FinalVariableValuesCommonAgentExtractor extends CommonExtractorBase {
  protected FinalVariableValuesCommonAgentExtractor(
      final FlowModel flowModel,
      final AutomatonModel model,
      final StateInformationResolver stateResolver,
      final VariableTargetServiceResolver resolver
  ) {
    super(flowModel, model, stateResolver, resolver);
  }

  @Override
  public void extract() {
    model.getAllAgentAutomatons().forEach(
        agentAutomaton -> {

          final AgentAutomatonModifiable agentAutomatonModifiable = (AgentAutomatonModifiable) agentAutomaton;
          logger.trace(
              LogMarker.EXTRACTION, "Extracting final variables values for Agent '{}'",
              agentAutomatonModifiable.getName()
          );
          if (agentAutomaton.isEnvironment()) {
            envExtractCommChannelAllowedValues(agentAutomatonModifiable);
          } else {
            agentExtractBufferAllowedValues(agentAutomatonModifiable);
          }
          logger.trace(
              LogMarker.EXTRACTION, "DONE Extracting final variables values for Agent '{}'",
              agentAutomatonModifiable.getName()
          );
          model.save(agentAutomatonModifiable);
        }
    );
  }

  private void agentExtractBufferAllowedValues(final AgentAutomatonModifiable agent) {
    agent.getVariables().forEach(
        varName -> {
          if (varName.startsWith(Consts.ISPL_VAR_PREFIX_BUFFER + Consts.ISPL_VAR_NAME_SEPARATOR)) {
            collectAndSetAllowedValues(agent, new QName(agent.getName(), varName));
          }
        }
    );
  }

  private void collectAndSetAllowedValues(final AgentAutomatonModifiable agent, final QName varName) {
    final Set<String>             res    = model.findAllVariableValuesFor(varName);
    final AgentVariableDefinition varDef = agent.getVariableDefinition(varName.getName());
    res.add(Consts.ISPL_VAR_ENUM_NONE);
    varDef.setAllowedValues(new ArrayList<>(res));
    varDef.setInitialValue(Consts.ISPL_VAR_ENUM_NONE);
    agent.save(varDef);
  }

  private void envExtractCommChannelAllowedValues(final AgentAutomatonModifiable agent) {
    agent.getVariables().forEach(
        varName -> {
          if (varName.startsWith(Consts.VAR_NAME_PREFIX_COMM_CHANNEL)) {
            collectAndSetAllowedValues(agent, new QName(FlowService.ENV, varName));
          }
        }
    );
  }
}
