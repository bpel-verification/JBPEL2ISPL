package de.hu_berlin.informatik.svt.jbepl2ispl.configuration;

import org.neo4j.ogm.session.SessionFactory;
import org.springframework.boot.autoconfigure.data.neo4j.Neo4jProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by Christoph Graupner on 2018-04-23.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Configuration
@EnableNeo4jRepositories(
    basePackages = {
        "de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.repository",
        "de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j.repository",
        "de.hu_berlin.informatik.svt.jbepl2ispl.model.variable.impl.neo4j.repository"
    },
    transactionManagerRef = "neo4jTransactionManager")
@EnableTransactionManagement
@EnableConfigurationProperties(Neo4jProperties.class)
public class Neo4jConfiguration {

  @Bean
  public org.neo4j.ogm.config.Configuration configuration(Neo4jProperties properties) {
    return properties.createConfiguration();
  }

  //    @Bean
//    public org.neo4j.ogm.config.Configuration configuration() {
//      return new org.neo4j.ogm.config.Configuration.Builder()
//          .uri("bolt://localhost:7687")
//          .credentials("neo4j", "test")
//          .build();
//    }
//
  @Bean(destroyMethod = "close")
  public SessionFactory getSessionFactory(org.neo4j.ogm.config.Configuration configuration) {
    SessionFactory sessionFactory =
        new SessionFactory(
            configuration,
            "de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph",
            "de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j",
            "de.hu_berlin.informatik.svt.jbepl2ispl.model.variable.impl.neo4j"
        );
    return sessionFactory;
  }

  @Bean
  public Neo4jTransactionManager neo4jTransactionManager(SessionFactory sessionFactory) {
    return new Neo4jTransactionManager(sessionFactory);
  }

//    @Bean(destroyMethod = "close")
//    public SessionFactory sessionFactory(GraphDatabaseService db) {
//      EmbeddedDriver       driver = new EmbeddedDriver(db);
//      final SessionFactory sessionFactory =
//          new SessionFactory(driver, "de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph");
//      final Session session = sessionFactory.openSession();
//      session.purgeDatabase();
//      session.clear();
//      return sessionFactory;
//    }
//
//    @Bean
//    GraphDatabaseService getDatabaseService() {
//      return new GraphDatabaseFactory()
//          .newEmbeddedDatabaseBuilder(new File("./data/bcit.neo4j").getAbsoluteFile())
//          .setConfig(GraphDatabaseSettings.pagecache_memory, "512M")
//          .newGraphDatabase();
//    }
}
