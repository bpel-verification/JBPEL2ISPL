package de.hu_berlin.informatik.svt.jbepl2ispl.utils;

import com.google.common.base.Strings;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class Jb2iUtils implements Naming {

  private static Pattern pattern = Pattern.compile("^\\$\\{\\{([^}]+)\\}\\}$");

  /**
   * Read the object from Base64 string.
   */
  public static Object fromBase64StringToObject(@NonNull String s)
      throws IOException, ClassNotFoundException {
    byte[] data = Base64.getDecoder().decode(s);
    ObjectInputStream ois = new ObjectInputStream(
        new ByteArrayInputStream(data));
    Object o = ois.readObject();
    ois.close();
    return o;
  }

  public static QName getVariableOfVariableReference(final String value) {
    final Matcher matcher = pattern.matcher(value);
    return matcher.matches() ? new QName(matcher.group(1)) : null;
  }

  public static boolean isNullOrEmpty(final QName qName) {
    return qName == null
           || (Strings.isNullOrEmpty(qName.getAgent()) && Strings.isNullOrEmpty(qName.getName()));
  }

  public static boolean isVariableReference(final String s) {
    return pattern.matcher(s).matches();
  }

  public static List<String> normalizeISPL(final List<String> isplString) {
    final ArrayList<Object> newList = new ArrayList<>();
    for (final String s: isplString) {
      newList.add(s.replaceAll("(--+)(.*)", "\n$1#~~#$2####"));
    }
    final String join = StringUtils.join(newList, " ");

    final String[] split = (" " + join)
        .replaceAll("[ \n\t]+", " ")
        .replaceAll("end (Protocol|Evaluation|Formulae|Evolution|Vars|Agent|InitStates|Obsvars)", "\nend $1\n")
        .replaceAll("(Protocol|Evolution|Vars|Other|Obsvars)\\s*:", "\n$1:\n")
//        .replaceAll("} ;", "};")
        .replaceAll("(Obsvars|Lobsvars|Actions)\\s*=\\s*\\{\\s*([^}]*)\\s*\\}\\s*;", "\n$1 = { $2 };")
        .replaceAll("[\t ]*([();.,=:{}])[\t ]*", "$1")
        .replaceAll("(and|or)\\s*\\(", "\n$1 (")
        .replaceAll("(and|or|if|->)([)(])", "$1 $2")
        .replaceAll("([)(])(and|or|if|->)", "$1 $2")
        .replaceAll(";", ";\n")
        .replaceAll("\n[ \t]*[\n]?", "\n")
        .replaceAll("([,:])([^ \t;])", "$1 $2")
        .replaceAll("([^ \t])([}])", "$1 $2")
        .replaceAll("([{])([^ \t])", "$1 $2")
        .replaceAll("([=])", " $1 ")
        .replaceAll("[ \t]{2,}", " ")
        .replaceAll("(--+.*)(#~~#)(.*)(####)", "\n$1$3\n")
        .split("\n");

    return Arrays.asList(split);
  }

  /**
   * Write the object to a Base64 string.
   */
  public static String toBase64String(Serializable o) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ObjectOutputStream    oos  = new ObjectOutputStream(baos);
    oos.writeObject(o);
    oos.close();
    return Base64.getEncoder().encodeToString(baos.toByteArray());
  }

  public static String unwrapFromBase64AsString(@NonNull final String s) {
    return new String(Base64.getDecoder().decode(s.getBytes()));
  }

  public static String wrapAsBase64(@NonNull final String s) {
    return Base64.getEncoder().encodeToString(s.getBytes());
  }
}
