package de.hu_berlin.informatik.svt.jbepl2ispl.ispl;

import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentVariableDefinition;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.*;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.emf.common.util.EList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;

/**
 * Created by Christoph Graupner on 2018-06-22.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
abstract class IsplSubWriterBase {
  protected final ISPLFactory    factory;
  protected final IsplWriter     isplWriter;
  protected final Logger         logger = LoggerFactory.getLogger(getClass());
  protected       AgentAutomaton currentAgent;

  IsplSubWriterBase(
      final ISPLFactory factory,
      final IsplWriter isplWriter
  ) {
    this.factory = factory;
    this.isplWriter = isplWriter;
  }

  protected void createVarDefs(
      final AgentAutomaton agent, final EList<VariableDefinition> varDefs, final LinkedList<String> variables
  ) {
    for (final String varName: variables) {
      AgentVariableDefinition agentVar = agent.getVariableDefinition(varName);
      logger.debug(LogMarker.ISPL, "Create VarDef for {}", varName);

      switch (agentVar.getValueType()) {

        case ENUM:
          final VarDefEnumList varDefEnumList = factory.createVarDefEnumList();
          varDefEnumList.setName(isplWriter.translateToIsplName(agentVar.getName()));
          EnumList enumlist = varDefEnumList.getEnumlist();
          if (enumlist == null) {
            enumlist = factory.createEnumList();
            varDefEnumList.setEnumlist(enumlist);
          }
          varDefs.add(varDefEnumList);
          for (String s: agentVar.getAllowedValues()) {
            enumlist.getIds().add(isplWriter.translateToIsplValue(s));
          }
          break;
        case BOOLEAN:
          final VarDefBoolean varDefBoolean = factory.createVarDefBoolean();
          varDefBoolean.setName(isplWriter.translateToIsplName(agentVar.getName()));
          varDefs.add(varDefBoolean);
          break;
        case INT:
          final VarDefInteger varDefInteger = factory.createVarDefInteger();
          varDefInteger.setName(isplWriter.translateToIsplName(agentVar.getName()));
          if (agentVar.hasAllowedValues() && agentVar.getAllowedValues().size() == 2) {
            varDefInteger.setBeginRange(Integer.valueOf(agentVar.getAllowedValues().get(0)));
            varDefInteger.setEndRange(Integer.valueOf(agentVar.getAllowedValues().get(1)));
          } else {
            logger.warn(
                LogMarker.MODEL_PROBLEM,
                "allowedValue should be exactly two int entries for getValueType()==INT, was [{}]",
                StringUtils.join(agentVar.getAllowedValues(), ", ")
            );
          }
          varDefs.add(varDefInteger);
          break;
      }
    }
  }
}
