package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

/**
 * Created by Christoph Graupner on 2018-06-18.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */

public interface FlowStateInvolvesServiceLink {
  FlowService getService();

  void setService(FlowService service);

  FlowState getState();

  void setState(FlowState state);
}
