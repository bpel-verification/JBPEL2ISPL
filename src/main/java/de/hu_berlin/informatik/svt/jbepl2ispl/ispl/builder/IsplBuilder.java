package de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder;

import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.InterpreterSystem;

/**
 * Created by Christoph Graupner on 2018-03-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface IsplBuilder extends IsplBuilderBase<IsplBuilder> {
  IsplAgentsBuilder agents();

  InterpreterSystem build();

  IsplEnvironmentBuilder environment();
}
