package de.hu_berlin.informatik.svt.jbepl2ispl.model;

/**
 * Created by Christoph Graupner on 2018-06-22.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public enum BeginEndType {
  BEGIN, END, BOTH, NONE
}
