package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by Christoph Graupner on 2018-03-06.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
@Service
public class MainFlowModelPostProcessor {

  private final Logger                   logger = LoggerFactory.getLogger(getClass());
  @Getter
  private final FlowModel                model;
  private final FlowModelPostProcessor[] postProcessors;

  public MainFlowModelPostProcessor(
      final FlowModel model,
      final ConnectNodesPostProcessor connectNodesPostProcessor,
      final CommitmentStatesPostProcessor commitmentStatesPostProcessor,
      final DetermineVariableTypePostProcessor determineVariableTypePostProcessor,
      final MarkInitialStatesPostProcessor markInitialStatesPostProcessor,
      final SourceTargetServiceForStatesPostProcessor sourceTargetServiceForStatesPostProcessor,
      final StateDuplicationPostProcessor stateDuplicationPostProcessor,
      final FlowAggregatesPostProcessor flowAggregatesPostProcessor,
      final MarkMessageSendingStatesPostProcessor markMessageSendingStatesPostProcessor,
      final SymbolicValuePostProcessor symbolicValuePostProcessor,
      final CommunicationPostProcessor communicationPostProcessor,
      final CommChannelPostProcessor commChannelPostProcessor
  ) {
    this.model = model;
    postProcessors = new FlowModelPostProcessor[]{
        connectNodesPostProcessor,
        sourceTargetServiceForStatesPostProcessor,
        commChannelPostProcessor,
        commitmentStatesPostProcessor,
        determineVariableTypePostProcessor,
        markInitialStatesPostProcessor,
        markMessageSendingStatesPostProcessor,
        stateDuplicationPostProcessor,
        flowAggregatesPostProcessor,
        symbolicValuePostProcessor,
        communicationPostProcessor
    };
  }

  public void finish() {
    logger.info(LogMarker.POSTPROCESSING, "Finishing model... BEGIN");
    for (final FlowModelPostProcessor postProcessor: postProcessors) {
      postProcessor.finish();
    }
    logger.info(LogMarker.POSTPROCESSING, "Finishing model... DONE");
  }
}
