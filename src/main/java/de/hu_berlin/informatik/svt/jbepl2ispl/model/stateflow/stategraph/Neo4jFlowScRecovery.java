package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowScRecovery;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NodeEntity(label = "ScRecovery")
@NoArgsConstructor
public class Neo4jFlowScRecovery extends FlowBase implements FlowScRecovery {
  @Getter
  @Id
  @GeneratedValue
  private Long id;
  private int   originalElementId;
  @Relationship(type = Naming.RELATION_FLOW_SC_TARGET)
  private State targetState;
}
