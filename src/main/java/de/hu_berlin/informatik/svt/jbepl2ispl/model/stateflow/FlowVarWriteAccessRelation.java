package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface FlowVarWriteAccessRelation extends FlowVarAccessRelation {
}
