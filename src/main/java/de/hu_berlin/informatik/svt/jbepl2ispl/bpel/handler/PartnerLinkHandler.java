package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.apache.ode.bpel.compiler.bom.PartnerLink;

import java.util.Arrays;

/**
 * Created by Christoph Graupner on 2018-02-26.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public class PartnerLinkHandler extends HandlerBase {

  public PartnerLinkHandler() {
    super(Arrays.asList(PartnerLink.class));
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    if (currentBpelObject instanceof PartnerLink) {
      final PartnerLink partnerLink = (PartnerLink) currentBpelObject;
      final FlowModel   graphModel  = compilerState.getFlowModel();
      final FlowService graphAgent = graphModel.factory().createService(
          partnerLink.getName(),
          compilerState.getCurrentBpelPath()
      );
      graphModel.save(graphAgent);
    }

    return (T) this;
  }
}
