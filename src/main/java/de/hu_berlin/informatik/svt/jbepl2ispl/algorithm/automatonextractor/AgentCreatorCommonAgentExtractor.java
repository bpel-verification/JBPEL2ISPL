package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor;

import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentAutomatonModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import org.springframework.stereotype.Service;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
@Service
public class AgentCreatorCommonAgentExtractor extends CommonExtractorBase {
  protected AgentCreatorCommonAgentExtractor(
      final FlowModel flowModel,
      final AutomatonModel model,
      final StateInformationResolver stateResolver,
      final VariableTargetServiceResolver resolver
  ) {
    super(flowModel, model, stateResolver, resolver);
  }

  @Override
  public void extract() {
    for (FlowService service: flowModel.getServices()) {

      AgentAutomatonModifiable agentAutomaton = getModel().factory().createAgentAutomaton(service, true);
      model.save(agentAutomaton);
      if (logger.isDebugEnabled()) {
        logger.debug(LogMarker.DEBUG_VALUES, "extract: create agent: {}", agentAutomaton);
      }
    }
  }
}
