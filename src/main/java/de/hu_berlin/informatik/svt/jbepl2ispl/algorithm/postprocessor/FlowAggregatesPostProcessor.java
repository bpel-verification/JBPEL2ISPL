package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.BpelFlowAggregateState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.BpelPath;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Christoph Graupner on 2018-06-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
public class FlowAggregatesPostProcessor extends FlowModelPostProcessor {

  protected FlowAggregatesPostProcessor(final FlowModel model) {
    super(model);
  }

  @Override
  public void finish() {
    deriveFlowAggregates();
  }

  private void createAggregate(final List<FlowState> toAggregate, FlowState flowBeginState) {
    if (toAggregate.isEmpty()) {
      return;
    }

    logger.trace(LogMarker.POSTPROCESSING, "Aggregating: \n|+++| {}", StringUtils.join(toAggregate, "\n|+++| "));
    BpelFlowAggregateState aggregateState = flowModel.factory().createBpelFlowAggregateState(flowBeginState);
    toAggregate.forEach(aggregateState::addState);
    flowModel.save(aggregateState);
  }

  private void deriveFlowAggregates() {
    //group BEGIN and END state
    logger.trace(LogMarker.POSTPROCESSING, "Deriving aggregates of <flow> branches...");

    Map<BpelPath, List<FlowState>> grouper = getFlows();

    grouper.forEach(this::handleFlow);
    logger.trace(LogMarker.POSTPROCESSING, "DONE Deriving aggregates of <flow> branches...");
  }

  private void doAggregation(
      final Map<BpelPath, List<? extends FlowState>> branchStatesMap,
      final Map<BpelPath, Integer> branchStatesMapPointer,
      final FlowState activityFlowBeginState
  ) {
    List<FlowState> toAggregate;
    do {
      toAggregate = new ArrayList<>(branchStatesMap.size());
      for (Map.Entry<BpelPath, List<? extends FlowState>> entry: branchStatesMap.entrySet()) {
        List<? extends FlowState> values  = entry.getValue();
        Integer                   pointer = branchStatesMapPointer.get(entry.getKey());
        //find next real activity
        final EnumSet<Activity> unwantedActivities =
            EnumSet.of(Activity.SEQUENCE, Activity.FLOW_BRANCH, Activity.EMPTY);
        while (pointer < values.size()
               && unwantedActivities
                   .contains(values.get(pointer).getCausingActivity())) {
          pointer++;
        }
        branchStatesMapPointer.put(entry.getKey(), pointer + 1); //plus 1 to start at next value in next round

        if (values.size() > pointer) {
          toAggregate.add(values.get(pointer));
        }
      } //for
      createAggregate(toAggregate, activityFlowBeginState);
    } while (false == toAggregate.isEmpty());
  }

  private void gatherAllStatesBetweenBeginEnd(
      final Map<BpelPath, List<FlowState>> beginEndGrouper,
      final Map<BpelPath, List<? extends FlowState>> branchStatesMap,
      final Map<BpelPath, Integer> branchStatesMapPointer
  ) {
    for (Map.Entry<BpelPath, List<FlowState>> entry: beginEndGrouper.entrySet()) {
      BpelPath        bpelPathBranch       = entry.getKey();
      List<FlowState> beginEndStatesBranch = entry.getValue();
      if (2 != beginEndStatesBranch.size()) {
        throw new IllegalStateException(
            "There are not only or just one of BEGIN and END flow marker states for " + bpelPathBranch);
      }
      logger.debug(LogMarker.POSTPROCESSING, "Finding states between {} -> {}", beginEndStatesBranch.get(0),
                   beginEndStatesBranch.get(1)
      );
      List<? extends FlowState> statesBetween = flowModel.findStatesBetween(
          beginEndStatesBranch.get(0),
          beginEndStatesBranch.get(1)
      );
      branchStatesMap.put(bpelPathBranch, statesBetween);
      branchStatesMapPointer.put(bpelPathBranch, 0);

      logger.debug(
          LogMarker.POSTPROCESSING,
          "Branches between: {} -> {}:\n\n ++++ {}",
          beginEndStatesBranch.get(0),
          beginEndStatesBranch.get(1),
          StringUtils.join(statesBetween, "\n ++++ ")
      );
    }
  }

  private Map<BpelPath, List<FlowState>> getFlows() {
    Map<BpelPath, List<FlowState>> grouper = new HashMap<>();
    for (final FlowState flowState: flowModel.findStatesWithActivity(Activity.FLOW)) {
      final BpelPath key = flowState.getBpelPath();
      if (false == grouper.containsKey(key)) {
        grouper.put(key, new LinkedList<>());
      }
      grouper.get(key).add(flowState);
    }
    return grouper;
  }

  private void groupFlowBranches(
      final List<? extends FlowState> branchToGroup, final Map<BpelPath, List<FlowState>> beginEndGrouper
  ) {
    branchToGroup.forEach(
        flowState -> {
          final BpelPath key = flowState.getBpelPath();
          if (false == beginEndGrouper.containsKey(key)) {
            beginEndGrouper.put(key, new LinkedList<>());
          }
          beginEndGrouper.get(key).add(flowState);
        }
    );
  }

  /**
   * @param bpelPath
   * @param flowStates of FlowState.Activity.FLOW
   */
  private void handleFlow(BpelPath bpelPath, List<FlowState> flowStates) {
    if (flowStates.size() != 2) {
      throw new IllegalStateException("There are not BEGIN<->END matching FLOW states with BpelPath " + bpelPath);
    }
    flowStates.sort(Comparator.comparingInt(FlowState::getElementId));
    final FlowState activityFlowBeginState = flowStates.get(0);
    logger.trace(
        LogMarker.POSTPROCESSING, "Aggregate branches of {} ({} -> {})", bpelPath, activityFlowBeginState,
        flowStates.get(1)
    );
    List<FlowState> branchesBegin = flowModel.findAllConnectedFollowingStates(activityFlowBeginState);
    List<FlowState> branchesEnd   = flowModel.findAllConnectedPrecedingStates(flowStates.get(1));
    logger.trace(
        LogMarker.POSTPROCESSING, "Found branches of {}\n begin: {},\n end {}", bpelPath,
        StringUtils.join(branchesBegin, "\n-- "),
        StringUtils.join(branchesEnd, "\n-- ")
    );

    if (branchesBegin.size() != branchesEnd.size()) {
      logger.error(
          LogMarker.MODEL_PROBLEM, "flow-begin markers ({}) do not match count of flow-end marker ({})",
          branchesBegin.size(), branchesEnd.size()
      );
    }

    Map<BpelPath, List<FlowState>>           beginEndGrouper        = new HashMap<>();
    Map<BpelPath, List<? extends FlowState>> branchStatesMap        = new HashMap<>();
    Map<BpelPath, Integer>                   branchStatesMapPointer = new HashMap<>();
    groupFlowBranches(branchesBegin, beginEndGrouper);
    groupFlowBranches(branchesEnd, beginEndGrouper);

    logger.trace(
        LogMarker.POSTPROCESSING, "Grouped branches of {}\n {}", bpelPath,
        StringUtils.join(beginEndGrouper, "\n-- ")
    );

    gatherAllStatesBetweenBeginEnd(beginEndGrouper, branchStatesMap, branchStatesMapPointer);

    //FIXME take the <links> into account
    logger.warn(LogMarker.TODO_EXCEPTION, "FIXME take the <links> into account");
    doAggregation(branchStatesMap, branchStatesMapPointer, activityFlowBeginState);

    logger.trace(LogMarker.POSTPROCESSING, "DONE Aggregate branches of {} ({})", bpelPath, activityFlowBeginState);
  }
}
