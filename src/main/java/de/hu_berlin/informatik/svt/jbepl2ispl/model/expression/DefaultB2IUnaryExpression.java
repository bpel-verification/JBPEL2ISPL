package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import lombok.Getter;

import java.util.Objects;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class DefaultB2IUnaryExpression implements B2IUnaryExpression {

  private static final long serialVersionUID = 1L;

  @Getter final Type type;

  @Getter
  B2IExpression term;

  protected DefaultB2IUnaryExpression(
      final Type type, final B2IExpression term
  ) {
    this.type = type;
    this.term = term;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof DefaultB2IUnaryExpression)) {
      return false;
    }
    final B2IUnaryExpression that = (B2IUnaryExpression) o;
    return getType() == that.getType() &&
           Objects.equals(getTerm(), that.getTerm());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getType(), getTerm());
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("B2IUnaryExpression{");
    sb.append("type=").append(type);
    sb.append(", term=").append(term);
    sb.append('}');
    return sb.toString();
  }
}
