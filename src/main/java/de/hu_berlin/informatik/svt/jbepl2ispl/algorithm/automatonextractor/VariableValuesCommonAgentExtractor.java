package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentIdentifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentVariableDefinition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentAutomatonModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
@Service
public class VariableValuesCommonAgentExtractor extends CommonExtractorBase {
  protected VariableValuesCommonAgentExtractor(
      final FlowModel flowModel,
      final AutomatonModel model,
      final StateInformationResolver stateResolver,
      final VariableTargetServiceResolver resolver
  ) {
    super(flowModel, model, stateResolver, resolver);
  }

  @Override
  public void extract() {
    model.getAllAgentAutomatons().forEach(
        agentAutomaton -> {
          final AgentAutomatonModifiable agentAutomatonModifiable = (AgentAutomatonModifiable) agentAutomaton;
          logger.trace(
              LogMarker.EXTRACTION, "Extracting variables values for Agent '{}'", agentAutomatonModifiable.getName());
          extractVariableValuesWithStateVar(
              agentAutomatonModifiable
          );
          if (agentAutomaton.isEnvironment()) {
            envExtractTraceLabelValues(agentAutomatonModifiable);
            envExtractVariablesValues(agentAutomatonModifiable);
            envExtractCommitmentIdValues(agentAutomatonModifiable);
            envExtractFulfillmentIdValues(agentAutomatonModifiable);
          } else {
            agentExtractVariablesValues(agentAutomatonModifiable);
          }
          model.save(agentAutomatonModifiable);
          logger.trace(
              LogMarker.EXTRACTION, "DONE Extracting variables values for Agent '{}'",
              agentAutomatonModifiable.getName()
          );
        }
    );
    model.getAllAgentAutomatons().forEach(
        agentAutomaton -> {
          final AgentAutomatonModifiable agentAutomatonModifiable = (AgentAutomatonModifiable) agentAutomaton;
          logger.trace(
              LogMarker.EXTRACTION, "Extracting initial values for Agent '{}'", agentAutomatonModifiable.getName());
          extractStep7_InitialVariablesValues(agentAutomatonModifiable);
          model.save(agentAutomatonModifiable);
          logger.trace(
              LogMarker.EXTRACTION, "DONE Extracting initial values for Agent '{}'",
              agentAutomatonModifiable.getName()
          );
        }
    );
  }

  private void agentExtractVariablesValues(
      final AgentAutomatonModifiable agent
  ) {
    agent.getVariables().forEach(
        varName -> {
          final AgentVariableDefinition varDef = agent.getVariableDefinition(varName);
          switch (varDef.getPurposeType()) {
            case SHARED_BUFFER:
              Set<String> values = model.findAllVariableValuesFor(varDef.getQName());
              values.remove(Consts.ISPL_VAR_ENUM_NONE);
              final ArrayList<String> values1 = new ArrayList<>();
              values1.add(Consts.ISPL_VAR_ENUM_NONE);
              values1.addAll(values);
              varDef.setAllowedValues(values1);
              agent.save(varDef);
              break;
            default:
              throw new NotImplementedException(varDef.getPurposeType().toString());
          }
        }
    );
  }

  private LinkedList<String> collectAllowedValues(final String varName) {
    Collection<FlowVarWriteAccessRelation> writeAccesses = flowModel.findAllWriteAccessesForVariable(varName);
    if (logger.isDebugEnabled()) {
      logger.debug(
          LogMarker.EXTRACTION, "generateAllowedValues: {}: VaWrites: {}", varName,
          StringUtils.join(writeAccesses, "\n--")
      );
    }

    Set<String> values = new HashSet<>();
    writeAccesses
        .stream()
        .sorted(Comparator.comparingInt(Identifiable::getElementId))
        .forEachOrdered(
            varWriteAccessRelation -> values.add(varWriteAccessRelation.getSymbolicValue())
        );

    return new LinkedList<>(values);
  }

  private void envExtractCommitmentIdValues(final AgentAutomatonModifiable agent) {
    flowModel.getAllCommitmentNodes().forEach(
        commitment -> {
          AgentState state = agent.findCorrespondingStateForFlowState(commitment.getTargetState().getFlowId());
          if (state != null) {
            state.setVariableValue(
                new QName(FlowService.ENV, Consts.VAR_NAME_SC_COMMITMENT_ID),
                String.valueOf(commitment.getCommitmentId())
            );
            agent.save(state);
          }
        }
    );
  }

  private void envExtractFulfillmentIdValues(final AgentAutomatonModifiable agent) {
    flowModel.getAllFulfillmentNodes().forEach(
        fulfillment -> {
          AgentState state = agent.findCorrespondingStateForFlowState(fulfillment.getTargetState().getFlowId());
          if (state != null) {
            state.setVariableValue(
                new QName(FlowService.ENV, Consts.VAR_NAME_SC_FULFILLMENT_ID),
                String.valueOf(fulfillment.getFulfillmentId())
            );
            agent.save(state);
          }
        }
    );
  }

  private void envExtractTraceLabelValues(final AgentAutomatonModifiable agent) {
    flowModel.getAllTraceLabelNodes().forEach(
        traceNode -> {
          AgentState state = agent.findCorrespondingStateForFlowState(traceNode.getTargetState().getFlowId());
          if (state != null) {
            state.setVariableValue(
                Naming.getTraceLabelName(traceNode.getTracedService().getName()),
                Boolean.valueOf(traceNode.getBehaviourType().getIsplCode()).toString()
            );
            agent.save(state);
          }
        }
    );
  }

  private void envExtractVariablesValues(final AgentAutomatonModifiable agent) {
    List<String> commChannelVars = new ArrayList<>(); //process after all other to get all values

    //generate values for BPEL declared vars only
    agent.getVariables().forEach(
        varName -> {
          final AgentVariableDefinition varDef = agent.getVariableDefinition(varName);
          if (varDef.isBpelDeclared()) {
            logger.debug(LogMarker.EXTRACTION, "Defining variable values for: {}: {}", varName, varDef);

            final FlowVariable.PurposeType purposeType = varDef.getPurposeType();
            switch (purposeType) {
              case COMM_CHANNEL:
                commChannelVars.add(varName);
                return;
              case SHARED_BUFFER:
                //should not be used
                throw new IllegalStateException(
                    FlowVariable.PurposeType.SHARED_BUFFER + " should not be used in " + FlowService.ENV
                );
              case OPERATIONAL:
                logger.error(
                    LogMarker.TODO_EXCEPTION, "Define OPERATIONAL values [{}]",
                    varDef
                ); //FIXME Define OPERATIONAL values
                if (varDef.getValueType() == AgentVariableDefinition.ValueType.ENUM) {
                  varDef.setAllowedValues(generateAllowedValuesOPERATIONAL());
                } else {

                }
                break;
              case CONTAINER:
                final LinkedList<String> values = collectAllowedValues(varName);
                switch (varDef.getValueType()) {
                  case ENUM:
                    values.addFirst(Consts.ISPL_VAR_ENUM_NONE);
                    break;
                }
                values.sort(String::compareTo);
                varDef.setAllowedValues(values);
                logger.error(
                    LogMarker.TODO_EXCEPTION, "Define Container values [{}]", varDef); //FIXME Define Container values
                break;
              case MESSAGE:
                final LinkedList<String> values1 = collectAllowedValues(varName);
                values1.addFirst(Consts.ISPL_VAR_ENUM_NONE);
                varDef.setAllowedValues(values1);
                break;
            }
            if (varDef.getValueType() == null) {
              logger.error(LogMarker.MODEL_PROBLEM, "{} is an {} vartype, so something should be done to handle {}",
                           varDef, purposeType, varDef.getVariableType()
              );
            }

            agent.save(varDef);
          }
        });

    //for COMM_CHANNELS
    commChannelVars.forEach(
        varName -> {
          final AgentVariableDefinition varDef = agent.getVariableDefinition(varName);
          logger.debug(LogMarker.EXTRACTION, "Defining variable values for commChannel: {}: {}", varName, varDef);

          final SortedSet<String> differentValues = new TreeSet<>();
          flowModel.getVariables().forEach(
              variable -> {
                if (FlowVariable.PurposeType.COMM_CHANNEL_RELEVANT.contains(variable.getPurposeType())) {
                  collectAllowedValues(variable.getName()).forEach(
                      s -> {
                        //FIXME it is type conversion, that's not supported by ISPL
                        if (s.matches("(true|false)")) {
                          differentValues.add("bool_" + s);
                        } else if (s.matches("^[A-Za-z].*")) {
                          differentValues.add(s);
                        } else {
                          differentValues.add("int_" + s);
                        }
                      }
                  );
                }
              }
          );
          varDef.setAllowedValues(new ArrayList<>(differentValues));
          logger.error(
              LogMarker.TODO_EXCEPTION, "Define CommChannel values [{}]",
              varDef
          ); //FIXME Define CommChannel values
          agent.save(varDef);
        }
    );
  }

  private void extractStep7_InitialVariablesValues(
      final AgentAutomatonModifiable agent
  ) {
    //iterate through all initial states and set vars
    final String traceLabelPrefix = Consts.ISPL_VAR_PREFIX_TRACE_LABEL + Consts.ISPL_VAR_NAME_SEPARATOR;
    agent.getVariables().forEach(
        varName -> {
          AgentVariableDefinition varDef = agent.getVariableDefinition(varName);
          switch (varDef.getValueType()) {
            case ENUM:
            case INT:
              FlowVarWriteAccessRelation initializingWriteForVariable =
                  flowModel.findLastInitialWriteForVariable(varName);
              if (initializingWriteForVariable == null) {
                varDef.setInitialValue(varDef.getAllowedValues().get(0));
              } else {
                varDef.setInitialValue(initializingWriteForVariable.getSymbolicValue());
              }
              agent.save(varDef);
              break;
            case BOOLEAN:
              if (varName.startsWith(traceLabelPrefix)) {
                varDef.setInitialValue(Boolean.toString(FlowScTraceLabel.Type.DESIRED.getIsplCode()));
              } else {
                initializingWriteForVariable = flowModel.findLastInitialWriteForVariable(varName);
                if (initializingWriteForVariable == null) {
                  varDef.setInitialValue(Boolean.toString(false));
                } else {
                  varDef.setInitialValue(
                      Boolean.toString(Boolean.valueOf(initializingWriteForVariable.getSymbolicValue()))
                  );
                }
              }
              agent.save(varDef);
              break;
          }
        }
    );
    flowModel.getStates()
             .stream()
             .filter(FlowState::isInitialState)
             .sorted(Comparator.comparingInt(Identifiable::getElementId))
             .forEachOrdered(
                 flowState -> {
                   flowModel.findAllVarWriteAccessRelationsForState(flowState)
                            .stream()
                            .sorted(Comparator.comparingInt(Identifiable::getElementId))
                            .forEachOrdered(
                                writeAccessRelation -> {
                                  if (agent.ownsVariable(writeAccessRelation.getVariableName().getName())) {
                                    if (logger.isDebugEnabled()) {
                                      logger.debug(LogMarker.DEBUG_VALUES, "INITIAL: {}", writeAccessRelation);
                                    }
                                    if (writeAccessRelation.getSymbolicValue() != null) {
                                      final AgentVariableDefinition variableDefinition =
                                          agent.getVariableDefinition(writeAccessRelation.getVariableName().getName());
                                      variableDefinition.setInitialValue(writeAccessRelation.getSymbolicValue());
                                      agent.save(variableDefinition);
                                    }
                                  }
                                }
                            );
                 }
             );
  }

  private void extractVariableValuesWithStateVar(
      final AgentAutomatonModifiable agent
  ) {
    agent.getVariables().forEach(
        varName -> {
          AgentVariableDefinition varDef = agent.getVariableDefinition(varName);
          if (Consts.ISPL_VAR_NAME_STATEVAR_ENV.equals(varName)) {
            final List<String> values = new ArrayList<>();

            final Collection<AgentState> states = agent.getStates();

            states.stream().sorted(Comparator.comparingInt(AgentIdentifiable::getElementId)).forEachOrdered(
                state -> values.add(state.getName())
            );

            varDef.setAllowedValues(values);
            if (logger.isDebugEnabled()) {
              logger.debug(
                  LogMarker.DEBUG_VALUES, "STATEVAR: {}\n with values: ", StringUtils.join(states, "\n++++ "),
                  StringUtils.join(values, "\n#### ")
              );
            }
            varDef = agent.save(varDef);
          } else if (varName.startsWith(Consts.ISPL_VAR_PREFIX_BUFFER + Consts.ISPL_VAR_NAME_SEPARATOR)) {
            List<String> values = new ArrayList<>();
            values.add(Consts.ISPL_VAR_ENUM_NONE);
//      flowModel.getAllVariableValuesByVariable()

            varDef.setAllowedValues(values);
            varDef = agent.save(varDef);
            if (logger.isDebugEnabled()) {
              logger.debug(LogMarker.DEBUG_VALUES, "BufferVar: {}", varDef);
            }
//      //FIXME maybe okay, but maybe not
//      logger.warn(LogMarker.TODO_EXCEPTION, "FIXME maybe okay, but maybe not the initial value [{}] of var [{}]",
//                  varDef.getInitialValue(), varDef.getName());

          } else if (Consts.VAR_NAME_SC_COMMITMENT_ID.equals(varName)
                     || Consts.VAR_NAME_SC_FULFILLMENT_ID.equals(varName))
          {
            final Collection<FlowVarWriteAccessRelation> varValues =
                flowModel.findAllWriteAccessesForVariable(varDef.getOriginVariable().getName());
            int max = 0;
            for (final FlowVarWriteAccessRelation flowVariableValue: varValues.stream()
                                                                              .sorted(Comparator.comparingInt(
                                                                                  Identifiable::getElementId))
                                                                              .collect(Collectors.toList())) {
              if (flowVariableValue.getSymbolicValue() == null) {
                continue;
              }
              max = Math.max(max, Integer.valueOf(flowVariableValue.getSymbolicValue()));
            }
            varDef.setAllowedValues(Arrays.asList("0", String.valueOf(max)));
            varDef = agent.save(varDef);
          } else {
            final Collection<FlowVarWriteAccessRelation> varValues;
            if (logger.isDebugEnabled()) {
              logger.debug(LogMarker.DEBUG_VALUES, "extractVariableValuesWithStateVar: varDef: {}", varDef);
            }
            switch (varDef.getValueType()) {
              case ENUM:
                final List<String> values = new ArrayList<>();
                values.add(Consts.ISPL_VAR_ENUM_NONE);
                varValues = flowModel.findAllWriteAccessesForVariable(varDef.getName());

                for (final FlowVarWriteAccessRelation flowVariableValue: varValues.stream()
                                                                                  .sorted(Comparator.comparingInt(
                                                                                      Identifiable::getElementId))
                                                                                  .collect(Collectors.toList())
                ) {
                  if (logger.isDebugEnabled()) {
                    logger.debug(
                        LogMarker.DEBUG_VALUES, "extractVariableValuesWithStateVar: flowVarValue{}", flowVariableValue);
                  }
                  if (flowVariableValue.getSymbolicValue() == null) {
                    continue;
                  }
                  values.add(flowVariableValue.getSymbolicValue());
                }
                varDef.setAllowedValues(values);
                break;
              case BOOLEAN:
                break;
              case INT:
                varValues = flowModel.findAllWriteAccessesForVariable(varDef.getName());
                int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
                for (final FlowVarWriteAccessRelation flowVariableValue: varValues.stream()
                                                                                  .sorted(Comparator.comparingInt(
                                                                                      Identifiable::getElementId))
                                                                                  .collect(Collectors.toList())) {
                  if (logger.isDebugEnabled()) {
                    logger.debug(
                        LogMarker.DEBUG_VALUES, "extractVariableValuesWithStateVar: flowVarValue {}",
                        flowVariableValue
                    );
                  }
                  if (flowVariableValue.getSymbolicValue() == null) {
                    continue;
                  }
                  min = Math.min(min, Integer.valueOf(flowVariableValue.getSymbolicValue()));
                  max = Math.max(max, Integer.valueOf(flowVariableValue.getSymbolicValue()));
                }
                //FIXME: get rid of "Math.min(min,-100)" and Math.max(max,100) by including conditions as value source
                varDef.setAllowedValues(Arrays.asList(String.valueOf(Math.min(min,-100)), String.valueOf(Math.max(max,100))));
                break;
            }
            varDef = agent.save(varDef);
          }

//          agent.getVariableDefinition(varName);
          logger.debug(//TODO: BUG: it's necessary to have the new values really saved. Don't know why.
                       LogMarker.DEBUG_VALUES, "Control Var: {}\n{}", varDef,
                       agent.getVariableDefinition(varDef.getName())
          );
        }
    );
  }

  private List<String> generateAllowedValuesOPERATIONAL() {
    logger.error(
        LogMarker.TODO_EXCEPTION,
        "EnvironmentExtractor.generateAllowedValuesOPERATIONAL: IMPLEMENT operational"
    ); //FIXME IMPLEMENT operational

    return Arrays.asList("OPERATIONAL");
  }
}
