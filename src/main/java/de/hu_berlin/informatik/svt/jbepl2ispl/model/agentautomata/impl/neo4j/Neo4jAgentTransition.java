package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.QNameNeo4jConverter;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentTransitionModifiable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.neo4j.ogm.annotation.*;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import java.util.Objects;

/**
 * Created by Christoph Graupner on 2018-05-29.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@RelationshipEntity(type = Naming.RELATION_ENTITY_AGENT_TRANSITION)
@NoArgsConstructor
public class Neo4jAgentTransition extends BaseAgentElement implements AgentTransitionModifiable {
  @Setter
  @Getter
  @Property
  private String          guard;
  @Setter
  private boolean         invertGuard = false;
  @Getter
  @Setter
  @Property
  @Convert(QNameNeo4jConverter.class)
  private QName           isplActionName;
  @Getter
  @Id
  @GeneratedValue
  private Long            relationshipId;
  @Getter
  @StartNode
  private Neo4jAgentState sourceState;
  @Getter
  @EndNode
  private Neo4jAgentState targetState;

  Neo4jAgentTransition(
      @NonNull final Neo4jAgentState sourceState,
      @NonNull final Neo4jAgentState targetState,
      final String guard,
      final String name,
      final int elementId
  ) {
    super(elementId, 0);
    this.sourceState = sourceState;
    this.targetState = targetState;
    this.guard = guard;
    setName(name);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Neo4jAgentTransition)) {
      return false;
    }
    final Neo4jAgentTransition that = (Neo4jAgentTransition) o;
    return Objects.equals(getRelationshipId(), that.getRelationshipId());
  }

  @Override
  public QName getAction() {
    return getIsplActionName();
  }

  @Override
  public void setAction(final QName name) {
    isplActionName = name;
  }

  @Override
  public Long getId() {
    return relationshipId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getRelationshipId());
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("AgentTransition{");
    sb.append("sourceState=").append(sourceState);
    sb.append(", targetState=").append(targetState);
    sb.append(", guard='").append(guard).append('\'');
    sb.append(", invertGuard=").append(invertGuard);
    sb.append(", isplActionName=").append(isplActionName);
    sb.append('}');
    return sb.toString();
  }

  @Override
  public boolean invertGuard() {
    return invertGuard;
  }
}
