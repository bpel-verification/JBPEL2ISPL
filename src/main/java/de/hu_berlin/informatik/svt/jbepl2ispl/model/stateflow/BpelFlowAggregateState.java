package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import java.util.Collection;

/**
 * Created by Christoph Graupner on 2018-06-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface BpelFlowAggregateState {
  void addState(FlowState state);

  <T extends FlowState> Collection<T> getAggregatedStates();
}
