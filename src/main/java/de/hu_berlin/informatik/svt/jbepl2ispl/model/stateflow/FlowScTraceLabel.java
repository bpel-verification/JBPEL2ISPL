package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface FlowScTraceLabel extends Identifiable {
  enum Type {
    DESIRED(1, true), UNDESIRED(0, false);

    private final int     id;
    private final boolean isplCode;

    Type(int id, final boolean isplCode) {
      this.id = id;
      this.isplCode = isplCode;
    }

    public static Type valueOf(int id) {
      switch (id) {
        case 0:
          return UNDESIRED;
        case 1:
          return DESIRED;
      }
      return null;
    }

    public int getId() {
      return id;
    }

    public boolean getIsplCode() {
      return isplCode;
    }
  }

  Type getBehaviourType();

  FlowState getTargetState();

  FlowService getTracedService();
}
