package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.repository;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.Transition;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
public interface TransitionRepository extends Neo4jRepository<Transition, Long> {
  @Query("MATCH (s:State)-[r:" + Naming.RELATION_FOLLOWED_BY + "]->(s2:State) WHERE id(s) = {stateId} RETURN r,s,s2")
  List<Transition> findBySourceState(@Param("stateId") Long state);

  @Query("MATCH (s:State)-[r:" + Naming.RELATION_FOLLOWED_BY + "]->(s2:State) WHERE id(s2) = {stateId} RETURN r,s,s2")
  List<Transition> findByTargetState(@Param("stateId") Long state);
}
