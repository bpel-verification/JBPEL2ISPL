package de.hu_berlin.informatik.svt.jbepl2ispl.utils;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import org.neo4j.ogm.typeconversion.AttributeConverter;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class QNameNeo4jConverter implements AttributeConverter<QName, String> {
  @Override
  public QName toEntityAttribute(final String value) {
    return new QName(value);
  }

  @Override
  public String toGraphProperty(final QName value) {
    return value == null ? null : value.getFullName();
  }
}
