package de.hu_berlin.informatik.svt.jbepl2ispl.bpel;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import lombok.Getter;
import lombok.NonNull;
import org.apache.ode.bpel.compiler.bom.Process;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Christoph Graupner on 2018-02-27.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public class CompilerState {
  @Getter
  private final Process             bpelProcess;
  @Getter
  private final BpelObjectStack     context      = new BpelObjectStack();
  @Getter
  private final FlowModel           flowModel;
  @Getter
  private       Map<String, String> varOwningMap = new HashMap<>();

  public CompilerState(
      @NonNull final Process bpelProcess,
      @NonNull final FlowModel flowModel
  ) {
    this.bpelProcess = bpelProcess;
    this.flowModel = flowModel;
  }

  public String getCurrentBpelPath() {
    return getContext().peekElement().getPath();
  }

  public String getVariableOwner(final String variableName) {
    return varOwningMap.getOrDefault(variableName, null);
  }

  void setVariableOwner(final String variableName, final String serviceName) {
    if (varOwningMap.containsKey(variableName)) {
      final String serviceMapped = varOwningMap.get(variableName);
      if (false == serviceName.equals(serviceMapped)) {
        throw new IllegalStateException(
            variableName + " is already assigned to " + serviceMapped + " but should be overridden with " + serviceName);
      }
    }
    varOwningMap.put(variableName, serviceName);
  }
}
