package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;

import java.util.Set;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AssignBranchValueGuessStrategy {

  Set<String> guessAssignBranchValues(FlowState assignState);
}
