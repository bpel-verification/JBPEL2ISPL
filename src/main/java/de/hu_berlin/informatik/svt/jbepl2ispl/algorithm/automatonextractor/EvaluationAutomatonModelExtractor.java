package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.AllKnowingFacade;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2ICompareExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2INaryExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae.B2IFormula;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
@Service
public class EvaluationAutomatonModelExtractor extends CommonExtractorBase {
  interface PropositionConditionFinderStrategy {

    B2IExpression getCommitmentConditionPropositionCondition(FlowScCommitment commitment);

    B2IExpression getFulfillmentConditionPropositionCondition(
        FlowScFulfillment fulfillment, FlowScCommitment commitment
    );
  }

  /**
   * @deprecated Should be replaced by {@link AdvancedPropositionConditionFinderStrategy} when it is ready
   */
  class SimplifiedPropositionConditionFinderStrategy implements PropositionConditionFinderStrategy {
    /**
     * @param flowScCommitment
     *
     * @return
     */
    public B2IExpression getCommitmentConditionPropositionCondition(final FlowScCommitment flowScCommitment) {
      return B2IExpression.compare(
          B2ICompareExpression.CompareType.EQ,
          B2IExpression.variableReference(FlowService.ENV, Consts.ISPL_VAR_NAME_STATEVAR_ENV),
          B2IExpression.term(flowScCommitment.getTargetState().getName())
      );
    }

    public B2IExpression getFulfillmentConditionPropositionCondition(
        final FlowScFulfillment fulfillment, final FlowScCommitment flowScCommitment
    ) {
      return B2IExpression.compare(
          B2ICompareExpression.CompareType.EQ,
          B2IExpression.variableReference(FlowService.ENV, Consts.ISPL_VAR_NAME_STATEVAR_ENV),
          B2IExpression.term(fulfillment.getTargetState().getName())
      );
    }
  }

  class AdvancedPropositionConditionFinderStrategy implements PropositionConditionFinderStrategy {
    /**
     * FIXME This is partly the version from Algorithm 6, but it needs to be completed.
     *
     * @param flowScCommitment
     *
     * @return
     */
    public B2INaryExpression getCommitmentConditionPropositionCondition(
        final FlowScCommitment flowScCommitment
    ) {
      return B2IExpression.and(
          B2IExpression.compare(
              B2ICompareExpression.CompareType.EQ,
              B2IExpression.variableReference(
                  flowScCommitment.getDebtor().getName(),
                  Naming.getBufferName(
                      flowScCommitment.getDebtor().getName(),
                      flowScCommitment.getCreditor().getName()
                  )
              ),
              getDebtorCommitmentCondition(flowScCommitment) //FIXME the right message
          ),
          B2IExpression.compare(
              B2ICompareExpression.CompareType.EQ,
              B2IExpression.variableReference(FlowService.ENV, Consts.ISPL_VAR_NAME_STATEVAR_ENV),
              B2IExpression.term(flowScCommitment.getTargetState().getName())
          )
      );
    }

    public B2INaryExpression getFulfillmentConditionPropositionCondition(
        final FlowScFulfillment fulfillment, final FlowScCommitment flowScCommitment
    ) {
      return B2IExpression.and(
          B2IExpression.compare(
              B2ICompareExpression.CompareType.EQ,
              B2IExpression.variableReference(
                  flowScCommitment.getCreditor().getName(),
                  Naming.getBufferName(
                      flowScCommitment.getDebtor().getName(),
                      flowScCommitment.getCreditor().getName()
                  )
              ),
              allKnowing.getSymbolicValueAt(fulfillment.getTargetState()) //FIXME the right message
          ),
          B2IExpression.compare(
              B2ICompareExpression.CompareType.EQ,
              B2IExpression.variableReference(FlowService.ENV, Consts.ISPL_VAR_NAME_STATEVAR_ENV),
              B2IExpression.term(fulfillment.getTargetState().getName())
          )

      );
    }
  }

  private final AllKnowingFacade allKnowing;
  private final Logger           logger = LoggerFactory.getLogger(getClass());
  PropositionConditionFinderStrategy propositionConditionFinderStrategy;

  protected EvaluationAutomatonModelExtractor(
      final FlowModel flowModel, final AutomatonModel model,
      final StateInformationResolver stateResolver,
      final VariableTargetServiceResolver resolver,
      final AllKnowingFacade allKnowing
  ) {
    super(flowModel, model, stateResolver, resolver);
    this.allKnowing = allKnowing;
    this.propositionConditionFinderStrategy = new SimplifiedPropositionConditionFinderStrategy();
  }

  public void extract() {
    extractAtomicPropositions();
    extractFormulae();
  }

  private void extractAtomicPropositions() {
    flowModel.getAllCommitmentNodes().forEach(
        flowScCommitment -> {

          // algorithm 6 (φ if i.b i_k = φ and cus.commitment−ID = x)
          model.setProposition(
              Naming.getCommitmentConditionName(flowScCommitment),
              propositionConditionFinderStrategy.getCommitmentConditionPropositionCondition(flowScCommitment)
          );

          /* //for indirect communication
          model.setProposition(
              Naming.getCommitmentContentName(flowScCommitment),
              B2IExpression.and(
                  B2IExpression.compare(
                      B2ICompareExpression.CompareType.EQ,
                      B2IExpression.term(1),
                      B2IExpression.term(1)
                  )
              )
          );
          */
        }
    );

    flowModel.getAllFulfillmentNodes().forEach(
        fulfillment -> {
          final FlowScCommitment flowScCommitment = fulfillment.getCommitment();
          model.setProposition(
              Naming.getFulfillmentConditionName(fulfillment),
              propositionConditionFinderStrategy.getFulfillmentConditionPropositionCondition(
                  fulfillment, flowScCommitment)
          );
        }
    );
  }

  private void extractCommitmentFormulas(final FlowScCommitment commitment) {
    List<B2IFormula> formulaList             = new ArrayList<>();
    List<FlowState>  fulfillmentMarkedStates = new ArrayList<>();
    final String     debtor                  = commitment.getDebtor().getName();
    flowModel.findAllFulfillmentNodesFor(commitment).forEach(
        fulfillment -> {
          fulfillment = flowModel.refresh(fulfillment, 3);
          fulfillmentMarkedStates.add(fulfillment.getTargetState());
          boolean directlyFollowing = model.areNeighborStates(
              commitment.getTargetState(),
              fulfillment.getTargetState()
          ); //if commitment is direct FOLLOWED_BY fulfillment
          final FlowScTraceLabel.Type desiredStateProposition = getDesiredStateProposition(commitment, fulfillment);

          model.addFormula(
              // AG (φ → EX ( i_BehaviorType ∧ C i→j ψ )) // PAPER:Property(3)
              B2IFormula.AG(
                  B2IFormula.implies(
                      B2IFormula.term(Naming.getCommitmentConditionName(commitment)),
                      B2IFormula.Unary(
                          //if EF or EX seems to be decided on path length
                          directlyFollowing ? B2IFormula.Function.EX : B2IFormula.Function.EF,
                          B2IFormula.and(
                              B2IFormula.term(
                                  desiredStateProposition == FlowScTraceLabel.Type.DESIRED
                                  ? Naming.getDesiredStateProposition(debtor)
                                  : Naming.getUnDesiredStateProposition(debtor)
                                  //as long as it is a fulfillment, it's desired
                              ),//FIXME determine if it is RED or GREEN"i_DesiredStates"
                              B2IFormula.SC(
                                  debtor,
                                  commitment.getCreditor().getName(),
                                  getDebtorBehaviorProposition(fulfillment)
                              )
                          )
                      )
                  )
              )
          );
          if (desiredStateProposition == FlowScTraceLabel.Type.DESIRED) {
            formulaList.add(
                B2IFormula.SC(
                    debtor,
                    commitment.getCreditor().getName(),
                    getDebtorBehaviorProposition(fulfillment)
                )
            );
          }
        }
    );
    //FIXME add the NullMsg behavior here
    final Collection<FlowState.PathReturn> nullMsgPaths = flowModel.findAllPathsBetweenExcluding(
        commitment.getTargetState(),
        flowModel.getEndState(),
        fulfillmentMarkedStates
    );
    if (nullMsgPaths.size() > 0) {
      model.addFormula(
          // AG (φ → EX ( i_BehaviorType ∧ C i→j ψ )) // PAPER:Paper(3)
          B2IFormula.AG(
              B2IFormula.implies(
                  B2IFormula.term(Naming.getCommitmentConditionName(commitment)),
                  B2IFormula.Unary(
                      //if EF or EX seems to be decided on path length
                      B2IFormula.Function.EF,
                      B2IFormula.and(
                          B2IFormula.term(
                              getTraceLabelType(commitment, nullMsgPaths) == FlowScTraceLabel.Type.DESIRED
                              ? Naming.getDesiredStateProposition(debtor)
                              : Naming.getUnDesiredStateProposition(debtor)
                              //as long as it is a fulfillment, it's desired
                          ),//FIXME determine if it is RED or GREEN"i_DesiredStates"
                          B2IFormula.SC(
                              debtor,
                              commitment.getCreditor().getName(),
                              Consts.PROPOSITION_NULL_MSG
                          )
                      )
                  )
              )
          )
      );
    }

    model.addFormula(
        // AF (φ ∧ (C i→j ψ))
        //AF (φ ∧ (( C i→j ψ_1 ) ∨ ( C i→j ψ_2 ) ∨ ... ∨ ( C i→j ψ_n )))  // Property(4)
        B2IFormula.AF(
            B2IFormula.and(
                B2IFormula.term(Naming.getCommitmentConditionName(commitment)),
                B2IFormula.or(
                    //as many as paths for fulfillment
                    formulaList
                )
            )
        )
    );

    extractFulfillmentLabel();
  }

  private void extractFormulae() {
    // resilience
    extractTraceLabelFormulas();

    flowModel.getAllCommitmentNodes().forEach(
        this::extractCommitmentFormulas
    );

    // recovery

    extracteRecoveryFormulas();
  }

  private void extractFulfillmentLabel() {
    flowModel.getAllFulfillmentNodes().forEach(
        fulfillment -> {
          final String debtor         = fulfillment.getCommitment().getDebtor().getName();
          final String creditor       = fulfillment.getCommitment().getCreditor().getName();
          final String debtorBehavior = getDebtorBehaviorProposition(fulfillment);
          model.addFormula(
              // AG((C i→j ψ) → AF (Fu(C i→j ψ)))   //Property(5)
              B2IFormula.AG(
                  B2IFormula.implies(
                      B2IFormula.SC(
                          debtor,
                          creditor,
                          debtorBehavior
                      ),
                      B2IFormula.AF(
                          B2IFormula.Fu(
                              debtor,
                              creditor,
                              debtorBehavior
                          )
                      )
                  )
              )
          );
          model.addFormula(
              // AG((C i→j ψ) → EF (Fu(C i→j ψ))) Property(6)
              B2IFormula.AG(
                  B2IFormula.implies(
                      B2IFormula.SC(
                          debtor,
                          creditor,
                          debtorBehavior
                      ),
                      B2IFormula.EF(
                          B2IFormula.Fu(
                              debtor,
                              creditor,
                              debtorBehavior
                          )
                      )
                  )
              )
          );

          model.addFormula(
              // // AG (( C i→j ψ ) → AF (α )) //algo 8
              B2IFormula.AG(
                  B2IFormula.implies(
                      B2IFormula.SC(
                          debtor,
                          creditor,
                          debtorBehavior
                      ),
                      B2IFormula.AF(
                          B2IFormula.term(Naming.getFulfillmentConditionName(fulfillment))
                      )
                  )
              )
          );
          model.addFormula(
              // // AG (( C i→j ψ ) → EF (α)) //algo 8
              B2IFormula.AG(
                  B2IFormula.implies(
                      B2IFormula.SC(
                          debtor,
                          creditor,
                          debtorBehavior
                      ),
                      B2IFormula.EF(
                          B2IFormula.term(Naming.getFulfillmentConditionName(fulfillment))
                      )
                  )
              )
          );
          model.addFormula(
              // AG ( C i→j ψ → ( i_UnDesState ∧ AX A ( i_UnDesState U i_DesState )))  // Property(7)
              B2IFormula.AG(
                  B2IFormula.implies(
                      B2IFormula.SC(
                          debtor,
                          creditor,
                          debtorBehavior
                      ),
                      B2IFormula.and(
                          B2IFormula.term(Naming.getUnDesiredStateProposition(debtor)),
                          B2IFormula.AX(
                              B2IFormula.A(
                                  B2IFormula.term(Naming.getUnDesiredStateProposition(debtor)),
                                  B2IFormula.term(Naming.getDesiredStateProposition(debtor))
                              )
                          )
                      )
                  )
              )
          );
        }
    );
  }

  private void extractTraceLabelFormulas() {
    model.getAllAgentAutomatons().forEach(
        agentAutomaton -> {
          if (agentAutomaton.isEnvironment()) {
            return;
          }
          agentAutomaton.getDesiredStates().forEach(
              s -> model.addFormula(
                  // EG(i_Desired ) // (1)
                  B2IFormula.EG(
                      B2IFormula.term(s)
                  )
              )
          );

          agentAutomaton.getUnDesiredStates().forEach(
              s -> model.addFormula(
                  // EF (EG(i_UnDesired )) // (2)
                  B2IFormula.EF(
                      B2IFormula.EG(
                          B2IFormula.term(s)
                      )
                  )
              )

          );
        }
    );
  }

  private void extracteRecoveryFormulas() {

    // AG ( C i→j ψ → ( i_UnDesState ∧ AX A ( i_UnDesState U ( i_DesState ∧ ω )))) // (8)
    // AG (ω → ( AF ( C i→j ψ_1 ∧ AF ( Fu ( C i→j ψ_1 ))) ∨ ... ∨ AF ( C i→j ψ_n ∧ AF ( Fu ( C i→j ψ_n )))))  // (9)
  }

  private B2IExpression getCreditorBehavior(final FlowScCommitment commitment) {
    return allKnowing.getSymbolicValueAt(commitment.getTargetState());
  }

  private String getDebtorBehaviorProposition(final FlowScFulfillment fulfillment) {
    return Naming.getFulfillmentConditionName(fulfillment);
  }

  /**
   * the <code>φ</code> in <code>i.b i_k = φ</code>
   *
   * @param commitment
   *
   * @return
   */
  private B2IExpression getDebtorCommitmentCondition(final FlowScCommitment commitment) {
    // buffer_i_k is the received message at <invoke> state of
    final String v                       = commitment.getTargetState().getInputVariable();
    final String symbolicVariableValueAt = resolver.getSymbolicVariableValueAt(v, commitment.getTargetState());
    return B2IExpression.term(symbolicVariableValueAt);
  }

  private FlowScTraceLabel.Type getDesiredStateProposition(
      @NonNull final FlowScCommitment commitment, @NonNull final FlowScFulfillment fulfillment
  ) {
    final Collection<FlowState.PathReturn> allPathsBetween = flowModel.findAllPathsBetween(
        commitment.getTargetState(), fulfillment.getTargetState());
    FlowScTraceLabel.Type flowState = getTraceLabelType(commitment, allPathsBetween);
    if (flowState != null) {
      return flowState;
    }
    return FlowScTraceLabel.Type.DESIRED;
  }

  private FlowScTraceLabel.Type getTraceLabelType(
      @NonNull final FlowScCommitment commitment, final Collection<FlowState.PathReturn> allPathsBetween
  ) {
    final String debtor = commitment.getDebtor().getName();
    for (FlowState.PathReturn pathReturn: allPathsBetween) {
      for (FlowState flowState: pathReturn.states) {
        if (flowState.isFlagged(Consts.PROPERTY_SC_TRACE_BEHAVIOUR_TYPE)
            && debtor.equals(flowState.getProperty(Consts.PROPERTY_SC_TRACE_SERVICE_NAME)))
        {
          return FlowScTraceLabel.Type.valueOf(
              flowState.getProperty(Consts.PROPERTY_SC_TRACE_BEHAVIOUR_TYPE));
        }
      }
    }
    return null;
  }
}
