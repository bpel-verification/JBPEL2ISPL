package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowScCommitment;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NodeEntity(label = "ScCommitment")
@NoArgsConstructor
public class Neo4jFlowScCommitment extends FlowBase implements FlowScCommitment {
  @Getter
  private int     commitmentId;
  @Getter
  @Relationship(type = Naming.RELATION_FLOW_SC_CREDITOR)
  private Service creditor;
  @Getter
  @Relationship(type = Naming.RELATION_FLOW_SC_DEBTOR)
  private Service debtor;
  @Getter
  @Id
  @GeneratedValue
  private Long    id;
  @Getter
  private int     originalElementId;
  @Relationship(type = Naming.RELATION_FLOW_SC_TARGET)
  @Getter
  private State   targetState;

  public Neo4jFlowScCommitment(
      final String name, final int commitmentID,
      @NonNull final FlowService debtor,
      @NonNull final FlowService creditor,
      @NonNull final FlowState targetState,
      final int originalElementId,
      final int elementId
  ) {
    super(name, elementId);
    this.creditor = (Service) creditor;
    this.debtor = (Service) debtor;
    this.commitmentId = commitmentID;
    this.targetState = (State) targetState;
    this.originalElementId = originalElementId;
  }
}
