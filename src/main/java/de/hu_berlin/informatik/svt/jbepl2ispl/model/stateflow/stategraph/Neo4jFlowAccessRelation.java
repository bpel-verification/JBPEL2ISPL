package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.BpelPath;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowId;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVarAccessRelation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.ogm.annotation.Properties;
import org.neo4j.ogm.annotation.Transient;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NoArgsConstructor
public abstract class Neo4jFlowAccessRelation implements FlowVarAccessRelation {
  @Getter
  private int                  alternativeCount = 0;
  @Transient
  private BpelPath             bpelPath;
  @Getter
  private String               bpelPathAsString;
  /**
   * Hier durch wird es möglich, die Elemente untereinander in eine Reihenfolge zu bringen, wie sie im
   * ursprünglich BPEL Dokument auftauchen.
   */
  @Getter
  private int                  elementId;
  @Transient
  private FlowId               flowId           = null;
  @Getter
  private String               name;
  @Properties(prefix = "processed")
  private Map<String, Boolean> processed        = new HashMap<>();
  @Properties(prefix = "stringProperty")
  private Map<String, String>  properties       = new HashMap<>();
  @Setter
  private String               symbolicValue;
  @Getter
  @Setter
  private String               value;
  @Getter
  private String               valueSource;
  @Getter
  private VarValueSourceType   valueSourceType;

  Neo4jFlowAccessRelation(
      final String name, final State state, final Variable variable,
      final VarValueSourceType varValueSourceType,
      final String value, final String valueSource, final int elementId, final String bpelPathAsString,
      final int alternativeCount
  ) {
    this.alternativeCount = alternativeCount;
    this.bpelPathAsString = bpelPathAsString;
    this.elementId = elementId;
    this.name = name;
    setState(state);
    this.value = value;
    this.valueSource = valueSource;
    this.valueSourceType = varValueSourceType;
    setVariable(variable);
  }

  public BpelPath getBpelPath() {
    if (bpelPathAsString == null) {
      throw new IllegalStateException(
          "bpelPathAsString is <null> in " + getClass().getSimpleName() + "/eID:" + getElementId());
    }
    if (bpelPath == null) {
      bpelPath = new BpelPath(bpelPathAsString);
    }
    return bpelPath;
  }

  @Override
  public void setBpelPath(final String bpelObjectPath) {
    bpelPathAsString = bpelObjectPath;
  }

  @Override
  public FlowId getFlowId() {
    if (flowId == null) {
      flowId = new FlowId(elementId, alternativeCount);
    }
    return flowId;
  }

  public String getProperty(final String propertyName) {
    return properties.getOrDefault(propertyName, null);
  }

  @Override
  public String getSymbolicValue() {
    return symbolicValue;
  }

  @Override
  public QName getVariableName() {
    return getVariable().getQName();
  }

  public boolean isProcessed(final String identifier) {
    return processed.getOrDefault(identifier, false);
  }

  public void setProcessed(final String identifier, final boolean state) {
    processed.put(identifier, state);
  }

  public void setProperty(final String propertyName, final String value) {
    properties.put(propertyName, value);
  }

  protected void cloneBase(Neo4jFlowAccessRelation from) {
    this.processed.putAll(from.processed);
    this.properties.putAll(from.properties);
  }

  protected abstract void setState(final State state);

  protected abstract void setVariable(final Variable variable);
}
