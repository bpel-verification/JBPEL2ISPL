package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.apache.ode.bpel.compiler.bom.SequenceActivity;

import java.util.Arrays;

/**
 * Created by Christoph Graupner on 2018-03-09.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class SequenceHandler extends HandlerBase {
  public SequenceHandler() {
    super(Arrays.asList(SequenceActivity.class));
  }

  @Override
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    if (currentBpelObject instanceof SequenceActivity) {
      SequenceActivity sequenceActivity = (SequenceActivity) currentBpelObject;
      final FlowModel  model            = compilerState.getFlowModel();
      FlowState state = model.factory().createState(
          sequenceActivity.getName(),
          "SEQ_BGN__" + getActivityName(sequenceActivity),
          Activity.SEQUENCE,
          BeginEndType.BEGIN,
          compilerState.getCurrentBpelPath()
      );
      model.save(state);
    }
    return super.handleBegin(currentBpelObject, compilerState);
  }

  @Override
  public <T extends HandlerBase> T handleEnd(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    if (currentBpelObject instanceof SequenceActivity) {
      SequenceActivity sequenceActivity = (SequenceActivity) currentBpelObject;
      final FlowModel  model            = compilerState.getFlowModel();
      FlowState state = model.factory().createState(
          sequenceActivity.getName(),
          "SEQ_END__" + getActivityName(sequenceActivity),
          Activity.SEQUENCE,
          BeginEndType.END,
          compilerState.getCurrentBpelPath()
      );
      model.save(state);
    }

    return super.handleEnd(currentBpelObject, compilerState);
  }
}
