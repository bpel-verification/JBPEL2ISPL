package de.hu_berlin.informatik.svt.jbepl2ispl;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;

import java.io.File;

/**
 * Created by Christoph Graupner on 2018-01-31.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
@SpringBootApplication
@Profile("!test")
public class MainApp implements ApplicationRunner {
  @Getter
  @Setter
  static        ApplicationContext applicationContext;
  @Setter
  static        Config             config;
  private final Logger             logger = LoggerFactory.getLogger(getClass());

  public MainApp(final ApplicationContext aApplicationContext, final Config thisConfig) {
    applicationContext = aApplicationContext;
    config = thisConfig;
  }

  private static Options createOptions() {
    Options lOptions = new Options();

    lOptions.addOption("h", "help", false, "Hilfe");
    lOptions.addOption(
        Option.builder("o")
              .longOpt("output")
              .hasArg(true)
              .argName("path-to-output-file")
              .desc("Name der Ausgabedatei")
              .build()
    );
//    lOptions.addOption("d", "debug", false, "Debug-Ausgaben");

    lOptions.addOption(
        Option.builder("D")
              .hasArgs()
              .desc("spring options")
              .build()
    );

//    lOptions.addOption(Option.builder("l")
//                             .longOpt("log-level")
//                             .argName("level")
//                             .type(Integer.TYPE)
//                             .hasArg()
//                             .desc("'Gesprächigkeit' des Loggings").build());

    return lOptions;
  }

  public static Config getConfig() {
    return MainApp.getApplicationContext().getBean(Config.class);
  }

  public static void main(String[] args) {
    SpringApplication.run(MainApp.class, args);
  }

  @Override
  public void run(final ApplicationArguments args) throws Exception {
    Options       lOptions = createOptions();
    DefaultParser lParser  = new DefaultParser();

    CommandLine lCommandLine = lParser.parse(lOptions, args.getSourceArgs());

    if (lCommandLine.getArgs().length != 1) {
      printUsage(lOptions);
      System.exit(1);
    } else {
      TransformationService transformationService = applicationContext.getBean(TransformationService.class);
      transformationService.setInputBpel(new File(lCommandLine.getArgs()[0]));
      if (lCommandLine.hasOption('o')) {
        transformationService.setOutputIspl(new File(lCommandLine.getOptionValue('o')));
      }
      transformationService.call();
      logger.info("END of Bpel2ISPL reached. Clean up resources and terminating hard now.");

      System.exit(0); //FIXME find the real problem that it is not terminating with remote neo4j
    }
  }

  private void printUsage(final Options options) {
    String header = "\nConverts an BPEL input file into an ISPL file for MCMAS\n\n";
    String footer = "\nPlease report issues at https://gitlab.com/bpel-verification/JBPEL2ISPL/issues";

    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp("jbpel2ispl", header, options, footer, true);
  }
}
