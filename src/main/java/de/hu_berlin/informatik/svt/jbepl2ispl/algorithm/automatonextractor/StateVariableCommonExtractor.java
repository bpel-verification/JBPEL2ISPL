package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentVariableDefinition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentAutomatonModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import org.springframework.stereotype.Service;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
@Service
public class StateVariableCommonExtractor extends CommonExtractorBase {

  protected StateVariableCommonExtractor(
      final FlowModel flowModel, final AutomatonModel model, final StateInformationResolver stateResolver,
      final VariableTargetServiceResolver resolver
  ) {
    super(flowModel, model, stateResolver, resolver);
  }

  @Override
  public void extract() {
    logger.trace(LogMarker.EXTRACTION, "Extracting stateVariable for Agent '{}'", FlowService.ENV);
    extractStateVariableWithName(
        (AgentAutomatonModifiable) model.getAgentAutomaton(FlowService.ENV), Consts.ISPL_VAR_NAME_STATEVAR_ENV);
    logger.trace(LogMarker.EXTRACTION, "DONE Extracting stateVariable for Agent '{}'", FlowService.ENV);
  }

  protected void extractStateVariableWithName(AgentAutomatonModifiable agent, final String name) {
    AgentVariableDefinition varDef = factory.createAgentVariableDefinition(
        name
    );
    varDef.setValueType(AgentVariableDefinition.ValueType.ENUM);
    varDef.setBpelDeclared(false);

    agent.save(varDef);
    model.save(agent);
  }
}
