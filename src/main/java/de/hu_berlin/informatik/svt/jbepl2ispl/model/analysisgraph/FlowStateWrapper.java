package de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;

/**
 * Created by Christoph Graupner on 2018-06-18.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface FlowStateWrapper extends FlowState {
  FlowState wrapped();
}
