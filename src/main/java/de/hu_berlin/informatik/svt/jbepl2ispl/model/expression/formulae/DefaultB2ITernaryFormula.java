package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public class DefaultB2ITernaryFormula extends DefaultB2INaryFormula implements B2ITernaryFormula {

  public DefaultB2ITernaryFormula(
      final Function function, B2IFormula term1, B2IFormula term2, B2IFormula term3
  ) {
    super(function, term1, term2, term3);
  }

  @Override
  public <T extends B2IFormula> T addTerm(
      final B2IFormula expression
  ) {
    throw new UnsupportedOperationException();
  }

  @Override
  public B2IFormula getTerm1() {
    return terms[0];
  }

  @Override
  public B2IFormula getTerm2() {
    return terms[1];
  }

  @Override
  public B2IFormula getTerm3() {
    return terms[2];
  }
}
