package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentVariableDefinition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVariable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.Variable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;
import java.util.Objects;

/**
 * Created by Christoph Graupner on 2018-05-29.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NodeEntity(label = Naming.AGENT_VARIABLE_DEFINITION)
@NoArgsConstructor
public class Neo4jVariableDefinition implements AgentVariableDefinition {
  @Getter
  List<String> allowedValues;
  @Getter
  @Setter
  boolean      bpelDeclared;
  @Getter
  @Id
  @GeneratedValue
  Long         id;
  @Getter
  @Setter
  String       initialValue;
  @Getter
  @Setter
  String       name;
  @Relationship(type = Naming.RELATION_AGENT_BELONGS_TO)
  @Getter
  private Neo4jAgentAutomaton      agent;
  @Getter
  private int                      elementId;
  @Relationship(type = Naming.RELATION_DEFINES)
  private Variable                 originVariable;
  @Setter
  private FlowVariable.PurposeType purposeType;
  @Getter
  @Setter
  private ValueType                valueType;
  private String                   variableType;

  Neo4jVariableDefinition(final String name, final int elementId) {
    this.name = name;
    this.elementId = elementId;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Neo4jVariableDefinition)) {
      return false;
    }
    final Neo4jVariableDefinition that = (Neo4jVariableDefinition) o;
    return Objects.equals(getAgent(), that.getAgent()) && Objects.equals(getName(), that.getName());
  }

  @Override
  public AgentAutomaton getBelongsto() {
    return agent;
  }

  @Override
  public String getIsplName() {
    return getName();
  }

  @Override
  public FlowVariable getOriginVariable() {
    return originVariable;
  }

  @Override
  public void setOriginVariable(final FlowVariable originVariable) {
    this.originVariable = (Variable) originVariable;
  }

  @Override
  public FlowVariable.PurposeType getPurposeType() {
    return purposeType != null
           ? purposeType
           : (getOriginVariable() == null ? null : getOriginVariable().getPurposeType());
  }

  @Override
  public QName getQName() {
    return new QName(agent != null ? agent.getName() : null, getName());
  }

  @Override
  public String getVariableType() {
    if (variableType == null) {
      return originVariable == null ? null : originVariable.getVariableType();
    }
    return variableType;
  }

  @Override
  public boolean hasAllowedValues() {
    return allowedValues != null && !allowedValues.isEmpty();
  }

  @Override
  public boolean hasInitialValue() {
    return initialValue != null;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getAgent(), getName());
  }

  @Override
  public void setAllowedValues(final List<String> values) {
    values.forEach(
        value -> {
          switch (getValueType()) {
            case ENUM:
              if (!value.matches("^[A-Za-z].*")) {
                throw new IllegalArgumentException(
                    "VarDef[" + getName() + "] is of value type ENUM, but allowed value '" + value +
                    "' does not begin with [A-Za-z]."
                );
              }
              break;
            case BOOLEAN:

              break;
            case INT:
              try {
                Integer.valueOf(value);
              } catch (NumberFormatException e) {
                throw new IllegalArgumentException(
                    "VarDef[" + getName() + "] is of value type INTEGER, but allowed value '" + value +
                    "' isn't of that type.");
              }
              break;
          }
        }
    );
    this.allowedValues = values;
  }

  @Override
  public void setBelongsTo(final AgentAutomaton agent) {
    this.agent = (Neo4jAgentAutomaton) agent;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("AgentVariableDefinition{");
    sb.append("name='").append(name).append('\'');
    sb.append(", agent=").append(agent == null ? null : agent.getName());
    sb.append(", elementId=").append(elementId);
    sb.append(", id=").append(id);
    sb.append(", allowedValues=").append(allowedValues);
    sb.append(", bpelDeclared=").append(bpelDeclared);
    sb.append(", valueType=").append(valueType);
    sb.append(", variableType='").append(variableType).append('\'');
    sb.append(", initialValue='").append(initialValue).append('\'');
    sb.append(", originVariable=").append(originVariable);
    sb.append('}');
    return sb.toString();
  }
}
