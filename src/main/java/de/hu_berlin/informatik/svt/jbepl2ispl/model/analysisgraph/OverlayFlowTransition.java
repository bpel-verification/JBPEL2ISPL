package de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.BpelPath;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowId;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowTransition;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * Created by Christoph Graupner on 2018-06-18.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class OverlayFlowTransition implements FlowTransitionWrapper {
  @Getter
  @Setter
  private int       alternativeCount;
  @Getter
  @Setter
  private int       elementId;
  private FlowId    flowId;
  @Getter
  @Setter
  private String    guard;
  @Getter
  @Setter
  private Long      id;
  @Setter
  private boolean   invertGuard;
  @Getter
  @Setter
  private QName     isplActionName;
  @Getter
  private String    name;
  @Getter
  @Setter
  private FlowState sourceState;
  @Getter
  @Setter
  private FlowState targetState;

  public OverlayFlowTransition(
      @NonNull FlowState sourceState, @NonNull FlowState targetState, String guard, QName isplActionName,
      Long id, int elementId, int alternativeCount
  ) {
    this.sourceState = sourceState;
    this.targetState = targetState;
    this.guard = guard;
    this.isplActionName = isplActionName;
    this.id = id;
    this.elementId = elementId;
    this.alternativeCount = alternativeCount;
  }

  public OverlayFlowTransition(
      @NonNull FlowState sourceState, @NonNull FlowState targetState, @NonNull FlowTransition toCloneFrom
  ) {
    this.sourceState = sourceState;
    this.targetState = targetState;
    this.guard = toCloneFrom.getGuard();
    this.isplActionName = toCloneFrom.getIsplActionName();
    this.id = toCloneFrom.getId();
    this.elementId = toCloneFrom.getElementId();
    this.alternativeCount = toCloneFrom.getAlternativeCount();
    this.invertGuard = toCloneFrom.invertGuard();
    this.name = toCloneFrom.getName();
  }

  @Override
  public BpelPath getBpelPath() {
    return null;
  }

  @Override
  public void setBpelPath(final String bpelObjectPath) {

  }

  @Override
  public String getBpelPathAsString() {
    return null;
  }

  @Override
  public FlowId getFlowId() {
    if (flowId == null) {
      flowId = new FlowId(elementId, alternativeCount);
    }
    return flowId;
  }

  @Override
  public String getProperty(final String propertyName) {
    return null;
  }

  @Override
  public boolean invertGuard() {
    return invertGuard;
  }

  @Override
  public boolean isFlagged(final String identifier) {
    return false;
  }

  @Override
  public void setFlagged(final String identifier, final boolean state) {

  }

  @Override
  public void setProperty(final String propertyName, final String value) {

  }

  @Override
  public FlowTransition wrapped() {
    return null;
  }
}
