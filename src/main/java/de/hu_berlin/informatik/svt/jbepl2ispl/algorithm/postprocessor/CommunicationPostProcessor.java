package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.MessagingType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowMessageSend;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.Jb2iUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
@Service
public class CommunicationPostProcessor extends FlowModelPostProcessor {
  protected final VariableTargetServiceResolver resolver;
  protected final StateInformationResolver      stateResolver;

  protected CommunicationPostProcessor(
      final FlowModel model,
      final VariableTargetServiceResolver resolver,
      final StateInformationResolver stateResolver
  ) {
    super(model);
    this.resolver = resolver;
    this.stateResolver = stateResolver;
  }

  @Override
  public void finish() {
    Collection<FlowMessageSend> messageSends = flowModel.getAllMessageSend(3);
    messageSends.forEach(
        flowMessageSend -> {
          switch (flowMessageSend.getType()) {
            case DIRECT:
              processDirectSend(flowMessageSend);
              break;
            case INDIRECT:
              break;
            case LOCAL:
              break;
            case INITIALISE:
              break;
          }
        }
    );

    final Collection<FlowState> localAssignments = flowModel.findStatesWithActivity(Activity.ASSIGN);
    localAssignments.forEach(
        state -> {
          switch (MessagingType.valueOf(state.getProperty(Consts.PROPERTY_MESSAGING_TYPE))) {
            case LOCAL:
              processLocalAssign(state);
              break;
          }
        }
    );
  }

  /**
   * FIXME it is overriding action
   *
   * @param messageSend
   */
  private void processDirectSend(final FlowMessageSend messageSend) {
    final FlowState receiverState = messageSend.getValueReceivingState(); //invoke-reply
    final FlowState originAssign  = messageSend.getOriginAssignState(); //assign

    QName actionNameSend = new QName(
        messageSend.getSendingService().getName(),
        String.format(
            Consts.ISPL_ACTION_SEND_FORMAT,
            messageSend.getSendingService().getName(),
            messageSend.getReceivingService().getName()
        )
    );
    QName actionNameReceive = new QName(
        messageSend.getReceivingService().getName(),
        String.format(
            Consts.ISPL_ACTION_RECEIVE_FORMAT,
            messageSend.getSendingService().getName(),
            messageSend.getReceivingService().getName()
        )
    );

    setActionNameForIncomingTransitions(originAssign, actionNameSend);
    setActionNameForIncomingTransitions(receiverState, actionNameReceive);
  }

  private void processLocalAssign(final FlowState state) {
    QName actionNameSend = new QName(
        FlowService.ENV,
        String.format(
            Consts.ISPL_ACTION_LOCAL_ASSIGN_FORMAT,
            state.getOutputVariable()
        )
    );
    setActionNameForIncomingTransitions(state, actionNameSend);
  }

  private void setActionNameForIncomingTransitions(final FlowState state, final QName actionName) {
    flowModel.findAllIncomingTransitions(state).forEach(
        flowTransition -> {
          if (false == Jb2iUtils.isNullOrEmpty(flowTransition.getIsplActionName())) {
            throw new NotImplementedException(
                String.format(
                    "Trying to set ActionName '%s' at '%s' for incoming transition '%s', but the action of transition is not <null>: ",
                    actionName,
                    state,
                    flowTransition
                )
            );
          }
          flowTransition.setIsplActionName(actionName);
          flowModel.save(flowTransition);
        }
    );
  }
}
