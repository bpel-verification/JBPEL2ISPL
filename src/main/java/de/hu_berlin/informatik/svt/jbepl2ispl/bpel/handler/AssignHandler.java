package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.ConditionStatement;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.XPathConditionStatement;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import org.apache.ode.bpel.compiler.bom.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by Christoph Graupner on 2018-05-11.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class AssignHandler extends HandlerBase {
  public AssignHandler() {
    super(Arrays.asList(AssignActivity.class));
  }

  @Override
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    if (currentBpelObject instanceof AssignActivity) {
      handleAssign((AssignActivity) currentBpelObject, compilerState);
    }

    return (T) this;
  }

  protected void handleAssign(final AssignActivity assignActivity, final CompilerState compilerState) {
    final FlowModel        model     = compilerState.getFlowModel();
    final StateFlowFactory factory   = model.factory();
    int                    copyCount = 0;

    for (Copy copy: assignActivity.getCopies()) {
      copyCount++;
      final From from = copy.getFrom();
      final To   to   = copy.getTo();

      if (hasMoreThanOneVariable(from)) {
        //it could be that through xpath (i.e. concat()) multiple vars are involved here -> create for each var an extra assign state
        logger.warn(
            LogMarker.BPEL,
            "We have more than 1 referenced variable in <assign>/from ({}) -> creating for each of them an assign state.",
            from
        );
        //create for each var an extra assign state
        final int finalCopyCount = copyCount;
        getVariables(from).forEach(
            s -> {
              final String stateName =
                  generateStateName(assignActivity.getName(), from, to) + "#" + finalCopyCount + "%" + s;
              logger.info(LogMarker.BPEL, "Creating for multi-<assign>/from: state with name: {}", stateName);
              FlowState flowState = factory.createState(
                  assignActivity.getName(),
                  stateName,
                  Activity.ASSIGN,
                  BeginEndType.BOTH,
                  compilerState.getCurrentBpelPath()
              );
              FlowVariable fromVar = model.findVariableByName(s);
              flowState.setInputVariable(fromVar.getName());

              FlowVariable toVariable =
                  model.findVariableByName(getVariableName(to, compilerState));
              VarValueSourceType fromOriginType = getFromOriginType(from);
              final String       fromValue      = getFromValue(from, compilerState);
              flowState.setOutputVariable(toVariable.getName());

              model.save(flowState);

              // this is a replacement for FlowVariableValue
              FlowVarReadAccessRelation readAccessRelation = factory.createVarReadAccessRelation(
                  flowState,
                  fromVar,
                  fromValue,
                  getFromOrigin(from),
                  fromOriginType,
                  compilerState.getCurrentBpelPath()
              );
              model.save(readAccessRelation);

              // this is a replacement for FlowVariableValue
              FlowVarWriteAccessRelation writeAccessRelation = factory.createVarWriteAccessRelation(
                  flowState,
                  toVariable,
                  fromValue,
                  getFromOrigin(from),
                  fromOriginType,
                  compilerState.getCurrentBpelPath()
              );
              model.save(writeAccessRelation);
            }
        );
      } else {
        FlowState flowState = factory.createState(
            assignActivity.getName(),
            generateStateName(assignActivity.getName(), from, to) + "#" + copyCount,
            Activity.ASSIGN,
            BeginEndType.BOTH,
            compilerState.getCurrentBpelPath()
        );

        FlowVariable toVariable =
            model.findVariableByName(getVariableName(to, compilerState));
        final VarValueSourceType fromOriginType = getFromOriginType(from);
        final String             fromValue      = getFromValue(from, compilerState);
        flowState.setOutputVariable(toVariable.getName());

        if (isVariableInvolved(from)) {
          FlowVariable fromVar = model.findVariableByName(getFromOrigin(from));
          flowState.setInputVariable(fromVar.getName());

          // this is a replacement for FlowVariableValue
          FlowVarReadAccessRelation readAccessRelation = factory.createVarReadAccessRelation(
              flowState,
              fromVar,
              fromValue,
              getFromOrigin(from),
              fromOriginType,
              compilerState.getCurrentBpelPath()
          );
          model.save(readAccessRelation);
        } else {
          // this is a replacement for FlowVariableValue
          FlowVarReadAccessRelation readAccessRelation = factory.createVarReadAccessRelation(
              flowState,
              model.findVariableByName(Consts.VAR_NAME_LITERAL),
              fromValue,
              getFromOrigin(from),
              fromOriginType,
              compilerState.getCurrentBpelPath()
          );
          flowState.setInputVariable(Consts.VAR_NAME_LITERAL);
          model.save(readAccessRelation);
        }

        model.save(flowState);

        // this is a replacement for FlowVariableValue
        // keep FlowVarWriteAccessRelation always after FlowVarReadAccessRelation!
        FlowVarWriteAccessRelation writeAccessRelation = factory.createVarWriteAccessRelation(
            flowState,
            toVariable,
            fromValue,
            getFromOrigin(from),
            fromOriginType,
            compilerState.getCurrentBpelPath()
        );

        model.save(writeAccessRelation);
      }
    }
  }

  private boolean checkIfItIsXML(final LiteralVal literalVal) {
    try {
      DocumentBuilderFactory dbFactory       = DocumentBuilderFactory.newInstance();
      DocumentBuilder        dBuilder        = dbFactory.newDocumentBuilder();
      Document               doc             = dBuilder.parse(new InputSource(new StringReader(literalVal.toString())));
      final Element          documentElement = doc.getDocumentElement();
      final NodeList         nodes           = documentElement.getChildNodes();
      for (int i = 0; i < nodes.getLength(); i++) {
        //trick: if it is not XML inside <literal> there is no Element (==<tag>) inside
        if (nodes.item(i) instanceof Element) {
          return true;
        }
      }
      return false;
    } catch (ParserConfigurationException | SAXException | IOException ignored) {
    }
    return false;
  }

  private String generateStateName(final String name, final From from, final To to) {
    String fromName, toName = "unknown";
    fromName = getFromOrigin(from);
    if (to.isVariableVal()) {
      toName = to.getAsVariableVal().getVariable();
    }

    return "ASSIGN_" + name + "__" + fromName + "__TO__" + toName;
  }

  private String getFromOrigin(final From from) {
    String fromName;
    if (from.isLiteralVal()) {
      fromName = "LITERAL";
    } else if (from.isVariableVal()) {
      fromName = from.getAsVariableVal().getVariable();
    } else {
      try {
        final ConditionStatement x = new XPathConditionStatement(from.getAsExpression().getTextValue());
        if (x.getVariables().isEmpty()) {
          fromName = "TEXT";
        } else {
          //FIXME if more than 1 variable involved it's unreliable
          fromName = x.getVariables().iterator().next();
        }
      } catch (XPathExpressionException e) {
        logger.error(LogMarker.BPEL, e.getLocalizedMessage(), e);
        fromName = "TEXT";
      }
    }
    return fromName;
  }

  private VarValueSourceType getFromOriginType(final From aFrom) {
    if (aFrom.isVariableVal()) {
      return VarValueSourceType.VARIABLE;
    } else if (aFrom.isLiteralVal()) {
      if (checkIfItIsXML(aFrom.getAsLiteralVal())) {
        return VarValueSourceType.LITERAL_XML;
      }
      return VarValueSourceType.LITERAL;
    } else {
      final String textValue = aFrom.getAsExpression().getTextValue();
      if (textValue != null) {
        try {
          new XPathConditionStatement(textValue);
          return VarValueSourceType.XPATH;
        } catch (XPathExpressionException ignored) {

        }
      }

      return VarValueSourceType.TEXT;
    }
  }

  private String getFromValue(final From from, final CompilerState compilerState) {
    if (from.isLiteralVal()) {
      if (checkIfItIsXML(from.getAsLiteralVal())) {
        return from.getAsLiteralVal().toString();
      } else {
        return from.getAsLiteralVal().getTextValue().trim();
      }
    } else if (from.isVariableVal()) {
      return "$" + from.getAsVariableVal().getVariable();
    } else {
      return from.getTextValue().trim();
    }
  }

  private String getVariableName(final To to, final CompilerState compilerState) {
    if (to.isVariableVal()) {
      return to.getAsVariableVal().getVariable();
    } else if (to.isPropertyVal()) {
      return to.getAsPropertyVal().getVariable();
    } else if (to.isExtensionVal()) {
      return to.getAsExtensionVal().getVariable();
    } else if (to.isPartnerLinkVal()) {
      throw new IllegalStateException(to + " is a PartnerLink variant <- unhandled");
    } else {
      //maybe an expression

      throw new IllegalStateException("<to> with expression: not yet implemented");
    }
  }

  private Collection<String> getVariables(final From from) {
    final String textValue = from.getAsExpression().getTextValue();
    if (textValue != null) {
      try {
        final ConditionStatement x = new XPathConditionStatement(textValue);
        return x.getVariables();
      } catch (XPathExpressionException ignored) {

      }
    }

    return new ArrayList<>();
  }

  private boolean hasMoreThanOneVariable(final From from) {
    if (!isVariableInvolved(from)) {
      return false;
    }
    final String textValue = from.getAsExpression().getTextValue();
    if (textValue != null) {
      try {
        final ConditionStatement x = new XPathConditionStatement(textValue);
        return x.getVariables().size() > 1;
      } catch (XPathExpressionException ignored) {

      }
    }
    return false;
  }

  private boolean isVariableInvolved(final From from) {
    if (from.isVariableVal()) {
      return true;
    }
    if (from.isLiteralVal() || from.isExtensionVal() || from.isPartnerLinkVal()) {
      return false;
    }

    try {
      final ConditionStatement x = new XPathConditionStatement(from.getAsExpression().getTextValue());
      return false == x.getVariables().isEmpty();
    } catch (XPathExpressionException e) {
      logger.error(LogMarker.BPEL, e.getLocalizedMessage(), e);
    }

    return false;
  }
}
