package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public class DefaultB2IBinaryFormula extends DefaultB2INaryFormula implements B2IBinaryFormula {

  protected DefaultB2IBinaryFormula(
      final Function function,
      final B2IFormula left,
      final B2IFormula right
  ) {
    super(function, left, right);
  }

  @Override
  public B2IFormula getLeft() {
    return terms[0];
  }

  @Override
  public B2IFormula getRight() {
    return terms[1];
  }
}
