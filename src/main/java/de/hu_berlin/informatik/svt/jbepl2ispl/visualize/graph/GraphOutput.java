package de.hu_berlin.informatik.svt.jbepl2ispl.visualize.graph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph.AnalysisGraph;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph.FlowStateWrapper;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph.FlowTransitionWrapper;
import de.hu_berlin.informatik.svt.jbepl2ispl.visualize.VisualOutputAutomaton;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.parse.Parser;
import lombok.NonNull;
import org.jgrapht.Graph;
import org.jgrapht.io.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Christoph Graupner on 2018-03-05.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
@Service
public class GraphOutput implements VisualOutputAutomaton {
  abstract class GraphAttributeProviderFactory {
    abstract ComponentAttributeProvider<FlowTransitionWrapper> createEdgeAttributeProvider();

    ComponentNameProvider<FlowTransitionWrapper> createEdgeLabelProvider() {
      return FlowTransitionWrapper::getGuard;
    }

    abstract ComponentAttributeProvider<FlowStateWrapper> createVertexAttributeProvider();

    ComponentNameProvider<FlowStateWrapper> createVertexIdNameProvider() {
      return v -> String.valueOf(v.getId());
    }

    ComponentNameProvider<FlowStateWrapper> createVertexLabelProvider() {
      return FlowStateWrapper::getDisplayName;
    }
  }
//  @Autowired    GraphDatabaseService databaseService;

  private class GraphVizProviderFactory extends GraphAttributeProviderFactory {
    ComponentAttributeProvider<FlowTransitionWrapper> createEdgeAttributeProvider() {
      return e -> {
        Map<String, Attribute> m     = new HashMap<>();
        final String           value = e.getGuard();
        if (value != null) {
          m.put("label", DefaultAttribute.createAttribute(value));
        }
        if (e.getIsplActionName() != null) {
          m.put("edgetooltip", DefaultAttribute.createAttribute(e.getIsplActionName().toString()));
          m.put("labeltooltip", DefaultAttribute.createAttribute(e.getIsplActionName().toString()));
          m.put("xlabel", DefaultAttribute.createAttribute(e.getIsplActionName().toString()));
        }
        return m;
      };
    }

    ComponentAttributeProvider<FlowStateWrapper> createVertexAttributeProvider() {
      return v -> {
        Map<String, Attribute> m     = new HashMap<>();
        String                 color = null;
        String                 style = "solid";
        if (v.getCausingActivity() != null) {
          switch (v.getCausingActivity()) {
            case FLOW:
              color = "magenta";
              style = "filled";
              m.put(
                  "shape",
                  DefaultAttribute.createAttribute("tab")
              );
              break;
            case IF:
              style = "filled";
            case IFCASE_ELSE:
            case IFCASE_ELSEIF:
              color = "yellow";
              m.put(
                  "shape",
                  DefaultAttribute.createAttribute("diamond")
              );
              break;
            case WHILE:
              color = "cyan";
              style = "filled";
              m.put(
                  "shape",
                  DefaultAttribute.createAttribute("note")
              );

              break;
            case PICK:
            case PICK_ONALARM:
            case PICK_ONMSG:
              color = "blue";
              style = "filled";
              m.put(
                  "shape",
                  DefaultAttribute.createAttribute("tab")
              );

              break;
            case INVOKE:
              color = "red";
              style = "filled";
              m.put(
                  "shape",
                  DefaultAttribute.createAttribute("parallelogram")
              );
              break;
            case RECEIVE:
              color = "green";
              style = "filled";
              m.put(
                  "shape",
                  DefaultAttribute.createAttribute("parallelogram")
              );

              break;
            case SEQUENCE:
              style = "dotted";
              color = "grey";
              m.put(
                  "fontcolor",
                  DefaultAttribute.createAttribute(color)
              );
              break;
            case ASSIGN:
            default:
              break;
          }
          if (color != null) {
            m.put(
                "color",
                DefaultAttribute.createAttribute(color)
            );
            m.put(
                "style",
                DefaultAttribute.createAttribute(style)
            );
          }
        }
        m.put("name", DefaultAttribute.createAttribute("node-" + v.getDisplayName()));
        if (v.getBpelPathAsString() != null) {
          m.put("tooltip", DefaultAttribute.createAttribute(v.getBpelPathAsString()));
        }
        if (v.getEntryActionName() != null) {
          m.put("xlabel", DefaultAttribute.createAttribute(v.getEntryActionName().toString()));
        }
        return m;
      };
    }
  }

  private class GraphMlProviderFactory extends GraphAttributeProviderFactory {
    ComponentAttributeProvider<FlowTransitionWrapper> createEdgeAttributeProvider() {
      return e -> {
        Map<String, Attribute> m     = new HashMap<>();
        final String           value = e.toString();
        if (value != null) {
          m.put("name", DefaultAttribute.createAttribute(e.toString()));
        }
        return m;
      };
    }

    ComponentAttributeProvider<FlowStateWrapper> createVertexAttributeProvider() {
      return v -> {
        Map<String, Attribute> m = new HashMap<>();
        m.put("name", DefaultAttribute.createAttribute("node-" + v.getDisplayName()));
        return m;
      };
    }

    private ComponentNameProvider<FlowTransitionWrapper> createEdgeIdProvider() {
      return new IntegerComponentNameProvider<>();
    }
  }

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public void render(
      AnalysisGraph graph, String filename
  ) throws IOException, ExportException {

//    exportToGraphML(graph, filename);

    exportToSVG(graph, filename);
  }

//  private String createGraphViz(
//      GraphDatabaseService graph,
//      GraphStyle graphStyle
//  ) {
//    try (Transaction tx = graph.beginTx()) {
//      GraphvizWriter        writer = new GraphvizWriter(graphStyle);
//      ByteArrayOutputStream out    = new ByteArrayOutputStream();
//      try {
//        writer.emit(out, Walker.fullGraph(graph));
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
//
//      tx.success();
//
//      try {
//        return "digraph G {\n" +
//               out.toString(StandardCharsets.UTF_8.name())
//               + "}"
//            ;
//      } catch (UnsupportedEncodingException e) {
//        throw new RuntimeException(e);
//      }
//    }
//  }

  private void exportToGraphML(
      @NonNull final Graph<FlowStateWrapper, FlowTransitionWrapper> aGraph, @NonNull final String aFilename
  )
      throws IOException, ExportException {
    GraphMlProviderFactory providerFactory = new GraphMlProviderFactory();
    GraphMLExporter<FlowStateWrapper, FlowTransitionWrapper> exporter = new GraphMLExporter<>(
        providerFactory.createVertexIdNameProvider(),
        providerFactory.createVertexLabelProvider(),
        providerFactory.createVertexAttributeProvider(),
        providerFactory.createEdgeIdProvider(),
        providerFactory.createEdgeLabelProvider(),
        providerFactory.createEdgeAttributeProvider()
    );
    /*
     * Set to export the internal edge weights
     */
    exporter.setExportEdgeWeights(true);

    /*
     * Register additional color attribute for vertices
     */
    exporter.registerAttribute("color", GraphMLExporter.AttributeCategory.NODE, AttributeType.STRING);

    /*
     * Register additional name attribute for vertices and edges
     */
    exporter.registerAttribute("name", GraphMLExporter.AttributeCategory.ALL, AttributeType.STRING);

    final File graphMlFile = new File(aFilename + ".graphml");
    Writer     writer      = new FileWriter(graphMlFile);
    exporter.exportGraph(aGraph, writer);
    writer.close();
    logger.info("Graph written as GraphML to: " + graphMlFile.getAbsolutePath());
  }

  private void exportToSVG(final AnalysisGraph aGraph, final String aFilename)
      throws ExportException, IOException {
    GraphVizProviderFactory providerFactory = new GraphVizProviderFactory();
    GraphExporter<FlowStateWrapper, FlowTransitionWrapper> exporter1 = new DOTExporter<>(
        providerFactory.createVertexIdNameProvider(),
        providerFactory.createVertexLabelProvider(),
        providerFactory.createEdgeLabelProvider(),
        providerFactory.createVertexAttributeProvider(),
        providerFactory.createEdgeAttributeProvider()
    );
    Writer stringWriter = new StringWriter();
    exporter1.exportGraph(aGraph.getGraph(), stringWriter);
    final String dot = stringWriter.toString();

    final File dotFile = new File(aFilename + ".dot");
    dotFile.delete();
    PrintWriter out = new PrintWriter(dotFile);
    out.print(dot);
    out.close();
    logger.info("Graph written as DOT to: " + dotFile.getAbsolutePath());

    MutableGraph g       = Parser.read(dot);
    final File   svgFile = new File(aFilename + ".svg");
    Graphviz.fromGraph(g).width(700).render(Format.SVG).toFile(svgFile);
    logger.info("Graph written as SVG to: " + svgFile.getAbsolutePath());
  }
}
