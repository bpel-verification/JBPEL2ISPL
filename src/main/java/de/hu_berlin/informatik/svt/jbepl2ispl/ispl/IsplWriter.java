package de.hu_berlin.informatik.svt.jbepl2ispl.ispl;

import com.google.common.io.Files;
import com.google.inject.Guice;
import com.google.inject.Injector;
import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplBuilder;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.impl.SimpleIsplBuilder;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentVariableDefinition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2ICompareExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpressionTerm;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.Jb2iUtils;
import de.hu_berlin.informatik.svt.mcmas.ispl.ISPLRuntimeModule;
import de.hu_berlin.informatik.svt.mcmas.ispl.ISPLStandaloneSetup;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.*;
import lombok.NonNull;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.LinkedList;

/**
 * Created by Christoph Graupner on 2018-02-21.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */

public class IsplWriter {

  final         ISPLFactory           factory;
  final         AutomatonModel        model;
  private final IsplAgentWriter       agentWriter;
  private final IsplEnvironmentWriter envWriter;
  private final Logger                logger = LoggerFactory.getLogger(getClass());

  IsplWriter(
      @NonNull final ISPLFactory isplFactory,
      @NonNull final AutomatonModel model
  ) {
    this.factory = isplFactory;
    this.model = model;
    agentWriter = new IsplAgentWriter(this, factory);
    envWriter = new IsplEnvironmentWriter(this, factory);
  }

  public void write(@NonNull File outputFile) throws IOException {
    final Path tempFile = java.nio.file.Files.createTempFile("jbpel2ispl-", ".ispl");

    final InterpreterSystem system = constructInterpreterSystem();
    new ISPLStandaloneSetup().createInjectorAndDoEMFRegistration();
    Injector injector = Guice.createInjector(new ISPLRuntimeModule());

    ResourceSet rs   = injector.getInstance(XtextResourceSet.class);
    URI         uri1 = URI.createFileURI(tempFile.toString());
    Resource    rs1  = rs.createResource(uri1);

    rs1.getContents().add(system);
    rs1.save(null);

//    Serializer serializer = injector.getInstance(Serializer.class);
//    String     s          = serializer.serialize(system);
    if (Files.getFileExtension(outputFile.toString()).equals(Consts.FILE_EXTENSION_ISPL)) {
      outputFile = new File(outputFile.getAbsolutePath() + Consts.FILE_EXTENSION_ISPL);
    }

    java.nio.file.Files.write(
        outputFile.toPath(),
        Jb2iUtils.normalizeISPL(java.nio.file.Files.readAllLines(tempFile))
    );
    logger.info("ISPL written to {}", outputFile);
  }

  IdLiteral asIdLiteral(final String name) {
    final IdLiteral ret = factory.createIdLiteral();
    ret.setValue(name);
    return ret;
  }

  <T> T getLiteral(@NonNull Object obj) {
    if (obj instanceof B2IExpressionTerm) {
      obj = ((B2IExpressionTerm) obj).getTerm();
    }
    if (obj instanceof Boolean) {
      final BoolValue boolValue = factory.createBoolValue();
      boolValue.setValue(getBoolEnumValueOf((Boolean) obj));
      return (T) boolValue;
    } else if (obj instanceof Number) {
      final NumberLiteral numLiteral = factory.createNumberLiteral();
      numLiteral.setValue(((Number) obj).intValue());
      return (T) numLiteral;
    } else /* if (obj instanceof String) */ {
      final String toString = obj.toString();
      if (toString.equalsIgnoreCase("true") || toString.equalsIgnoreCase("false")) {
        final BoolValue boolValue = factory.createBoolValue();
        boolValue.setValue(getBoolEnumValueOf(Boolean.valueOf(toString)));
        return (T) boolValue;
      }
      try {
        int                 x          = Integer.valueOf(toString);
        final NumberLiteral numLiteral = factory.createNumberLiteral();
        numLiteral.setValue(x);
        return (T) numLiteral;
      } catch (NumberFormatException ex) {
        if (obj.toString().contains(".")) {
          throw new IllegalStateException(obj + " has a '.' in it");
        }

        final IdLiteral literal = factory.createIdLiteral();
        literal.setValue(translateToIsplValue(toString));
        return (T) literal;
      }
    }
  }

  LogicOperator getLogicOp(final B2ICompareExpression.CompareType type) {
    switch (type) {
      case EQ:
        return LogicOperator.EQ;
      case GT:
        return LogicOperator.GT;
      case GE:
        return LogicOperator.GE;
      case LT:
        return LogicOperator.LT;
      case LE:
        return LogicOperator.LE;
    }
    return null;
  }

  String translateToIsplName(@NonNull final String name) {
    return name.replaceAll("[#~\\-<>() @$§=?/\\\\]", "_");
  }

  String translateToIsplValue(@NonNull final String s) {
    return translateToIsplName(s);
  }

  private InitialStates appendInitialStates(final InterpreterSystem system) {
    final InitialStates         initialStates = factory.createInitialStates();
    LinkedList<IsBoolCondition> values        = new LinkedList<>();

    model.getAllAgentAutomatons().stream().sorted(Comparator.comparingInt(Identifiable::getElementId)).forEachOrdered(
        agentAutomaton -> {
          agentAutomaton.getVariables().stream().sorted().forEachOrdered(
              var -> {
                IsBoolConditionCompare             compare      = factory.createIsBoolConditionCompare();
                AgentVarRef                        operand      = factory.createAgentVarRef();
                IsBoolConditionCompareOperandOther operandOther = null;

                final AgentVariableDefinition agentVarDef = agentAutomaton.getVariableDefinition(var);
                logger.debug("Initial Value of {}: {}", var, agentVarDef.getInitialValue());
                if (agentVarDef.hasInitialValue()) {
                  switch (agentVarDef.getValueType()) {
                    case ENUM:
                      operandOther = factory.createIdLiteral();
                      ((IdLiteral) operandOther).setValue(
                          translateToIsplValue(agentVarDef.getInitialValue())); //FIXME get the real varvalue
                      break;
                    case BOOLEAN:
                      operandOther = factory.createBoolValue();
                      ((BoolValue) operandOther).setValue(getBoolEnumValueOf(agentVarDef.getInitialValue()));
                      break;
                    case INT:
                      operandOther = factory.createNumberLiteral();
                      //FIXME get the real varvalue
                      ((NumberLiteral) operandOther).setValue(
                          Integer.valueOf(agentVarDef.getInitialValue())
                      );
                      break;
                    default:
                      logger.error(LogMarker.TODO_EXCEPTION, "Implement the case {}", agentVarDef.getValueType());
                      break;
                  }
                } else {
                  logger.warn(
                      LogMarker.MODEL_PROBLEM, "Variable '{}' has no initial value.\nVarDef: {}", var, agentVarDef);
                  operandOther = factory.createIdLiteral();
                  ((IdLiteral) operandOther).setValue(translateToIsplValue("INITIAL_VALUE_MISSING"));
                }

                operand.setVarName(translateToIsplName(var));
                operand.setAgent(translateToIsplName(agentAutomaton.getName()));

                compare.setOperand1(operand);
                compare.setOperand2(operandOther);

                values.add(compare);
              }
          );
        }
    );

    //recursive generation with AND

    IsBoolConditionAnd rootAnd = factory.createIsBoolConditionAnd();
    IsBoolConditionAnd iter    = rootAnd;
    iter.setLeft(values.removeFirst());
    IsBoolCondition last = values.removeLast();

    for (IsBoolCondition isBoolCondition: values) {
      IsBoolConditionAnd tmp = factory.createIsBoolConditionAnd();
      tmp.setLeft(isBoolCondition);

      IsBoolConditionParenthesized parenthesized = factory.createIsBoolConditionParenthesized();
      parenthesized.setCondition(tmp);

      iter.setRight(parenthesized);
      iter = tmp;
    }
    iter.setRight(last);

    initialStates.setCondition(rootAnd);

    system.setInitialState(initialStates);
    return initialStates;
  }

  private InterpreterSystem constructInterpreterSystem() {
    IsplBuilder builder = new SimpleIsplBuilder(factory);

    envWriter.buildEnvironment(builder.environment());
    agentWriter.buildAgents(builder.agents());
    final InterpreterSystem system = builder.build();
    system.setEvaluation(new IsplEvaluationWriter(factory, this).createEvaluation());
    appendInitialStates(system);
    system.setFormulae(new IsplFormulaWriter(factory, this).createFormulae());

    return system;
  }

  private BoolEnum getBoolEnumValueOf(final Boolean x) {
    if (x) {
      return BoolEnum.TRUE;
    }
    return BoolEnum.FALSE;
  }

  private BoolEnum getBoolEnumValueOf(final String initialValue) {
    if (Boolean.valueOf(initialValue)) {
      return BoolEnum.TRUE;
    }
    return BoolEnum.FALSE;
  }
}
