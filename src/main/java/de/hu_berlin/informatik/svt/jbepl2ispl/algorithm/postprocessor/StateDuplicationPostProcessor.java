package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.AssignBranchValueGuessStrategy;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.MessagingType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Christoph Graupner on 2018-06-27.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
class StateDuplicationPostProcessor extends FlowModelPostProcessor {

  @Getter
  private final AssignBranchValueGuessStrategy communicationTypeDetector;

  StateDuplicationPostProcessor(
      final FlowModel flowModel,
      final AssignBranchValueGuessStrategy communicationTypeDetector
  ) {
    super(flowModel);
    this.communicationTypeDetector = communicationTypeDetector;
  }

  @Override
  public void finish() {
    deriveAssignBranchStatesAndActionNames();
  }

  private void deriveAssignBranchStatesAndActionNames() {
    logger.trace(LogMarker.POSTPROCESSING, "Deriving <assign> branches and action names...");
    for (final FlowState state: flowModel.findStatesWithActivity(Activity.ASSIGN)) {
      switch (getMessagingType(state)) {
        case LOCAL:
          break;

        case DIRECT:
          break;
        case INDIRECT:
          logger.debug(LogMarker.POSTPROCESSING, "Indirect sending... duplicating states...");
          if (state.isInitialState()) {
            logger.error(LogMarker.MODEL_PROBLEM, "Something is really wrong with {}", state);
          }

          //TODO duplicate state and name the actions
          Set<String> values = getCommunicationTypeDetector().guessAssignBranchValues(state);
          if (values.size() <= 0) {
            logger.warn(LogMarker.POSTPROCESSING,
                        "For indirect communication there should be at least two values guessed for state {}", state
            );
          }
          final Iterator<String> it = values.iterator();
          //just do it for the original state
          String firstValue = it.next();

          //for all other states
          it.forEachRemaining(
              value -> {
                FlowState newState = duplicateState(
                    state,
                    value
                );

                flowModel.save(flowModel.factory().createAlternativeLink(state, newState));

                flowModel.save(newState);
              });

          //do original state modifications after cloning, otherwise we would influence the clones
          final Collection<FlowVarWriteAccessRelation> writeAccessRelations =
              flowModel.findAllVarWriteAccessRelationsForState(state);
          writeAccessRelations.forEach(
              relation -> {
                relation.setValue(firstValue);
                flowModel.save(relation);
              }
          );

          state.setName(state.getPlainName() + Consts.ISPL_STATE_NAME_DELEMITTER + firstValue);
          flowModel.save(state);

          break;
      }
    }
    logger.trace(LogMarker.POSTPROCESSING, "DONE Deriving <assign> branches and action names...");
  }

  private FlowState duplicateState(final FlowState state, String value) {
    logger.debug(LogMarker.POSTPROCESSING, "Duplicating State '{}'...", state);
    //duplicate itself
    FlowState newState = flowModel.factory().cloneState(
        state, state.getPlainName() + Consts.ISPL_STATE_NAME_DELEMITTER + value
    );

    flowModel.save(newState);
    //duplicate connections
    for (final FlowTransition incomingTransition: flowModel.findAllIncomingTransitions(state)) {
      flowModel.save(flowModel.factory()
                              .createTransition(
                                  incomingTransition.getSourceState(),
                                  newState,
                                  incomingTransition.getGuard(),
                                  incomingTransition.getName() + Consts.ISPL_VAR_NAME_SEPARATOR + value,
                                  incomingTransition.getBpelPathAsString()
                              )
      );
    }
    for (final FlowTransition outboundTransition: flowModel.findAllOutgoingTransitions(state)) {
      flowModel.save(flowModel.factory()
                              .createTransition(
                                  newState,
                                  outboundTransition.getTargetState(),
                                  outboundTransition.getGuard(),
                                  outboundTransition.getName() + Consts.ISPL_VAR_NAME_SEPARATOR + value,
                                  outboundTransition.getBpelPathAsString()
                              )
      );
    }

    final Collection<FlowVarReadAccessRelation> variablesReadingFrom =
        flowModel.findAllVarReadAccessRelationsForState(state);
    if (variablesReadingFrom != null) {
      variablesReadingFrom.forEach(
          relation -> flowModel.save(
              flowModel.factory()
                       .createVarReadAccessRelation(
                           newState,
                           relation.getVariable(),
                           relation.getValue(),
                           relation.getValueSource(),
                           relation.getValueSourceType(),
                           relation.getBpelPathAsString()
                       )
          )
      );
    }

    flowModel.findAllVarWriteAccessRelationsForState(state).forEach(
        relation -> flowModel.save(
            flowModel.factory()
                     .createVarWriteAccessRelation(
                         newState,
                         relation.getVariable(),
                         value,
                         relation.getValueSource(),
                         relation.getValueSourceType(),
                         relation.getBpelPathAsString()
                     )
        )
    );

    flowModel.findAllMessagingSendForState(state).forEach(
        flowMessageSend -> {
          FlowMessageSend clone = flowModel.factory().cloneMessageSend(flowMessageSend);
          if (state.equals(flowMessageSend.getValueReceivingState())) {
            clone.setValueReceivingState(newState);
          }
          if (state.equals(flowMessageSend.getValueSendingState())) {
            clone.setValueSendingState(newState);
          }
          if (flowMessageSend.getIntermediateReceivingState().contains(state)) {
            flowMessageSend.removeIntermediateReceivingState(state);
            flowMessageSend.addIntermediateReceivingState(newState);
          }
          if (flowMessageSend.getIntermediateSendingState().contains(state)) {
            flowMessageSend.removeIntermediateSendingState(state);
            flowMessageSend.addIntermediateSendingState(newState);
          }
          flowModel.save(clone);
        }
    );

    logger.debug(LogMarker.POSTPROCESSING, "DONE Duplicating State. Result: '{}'...", newState);

    return newState;
  }

  private MessagingType getMessagingType(final FlowState state) {
    return MessagingType.valueOf(state.getProperty(Consts.PROPERTY_MESSAGING_TYPE));
  }
}
