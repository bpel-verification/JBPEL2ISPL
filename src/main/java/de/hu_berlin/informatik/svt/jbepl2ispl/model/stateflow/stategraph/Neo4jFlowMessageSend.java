package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.MessagingType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowMessageSend;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NodeEntity(label = Naming.FLOW_MESSAGING)
@NoArgsConstructor
public class Neo4jFlowMessageSend extends FlowBaseBpel implements FlowMessageSend {
  @Getter
  @Id
  @GeneratedValue
  Long id;

  @Getter
  MessagingType type;
  @Getter
  @Setter
  private String     bufferVariableName;
  @Getter
  @Setter
  private String     commChannelValue;
  @Getter
  @Setter
  private String     commChannelVariableName;
  @Getter
  @Relationship(type = Naming.RELATION_FLOW_MSG_INTERMEDIATE_STATE_TO)
  private Set<State> intermediateReceivingStates = new HashSet<>();
  @Relationship(type = Naming.RELATION_FLOW_MSG_INTERMEDIATE_STATE_FROM)
  @Getter
  private Set<State> intermediateSendingStates   = new HashSet<>();
  @Relationship(type = Naming.RELATION_FLOW_MSG_ORIGIN_STATE)
  @Getter
  private State      originState;
  @Relationship(type = Naming.RELATION_FLOW_MSG_RECEIVER)
  @Getter
  private Service    receivingService;
  @Getter
  @Setter
  private String     sendVariableName;
  @Relationship(type = Naming.RELATION_FLOW_MSG_SENDER)
  @Getter
  private Service    sendingService;
  @Relationship(type = Naming.RELATION_FLOW_MSG_VALUE_TO)
  @Getter
  private State      valueReceivingState;
  @Relationship(type = Naming.RELATION_FLOW_MSG_VALUE_FROM)
  @Getter
  private State      valueSendingState;

  Neo4jFlowMessageSend(
      final State originState, final State valueSendingState, final State valueReceivingState, final MessagingType type,
      final int elementId
  ) {
    super(elementId, "/");
    this.type = type;
    this.valueSendingState = valueSendingState;
    this.valueReceivingState = valueReceivingState;
    this.originState = originState;
  }

  Neo4jFlowMessageSend(final Neo4jFlowMessageSend messageSend, final int alternativeCount) {
    super(messageSend.getElementId(), messageSend.bpelPathAsString);
    super.cloneBase(messageSend);
    this.alternativeCount = alternativeCount;
    this.type = messageSend.type;
    this.valueSendingState = messageSend.valueSendingState;
    this.valueReceivingState = messageSend.valueReceivingState;
    this.originState = messageSend.originState;
    this.bufferVariableName = messageSend.bufferVariableName;
    this.sendingService = messageSend.sendingService;
    this.receivingService = messageSend.receivingService;
    this.commChannelVariableName = messageSend.commChannelVariableName;
    this.intermediateReceivingStates = new HashSet<>(messageSend.intermediateReceivingStates);
    this.intermediateSendingStates = new HashSet<>(messageSend.intermediateSendingStates);
    this.sendVariableName = messageSend.sendVariableName;
    this.name = messageSend.name;
  }

  @Override
  public void addIntermediateReceivingState(@NonNull final FlowState flowState) {
    intermediateReceivingStates.add((State) flowState);
  }

  @Override
  public void addIntermediateSendingState(@NonNull final FlowState flowState) {
    intermediateSendingStates.add((State) flowState);
  }

  @Override
  public Collection<State> getIntermediateReceivingState() {
    return intermediateReceivingStates;
  }

  @Override
  public Collection<State> getIntermediateSendingState() {
    return intermediateSendingStates;
  }

  @Override
  public FlowState getOriginAssignState() {
    return originState;
  }

  @Override
  public void removeIntermediateReceivingState(@NonNull final FlowState state) {
    intermediateSendingStates.remove(state);
  }

  @Override
  public void removeIntermediateSendingState(final FlowState state) {
    intermediateSendingStates.remove(state);
  }

  @Override
  public void setReceivingService(final FlowService receivingService) {
    this.receivingService = (Service) receivingService;
  }

  @Override
  public void setSendingService(final FlowService sendingService) {
    this.sendingService = (Service) sendingService;
  }

  @Override
  public void setValueReceivingState(final FlowState state) {
    this.valueReceivingState = (State) state;
  }

  @Override
  public void setValueSendingState(final FlowState state) {
    this.valueSendingState = (State) state;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("FlowMessageSend{");
    sb.append("elementId=").append(elementId);
    sb.append(", type=").append(type);
    sb.append(", originState=").append(originState);
    sb.append(", valueFromState=").append(valueSendingState);
    sb.append(", valueToState=").append(valueReceivingState);
    sb.append(", intermediateFromStates=").append(intermediateSendingStates);
    sb.append(", intermediateToStates=").append(intermediateReceivingStates);
    sb.append(", receivingService=").append(receivingService);
    sb.append(", sendingService=").append(sendingService);
    sb.append('}');
    return sb.toString();
  }
}
