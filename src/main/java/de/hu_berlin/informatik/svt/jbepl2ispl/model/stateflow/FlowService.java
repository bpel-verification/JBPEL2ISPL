package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;

import java.util.Set;

/**
 * A FlowService is a WebService that is either defined as a <partnerLink> in
 * BPEL or the BPEL process itself (a.k.a. Environment).
 * <p>
 * In the world of ISPL it will be mapped as an "Agent".
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public interface FlowService extends BpelIdentifiable, FlowAdditional {
  String ENV = Consts.AGENT_ENV_NAME;

  FlowService addOperation(FlowServiceOperation operation);

  void addVariable(String variable);

  String getName();

  <T extends FlowServiceOperation> Set<T> getOperations();

  Set<String> getVariables();
}
