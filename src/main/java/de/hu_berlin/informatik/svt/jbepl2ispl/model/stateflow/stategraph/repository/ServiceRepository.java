package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.repository;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.Service;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
public interface ServiceRepository extends Neo4jRepository<Service, Long> {
  @Query(
      "MATCH p=allShortestPaths((s1:State)-[:FLOW_WRITE_TO|FLOW_READS_FROM*1..200]->(s2:State {causingActivity: \"INVOKE\"}))" +
      " WHERE id(s1)<>id(s2) and (s1.causingActivity =\"ASSIGN\" or s1.causingActivity =\"RECEIVE\")" +
      " WITH s1,s2" +
      " MATCH (s1)-[:SOURCE_SERVICE|TARGET_SERVICE]->(srv:Service), (s2)-[:TARGET_SERVICE|SOURCE_SERVICE]->(srv2:Service)" +
      " WHERE id(srv)<>id(srv2) and srv.name= {agentName} and srv2.name<>\"Environment\"" +
      " RETURN DISTINCT srv2")
  Service findAgentsItTalksTo(@Param("agentName") String agentName);

  @Query("MATCH (s:Service)-[:FL_RECEIVER|FL_SENDER]-(m:Messaging)-[:FL_RECEIVER|FL_SENDER]-(st:Service)" +
         " WHERE st.name={serviceName} AND id(st)<>id(s)" +
         " RETURN DISTINCT s")
  Collection<Service> findAllServicesItTalksToForService(@Param("serviceName") String serviceName);

  FlowService findByName(String serviceName);

  @Query(" MATCH (s1:State)-[:FLOW_WRITE_TO]->(v:Variable {name: {varName}})" +
         " WITH s1,v" +
         " MATCH p=shortestPath((v)-[:FLOW_WRITE_TO|FLOW_READS_FROM*1..200]->(s2:State))" +
         " WHERE id(s1)<>id(s2) and id(s2)={startingStateId}" +
         " WITH s1,s2,relationships(p) as rl" +
         " WHERE ALL(idx in range(0, size(rl)-2) WHERE (rl[idx]).elementId > 1 AND (rl[idx]).elementId < (rl[idx+1]).elementId)" +
         " WITH s1,s2" +
         " MATCH (s1)-[:SOURCE_SERVICE|TARGET_SERVICE]->(srv1:Service)" +
         " MATCH (s2)-[:TARGET_SERVICE|SOURCE_SERVICE]->(srv2:Service)" +
         " WHERE srv1.name<>\"Environment\" AND srv2.name<>\"Environment\"" +
         " RETURN srv1" +
         " ORDER BY s1.elementId DESC" +
         " LIMIT 1")
  Service findFirstStateWhichWritesToBackwardFrom(
      @Param("varName") String variableName, @Param("startingStateId") long startingStateId
  );
}
