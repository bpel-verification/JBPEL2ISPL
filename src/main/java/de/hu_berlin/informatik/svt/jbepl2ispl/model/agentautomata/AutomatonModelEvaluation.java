package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae.B2IFormula;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;

import java.util.List;
import java.util.Set;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public interface AutomatonModelEvaluation {

  void addFormula(B2IFormula formula);

  boolean areNeighborStates(FlowState state1, FlowState state2);

  List<B2IFormula> getFormulas();

  B2IExpression getPropositionDefinition(final String name);

  Set<String> getPropositionNames();

  void setProposition(String name, B2IExpression definition);
}
