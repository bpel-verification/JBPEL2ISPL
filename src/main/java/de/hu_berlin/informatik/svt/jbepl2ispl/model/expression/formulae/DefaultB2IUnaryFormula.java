package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public class DefaultB2IUnaryFormula extends DefaultB2INaryFormula implements B2IUnaryFormula {

  DefaultB2IUnaryFormula(final Function function, final B2IFormula expression) {
    super(function, expression);
  }

  @Override
  public B2IFormula getTerm() {
    return terms[0];
  }
}
