package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.apache.ode.bpel.compiler.bom.ReplyActivity;

import java.util.Arrays;

/**
 * Created by Christoph Graupner on 2018-05-11.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class ReplyHandler extends HandlerBase {
  public ReplyHandler() {
    super(Arrays.asList(ReplyActivity.class));
  }

  @Override
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    if (currentBpelObject instanceof ReplyActivity) {
      handleReply((ReplyActivity) currentBpelObject, compilerState);
    }
    return (T) this;
  }

  private void handleReply(final ReplyActivity replyActivity, final CompilerState compilerState) {
    final FlowModel        model       = compilerState.getFlowModel();
    final StateFlowFactory factory     = model.factory();
    final FlowService      flowService = model.findServiceByName(replyActivity.getPartnerLink());

    final FlowState state = factory.createState(
        replyActivity.getName(), "REPLY__" + replyActivity.getName() + "__" + replyActivity.getVariable(),
        Activity.REPLY,
        BeginEndType.BOTH,
        compilerState.getCurrentBpelPath()
    );
    if (replyActivity.getVariable() != null) {
      FlowVariable flowVariable = model.findVariableByName(replyActivity.getVariable());
      flowVariable.setProperty(Consts.PROPERTY_VAR_IS_EXTERNAL_BPEL_CLIENT_OUTPUT, Boolean.toString(true));
      flowVariable = model.save(flowVariable);
      state.setInputVariable(flowVariable.getName());

      model.save(state);

      flowService.addVariable(flowVariable.getName());

      // this is a replacement for FlowVariableValue
      final FlowVarReadAccessRelation readAccess = factory.createVarReadAccessRelation(
          state,
          flowVariable,
          "REPLY_OUTPUT__" + replyActivity.getName(),
          "REPLY_VALUE__" + replyActivity.getPartnerLink() + '#' + replyActivity.getOperation(),
          VarValueSourceType.REPLY,
          compilerState.getCurrentBpelPath()
      );
      model.save(readAccess);
    }

    FlowServiceOperation agentOperation = factory.createServiceOperation(
        flowService,
        replyActivity.getOperation(),
        "REPLY_VALUE",
        replyActivity.getVariable(),
        compilerState.getCurrentBpelPath()
    );
    agentOperation = model.save(agentOperation);

    flowService.addOperation(agentOperation);

    model.save(flowService);

    state.setTargetServiceOperation(agentOperation);
    model.save(state);
  }
}
