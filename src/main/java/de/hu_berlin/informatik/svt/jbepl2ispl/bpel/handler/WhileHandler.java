package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowTransition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.StateFlowFactory;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.apache.ode.bpel.compiler.bom.ForEachActivity;
import org.apache.ode.bpel.compiler.bom.WhileActivity;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Created by Christoph Graupner on 2018-03-04.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public class WhileHandler extends HandlerBase {
  Deque<FlowState> stack = new LinkedList<>();

  public WhileHandler() {
    super(Arrays.asList(WhileActivity.class, ForEachActivity.class));
  }

  @Override
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    if (currentBpelObject instanceof WhileActivity) {
      WhileActivity whileActivity = (WhileActivity) currentBpelObject;
      assert whileActivity.getCondition() != null; // the XSD requires a condition set

      final FlowModel        model   = compilerState.getFlowModel();
      final StateFlowFactory factory = model.factory();
      FlowState state = factory.createState(
          whileActivity.getName(),
          "WHILE__" + whileActivity.getName() + "__BEGIN",
          Activity.WHILE,
          BeginEndType.BEGIN,
          compilerState.getCurrentBpelPath()
      );
      model.save(state);
      stack.push(state);

      //inbound transition
      FlowTransition transition = factory.createTransition(
          model.findPreviousState(state),
          state,
          null,
          null,
          compilerState.getCurrentBpelPath()
      );
      model.save(transition);

      if (whileActivity.getCondition() != null) {
        createReadAccessForCondition(whileActivity.getCondition().getTextValue().trim(), state, model, compilerState);
      }
    }
    return super.handleBegin(currentBpelObject, compilerState);
  }

  @Override
  public <T extends HandlerBase> T handleEnd(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    if (currentBpelObject instanceof WhileActivity) {
      WhileActivity          whileActivity = (WhileActivity) currentBpelObject;
      final FlowModel        model         = compilerState.getFlowModel();
      final StateFlowFactory factory       = model.factory();

      FlowState endState = factory.createState(
          whileActivity.getName(),
          "//WHILE__" + whileActivity.getName() + "__END",
          Activity.WHILE,
          BeginEndType.END,
          compilerState.getCurrentBpelPath()
      );
      model.save(endState);

      final FlowState beginState = model.refresh(stack.pop(), 3);

      //outbound transition -> to while block
      final FlowState followingState = model.findFollowingState(beginState);

      FlowTransition transition = factory.createTransition(
          beginState,
          followingState,
          whileActivity.getCondition().getTextValue().trim(),
          "WHILE_ENTRY",
          compilerState.getCurrentBpelPath()
      );
      model.save(transition);

      if (followingState.getId() != endState.getId()) {
        //outbound transition -> skip while block
        transition = factory.createTransition(
            beginState,
            endState,
            whileActivity.getCondition().getTextValue().trim(),
            "WHILE_ELSE",
            compilerState.getCurrentBpelPath()
        );
        transition.setInvertGuard(true);
        model.save(transition);
      }

      //close loop transition
      transition = factory.createTransition(
          model.findPreviousState(endState),
          beginState,
          "//LOOP_CLOSE",
          "//LOOP_CLOSE",
          compilerState.getCurrentBpelPath()
      );
      transition.setProperty(Consts.PROPERTY_NAME_LOOP_CLOSE, whileActivity.getName());
      model.save(transition);
    }
    return super.handleEnd(currentBpelObject, compilerState);
  }
}
