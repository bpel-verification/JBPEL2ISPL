package de.hu_berlin.informatik.svt.jbepl2ispl.configuration;

import de.hu_berlin.informatik.svt.mcmas.ispl.ISPLStandaloneSetup;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.ISPLFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Christoph Graupner on 2018-04-23.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Configuration
class IsplConfiguration {

  @Bean
  ISPLFactory beanIsplFactory() {
    ISPLStandaloneSetup.doSetup();
    return ISPLFactory.eINSTANCE;
  }
}
