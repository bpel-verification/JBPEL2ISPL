package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowServiceOperation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Objects;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NodeEntity
@NoArgsConstructor
public class ServiceOperation extends FlowBaseBpel implements FlowServiceOperation {
  @Getter
  @Id
  @GeneratedValue
  private Long id;

  @Getter
  private String  inputVariableName;
  @Getter
  private String  name;
  @Getter
  private String  outputVariableName;
  @Getter
  @Relationship(type = Naming.RELATION_PROVIDES_OP, direction = Relationship.INCOMING)
  private Service providingService;

  ServiceOperation(
      final Service providingService,
      final String action, final String sendVariable, final String outputVariable, final int elementId,
      final String currentBpelPath
  ) {
    super(elementId, currentBpelPath);
    this.providingService = providingService;
    this.name = action;
    this.inputVariableName = sendVariable;
    this.outputVariableName = outputVariable;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof ServiceOperation)) {
      return false;
    }
    final ServiceOperation that = (ServiceOperation) o;
    return Objects.equals(getInputVariableName(), that.getInputVariableName()) &&
           Objects.equals(getName(), that.getName()) &&
           Objects.equals(getOutputVariableName(), that.getOutputVariableName());
  }

  @Override
  public int hashCode() {

    return Objects.hash(getInputVariableName(), getName(), getOutputVariableName());
  }
}
