package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.repository;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.State;
import org.springframework.data.neo4j.annotation.Depth;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
public interface StateRepository extends Neo4jRepository<State, Long> {

  @Query("MATCH (s:State) WHERE s.bpelPathAsString =~ {regEx} RETURN s ORDER BY s.elementId,s.alternativeCount")
  List<State> findAllByBpelPathRegEx(@Param("regEx") String name);

  @Depth(3)
  Collection<? extends FlowState> findAllByCausingActivity(Activity activityType);

  @Query(
      "MATCH (s:State)-[r:SOURCE_SERVICE]->(srv:Service {elementId:{serviceElementId}}) "
      + " RETURN s,r ORDER BY s.elementId"
  )
  Collection<State> findAllBySourceService(@Param("serviceElementId") int elementId);

  @Query(
      "MATCH (s:State)-[r:TARGET_SERVICE]->(srv:Service {elementId:{serviceElementId}}) "
      + " RETURN s,r ORDER BY s.elementId"
  )
  Collection<State> findAllByTargetService(@Param("serviceElementId") int serviceName);

//  @Query(
//      "MATCH (s:State),(srv:Service {name:'{serviceName}'}) "
//          + " WHERE (s)-[:SOURCE_SERVICE|TARGET_SERVICE]-(srv)"
//          + " RETURN s"// ORDER BY s.elementId
//  )

  @Query(
      "MATCH (s:State)-[r:SOURCE_SERVICE|TARGET_SERVICE]->(srv:Service {name:{serviceName}}) "
//          + " WHERE (s)-[r:SOURCE_SERVICE|TARGET_SERVICE]->(srv)"
      + " RETURN s,r ORDER BY s.elementId"
  )
  @Depth(3)
  List<State> findAllByTargetServiceOrBySourceService(@Param("serviceName") String serviceName);

  @Query("MATCH (s1:State)-[:FOLLOWED_BY]->(s2:State) WHERE id(s1) = {id} RETURN s2")
  List<State> findAllConnectedFollowingStates(@Param("id") Long id);

  @Query("MATCH (s1:State)-[:FOLLOWED_BY]->(s2:State) WHERE id(s2) = {id} RETURN s1")
  List<State> findAllConnectedPrecedingStates(@Param("id") Long id);

  @Query("MATCH p=((source:State)-[:FOLLOWED_BY*1..200]->(target:State))" +
         " WHERE id(source) = {sourceStateId} AND id(target) = {targetStateId}" +
         " RETURN nodes(p) as nodes, relationships(p) as relations")
  Iterable<Map<String, Object>> findAllPathsBetween(
      @Param("sourceStateId") final Long id, @Param("targetStateId") final Long aLong
  );

  @Query(
      "MATCH p=allShortestPaths((s1:State)-[:FOLLOWED_BY*]->(s2:State)) WHERE id(s1) = {sourceStateId} AND id(s2) = {targetStateId} RETURN nodes(p)")
  List<State> findAllStatesBetween(@Param("sourceStateId") Long beginStateId, @Param("targetStateId") Long endStateId);

  @Query("MATCH p=((source:State)-[:FOLLOWED_BY*1..200]->(target:State))" +
         " WHERE id(source) = {sourceStateId} AND id(target) = {targetStateId}" +
         " AND NONE(x IN NODES(p) WHERE x:State AND id(x) IN {excludedIds})" +
         " RETURN nodes(p) as nodes, relationships(p) as relations")
  Iterable<Map<String, Object>> findAllStatesBetweenExcluding(
      @Param("sourceStateId") final Long id, @Param("targetStateId") final Long aLong,
      @Param("excludedIds") Collection<Long> exclude
  );

  @Query(
      "MATCH (s:State {causingActivity: {activityName}})-[:TARGET_SERVICE]->(srv:Service) WHERE id(srv)= {flowServiceId} RETURN s")
  Collection<State> findAllStatesByActivityAndSourceService(
      @Param("activityName") Activity activity, @Param("flowServiceId") Long elementId
  );

  @Query("MATCH (s:State)-[:INVOLVES]->(:Service {name: {serviceName}}) RETURN s")
  List<State> findAllStatesByInvolvingLink(@Param("serviceName") String serviceName);

  @Query("MATCH (s1:State)-[:FLOW_WRITE_TO]->(varWrite:Variable)<-[:FLOW_WRITE_TO]-(s2:State)" +
         " WHERE (s1)-[:FOLLOWED_BY]-(s2)" +
         " WITH s1,s2,varWrite" +
         " MATCH (s1)<-[:FLOW_READS_FROM]-(varRead:Variable)" +
         " WHERE ((s2)<-[:FLOW_READS_FROM]-(varRead) OR (s2)<-[:FLOW_READS_FROM]-(:Variable {name: {varNameLiteral}}))" +
         " RETURN s1,s2,varWrite,varRead")
  Iterable<Map<String, Object>> findAllStatesWithSameVarWriteAndSameOrLiteralRead(@Param("varNameLiteral") String varNameLiteral);

  @Query("MATCH (s:State) WHERE s.elementId = {elementId} RETURN s")
  @Depth(3)
  State findFirstByElementId(@Param("elementId") int elementId);

  @Depth(2)
  State findFirstByElementIdAndAlternativeCount(int elementId, int alternateId);

  @Query("MATCH (s1:State)-[r:FLOW_WRITE_TO]->(v:Variable {name: {varName}})" +
         " WHERE id(s1) = {startingStateId}" +
         " WITH s1,v,r" +
         " MATCH p=shortestPath((v)-[:FLOW_READS_FROM|FLOW_WRITE_TO*1..200]->(s2:State))" +
         " WHERE id(s1)<>id(s2) AND s1.elementId < s2.elementId AND (s2.causingActivity IN [\"INVOKE\", \"REPLY\"])" +
         " WITH s2,relationships(p) as rl,r" +
         " WHERE ALL(idx in range(0, size(rl)-2) WHERE (rl[idx]).elementId > r.elementId AND (rl[idx]).elementId < (rl[idx+1]).elementId)" +
         " AND NONE( n IN nodes(p)[2..length(p)-1] WHERE n.causingActivity IN [ \"INVOKE\", \"REPLY\"])" +
         " RETURN s2" +
         " LIMIT 1")
  State findFirstInvokeOrReplyStateWhichReadsFromForwardFrom(
      @Param("varName") String variableName, @Param("startingStateId") long startingStateId
  );

  @Query(" MATCH (s1:State)-[:FLOW_WRITE_TO]->(v:Variable {name: {varName}})" +
//         " WHERE s1.causingActivity = \"INVOKE\" OR s1.causingActivity = \"RECEIVE\"" +
         " WITH s1,v" +
         " MATCH p=shortestPath((v)-[:FLOW_WRITE_TO|FLOW_READS_FROM*1..200]->(s2:State))" +
         " WHERE id(s1)<>id(s2) AND id(s2)={startingStateId} AND s1.elementId < s2.elementId" +
         " WITH s1,s2,relationships(p) as rl" +
         " WHERE ALL(idx IN range(0, size(rl)-2) WHERE (rl[idx]).elementId > 1 AND (rl[idx]).elementId < (rl[idx+1]).elementId)" +
         " AND NONE( n IN nodes(p)[2..length(p)-1] WHERE n.causingActivity IN [ \"INVOKE\", \"REPLY\"])" +
         " WITH s1,s2" +
//         " MATCH (s1)-[:SOURCE_SERVICE|TARGET_SERVICE]->(srv1:Service)" +
//         " MATCH (s2)-[:TARGET_SERVICE|SOURCE_SERVICE]->(srv2:Service)" +
//         " WHERE srv1.name<>\"Environment\" AND srv2.name<>\"Environment\"" +
         " RETURN s1" +
         " ORDER BY s1.elementId DESC" +
         " LIMIT 1")
  State findFirstStateWhichWritesToBackwardFrom(
      @Param("varName") String variableName, @Param("startingStateId") long startingStateId
  );

  @Query("MATCH (s:State) WHERE s.elementId > {elementId} RETURN s ORDER BY s.elementId ASC LIMIT 1")
  @Depth(3)
  State findFollowingByElementId(@Param("elementId") int stateId);

  @Query(
      "MATCH (s:State) WHERE s.elementId > {elementId} AND NOT s.causingActivity IN {excluded} RETURN s ORDER BY s.elementId ASC LIMIT 1"
  )
  State findFollowingByElementIdAndExcludeActivity(
      @Param("elementId") int stateElementId, @Param("excluded") Activity[] excludeActivities
  );

  //  @Query("MATCH (n:State {elementId: {eId}})-[:FOLLOWED_BY*]->(s:State {beginEndType: 'BEGIN'})-[g:FOLLOWED_BY]->()" +
//         " WHERE g.guard =~ {variableSearch} AND s.causingActivity =~ '(IF|IF_CASE)' RETURN s LIMIT 1")
  //Following is faster, but assumes each state node is connected in the same single tree of FOLLOWED_BY node
  @Query("MATCH (s:State {beginEndType: 'BEGIN'})-[g:FOLLOWED_BY]->()" +
         " WHERE g.guard =~ {variableSearch} AND s.causingActivity =~ '(IF|IF_CASE)' AND s.elementId > {eId} RETURN g,s")
  List<State> findIfStatesWithVariableInConditionFromElementId(
      @Param("eId") int elementId, @Param("variableSearch") String variableName
  );

  @Query("MATCH (s:State) WHERE NOT (s)-[:FOLLOWED_BY]->() RETURN s ORDER BY s.elementId LIMIT 1")
  State findLastState();

  @Query("MATCH (s:State) WHERE s.elementId < {elementId} RETURN s ORDER BY s.elementId DESC LIMIT 1")
  @Depth(4)
  State findPreviousByElementId(@Param("elementId") int stateId);
  //  @Query("MATCH (s1:State)<-[rating:FOLLLOW]-(s2:State) WHERE id(m) = {stateId} RETURN s2")
//  FlowState getPrecedingState(@Param("stateId") long aStateId);
}
