package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

/**
 * Created by Christoph Graupner on 2018-03-02.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public interface FlowServiceOperation extends BpelIdentifiable, FlowAdditional {
  String getInputVariableName();

  String getName();

  String getOutputVariableName();
}
