package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;
import lombok.NonNull;

import java.util.Collection;

/**
 * Created by Christoph Graupner on 2018-03-01.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public interface FlowModel extends FlowModelQueries {
  String IF_BRANCH_HELPER_GUARD = Consts.IF_BRANCH_HELPER_GUARD;
  String IF_ELSE_BRANCH_GUARD   = Consts.IF_ELSE_BRANCH_GUARD;

  void clear();

  StateFlowFactory factory();

  <T extends FlowScCommitment> Collection<T> getAllCommitmentNodes();

  <T extends FlowScFulfillment> Collection<T> getAllFulfillmentNodes();

  <T extends FlowMessageSend> Collection<T> getAllMessageSend(final int depth);

  <T extends FlowScTraceLabel> Collection<T> getAllTraceLabelNodes();

  <T extends FlowState> T getEndState();

  <T extends FlowService> Collection<T> getServices();

  <T extends FlowState> Collection<T> getStates();

  <T extends FlowTransition> Collection<T> getTransitions();

  <T extends FlowVariable> Collection<T> getVariables();

  FlowState refresh(@NonNull FlowState object, final int objLoadDepth);

  <T extends FlowScFulfillment> T refresh(FlowScFulfillment fulfillment, final int objLoadDepth);

  <T extends Identifiable> Collection<T> refreshAll(
      final Class<T> aClass, Collection<T> entities, final int loadDepth
  );

  void removeState(@NonNull FlowState state);

  FlowState save(@NonNull FlowState state);

  BpelFlowAggregateState save(@NonNull BpelFlowAggregateState state);

  FlowVariable save(@NonNull FlowVariable variable);

  FlowTransition save(@NonNull FlowTransition transition);

  FlowService save(@NonNull FlowService flowService);

  FlowVarWriteAccessRelation save(@NonNull FlowVarWriteAccessRelation relation);

  FlowVarReadAccessRelation save(@NonNull FlowVarReadAccessRelation relation);

  FlowServiceOperation save(@NonNull FlowServiceOperation agentOperation);

  FlowAlternativeStateLink save(@NonNull FlowAlternativeStateLink alternativeLink);

  FlowStateInvolvesServiceLink save(FlowStateInvolvesServiceLink involvesServiceLink);

  FlowMessageSend save(@NonNull FlowMessageSend messageSend);

  FlowScCommitment save(@NonNull FlowScCommitment commitment);

  FlowScFulfillment save(@NonNull FlowScFulfillment fulfillment);

  FlowScRecovery save(@NonNull FlowScRecovery recovery);

  FlowScTraceLabel save(@NonNull FlowScTraceLabel traceLabel);
}
