package de.hu_berlin.informatik.svt.jbepl2ispl.ispl;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae.*;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.*;

import java.util.LinkedList;
import java.util.Objects;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
class IsplFormulaWriter extends IsplSubWriterBase {
  private AutomatonModel model;

  IsplFormulaWriter(
      final ISPLFactory factory,
      final IsplWriter isplWriter
  ) {
    super(factory, isplWriter);
    model = isplWriter.model;
  }

  Formulae createFormulae() {
    final Formulae formulae = factory.createFormulae();
    model.getFormulas().forEach(
        formula -> formulae.getFormula().add(encodeFormula(formula))
    );

    return formulae;
  }

  private Formula asFormulaAnd(final LinkedList<Formula> results) {
    if (results.size() == 1) {
      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }

    FormulaAnd rootAnd = factory.createFormulaAnd();
    FormulaAnd iterAnd = rootAnd;

    iterAnd.setLeft(results.removeFirst());
    Formula lastAnd = results.removeLast();

    for (Formula isBoolCondition: results) {
      FormulaAnd tmp = factory.createFormulaAnd();
      tmp.setLeft(isBoolCondition);

      FormulaParenthesized parenthesizedAnd = asParenthesized(tmp);

      iterAnd.setRight(parenthesizedAnd);
      iterAnd = tmp;
    }
    iterAnd.setRight(lastAnd);
    return rootAnd;
  }

  private Formula asFormulaOr(final LinkedList<Formula> results) {
    if (results.size() == 1) {
      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }

    FormulaOr rootOr = factory.createFormulaOr();
    FormulaOr iterOr = rootOr;

    iterOr.setLeft(results.removeFirst());
    Formula lastOr = results.removeLast();

    for (Formula isBoolCondition: results) {
      FormulaOr tmp = factory.createFormulaOr();
      tmp.setLeft(isBoolCondition);

      FormulaParenthesized parenthesizedOr = asParenthesized(tmp);

      iterOr.setRight(parenthesizedOr);
      iterOr = tmp;
    }
    iterOr.setRight(lastOr);
    return rootOr;
  }

  private FormulaParenthesized asParenthesized(final Formula formula) {
    if (formula instanceof FormulaParenthesized)  //don't do parenthesis if they are already the last surrounding thing
    {
      return (FormulaParenthesized) formula;
    }
    final FormulaParenthesized ret = factory.createFormulaParenthesized();
    ret.setFormula(formula);
    return ret;
  }

  private Formula encodeFormula(final B2IFormula formula) {
    switch (formula.getFunction()) {
      case A:
        FormulaQuantified formulaQuantified = factory.createFormulaQuantified();
        formulaQuantified.setQuantifier(FormulaQuantifier.A);
        formulaQuantified.setLeft(encodeFormula(((B2IBinaryFormula) formula).getLeft()));
        formulaQuantified.setRight(encodeFormula(((B2IBinaryFormula) formula).getRight()));
        return formulaQuantified;
      case E:
        formulaQuantified = factory.createFormulaQuantified();
        formulaQuantified.setQuantifier(FormulaQuantifier.E);
        formulaQuantified.setLeft(encodeFormula(((B2IBinaryFormula) formula).getLeft()));
        formulaQuantified.setRight(encodeFormula(((B2IBinaryFormula) formula).getRight()));
        return asParenthesized(formulaQuantified);
      case SC:
        FormulaFunctionMCMAS_SC_SC extensionMCMAS_sc = factory.createFormulaFunctionMCMAS_SC_SC();
        extensionMCMAS_sc.setDebtor(isplWriter.translateToIsplName(((B2IFormulaSC) formula).getDebtor()));
        extensionMCMAS_sc.setCreditor(isplWriter.translateToIsplName(((B2IFormulaSC) formula).getCreditor()));
        extensionMCMAS_sc.setCommContent(
            isplWriter.translateToIsplName(((B2IFormulaSC) formula).getCommitmentContent()));
        return extensionMCMAS_sc;
      case FU:
        FormulaFunctionMCMAS_SC_Fu extensionMCMAS_fu = factory.createFormulaFunctionMCMAS_SC_Fu();
        extensionMCMAS_fu.setDebtor(isplWriter.translateToIsplName(((B2IFormulaSC) formula).getDebtor()));
        extensionMCMAS_fu.setCreditor(isplWriter.translateToIsplName(((B2IFormulaSC) formula).getCreditor()));
        extensionMCMAS_fu.setCommContent(
            isplWriter.translateToIsplName(((B2IFormulaSC) formula).getCommitmentContent()));
        return extensionMCMAS_fu;
      case AG:
        FormulaFunctionAG functionAG = factory.createFormulaFunctionAG();
        functionAG.setFormula(
            asParenthesized(
                Objects.requireNonNull(encodeFormula(((B2IUnaryFormula) formula).getTerm()), formula.toString())
            )
        );
        return functionAG;
      case EG:
        FormulaFunctionEG functionEG = factory.createFormulaFunctionEG();
        functionEG.setFormula(
            asParenthesized(encodeFormula(((B2IUnaryFormula) formula).getTerm()))
        );
        return functionEG;
      case AX:
        FormulaFunctionAX functionAX = factory.createFormulaFunctionAX();
        functionAX.setFormula(
            asParenthesized(encodeFormula(((B2IUnaryFormula) formula).getTerm()))
        );
        return functionAX;
      case EX:
        FormulaFunctionEX functionEX = factory.createFormulaFunctionEX();
        functionEX.setFormula(
            asParenthesized(encodeFormula(((B2IUnaryFormula) formula).getTerm()))
        );
        return functionEX;
      case AF:
        FormulaFunctionAF functionAF = factory.createFormulaFunctionAF();
        functionAF.setFormula(
            asParenthesized(encodeFormula(((B2IUnaryFormula) formula).getTerm()))
        );
        return functionAF;
      case EF:
        FormulaFunctionEF functionEF = factory.createFormulaFunctionEF();
        functionEF.setFormula(
            asParenthesized(encodeFormula(((B2IUnaryFormula) formula).getTerm()))
        );
        return functionEF;
      case K:
        break;
      case GK:
        break;
      case GCK:
        break;
      case O:
        break;
      case DK:
        break;
      case X:
        break;
      case F:
        break;
      case G:
        FormulaFunctionATL_G functionATL_g = factory.createFormulaFunctionATL_G();
        functionATL_g.setFormula(
            asParenthesized(encodeFormula(((B2IUnaryFormula) formula).getTerm()))
        );
        return functionATL_g;
      case U:
        FormulaFunctionATLUntil functionATLUntil = factory.createFormulaFunctionATLUntil();
        functionATLUntil.setLeft(encodeFormula(((B2IBinaryFormula) formula).getLeft()));
        functionATLUntil.setRight(encodeFormula(((B2IBinaryFormula) formula).getRight()));
        return functionATLUntil;
      case LTL_G:
        break;
      case LTL_F:
        break;
      case LTL_X:
        break;
      case LTL_U:
        break;
      case LTL_K:
        break;
      case LTL_GK:
        break;
      case LTL_GCK:
        break;
      case LTL_DK:
        break;
      case CTL_A:
        break;
      case CTL_E:
        break;
      case CTL_K:
        break;
      case CTL_GK:
        break;
      case CTL_GCK:
        break;
      case CTL_DK:
        break;
      case CTL_G:
        break;
      case CTL_F:
        break;
      case CTL_X:
        break;
      case CTL_U:
        break;
      case AND:
        LinkedList<Formula> results = new LinkedList<>();
        for (final B2IFormula term: ((B2INaryFormula) formula).getTerms()) {
          results.add(encodeFormula(term));
        }

        Formula rootAnd = asFormulaAnd(results);
        return asParenthesized(rootAnd);
      case OR:
        results = new LinkedList<>();
        for (final B2IFormula term: ((B2INaryFormula) formula).getTerms()) {
          results.add(encodeFormula(term));
        }

        Formula rootOr = asFormulaOr(results);
        return asParenthesized(rootOr);
      case NOT:
        FormulaNegated formulaNegated = factory.createFormulaNegated();
        formulaNegated.setFormula(encodeFormula(((B2IUnaryFormula) formula).getTerm()));
        return asParenthesized(formulaNegated);
      case PARENTHESIZED:
        return asParenthesized(encodeFormula(((B2IUnaryFormula) formula).getTerm()));
      case TERM:
        FormulaAtomicPropositionVar formulaAtomicPropositionVar = factory.createFormulaAtomicPropositionVar();
        formulaAtomicPropositionVar.setVarName(
            isplWriter.translateToIsplName(((B2IFormulaTerminal) formula).getTerm()));
        return formulaAtomicPropositionVar;
      case IMPLY:
        FormulaImplies formulaImplies = factory.createFormulaImplies();
        formulaImplies.setLeft(asParenthesized(encodeFormula(((B2IBinaryFormula) formula).getLeft())));
        formulaImplies.setRight(asParenthesized(encodeFormula(((B2IBinaryFormula) formula).getRight())));
        return asParenthesized(formulaImplies);
      case STATES_REF:
        final FormulaAtomicPropositionStates formulaAtomicProposition = factory.createFormulaAtomicPropositionStates();
        formulaAtomicProposition.setProposition(toPropositionStates(((B2IFormulaStatesRef) formula).getType()));
        formulaAtomicProposition.setAgentName(
            isplWriter.translateToIsplName(((B2IFormulaStatesRef) formula).getAgent()));
        return formulaAtomicProposition;
    }
    return null;
  }

  private PropositionStates toPropositionStates(final B2IFormulaStatesRef.StatesType statesType) {
    switch (statesType) {

      case DESIRED:
        return PropositionStates.GREEN_STATES;
      case UN_DESIRED:
        return PropositionStates.RED_STATES;
    }
    return null;
  }
}
