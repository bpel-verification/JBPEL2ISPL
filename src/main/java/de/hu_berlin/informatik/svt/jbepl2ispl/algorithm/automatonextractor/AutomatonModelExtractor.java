package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by Christoph Graupner on 2018-05-20.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
public class AutomatonModelExtractor {
  @Getter final FlowModel             flowModel;
  private final CommonExtractorBase[] extractors;
  private final Logger                logger = LoggerFactory.getLogger(getClass());

  public AutomatonModelExtractor(
      final FlowModel flowModel,
      AgentCreatorCommonAgentExtractor agentCreatorCommonAgentExtractor,
      VariableCommonAgentExtractor variableCommonAgentExtractor,
      StatesCommonExtractor statesCommonExtractor,
      StateVariableCommonExtractor stateVariableCommonExtractor,
      EvaluationAutomatonModelExtractor evaluationAutomatonModelExtractor,
      VariableValuesCommonAgentExtractor valuesCommonAgentExtractor,
      FinalVariableValuesCommonAgentExtractor finalVariableValuesCommonAgentExtractor,
      ProtocolCommonAgentExtractor protocolCommonAgentExtractor,
      EvolutionCommonAgentExtractor evolutionCommonAgentExtractor
  ) {
    this.flowModel = flowModel;
    extractors = new CommonExtractorBase[]{
        agentCreatorCommonAgentExtractor,
        variableCommonAgentExtractor,
        statesCommonExtractor,
        stateVariableCommonExtractor,
        evolutionCommonAgentExtractor,
        protocolCommonAgentExtractor,
        valuesCommonAgentExtractor,
        evaluationAutomatonModelExtractor,
        finalVariableValuesCommonAgentExtractor
    };
  }

  public AutomatonModelExtractor extract() {
    for (final CommonExtractorBase extractor: extractors) {
      extractor.extract();
    }

    return this;
  }
}
