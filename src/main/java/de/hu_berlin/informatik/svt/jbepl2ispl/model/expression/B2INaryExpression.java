package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public interface B2INaryExpression extends B2IExpression {
  @SuppressWarnings("unchecked")
  <T extends B2IExpression> T addTerm(B2IExpression expression);

  @Override
  boolean equals(Object o);

  @Override
  int hashCode();

  boolean isEmpty();

  int size();

  @Override
  String toString();

  B2IExpression[] getTerms();
}
