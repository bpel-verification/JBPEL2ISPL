package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j.repository;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j.Neo4jAgentState;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Christoph Graupner on 2018-05-29.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
public interface Neo4jAgentStateRepository extends Neo4jRepository<Neo4jAgentState, Long> {
  @Query("MATCH (s1)--(s2) WHERE id(s1) = {idS1} AND id(s2) = {idS2} RETURN count(s1) > 0")
  boolean areTheyNeighborStates(@Param("idS1") Long id1,@Param("idS2")  Long id2);

  @Query(
      "MATCH (n:AgentAutomaton {name: {agentName} })-[:AUTOMATON_STATE]->(s:AgentState) RETURN DISTINCT s ORDER BY s.elementId")
  List<Neo4jAgentState> findAllForAgent(@Param("agentName") String name);

  @Query(
      "MATCH (n:AgentAutomaton {name: {agentName} })-[:AUTOMATON_STATE]->(s:AgentState {entryAction: {actionName}}) RETURN DISTINCT s ORDER BY s.elementId")
  List<Neo4jAgentState> findAllStatesByActionForAgent(
      @Param("agentName") String agentName, @Param("actionName") String actionName
  );


  @Query(
      "MATCH (s:AgentState)" +
      " WHERE any(k IN keys(s) WHERE k = {varName})" +
      " RETURN s[{varName}]")
  List<String> findAllValuesForVariable(@Param("varName") String varName);

  @Query(
      "MATCH (n:AgentAutomaton {name: {agentName} })-[:AUTOMATON_STATE]->(s:AgentState)" +
      " WHERE s.elementId = {elementId} AND s.alternativeCount = {alternativeId}" +
      " RETURN s")
  Neo4jAgentState findExactlyOneByAgentAutomatonAndElementIdAndAlternativeCount(
      @Param("agentName") String automaton, @Param("elementId") int elementId, @Param("alternativeId") int alternateId
  );

  @Query(
      "MATCH (ag:AgentAutomaton {name: {name} })-[:AUTOMATON_STATE]->(as:AgentState) WHERE NOT (as)-[:AGENT_TRANSITION]->() RETURN as LIMIT 1")
  Neo4jAgentState findLastStateForAgent(@Param("name") String name);
}
