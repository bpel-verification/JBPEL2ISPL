package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AllKnowingFacade extends VariableTargetServiceResolver, StateInformationResolver {
}
