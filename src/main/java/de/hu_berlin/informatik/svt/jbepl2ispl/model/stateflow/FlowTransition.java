package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import lombok.NonNull;

/**
 * Created by Christoph Graupner on 2018-03-01.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public interface FlowTransition extends BpelIdentifiable, FlowAdditional {
  String getGuard();

  void setGuard(String guard);

  QName getIsplActionName();

  void setIsplActionName(QName actionName);

  FlowState getSourceState();

  void setSourceState(@NonNull FlowState sourceState);

  FlowState getTargetState();

  void setTargetState(@NonNull FlowState targetState);

  /**
   * Indicate whether the guard expression should be negated (for ELSE) or not.
   *
   * @return
   */
  boolean invertGuard();

  void setInvertGuard(boolean value);
}
