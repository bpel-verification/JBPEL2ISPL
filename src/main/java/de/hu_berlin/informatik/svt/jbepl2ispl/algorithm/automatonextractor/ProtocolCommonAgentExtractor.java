package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentTransition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentAutomatonModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpressionActionPair;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.HashSet;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
@Service
public class ProtocolCommonAgentExtractor extends CommonExtractorBase {
  protected ProtocolCommonAgentExtractor(
      final FlowModel flowModel,
      final AutomatonModel model,
      final StateInformationResolver stateResolver,
      final VariableTargetServiceResolver resolver
  ) {
    super(flowModel, model, stateResolver, resolver);
  }

  @Override
  public void extract() {
    final AgentAutomaton envAutomaton = model.getAgentAutomaton(FlowService.ENV);
    model.getAllAgentAutomatons().forEach(
        agent -> {
          logger.trace(LogMarker.EXTRACTION, "Extracting protocol conditions for Agent '{}'", agent.getName());
          extractProtocolConditions((AgentAutomatonModifiable) agent, envAutomaton);
          logger.trace(LogMarker.EXTRACTION, "DONE Extracting protocol conditions for Agent '{}'", agent.getName());
        }
    );
  }

  private void extractProtocolConditions(
      final AgentAutomatonModifiable agent, final AgentAutomaton envAutomaton
  ) {
    //IDEE:
    // für alle states, die outbound transitions nach ihren actions fragen,
    // state == state-variable ist dann die bedingung

    final HashSet<B2IExpressionActionPair> ret = new HashSet<>();
    agent.getStates().stream().sorted(Comparator.comparingInt(Identifiable::getElementId)).forEachOrdered(
        state -> {
          final HashSet<QName> hashSet = new HashSet<>();
          final AgentState envState = agent.isEnvironment()
                                      ? state
                                      : envAutomaton.findCorrespondingStateForFlowState(state.getOriginStateId());
          for (final AgentTransition outboundTransition: model.getAllOutboundTransitions(envState)) {
            if (outboundTransition.getAction() != null) {
              hashSet.add(outboundTransition.getAction());
            }
          }
          if (hashSet.isEmpty() == false) {
            ret.add(
                new B2IExpressionActionPair(
                    B2IExpression.and(
                        B2IExpression.equal(
                            B2IExpression.variableReference(
                                FlowService.ENV,
                                Consts.ISPL_VAR_NAME_STATEVAR_ENV
                            ),
                            B2IExpression.term(envState.getName())
                        )
                    ),
                    hashSet
                )
            );
          }
//      logger.debug(LogMarker.DEBUG_VALUES, "For State [{}] Actions:  {}", state, StringUtils.join(ret, "\n "));
        }
    );
    agent.setProtocolConditions(ret);
    model.save(agent);
  }
}
