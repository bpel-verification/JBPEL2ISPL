package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public interface B2INaryFormula extends B2IFormula {
  @SuppressWarnings("unchecked")
  <T extends B2IFormula> T addTerm(B2IFormula expression);

  @Override
  boolean equals(Object o);

  B2IFormula[] getTerms();

  @Override
  int hashCode();

  boolean isEmpty();

  int size();

  @Override
  String toString();
}
