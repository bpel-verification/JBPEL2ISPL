package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.MessagingType;

import java.util.Collection;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface FlowMessageSend extends Identifiable {
  void addIntermediateReceivingState(FlowState flowState);

  void addIntermediateSendingState(FlowState flowState);

  String getBufferVariableName();

  String getCommChannelValue();

  void removeIntermediateReceivingState(FlowState state);

  void removeIntermediateSendingState(FlowState state);

  FlowState getOriginAssignState();

  void setBufferVariableName(String bufferVariableName);

  String getCommChannelVariableName();

  void setCommChannelValue(String value);

  void setCommChannelVariableName(String bufferVariableName);

  <T extends FlowState> Collection<T> getIntermediateReceivingState();

  <T extends FlowState> Collection<T> getIntermediateSendingState();

  FlowService getReceivingService();

  void setReceivingService(FlowService receivingService);

  String getSendVariableName();

  void setSendVariableName(String bufferVariableName);

  FlowService getSendingService();

  void setSendingService(FlowService sendingService);

  MessagingType getType();

  FlowState getValueReceivingState();

  void setValueReceivingState(FlowState state);

  FlowState getValueSendingState();

  void setValueSendingState(FlowState state);
}
