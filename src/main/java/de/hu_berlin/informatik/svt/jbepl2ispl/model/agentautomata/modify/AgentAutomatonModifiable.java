package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentTransition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentVariableDefinition;
import lombok.NonNull;

/**
 * Created by Christoph Graupner on 2018-04-21.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AgentAutomatonModifiable extends AgentAutomaton {
  AgentAutomatonModifiable addState(@NonNull AgentState state);

  void addTalksTo(@NonNull AgentAutomaton otherAgent);

  AgentTransition save(AgentTransition transition);

  AgentVariableDefinition save(AgentVariableDefinition varDef);

  AgentState save(AgentState state);
}
