package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVarWriteAccessRelation;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVariable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
public class SymbolicValuePostProcessor extends FlowModelPostProcessor {
  private final Logger               logger = LoggerFactory.getLogger(getClass());
  private       Map<String, Integer> count;

  protected SymbolicValuePostProcessor(final FlowModel model) {
    super(model);
  }

  public void finish() {
    logger.trace(LogMarker.POSTPROCESSING, "Extracting variables values for all Agents");
    count = new HashMap<>();
    flowModel.getVariables()
             .stream()
             .sorted(Comparator.comparingInt(Identifiable::getElementId))
             .forEachOrdered(this::process);
    logger.trace(LogMarker.POSTPROCESSING, "DONE Extracting variables values for all Agents");
  }

  private void process(final FlowVariable flowVariable) {
    if (logger.isDebugEnabled()) {
      logger.debug(
          LogMarker.DEBUG_VALUES, "extractStep6_GenerateSymbolicValues: flowVariable: {}", flowVariable);
    }
    final String variableName = flowVariable.getName();
    processWriteAccess(flowVariable, variableName);
    processReadAccess(variableName);
  }

  /**
   * Just copy value from last write access
   * FIXME: maybe do it buffervalue calculation
   *
   * @param variableName
   */
  private void processReadAccess(final String variableName) {
    flowModel.findAllVarReadAccessRelationsForVariable(variableName)
             .stream()
             .sorted(Comparator.comparingInt(Identifiable::getElementId))
             .forEachOrdered(
                 readRelation -> {
                   FlowVarWriteAccessRelation writeAccessRelation =
                       flowModel.findLastWriteAccessBeforeRead(variableName, readRelation);
                   if (writeAccessRelation == null) {
                     readRelation.setSymbolicValue(Consts.ISPL_VAR_ENUM_NONE);
                   } else {
                     readRelation.setSymbolicValue(writeAccessRelation.getSymbolicValue());
                     readRelation.setValue("FROM write@" + writeAccessRelation.getElementId());
                   }
                   flowModel.save(readRelation);
                 }
             );
  }

  private void processWriteAccess(final FlowVariable flowVariable, final String variableName) {
    flowModel.findAllWriteAccessesForVariable(variableName)
             .stream()
             .sorted(Comparator.comparingInt(Identifiable::getElementId))
             .forEachOrdered(
                 writeAccessRelation -> {
                   switch (flowVariable.getPurposeType()) {
                     case OPERATIONAL:
                       switch (flowVariable.getValueType()) {
                         case STRING:
                           final Integer symbolicValue = count.getOrDefault(variableName, 0);
                           count.put(variableName, symbolicValue + 1);

                           writeAccessRelation.setSymbolicValue(
                               Naming.getEnumVariableValueName(variableName, symbolicValue));
                           flowModel.save(writeAccessRelation);
                           break;
                         case BOOLEAN:
                         case INT:
                           if (logger.isDebugEnabled()) {
                             logger.debug(
                                 LogMarker.DEBUG_VALUES,
                                 "+- extractStep6_GenerateSymbolicValues: setSymbolicValue: {} -> sv:{}",
                                 flowVariable,
                                 writeAccessRelation.getValue()
                             );
                           }
                           try {
                             writeAccessRelation.setSymbolicValue(
                                 String.valueOf(Integer.valueOf(writeAccessRelation.getValue()))
                             );
                             flowModel.save(writeAccessRelation);
                           } catch (NumberFormatException e) {
                             //ignore
                           }
                           break;
                         case UNKNOWN:
                         default:
                           writeAccessRelation.setSymbolicValue("TODO SymValue");
                           flowModel.save(writeAccessRelation);
                           break;
                       }
                       break;
                     case SHARED_BUFFER:
                       break;
                     case COMM_CHANNEL:
                       // both should be calculated later in extractor
                       Integer symbolicValue = count.getOrDefault(variableName, 0);
                       count.put(variableName, symbolicValue + 1);

                       writeAccessRelation.setSymbolicValue(
                           Naming.getEnumVariableValueName(variableName, symbolicValue));
                       flowModel.save(writeAccessRelation);
                       break;
                     case CONTAINER:
                       switch (flowVariable.getValueType()) {
                         case STRING:
                           symbolicValue = count.getOrDefault(variableName, 0);
                           count.put(variableName, symbolicValue + 1);

                           writeAccessRelation.setSymbolicValue(
                               Naming.getEnumVariableValueName(variableName, symbolicValue));
                           flowModel.save(writeAccessRelation);
                           break;
                         case BOOLEAN:
                           try {
                             writeAccessRelation.setSymbolicValue(
                                 String.valueOf(Boolean.valueOf(writeAccessRelation.getValue()))
                             );
                           } catch (NumberFormatException e) {
                             //ignore
                             writeAccessRelation.setSymbolicValue("UNDEFINED Bool SymValue");
                           }
                           flowModel.save(writeAccessRelation);
                           break;
                         case INT:
                           if (logger.isDebugEnabled()) {
                             logger.debug(
                                 LogMarker.DEBUG_VALUES,
                                 "+- extractStep6_GenerateSymbolicValues: setSymbolicValue: {} -> sv:{}",
                                 flowVariable,
                                 writeAccessRelation.getValue()
                             );
                           }
                           try {
                             writeAccessRelation.setSymbolicValue(
                                 String.valueOf(Integer.valueOf(writeAccessRelation.getValue()))
                             );
                           } catch (NumberFormatException e) {
                             writeAccessRelation.setSymbolicValue("UNDEFINED Int SymValue");
                           }
                           flowModel.save(writeAccessRelation);
                           break;
                         case UNKNOWN:
                         default:
                           writeAccessRelation.setSymbolicValue("UNKNOWN SymValue");
                           flowModel.save(writeAccessRelation);
                           break;
                       }
                       break;
                     case MESSAGE:
                       symbolicValue = count.getOrDefault(variableName, 0);
                       count.put(variableName, symbolicValue + 1);

                       writeAccessRelation.setSymbolicValue(
                           Naming.getEnumVariableValueName(variableName, symbolicValue));
                       flowModel.save(writeAccessRelation);

                       if (logger.isDebugEnabled()) {
                         logger.debug(
                             LogMarker.DEBUG_VALUES,
                             "+- extractStep6_GenerateSymbolicValues: WriteAccesseForVariable: {} -> sv:{}",
                             flowVariable,
                             writeAccessRelation
                         );
                       }
                       break;
                   }
                 }
             );
  }
}
