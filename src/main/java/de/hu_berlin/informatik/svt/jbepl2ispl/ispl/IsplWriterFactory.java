package de.hu_berlin.informatik.svt.jbepl2ispl.ispl;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.ISPLFactory;
import lombok.NonNull;
import org.springframework.stereotype.Service;

/**
 * Created by Christoph Graupner on 2018-02-21.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
@Service
public class IsplWriterFactory {
  private final ISPLFactory ispl;

  public IsplWriterFactory(@NonNull final ISPLFactory ispl) {
    this.ispl = ispl;
  }

  public IsplWriter createWriter(@NonNull AutomatonModel model) {
    return new IsplWriter(ispl, model);
  }
}
