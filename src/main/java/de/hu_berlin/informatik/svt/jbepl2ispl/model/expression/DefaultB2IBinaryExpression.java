package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import lombok.Getter;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public class DefaultB2IBinaryExpression implements B2IBinaryExpression {
  private static final long serialVersionUID = 1L;

  @Getter final Type type;
  @Getter
  B2IExpression left;
  @Getter
  B2IExpression right;

  public DefaultB2IBinaryExpression(
      final Type type,
      final B2IExpression left,
      final B2IExpression right
  ) {
    this.type = type;
    this.left = left;
    this.right = right;
  }
}
