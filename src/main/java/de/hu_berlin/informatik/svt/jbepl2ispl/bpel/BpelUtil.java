package de.hu_berlin.informatik.svt.jbepl2ispl.bpel;

import org.apache.ode.bpel.compiler.bom.BpelObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by Christoph Graupner on 2018-02-26.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public class BpelUtil {
  @SuppressWarnings("unchecked")
  public static List<BpelObject> getChildren(BpelObject pParent)
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
  {
    Method m = BpelObject.class.getDeclaredMethod("getChildren");
    m.setAccessible(true);
    return (List<BpelObject>) m.invoke(pParent);
  }
}
