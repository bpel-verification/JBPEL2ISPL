package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowMessageSend;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import lombok.NonNull;

import java.util.Collection;

/**
 * Created by Christoph Graupner on 2018-06-27.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface VariableTargetServiceResolver {
  <T extends FlowMessageSend> Collection<T> findAllMessagingSendForState(FlowState startState);

  <T extends FlowMessageSend> Collection<T> findAllRelevantMessagingSendForStateAndService(
      FlowState state, final String forService
  );

  /**
   * The service the value comes from, is the service that
   * was last &lt;invoke&gt;d before flowState and has as
   * output variable the input variable (maybe with jumps over OPERATIONAL
   * vars) of flowState.
   *
   * @param flowState
   *
   * @return Name of the service that matches the criteria.
   */
  String getServiceValueComesFrom(@NonNull FlowState flowState);

  String getServiceValueGoesTo(@NonNull FlowState flowState);

  String getSymbolicVariableValueAt(@NonNull FlowState state);

  String getSymbolicVariableValueAt(@NonNull String varName,@NonNull  FlowState flowState);
}
