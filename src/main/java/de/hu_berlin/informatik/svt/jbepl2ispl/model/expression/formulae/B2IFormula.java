package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae;

import java.util.Collection;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public interface B2IFormula {
  enum Function {
    A, E,
    SC, FU,
    AG, EG, AX, EX,
    /**
     * <pre>AF formula </pre>
     */
    AF, EF,
    K, GK, GCK, O, DK,

    /**
     * <pre> &lt;ID&gt; X ( formula ) </pre>
     */
    X,
    /**
     * <pre> &lt;ID&gt; F ( formula ) </pre>
     */
    F,
    /**
     * <pre> &lt;ID&gt; G ( formula ) </pre>
     */
    G,
    /**
     * <pre> &lt;ID&gt; ( formula U formula ) </pre>
     */
    U,

    LTL_G, LTL_F, LTL_X, LTL_U,
    LTL_K, LTL_GK, LTL_GCK, LTL_DK,

    CTL_A, CTL_E, CTL_K, CTL_GK, CTL_GCK, CTL_DK,
    CTL_G, CTL_F, CTL_X, CTL_U,

    AND, OR, NOT, PARENTHESIZED, TERM, IMPLY, STATES_REF
  }

  /**
   * <code><b>A</b>( <i><[left]></i> <b>U</b> <i><[right]></i> )</code>
   *
   * @param left  formula
   * @param right formula
   *
   * @return
   */
  static DefaultB2IBinaryFormula A(final B2IFormula left, final B2IFormula right) {
    return new DefaultB2IBinaryFormula(Function.A, left, right);
  }

  /**
   * <code><b>AF</b>( <i><[formula]></i> )</code>
   *
   * @param formula
   *
   * @return
   */
  static DefaultB2IUnaryFormula AF(final B2IFormula formula) {
    return new DefaultB2IUnaryFormula(Function.AF, formula);
  }

  /**
   * <code><b>AG</b>( <i><[formula]></i> )</code>
   *
   * @param formula
   *
   * @return
   */
  static DefaultB2IUnaryFormula AG(final B2IFormula formula) {
    return new DefaultB2IUnaryFormula(Function.AG, formula);
  }

  /**
   * <code><b>AX</b>( <i><[formula]></i> )</code>
   *
   * @param formula
   *
   * @return
   */
  static DefaultB2IUnaryFormula AX(final B2IFormula formula) {
    return new DefaultB2IUnaryFormula(Function.AX, formula);
  }

  /**
   * <code><b>E</b>( <i><[left]></i> <b>U</b> <i><[right]></i> )</code>
   *
   * @param left  formula
   * @param right formula
   *
   * @return
   */
  static DefaultB2IBinaryFormula E(final B2IFormula left, final B2IFormula right) {
    return new DefaultB2IBinaryFormula(Function.E, left, right);
  }

  /**
   * <code><b>EF</b>( <i><[formula]></i> )</code>
   *
   * @param formula
   *
   * @return
   */
  static DefaultB2IUnaryFormula EF(final B2IFormula formula) {
    return new DefaultB2IUnaryFormula(Function.EF, formula);
  }

  /**
   * <code><b>EG</b>( <i><[formula]></i> )</code>
   *
   * @param formula
   *
   * @return
   */
  static DefaultB2IUnaryFormula EG(final B2IFormula formula) {
    return new DefaultB2IUnaryFormula(Function.EG, formula);
  }

  /**
   * <code><b>EX</b>( <i><[formula]></i> )</code>
   *
   * @param formula
   *
   * @return
   */
  static DefaultB2IUnaryFormula EX(final B2IFormula formula) {
    return new DefaultB2IUnaryFormula(Function.EX, formula);
  }
  static DefaultB2IUnaryFormula Unary(final Function function, final B2IFormula formula) {
    return new DefaultB2IUnaryFormula(function, formula);
  }

  static B2IFormulaSC Fu(
      final String debtor,
      final String creditor,
      final String commitmentContent
  ) {
    return new B2IFormulaSC(Function.FU, debtor, creditor, commitmentContent);
  }

  /**
   * <code><b>G</b>( <i><[formula]></i> )</code>
   *
   * @param formula
   *
   * @return
   */
  static DefaultB2IUnaryFormula G(final B2IFormula formula) {
    return new DefaultB2IUnaryFormula(Function.G, formula);
  }

  static B2IFormulaSC SC(
      final String debtor,
      final String creditor,
      final String commitmentContent
  ) {
    return new B2IFormulaSC(Function.SC, debtor, creditor, commitmentContent);
  }

  /**
   * <code>( <i><[left]></i> <b>U</b> <i><[right]></i> )</code>
   *
   * @param left  formula
   * @param right formula
   *
   * @return
   */
  static DefaultB2IBinaryFormula U(final B2IFormula left, final B2IFormula right) {
    return new DefaultB2IBinaryFormula(Function.U, left, right);
  }

  @SafeVarargs
  static <T extends B2IFormula> B2INaryFormula and(T... terms) {
    return new DefaultB2INaryFormula(Function.AND, terms);
  }

  /**
   * <code><i><[left]></i> <b>-></b> <i><[right]></i></code>
   *
   * @param left  formula
   * @param right formula
   *
   * @return
   */
  static DefaultB2IBinaryFormula implies(final B2IFormula left, final B2IFormula right) {
    return new DefaultB2IBinaryFormula(Function.IMPLY, left, right);
  }

  static B2IUnaryFormula negate(B2IFormula term) {
    return new DefaultB2IUnaryFormula(Function.NOT, term);
  }

  @SafeVarargs
  static <T extends B2IFormula> B2INaryFormula or(T... terms) {
    return new DefaultB2INaryFormula(Function.OR, terms);
  }

  static <T extends B2IFormula> B2INaryFormula or(Collection<T> terms) {
    final B2INaryFormula condition = new DefaultB2INaryFormula(Function.OR);
    terms.forEach(condition::addTerm);
    return condition;
  }

  static B2IUnaryFormula parenthesized(B2IFormula term) {
    return new DefaultB2IUnaryFormula(Function.PARENTHESIZED, term);
  }

  static B2IFormulaTerminal term(String term) {
    return new B2IFormulaTerminal(term);
  }

  Function getFunction();
}
