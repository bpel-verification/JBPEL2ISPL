package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae;

import lombok.Getter;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public class B2IFormulaTerminal implements B2IFormula {
  @Getter final String term;

  public B2IFormulaTerminal(final String term) {
    this.term = term;
  }

  @Override
  public Function getFunction() {
    return Function.TERM;
  }
}
