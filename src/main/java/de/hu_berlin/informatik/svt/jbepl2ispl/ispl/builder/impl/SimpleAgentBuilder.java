package de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.impl;

import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplAgentBuilder;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplAgentBuilderBase;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplAgentsBuilder;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.*;

import java.util.Arrays;

/**
 * Created by Christoph Graupner on 2018-03-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@SuppressWarnings("unchecked")
public class SimpleAgentBuilder
    extends
    AgentBuilderBase<IsplAgentsBuilder, AgentVarDef, AgentActionDef, AgentProtocolDef, AgentEvolutionDef, AgentRedStatesDef, SimpleAgent>
    implements IsplAgentBuilder
{

  public SimpleAgentBuilder(final ISPLFactory factory, final IsplAgentsBuilder parent) {
    super(factory, parent, factory.createSimpleAgent());
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplAgentsBuilder, AgentVarDef, AgentActionDef, AgentProtocolDef, AgentEvolutionDef, AgentRedStatesDef>> T actions(
      final String... actionNames
  ) {
    agent.getActions().getActionNames().addAll(Arrays.asList(actionNames));
    return (T) this;
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplAgentsBuilder, AgentVarDef, AgentActionDef, AgentProtocolDef, AgentEvolutionDef, AgentRedStatesDef>> T actions(
      final AgentActionDef actionDefinition
  ) {
    agent.setActions(actionDefinition);
    return (T) this;
  }

  @Override
  public Object build() {
    return agent;
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplAgentsBuilder, AgentVarDef, AgentActionDef, AgentProtocolDef, AgentEvolutionDef, AgentRedStatesDef>> T evolution(
      final AgentEvolutionDef agentEvolutionDef
  ) {
    agent.setEvolution(agentEvolutionDef);
    return (T) this;
  }

  @Override
  public <T extends IsplAgentBuilder> T lobsVars(
      final AgentLobsVarDef agentLobVars
  ) {
    agent.setLobsVarDef(agentLobVars);
    return (T) this;
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplAgentsBuilder, AgentVarDef, AgentActionDef, AgentProtocolDef, AgentEvolutionDef, AgentRedStatesDef>> T name(
      final String name
  ) {
    agent.setName(name);
    return (T) this;
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplAgentsBuilder, AgentVarDef, AgentActionDef, AgentProtocolDef, AgentEvolutionDef, AgentRedStatesDef>> T protocol(
      final AgentProtocolDef agentProtocolDefinition
  ) {
    agent.setProtocol(agentProtocolDefinition);
    return (T) this;
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplAgentsBuilder, AgentVarDef, AgentActionDef, AgentProtocolDef, AgentEvolutionDef, AgentRedStatesDef>> T redStates(
      final AgentRedStatesDef agentRedStatesDefinition
  ) {
    agent.setRedDef(agentRedStatesDefinition);
    return (T) this;
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplAgentsBuilder, AgentVarDef, AgentActionDef, AgentProtocolDef, AgentEvolutionDef, AgentRedStatesDef>> T variables(
      final AgentVarDef variableDefinition
  ) {
    agent.setVariables(variableDefinition);
    return (T) this;
  }
}
