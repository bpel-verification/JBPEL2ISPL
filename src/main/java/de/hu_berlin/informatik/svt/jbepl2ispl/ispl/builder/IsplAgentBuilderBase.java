package de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder;

/**
 * Created by Christoph Graupner on 2018-03-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface IsplAgentBuilderBase<PARENT extends IsplBuilderBase, VARDEF, ACTION_DEF, PROTOCOL_DEF, EVOLUTION_DEF, RED_STATES_DEF>
    extends IsplBuilderBase<PARENT>
{
  <T extends IsplAgentBuilderBase<PARENT, VARDEF, ACTION_DEF, PROTOCOL_DEF, EVOLUTION_DEF, RED_STATES_DEF>>
  T actions(String... actionNames);

  <T extends IsplAgentBuilderBase<PARENT, VARDEF, ACTION_DEF, PROTOCOL_DEF, EVOLUTION_DEF, RED_STATES_DEF>>
  T actions(ACTION_DEF actionDefinition);

  <T extends IsplAgentBuilderBase<PARENT, VARDEF, ACTION_DEF, PROTOCOL_DEF, EVOLUTION_DEF, RED_STATES_DEF>>
  T evolution(final EVOLUTION_DEF evolutionDef);

  <T extends IsplAgentBuilderBase<PARENT, VARDEF, ACTION_DEF, PROTOCOL_DEF, EVOLUTION_DEF, RED_STATES_DEF>>
  T name(String name);

  <T extends IsplAgentBuilderBase<PARENT, VARDEF, ACTION_DEF, PROTOCOL_DEF, EVOLUTION_DEF, RED_STATES_DEF>>
  T protocol(final PROTOCOL_DEF agentProtocolDefinition);

  <T extends IsplAgentBuilderBase<PARENT, VARDEF, ACTION_DEF, PROTOCOL_DEF, EVOLUTION_DEF, RED_STATES_DEF>> T
  redStates(RED_STATES_DEF agentRedStatesDefinition);

  <T extends IsplAgentBuilderBase<PARENT, VARDEF, ACTION_DEF, PROTOCOL_DEF, EVOLUTION_DEF, RED_STATES_DEF>>
  T variables(VARDEF variableDefinition);
}
