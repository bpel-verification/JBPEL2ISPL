package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm;

import com.google.common.collect.Sets;
import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
public class StaticAssignBranchValueGuessStrategy implements AssignBranchValueGuessStrategy {
  final         FlowModel flowModel;
  private final Logger    logger = LoggerFactory.getLogger(getClass());

  public StaticAssignBranchValueGuessStrategy(
      final FlowModel flowModel
  ) {
    this.flowModel = flowModel;
  }

  @Override
  public Set<String> guessAssignBranchValues(
      final FlowState assignState
  ) {
    //FIXME implement branch guessing
    logger.error(
        LogMarker.TODO_EXCEPTION, "{}: branch guessing", Consts.NEEDS_IMPLEMENTATION
    );

    switch (flowModel.findVariableByName(assignState.getOutputVariable()).getValueType()) {
      case BOOLEAN:
        return Sets.newHashSet("true", "false");
      case INT:
        return Sets.newHashSet("3000", "4000");
      case STRING:
      case UNKNOWN:
        return Sets.newHashSet("MsgGuess3000", "MsgGuess4000");
    }
    return null;
  }
}
