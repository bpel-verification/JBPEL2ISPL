package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor;

import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor.evolution.AgentEvolutionExtractor;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor.evolution.EnvironmentEvolutionExtractor;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import org.springframework.stereotype.Service;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
@Service
public class EvolutionCommonAgentExtractor extends CommonExtractorBase {
  protected EvolutionCommonAgentExtractor(
      final FlowModel flowModel,
      final AutomatonModel model,
      final StateInformationResolver stateResolver,
      final VariableTargetServiceResolver resolver
  ) {
    super(flowModel, model, stateResolver, resolver);
  }

  @Override
  public void extract() {
    model.getAllAgentAutomatons().forEach(
        agent -> {
          logger.trace(LogMarker.EXTRACTION, "Extracting state evolution conditions for Agent '{}'", agent.getName());

          if (agent.isEnvironment()) {
            new EnvironmentEvolutionExtractor(flowModel, model, resolver, stateResolver).extract(agent);
          } else {
            final AgentEvolutionExtractor evoExtractor =
                new AgentEvolutionExtractor(flowModel, model, resolver, stateResolver);
            evoExtractor.extract(agent);
          }
          logger.trace(
              LogMarker.EXTRACTION, "DONE Extracting state evolution conditions for Agent '{}'", agent.getName());
        }
    );
  }
}
