package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.repository;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.Neo4jFlowMessageSend;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
public interface FlowMessageSendRepository extends Neo4jRepository<Neo4jFlowMessageSend, Long> {
  @Query("MATCH (s:Service)-[:FL_RECEIVER|FL_SENDER]-(m:Messaging)-[:FL_RECEIVER|FL_SENDER]-(st:Service)" +
         " WHERE st.name={serviceName} AND id(st)<>id(s)" +
         " RETURN m")
  Collection<Neo4jFlowMessageSend> findAllMessagingForService(@Param("serviceName") String serviceName);

  @Query("MATCH (s:State)-[:FL_VALUE_TO|FL_VALUE_FROM]-(m:Messaging)-[:FL_RECEIVER|FL_SENDER]-(st:Service)" +
         " WHERE st.name={serviceName} AND id(s)={eId}" +
         " RETURN m")
  Collection<Neo4jFlowMessageSend> findAllMessagingForStateAndService(@Param("eId") Long id,@Param("serviceName") String forService);

  @Query("MATCH (fm:Messaging)--(s:State) WHERE id(s)={startStateId} RETURN fm")
  Collection<Neo4jFlowMessageSend> findMessagingForState(@Param("startStateId") Long stateId);
}
