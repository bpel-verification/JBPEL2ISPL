package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;

import java.util.Collection;
import java.util.Set;

/**
 * Created by Christoph Graupner on 2018-04-21.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AgentAutomaton extends AgentAutomatonAnalysis, AgentIdentifiable {

  Collection<String> getActionOthers();

  <T extends AgentTransition> Collection<T> getAllInboundTransitions(AgentState state);

  <T extends AgentTransition> Collection<T> getAllOutboundTransitions(AgentState state);

  AgentVariableDefinition getBufferVariableForAgent(AgentAutomaton agent);

  AgentState getLastState();

  String getName();

  AgentVariableDefinition getStateVariableDef();

  <T extends AgentState> Collection<T> getStates();

  <T extends AgentAutomaton> Set<T> getTalksToAgents();

  QName getTraceLabelName();

  <T extends AgentTransition> Collection<T> getTransitions();

  AgentVariableDefinition getVariableDefinition(String name);

  Collection<String> getVariables();

  boolean isEnvironment();
}
