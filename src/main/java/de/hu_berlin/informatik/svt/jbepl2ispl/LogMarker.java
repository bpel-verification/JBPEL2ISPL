package de.hu_berlin.informatik.svt.jbepl2ispl;

/**
 * Created by Christoph Graupner on 2018-02-08.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Created by Christoph Graupner on 2016-09-17.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public abstract class LogMarker {
  public static final Marker APPLICATION    = MarkerFactory.getMarker("APPLICATION");
  public static final Marker BPEL           = MarkerFactory.getMarker("BPEL");
  public static final Marker CONFIG         = MarkerFactory.getMarker("CONFIG");
  public static final Marker DEBUG_VALUES   = MarkerFactory.getMarker("DEBUG_VALUES");
  public static final Marker EXCEPTION      = MarkerFactory.getMarker("EXCEPTION");
  public static final Marker EXTRACTION     = MarkerFactory.getMarker("EXTRACT");
  public static final Marker ISPL           = MarkerFactory.getMarker("ISPL");
  public static final Marker MODEL_PROBLEM  = MarkerFactory.getMarker("MODEL_PROBLEM");
  public static final Marker POSTPROCESSING = MarkerFactory.getMarker("POST_PROCESS");
  public static final Marker TEST           = MarkerFactory.getMarker("TEST");
  public static final Marker TODO_EXCEPTION = MarkerFactory.getMarker("TODO");

  private LogMarker() {
  }
}
