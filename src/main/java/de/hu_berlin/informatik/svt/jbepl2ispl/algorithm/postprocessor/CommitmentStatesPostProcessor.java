package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
public class CommitmentStatesPostProcessor extends FlowModelPostProcessor {
  private int fulfillmentCounter = Integer.MAX_VALUE;

  protected CommitmentStatesPostProcessor(final FlowModel model) {
    super(model);
  }

  @Override
  public void finish() {
    logger.trace(LogMarker.POSTPROCESSING, "Processing Commitments...");
    processCommitmentStates();
    processFulfillmentStates();

    processMessageFlow();

    processTraceLabelStates();
    logger.trace(LogMarker.POSTPROCESSING, "DONE Processing Commitments...");
  }

  private void processCommitmentStates() {
    Collection<FlowState> commitmentStates = flowModel.findStatesWithActivity(Activity.SC_COMMITMENT);
    commitmentStates.forEach(
        flowState -> {
          final FlowState followingState =
              flowModel.findFollowingStateNotOfActivity(flowState, Activity.SC_COMMITMENT, Activity.SC_FULFILLMENT,
                                                        Activity.SC_TRACE_LABEL, Activity.SC_RECOVERY
              );
          if (followingState.getCausingActivity() != Activity.INVOKE) {
            throw new IllegalStateException(
                "The marked state by a CommitmentLabel must be an INVOKE activity, got " + followingState);
          }

          final String  debtor       = flowState.getProperty(Consts.PROPERTY_SC_DEBTOR);
          final Integer commitmentID = Integer.valueOf(flowState.getProperty(Consts.PROPERTY_SC_COMMITMENT_ID));
          final String  creditor     = flowState.getProperty(Consts.PROPERTY_SC_CREDITOR);
          final FlowScCommitment commitment =
              flowModel.factory().createScCommitment(
                  flowState.getPlainName(),
                  commitmentID,
                  flowModel.findServiceByName(debtor),
                  flowModel.findServiceByName(creditor),
                  followingState,
                  flowState.getElementId()
              );
          flowModel.save(commitment);
          followingState.setFlagged(Consts.PROPERTY_SC_COMMITMENT_ID, true);
          flowModel.save(followingState);

          final FlowVarWriteAccessRelation varWriteAccessRelation = flowModel.factory().createVarWriteAccessRelation(
              followingState,
              flowModel.findVariableByName(Consts.VAR_NAME_SC_COMMITMENT_ID),
              String.valueOf(commitment.getCommitmentId()),
              commitment.getName(),
              VarValueSourceType.LITERAL,
              flowState.getBpelPathAsString()
          );
          flowModel.save(varWriteAccessRelation);
        }
    );
    commitmentStates.forEach(flowModel::removeState);
  }

  private void processFulfillmentStates() {
    Collection<FlowState> fulfillStates = flowModel.findStatesWithActivity(Activity.SC_FULFILLMENT);
    fulfillStates.forEach(
        flowState -> {
          final FlowState followingState = flowModel.refresh(
              flowModel.findFollowingStateNotOfActivity(flowState, Activity.SC_COMMITMENT, Activity.SC_FULFILLMENT,
                                                        Activity.SC_TRACE_LABEL, Activity.SC_RECOVERY
              ), 3);
          if (followingState.getCausingActivity() != Activity.INVOKE) {
            throw new IllegalStateException(
                "The marked state by a FulfillmentLabel must be an INVOKE activity, got " + followingState);
          }
          final FlowScCommitment commitmentStateById = flowModel.findCommitmentStateById(
              Integer.valueOf(flowState.getProperty(Consts.PROPERTY_SC_COMMITMENT_ID))
          );
          if (false ==
              commitmentStateById.getCreditor().getName().equals(followingState.getTargetService().getName()))
          {
            throw new IllegalStateException(
                "The creditor of commitment " + commitmentStateById.getName() + " must be '"
                + commitmentStateById.getCreditor().getName() + "' but we got '"
                + followingState.getTargetService().getName() + "' at the fulfillment label '"
                + flowState.getPlainName() + "'");
          }
          final FlowScFulfillment fulfillment =
              flowModel.factory().createScFulfillment(
                  flowState.getPlainName(),
                  commitmentStateById,
                  Integer.valueOf(flowState.getProperty(Consts.PROPERTY_SC_FULFILLMENT_ID)),
                  followingState,
                  flowState.getElementId()
              );
          fulfillment.setFlagged(Consts.PROPERTY_SC_FULFILLMENT_ID, true);
          flowModel.save(fulfillment);

          followingState.setFlagged(Consts.PROPERTY_SC_FULFILLMENT_ID, true);
          followingState.setProperty(Consts.PROPERTY_SC_FULFILLMENT_ID, String.valueOf(fulfillment.getFulfillmentId()));
          flowModel.save(followingState);

          final FlowVarWriteAccessRelation varWriteAccessRelation = flowModel.factory().createVarWriteAccessRelation(
              followingState,
              flowModel.findVariableByName(Consts.VAR_NAME_SC_FULFILLMENT_ID),
              String.valueOf(fulfillment.getFulfillmentId()),
              fulfillment.getName(),
              VarValueSourceType.LITERAL,
              flowState.getBpelPathAsString()
          );
          flowModel.save(varWriteAccessRelation);
        }
    );
    fulfillStates.forEach(flowModel::removeState);
  }

  private void processMessageFlow() {
    Collection<FlowScCommitment> commitments = flowModel.getAllCommitmentNodes();
    commitments.forEach(
        commitment -> {
          final String creditor = commitment.getCreditor().getName();
          flowModel.findAllMessagingSendForState(commitment.getTargetState()).forEach(
              flowMessageSend -> {
                if (creditor.equals(flowMessageSend.getReceivingService().getName())) {
                  FlowState valueReceivingState = flowMessageSend.getValueReceivingState();
                  if (false == valueReceivingState.isFlagged(Consts.PROPERTY_SC_FULFILLMENT_ID)) {
                    valueReceivingState.setFlagged(Consts.PROPERTY_SC_FULFILLMENT_ID, true);
                    valueReceivingState = flowModel.save(valueReceivingState);
                    final FlowScFulfillment fulfillment =
                        flowModel.factory().createScFulfillment(
                            "FULFILL__" + valueReceivingState.getPlainName(),
                            commitment,
                            fulfillmentCounter--,
                            valueReceivingState,
                            valueReceivingState.getElementId()
                        );
                    fulfillment.setFlagged(Consts.PROPERTY_SC_FULFILLMENT_DERIVED, true);
                    fulfillment.setFlagged(Consts.PROPERTY_SC_FULFILLMENT_ID, true);
                    flowModel.save(fulfillment);
                  }
                }
              }
          );
        }
    );
  }

  private void processTraceLabelStates() {
    Collection<FlowState> traceLabelStates = flowModel.findStatesWithActivity(Activity.SC_TRACE_LABEL);
    traceLabelStates.forEach(
        flowState -> {
          final FlowState followingState =
              flowModel.findFollowingStateNotOfActivity(flowState, Activity.SC_COMMITMENT, Activity.SC_FULFILLMENT,
                                                        Activity.SC_TRACE_LABEL, Activity.SC_RECOVERY
              );
          final FlowScTraceLabel scTraceLabel =
              flowModel.factory().createScTraceLabel(
                  flowState.getPlainName(),
                  flowModel.findServiceByName(flowState.getProperty(Consts.PROPERTY_SC_TRACE_SERVICE_NAME)),
                  followingState,
                  FlowScTraceLabel.Type.valueOf(flowState.getProperty(Consts.PROPERTY_SC_TRACE_BEHAVIOUR_TYPE)),
                  flowState.getElementId()
              );
          flowModel.save(scTraceLabel);
          followingState.setFlagged(Consts.PROPERTY_SC_TRACE_BEHAVIOUR_TYPE, true);
          followingState.setProperty(
              Consts.PROPERTY_SC_TRACE_BEHAVIOUR_TYPE,
              flowState.getProperty(Consts.PROPERTY_SC_TRACE_BEHAVIOUR_TYPE)
          );
          followingState.setProperty(
              Consts.PROPERTY_SC_TRACE_SERVICE_NAME,
              flowState.getProperty(Consts.PROPERTY_SC_TRACE_SERVICE_NAME)
          );
          flowModel.save(followingState);
        }
    );
    traceLabelStates.forEach(flowModel::removeState);
  }
}
