package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVariable;

import java.util.List;

/**
 * Created by Christoph Graupner on 2018-04-28.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AgentVariableDefinition extends AgentIdentifiable {

  enum ValueType {
    ENUM, BOOLEAN, INT
  }

  List<String> getAllowedValues();

  FlowVariable.PurposeType getPurposeType();

  void setAllowedValues(List<String> values);

  AgentAutomaton getBelongsto();

  String getInitialValue();

  void setInitialValue(String value);

  String getName();

  void setName(String name);

  FlowVariable getOriginVariable();

  void setOriginVariable(FlowVariable originVariable);

  QName getQName();

  ValueType getValueType();

  void setValueType(ValueType valueType);

  String getVariableType();

  boolean hasAllowedValues();

  boolean hasInitialValue();

  /**
   * Determines if this variable is declared in BPEL or is it virtually defined for the verification
   *
   * @return
   */
  boolean isBpelDeclared();

  void setBpelDeclared(boolean value);

  void setBelongsTo(AgentAutomaton agent);

  void setPurposeType(FlowVariable.PurposeType purposeType);
}
