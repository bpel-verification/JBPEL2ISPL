package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.BpelIdentifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.BpelPath;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.neo4j.ogm.annotation.Transient;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NoArgsConstructor
abstract class FlowBaseBpel extends FlowBase implements BpelIdentifiable {
  @Getter
  protected String   bpelPathAsString;
  @Transient
  private   BpelPath bpelPath;

  FlowBaseBpel(final int elementId, @NonNull final String bpelPathAsString) {
    super(elementId);
    this.bpelPathAsString = bpelPathAsString;
  }

  @Override
  public BpelPath getBpelPath() {
    if (bpelPathAsString == null) {
      throw new IllegalStateException(
          "bpelPathAsString is <null> in " + getClass().getSimpleName() + "/eID:" + getElementId());
    }
    if (bpelPath == null) {
      bpelPath = new BpelPath(bpelPathAsString);
    }
    return bpelPath;
  }

  @Override
  public void setBpelPath(final String bpelObjectPath) {
    bpelPathAsString = bpelObjectPath;
  }
}
