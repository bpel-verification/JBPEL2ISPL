package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class DefaultB2INaryExpression implements B2INaryExpression {

  private static final long serialVersionUID = 1L;

  @Getter final Type type;
  @Getter
  B2IExpression[] terms;

  protected DefaultB2INaryExpression(
      final Type type, @NonNull B2IExpression... terms
  ) {
    this.type = type;
    this.terms = terms;
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends B2IExpression> T addTerm(B2IExpression expression) {
    this.terms = Arrays.copyOf(terms, terms.length + 1);
    terms[terms.length - 1] = expression;
    return (T) this;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof DefaultB2INaryExpression)) {
      return false;
    }
    final B2INaryExpression that = (B2INaryExpression) o;
    return getType() == that.getType() &&
           Arrays.equals(getTerms(), that.getTerms());
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(getType());
    result = 31 * result + Arrays.hashCode(getTerms());
    return result;
  }

  @Override
  public boolean isEmpty() {
    return terms.length == 0;
  }

  @Override
  public int size() {
    return terms.length;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("B2INaryExpression");
    sb.append("{")
      .append(StringUtils.join(terms, " " + type + " "));
    sb.append('}');
    return sb.toString();
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T extends B2IExpression> T sort() {
    for (final B2IExpression term: terms) {
      switch (type) {
        case TERM:
          break;
        default:
          term.sort();
          break;
      }
    }

    switch (type) {
      case NUM_ADD:
      case NUM_MUL:
      case AND:
      case OR:
        SortedMap<String, B2IExpression> sorted = new TreeMap<>();
        for (final B2IExpression term: terms) {
          sorted.put(term.toString(), term);
        }
        int i = 0;
        for (final B2IExpression value: sorted.values()) {
          terms[i] = value;
          i++;
        }
        break;
    }
    return (T) this;
  }
}
