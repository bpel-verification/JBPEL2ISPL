package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVarWriteAccessRelation;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVariable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
public class DetermineVariableTypePostProcessor extends FlowModelPostProcessor {

  protected DetermineVariableTypePostProcessor(final FlowModel model) {
    super(model);
  }

  @Override
  public void finish() {
    Set<String> variablesInInvoke = new HashSet<>();
    flowModel.findStatesWithActivity(Activity.INVOKE).forEach(
        flowState -> {
          variablesInInvoke.add(flowState.getInputVariable());
          variablesInInvoke.add(flowState.getOutputVariable());
        }
    );
    flowModel.findStatesWithActivity(Activity.RECEIVE).forEach(
        flowState -> {
          variablesInInvoke.add(flowState.getInputVariable());
          variablesInInvoke.add(flowState.getOutputVariable());
        }
    );
    flowModel.findStatesWithActivity(Activity.REPLY).forEach(
        flowState -> {
          variablesInInvoke.add(flowState.getInputVariable());
          variablesInInvoke.add(flowState.getOutputVariable());
        }
    );
    variablesInInvoke.remove(null);

    flowModel.getVariables().forEach(
        flowVariable -> {
          if (logger.isDebugEnabled()) {
            logger.debug(LogMarker.DEBUG_VALUES, "finish: variable: {}", flowVariable);
          }

          if (variablesInInvoke.contains(flowVariable.getName())) {
            flowVariable.setPurposeType(FlowVariable.PurposeType.MESSAGE);
            flowModel.save(flowVariable);
            return;
          }

          //Container Variable?
          //-> value assigned from message variable
          Collection<FlowVarWriteAccessRelation>
              writeAccessesForVariable = flowModel.findAllWriteAccessesForVariable(flowVariable.getName());
          if (false == writeAccessesForVariable.isEmpty()) {
            for (FlowVarWriteAccessRelation relation: writeAccessesForVariable) {
              if (relation.getValueSourceType() == VarValueSourceType.VARIABLE) {
                if (variablesInInvoke.contains(relation.getValueSource())) {
                  flowVariable.setPurposeType(FlowVariable.PurposeType.CONTAINER);
                  flowModel.save(flowVariable);
                  return; //continue with the next flowVariable
                }
              }
            }
          }

          //operational variable?
          if (false == (flowVariable.isInternalOnly() || flowVariable.isVirtual())) {
            switch (flowVariable.getBpelType()) {
              case SCHEMA:
              case ELEMENT:
                flowVariable.setPurposeType(FlowVariable.PurposeType.OPERATIONAL);
                flowModel.save(flowVariable);
                break;
              case MESSAGE:
                logger.error(
                    LogMarker.TODO_EXCEPTION,
                    "DetermineVariableTypePostProcessor.finish: There is a container detection without assignment from MESSAGE: {}",
                    flowVariable
                ); //FIXME There is a container detection without assignment from MESSAGE: {}
                flowVariable.setPurposeType(FlowVariable.PurposeType.CONTAINER);
                flowModel.save(flowVariable);
                break;
            }
          }
        }
    );
  }
}
