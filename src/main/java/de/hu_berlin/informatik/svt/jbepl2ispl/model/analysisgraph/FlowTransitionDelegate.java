package de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.BpelPath;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowId;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowTransition;
import lombok.NonNull;

import java.util.Objects;

/**
 * Created by Christoph Graupner on 2018-06-18.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */

public class FlowTransitionDelegate implements FlowTransitionWrapper {
  final FlowTransition delegate;

  FlowTransitionDelegate(@NonNull final FlowTransition delegate) {
    this.delegate = delegate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof FlowTransitionDelegate)) {
      return false;
    }
    final FlowTransitionDelegate that = (FlowTransitionDelegate) o;
    return Objects.equals(delegate, that.delegate);
  }

  @Override
  public int getAlternativeCount() {
    return delegate.getAlternativeCount();
  }

  @Override
  public BpelPath getBpelPath() {
    return delegate.getBpelPath();
  }

  @Override
  public void setBpelPath(final String bpelObjectPath) {
    delegate.setBpelPath(bpelObjectPath);
  }

  @Override
  public String getBpelPathAsString() {
    return delegate.getBpelPathAsString();
  }

  @Override
  public int getElementId() {
    return delegate.getElementId();
  }

  @Override
  public FlowId getFlowId() {
    return delegate.getFlowId();
  }

  @Override
  public String getGuard() {
    return delegate.getGuard();
  }

  @Override
  public void setGuard(final String guard) {
    delegate.setGuard(guard);
  }

  @Override
  public Long getId() {
    return delegate.getId();
  }

  @Override
  public QName getIsplActionName() {
    return delegate.getIsplActionName();
  }

  @Override
  public void setIsplActionName(final QName actionName) {
    delegate.setIsplActionName(actionName);
  }

  @Override
  public String getName() {
    return delegate.getName();
  }

  @Override
  public String getProperty(final String propertyName) {
    return delegate.getProperty(propertyName);
  }

  @Override
  public FlowState getSourceState() {
    return delegate.getSourceState();
  }

  @Override
  public void setSourceState(final FlowState sourceState) {
    delegate.setSourceState(sourceState);
  }

  @Override
  public FlowState getTargetState() {
    return delegate.getTargetState();
  }

  @Override
  public void setTargetState(final FlowState targetState) {
    delegate.setTargetState(targetState);
  }

  @Override
  public int hashCode() {
    return Objects.hash(delegate);
  }

  @Override
  public boolean invertGuard() {
    return delegate.invertGuard();
  }

  @Override
  public boolean isFlagged(final String identifier) {
    return delegate.isFlagged(identifier);
  }

  @Override
  public void setInvertGuard(final boolean value) {
    delegate.setInvertGuard(value);
  }

  @Override
  public void setFlagged(final String identifier, final boolean state) {
    delegate.setFlagged(identifier, state);
  }

  @Override
  public void setProperty(final String propertyName, final String value) {
    delegate.setProperty(propertyName, value);
  }

  @Override
  public FlowTransition wrapped() {
    return delegate;
  }
}
