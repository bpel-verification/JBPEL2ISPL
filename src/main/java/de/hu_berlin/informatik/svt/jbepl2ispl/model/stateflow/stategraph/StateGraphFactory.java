package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.MessagingType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import lombok.NonNull;
import org.neo4j.ogm.annotation.Transient;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Transient
public class StateGraphFactory implements StateFlowFactory {
  private static final int OFFSET_ACCESS_COUNT = 10000;

  private final Map<String, Integer>   alternativeCount     = new HashMap<>();
  private final Map<Class<?>, Integer> elementIdCount       = new HashMap<>();
  /**
   * Map&lt;[elementID],[count]&gt;
   */
  private final Map<Integer, Integer>  accessElementIdCount = new HashMap<>();

  @Override
  public FlowMessageSend cloneMessageSend(
      final FlowMessageSend messageSend
  ) {
    return new Neo4jFlowMessageSend(
        (Neo4jFlowMessageSend) messageSend, getAlternativeCount(FlowMessageSend.class, messageSend.getElementId()));
  }

  public FlowState cloneState(final FlowState state, final String cloneStateName) {
    return new State(cloneStateName, (State) state, getAlternativeCount(FlowState.class, state.getElementId()));
  }

  @Override
  public FlowAlternativeStateLink createAlternativeLink(
      final FlowState state, final FlowState altState
  ) {
    return new AlternativeStateLinkRelation((State) state, (State) altState);
  }

  @Override
  public BpelFlowAggregateState createBpelFlowAggregateState(
      @NonNull final FlowState flowBeginState
  ) {
    return new Neo4jBpelFlowAggregateState((State) flowBeginState);
  }

  @Override
  public FlowMessageSend createFlowMessageSend(
      @NonNull FlowState originState, @NonNull final FlowState valueFromState,
      final FlowState valueToState,
      @NonNull final MessagingType messagingType
  ) {
    return new Neo4jFlowMessageSend(
        (State) originState, (State) valueFromState, (State) valueToState,
        messagingType,
        getElementId(FlowMessageSend.class)
    );
  }

  @Override
  public FlowScCommitment createScCommitment(
      @NonNull final String nameAttribute,
      final int commitmentID,
      @NonNull final FlowService debtor,
      @NonNull final FlowService creditor,
      @NonNull final FlowState targetState,
      final int originalElementId
  ) {
    return new Neo4jFlowScCommitment(
        nameAttribute,
        commitmentID,
        debtor,
        creditor,
        targetState,
        originalElementId,
        getElementId(FlowScCommitment.class)
    );
  }

  @Override
  public FlowScFulfillment createScFulfillment(
      @NonNull final String nameAttribute,
      @NonNull final FlowScCommitment commitmentID,
      final int fulfillmentID,
      @NonNull final FlowState targetState,
      final int originalElementId
  ) {
    return new Neo4jFlowScFulfillment(
        nameAttribute,
        commitmentID,
        fulfillmentID,
        targetState,
        originalElementId,
        getElementId(FlowScFulfillment.class)
    );
  }

  @Override
  public FlowScTraceLabel createScTraceLabel(
      @NonNull final String nameAttribute,
      @NonNull final FlowService tracedService,
      @NonNull final FlowState targetState,
      @NonNull final FlowScTraceLabel.Type behaviourType,
      final int originalElementId
  ) {
    return new Neo4jFlowScTraceLabel(
        nameAttribute,
        tracedService,
        targetState,
        behaviourType,
        originalElementId,
        getElementId(FlowScTraceLabel.class)
    );
  }

  @Override
  public FlowService createService(
      @NonNull final String name, final String bpelObjectPath
  ) {
    return new Service(name, getElementId(FlowService.class), bpelObjectPath);
  }

  @Override
  public FlowServiceOperation createServiceOperation(
      @NonNull final FlowService service, @NonNull final String action,
      final String inputVariable, final String outputVariable,
      final String currentBpelPath
  ) {
    return new ServiceOperation(
        (Service) service,
        action,
        inputVariable,
        outputVariable,
        getElementId(FlowServiceOperation.class),
        currentBpelPath
    );
  }

  @Override
  public FlowState createState(
      final String originalBpelName, final String stateName, final Activity causingActivity,
      final BeginEndType beginEndType,
      final String currentBpelPath
  ) {
    return new State(
        originalBpelName,
        stateName,
        causingActivity,
        beginEndType,
        getElementId(FlowState.class),
        currentBpelPath
    );
  }

  @Override
  public FlowStateInvolvesServiceLink createStateInvolvesServiceLink(
      final FlowState state, final FlowService agent
  ) {
    return new Neo4jFlowStateInvolvesServiceLink(
        (State) state,
        (Service) agent,
        getElementId(FlowStateInvolvesServiceLink.class)
    );
  }

  @Override
  public FlowTransition createTransition(
      @NonNull final FlowState sourceState, @NonNull final FlowState targetState, final String guard,
      final String name, final String bpelPathAsString
  ) {
    return new Transition(
        (State) sourceState,
        (State) targetState,
        guard,
        name,
        getElementId(FlowTransition.class),
        bpelPathAsString
    );
  }

  @Override
  public FlowTransition createTransition(
      final FlowState sourceState, final FlowState targetState, final String guard, final String name,
      final String bpelPathAsString, final int elementId
  ) {
    return new Transition(
        (State) sourceState,
        (State) targetState,
        guard,
        name,
        elementId,
        bpelPathAsString
    );
  }

  @Override
  public FlowVarReadAccessRelation createVarReadAccessRelation(
      final FlowState state,
      final FlowVariable variable, final String value, final String valueSource,
      final VarValueSourceType varValueSourceType, final String currentBpelPath
  ) {
    return new Neo4jFlowReadAccessRelation(
        (State) state,
        (Variable) variable,
        varValueSourceType,
        value,
        valueSource,
        getAccessElementId(state),
        currentBpelPath
    );
  }

  @Override
  public FlowVarWriteAccessRelation createVarWriteAccessRelation(
      final FlowState state,
      final FlowVariable variable, final String value, final String valueSource,
      final VarValueSourceType varValueSourceType, final String currentBpelPath
  ) {
    return new Neo4jFlowWriteAccessRelation(
        (State) state,
        (Variable) variable,
        varValueSourceType,
        value,
        valueSource,
        getAccessElementId(state),
        currentBpelPath
    );
  }

  /**
   * ensures an ordering among accessRelations even if they are created at a later process state
   *
   * @param state
   *
   * @return
   */
  private int getAccessElementId(final FlowState state) {
    final Integer ret = accessElementIdCount.getOrDefault(state.getElementId(), 0);
    accessElementIdCount.put(state.getElementId(), ret + 1);
    return state.getElementId() * OFFSET_ACCESS_COUNT + ret;
  }

  @Override
  public FlowVariable createVariable(
      @NonNull final String name, final String variableOriginalType,
      @NonNull final String currentBpelPath
  ) {
    return new Variable(
        name, variableOriginalType, getElementId(FlowVariable.class), currentBpelPath);
  }

  private int getAlternativeCount(@NonNull final Class<?> aClass, final int elementId) {
    final Integer ret = alternativeCount.getOrDefault(aClass.getName() + elementId, 1);
    alternativeCount.put(aClass.getName() + elementId, ret + 1);
    return ret;
  }

  private int getElementId(@NonNull final Class<?> aClass) {
    final Integer ret = elementIdCount.getOrDefault(aClass, 0);
    elementIdCount.put(aClass, ret + 1);
    return ret;
  }
}
