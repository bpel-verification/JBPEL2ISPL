package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.repository;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.Neo4jFlowScFulfillment;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
public interface Neo4jFlowScFulfillmentRepository extends Neo4jRepository<Neo4jFlowScFulfillment, Long> {
  @Query("MATCH (f:ScFulfillment)-[:SC_FULFILLMENT_OF]->(c:ScCommitment) WHERE id(c) = {nodeIdComm} RETURN f")
  Collection<Neo4jFlowScFulfillment> findAllByCommitment(@Param("nodeIdComm") Long id);
}
