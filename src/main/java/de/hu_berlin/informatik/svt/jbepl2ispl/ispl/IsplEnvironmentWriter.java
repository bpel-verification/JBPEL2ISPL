package de.hu_berlin.informatik.svt.jbepl2ispl.ispl;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplEnvironmentBuilder;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentIdentifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentVariableDefinition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.*;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.*;
import lombok.NonNull;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.emf.common.util.EList;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Christoph Graupner on 2018-06-20.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
class IsplEnvironmentWriter extends IsplSubWriterBase {

  IsplEnvironmentWriter(
      final IsplWriter isplWriter, final ISPLFactory factory
  ) {
    super(factory, isplWriter);
  }

  void buildEnvironment(final IsplEnvironmentBuilder builder) {
    currentAgent = isplWriter.model.getAgentAutomaton(FlowService.ENV);
    builder //.obsVars(createEnvObsVars(agent))
            .name(currentAgent.getName())
            .variables(createEnvironmentVariablesDefinition(currentAgent))
            .actions(createEnvActionDefinition(currentAgent))
            .protocol(createEnvProtocolDefinition(currentAgent))
            .evolution(createEnvEvolutionDefinition(currentAgent));
  }

  private void appendAgentProtocolOther(
      final EnvProtocolDef agentProtocolDef, final Collection<String> actionOthers
  ) {
    if (actionOthers != null && false == actionOthers.isEmpty()) {
      final OtherBranch otherBranch = factory.createOtherBranch();
      otherBranch.getIds().addAll(actionOthers);
      agentProtocolDef.setOtherbranch(otherBranch);
    }
  }

  private EnvBoolCondition asAgentProtocolBoolConditionAnd(final LinkedList<EnvBoolConditionCompare> results) {
    if (results.size() == 1) {

      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }
    EnvBoolConditionAnd rootAnd = factory.createEnvBoolConditionAnd();
    EnvBoolConditionAnd iterAnd = rootAnd;

    iterAnd.setLeft(results.removeFirst());
    EnvBoolConditionCompare last = results.removeLast();

    for (EnvBoolConditionCompare isBoolCondition: results) {
      EnvBoolConditionAnd tmp = factory.createEnvBoolConditionAnd();
      tmp.setLeft(isBoolCondition);

      EnvBoolConditionParenthesized parenthesizedAnd = asParenthesized(tmp);

      iterAnd.setRight(parenthesizedAnd);
      iterAnd = tmp;
    }
    iterAnd.setRight(last);
    return rootAnd;
  }

  private EnvArithmeticExpressionAddition asEnvArithmeticExpressionAddition(
      final LinkedList<EnvBitExpression> results, final AdditionOp additionOp
  ) {
    if (results.size() <= 1) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }
    EnvArithmeticExpressionAddition rootMult = factory.createEnvArithmeticExpressionAddition();
    rootMult.setOperator(additionOp);
    EnvArithmeticExpressionAddition iterMult = rootMult;

    iterMult.setLeft((EnvArithmeticExpression) results.removeFirst());
    EnvBitExpression last = results.removeLast();

    for (EnvBitExpression envBitExpression: results) {
      EnvArithmeticExpressionAddition tmp = factory.createEnvArithmeticExpressionAddition();
      tmp.setOperator(additionOp);
      tmp.setLeft((EnvArithmeticExpression) envBitExpression);

      EnvArithmeticExpressionParenthesized parenthesizedMult = asParenthesized(tmp);

      iterMult.setRight(parenthesizedMult);
      iterMult = tmp;
    }
    iterMult.setRight((EnvArithmeticExpression) last);
    return rootMult;
  }

  private EnvArithmeticExpressionMultiplication asEnvArithmeticExpressionMult(
      final LinkedList<EnvBitExpression> results, final MultiplicationOp multiplicationOp
  ) {
    if (results.size() <= 1) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }
    EnvArithmeticExpressionMultiplication rootMult = factory.createEnvArithmeticExpressionMultiplication();
    rootMult.setOperator(multiplicationOp);
    EnvArithmeticExpressionMultiplication iterMult = rootMult;

    iterMult.setLeft((EnvArithmeticExpression) results.removeFirst());
    EnvBitExpression last = results.removeLast();

    for (EnvBitExpression isBoolCondition: results) {
      EnvArithmeticExpressionMultiplication tmp = factory.createEnvArithmeticExpressionMultiplication();
      tmp.setOperator(multiplicationOp);
      tmp.setLeft((EnvArithmeticExpression) isBoolCondition);

      EnvArithmeticExpressionParenthesized parenthesizedMult = asParenthesized(tmp);

      iterMult.setRight(parenthesizedMult);
      iterMult = tmp;
    }
    iterMult.setRight((EnvArithmeticExpression) last);
    return rootMult;
  }

  private EnvBitExpression asEnvBitExpressionAnd(final LinkedList<EnvBitExpression> results) {
    if (results.size() == 1) {
      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }
    EnvBitExpressionAnd rootAnd = factory.createEnvBitExpressionAnd();
    EnvBitExpressionAnd iterAnd = rootAnd;

    iterAnd.setLeft(results.removeFirst());
    EnvBitExpression last = results.removeLast();

    for (EnvBitExpression isBoolCondition: results) {
      EnvBitExpressionAnd tmp = factory.createEnvBitExpressionAnd();
      tmp.setLeft(isBoolCondition);

      EnvBitExpressionParenthesized parenthesizedAnd =
          asParenthesized(tmp);

      iterAnd.setRight(parenthesizedAnd);
      iterAnd = tmp;
    }
    iterAnd.setRight(last);
    return rootAnd;
  }

  private EnvBitExpression asEnvBitExpressionOr(final LinkedList<EnvBitExpression> results) {
    if (results.size() == 1) {
      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }
    EnvBitExpressionOr rootOr = factory.createEnvBitExpressionOr();
    EnvBitExpressionOr iterOr = rootOr;

    iterOr.setLeft(results.removeFirst());
    EnvBitExpression last = results.removeLast();

    for (EnvBitExpression isBoolCondition: results) {
      EnvBitExpressionOr tmp = factory.createEnvBitExpressionOr();
      tmp.setLeft(isBoolCondition);

      EnvBitExpressionParenthesized parenthesizedOr = asParenthesized(tmp);

      iterOr.setRight(parenthesizedOr);
      iterOr = tmp;
    }
    iterOr.setRight(last);
    return rootOr;
  }

  private EnvEvoBoolCondition asEnvEvoBoolConditionAnd(final LinkedList<EnvEvoBoolCondition> results) {
    if (results.size() == 1) {
      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }
    EnvEvoBoolConditionAnd rootAnd = factory.createEnvEvoBoolConditionAnd();
    EnvEvoBoolConditionAnd iterAnd = rootAnd;

    iterAnd.setLeft(results.removeFirst());
    EnvEvoBoolCondition last = results.removeLast();

    for (EnvEvoBoolCondition isBoolCondition: results) {
      EnvEvoBoolConditionAnd tmp = factory.createEnvEvoBoolConditionAnd();
      tmp.setLeft(isBoolCondition);

      EnvEvoBoolConditionParenthesized parenthesizedAnd =
          asParenthesized(tmp);

      iterAnd.setRight(parenthesizedAnd);
      iterAnd = tmp;
    }
    iterAnd.setRight(last);
    return rootAnd;
  }

  private EnvEvoBoolCondition asEnvEvoBoolConditionOr(final LinkedList<EnvEvoBoolCondition> results) {
    if (results.size() == 1) {
      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }

    EnvEvoBoolConditionOr rootOr = factory.createEnvEvoBoolConditionOr();
    EnvEvoBoolConditionOr iterOr = rootOr;

    iterOr.setLeft(results.removeFirst());
    EnvEvoBoolCondition lastOr = results.removeLast();

    for (EnvEvoBoolCondition isBoolCondition: results) {
      EnvEvoBoolConditionOr tmp = factory.createEnvEvoBoolConditionOr();
      tmp.setLeft(isBoolCondition);

      EnvEvoBoolConditionParenthesized parenthesizedOr =
          asParenthesized(tmp);

      iterOr.setRight(parenthesizedOr);
      iterOr = tmp;
    }
    iterOr.setRight(lastOr);
    return rootOr;
  }

  private EnvLiteral asEnvLiteral(final Object obj) {
    if (obj instanceof B2IExpression) {
      if (logger.isDebugEnabled()) {
        logger.debug(LogMarker.DEBUG_VALUES, "asEnvLiteral: obj: {}", obj);
      }
      switch (((B2IExpression) obj).getType()) {
        case VARIABLE_REF:
          final QName qName = ((B2IVarRefExpression) obj).getTerm();
          if (false == qName.isInEnvironment() && null != qName.getAgent()) {
            throw new IllegalStateException(obj + " should not reference a variable outside Environment, but it does.");
          }
          return isplWriter.getLiteral(qName.getName());
        case ACTION_COMPARE:

          break;
      }
    }
    return isplWriter.getLiteral(obj);
  }

  private EnvBoolConditionParenthesized asParenthesized(final EnvBoolCondition condition) {
    if (condition instanceof EnvBoolConditionParenthesized) {
      return (EnvBoolConditionParenthesized) condition;
    }
    final EnvBoolConditionParenthesized parenthesized = factory.createEnvBoolConditionParenthesized();
    parenthesized.setCondition(condition);
    return parenthesized;
  }

  private EnvEvoBoolResultParenthesized asParenthesized(final EnvEvoBoolResult result) {
    if (result instanceof EnvEvoBoolResultParenthesized) {
      return (EnvEvoBoolResultParenthesized) result;
    }
    EnvEvoBoolResultParenthesized parenthesized = factory.createEnvEvoBoolResultParenthesized();
    parenthesized.setResult(result);

    return parenthesized;
  }

  private EnvEvoBoolConditionParenthesized asParenthesized(final EnvEvoBoolCondition condition) {
    if (condition instanceof EnvEvoBoolConditionParenthesized) {
      return (EnvEvoBoolConditionParenthesized) condition;
    }
    EnvEvoBoolConditionParenthesized parenthesizedAnd = factory.createEnvEvoBoolConditionParenthesized();
    parenthesizedAnd.setCondition(condition);
    return parenthesizedAnd;
  }

  private EnvBitExpressionParenthesized asParenthesized(final EnvBitExpression expression) {
    if (expression instanceof EnvBitExpressionParenthesized) {
      return (EnvBitExpressionParenthesized) expression;
    }
    EnvBitExpressionParenthesized parenthesizedAnd = factory.createEnvBitExpressionParenthesized();
    parenthesizedAnd.setExpression(expression);
    return parenthesizedAnd;
  }

  private EnvArithmeticExpressionParenthesized asParenthesized(final EnvArithmeticExpression expression) {

    if (expression instanceof EnvArithmeticExpressionParenthesized) {
      return (EnvArithmeticExpressionParenthesized) expression;
    }
    EnvArithmeticExpressionParenthesized parenthesizedAnd = factory.createEnvArithmeticExpressionParenthesized();
    parenthesizedAnd.setExpression(expression);
    return parenthesizedAnd;
  }

  private EnvActionDef createEnvActionDefinition(final AgentAutomaton agent) {
    final EnvActionDef agentActionDef = factory.createEnvActionDef();
    agentActionDef.getActionNames().add(Consts.ISPL_ACTION_NAME_NONE);
    agent.getActions()
         .stream()
         .filter(QName::isInEnvironment)
         .sorted()
         .forEachOrdered(qName -> agentActionDef.getActionNames().add(qName.getName()));
    return agentActionDef;
  }

  private EnvEvolutionDef createEnvEvolutionDefinition(final AgentAutomaton agent) {
    final EnvEvolutionDef agentEvolutionDef = factory.createEnvEvolutionDef();

    agent.getStates()
         .stream()
         .filter(agentState -> agentState.getCausingActivity() != Activity.__INITIAL_STATE)
         .sorted(Comparator.comparingInt(AgentIdentifiable::getElementId))
         .forEachOrdered(
             agentState -> {
               final EnvEvolutionStatement evolutionStatement = factory.createEnvEvolutionStatement();

               final B2IExpression             stateEntryConditions = agentState.getStateEntryConditions();
               final Map<QName, B2IExpression> assignmentMap        = agentState.getAssignmentMap();

               logger.debug(LogMarker.DEBUG_VALUES, "ISPL: {}.Evolution {}:\n {}\n -> {}",
                            agent.getName(),
                            agentState,
                            stateEntryConditions,
                            assignmentMap
               );

               evolutionStatement.setCondition(
                   asParenthesized(encodeConditionTermForEvolution(stateEntryConditions))
               );
               evolutionStatement.setResult(
                   asParenthesized(encodeAssignments(assignmentMap))
               );

               agentEvolutionDef.getEvolutionStatements().add(evolutionStatement);
             }
         );

    return agentEvolutionDef;
  }

  private EnvProtocolDef createEnvProtocolDefinition(@NonNull final AgentAutomaton agent) {
    final EnvProtocolDef agentProtocolDef = factory.createEnvProtocolDef();
    appendAgentProtocolOther(agentProtocolDef, agent.getActionOthers());

    final Set<B2IExpressionActionPair> actionActivationConditions = agent.getProtocolConditions();
    if (actionActivationConditions.isEmpty()) {
      logger.warn(LogMarker.ISPL, "No actions for {}", agent);
      appendAgentProtocolOther(agentProtocolDef, Arrays.asList("nothing"));
    } else {
      int entries = 0;
      for (final B2IExpressionActionPair actionPair: actionActivationConditions) {
        //FIXME how to handle things, there no action is for condition?
        if (actionPair.getActions().isEmpty()) {
          logger.warn(LogMarker.ISPL, "Empty actions for {}: {}", agent, actionPair);
          continue;
        }
        final B2IExpression condition = actionPair.getExpression();

        EnvProtocolStatement agentProtocolStatement = factory.createEnvProtocolStatement();
        agentProtocolStatement.setCondition(
            asParenthesized(
                getAgentProtocolCondition(
                    agent,
                    (B2INaryExpression) condition
                )
            )
        );
        final EnabledIdList enabledIdList = factory.createEnabledIdList();
        for (final QName actionName: actionPair.getActions()) {
          if (actionName == null) {
            logger.error(LogMarker.ISPL, "actionName is null");
            continue;
          }

          if (!actionName.belongsToAgent(currentAgent.getName())) {
            continue;
          }

          enabledIdList.getIds().add(actionName.getName()); //strip agent name //TODO: check agent name
        }
        if (!enabledIdList.getIds().isEmpty()) { // only add if there are actions for the state
          agentProtocolStatement.setActionEnabledIdList(enabledIdList);

          agentProtocolDef.getProtocolStatements().add(agentProtocolStatement);
          entries++;
        }
      }

      if (entries == 0) {
        logger.warn(LogMarker.ISPL, "No actions for {} although there transitions", agent);
        appendAgentProtocolOther(agentProtocolDef, Arrays.asList(Consts.ISPL_ACTION_NAME_NONE));
      }
    }

    return agentProtocolDef;
  }

  private VarDef createEnvironmentVariablesDefinition(final AgentAutomaton agent) {
    final VarDef vd = factory.createVarDef();

    final EList<VariableDefinition> varDefs = vd.getVarDefs();

    //noinspection Duplicates
    final LinkedList<String> variables = new LinkedList<>(agent.getVariables());
    variables.sort(String::compareToIgnoreCase);

    for (final String varName: variables) {
      AgentVariableDefinition agentVar = agent.getVariableDefinition(varName);
      if (agentVar == null) {
        logger.error(LogMarker.MODEL_PROBLEM, "Agent '{}' has no VariableDefinition for '{}'", agent, varName);
        throw new IllegalStateException("Agent " + agent + " has no VariableDefinition for " + varName);
      }
      if (agentVar.getValueType() == null) {
        logger.error(LogMarker.MODEL_PROBLEM, "Agent '{}' has no ValueType for '{}'", agent.getName(), varName);
        throw new IllegalStateException("Agent '" + agent.getName() + "' has no ValueType for '" + varName + "'");
      }

      switch (agentVar.getValueType()) {

        case ENUM:
          final VarDefEnumList varDefEnumList = factory.createVarDefEnumList();
          varDefEnumList.setName(isplWriter.translateToIsplName(agentVar.getName()));
          EnumList enumlist = varDefEnumList.getEnumlist();
          if (enumlist == null) {
            enumlist = factory.createEnumList();
            varDefEnumList.setEnumlist(enumlist);
          }
          varDefs.add(varDefEnumList);
          if (agentVar.hasAllowedValues()) {
            for (String s: agentVar.getAllowedValues()) {
              enumlist.getIds().add(isplWriter.translateToIsplValue(s));
            }
          }
          break;
        case BOOLEAN:
          final VarDefBoolean varDefBoolean = factory.createVarDefBoolean();
          varDefBoolean.setName(isplWriter.translateToIsplName(agentVar.getName()));
          varDefs.add(varDefBoolean);
          break;
        case INT:
          final VarDefInteger varDefInteger = factory.createVarDefInteger();
          varDefInteger.setName(isplWriter.translateToIsplName(agentVar.getName()));
          if (agentVar.hasAllowedValues() && agentVar.getAllowedValues().size() == 2) {
            varDefInteger.setBeginRange(Integer.valueOf(agentVar.getAllowedValues().get(0)));
            varDefInteger.setEndRange(Integer.valueOf(agentVar.getAllowedValues().get(1)));
          } else {
            logger.warn(
                LogMarker.MODEL_PROBLEM,
                "allowedValue should be exactly two int entries for getValueType()==INT, was [{}]",
                StringUtils.join(agentVar.getAllowedValues(), ", ")
            );
          }
          varDefs.add(varDefInteger);
          break;
      }
    }

    return vd;
  }

  private EnvBitExpression encodeAssignmentValue(final B2IExpression assignment) {
    logger.debug(LogMarker.ISPL, "Assignment: {}", assignment);
    LinkedList<EnvBitExpression> results = new LinkedList<>();
    switch (assignment.getType()) {
      case PARENTHESIZED:
        B2IUnaryExpression unaryExpression = (B2IUnaryExpression) assignment;
        EnvBitExpressionParenthesized parenthesized = factory.createEnvBitExpressionParenthesized();
        parenthesized.setExpression(encodeAssignmentValue(unaryExpression.getTerm()));
        results.add(parenthesized);
        break;
      case NUM_ADD:
        B2INaryExpression xExpression = (B2INaryExpression) assignment;
        results = Arrays.stream(xExpression.getTerms())
                        .sorted()
                        .map(this::encodeAssignmentValue)
                        .collect(Collectors.toCollection(LinkedList::new));
        return asEnvArithmeticExpressionAddition(results, AdditionOp.PLUS);
      case NUM_SUB:
        xExpression = (B2INaryExpression) assignment;
        results = Arrays.stream(xExpression.getTerms())
                        .map(this::encodeAssignmentValue)
                        .collect(Collectors.toCollection(LinkedList::new));
        return asEnvArithmeticExpressionAddition(results, AdditionOp.MINUS);
      case NUM_MUL:
        xExpression = (B2INaryExpression) assignment;
        results = Arrays.stream(xExpression.getTerms())
                        .sorted()
                        .map(this::encodeAssignmentValue)
                        .collect(Collectors.toCollection(LinkedList::new));
        return asEnvArithmeticExpressionMult(results, MultiplicationOp.MULTI);
      case NUM_DIV:
        xExpression = (B2INaryExpression) assignment;
        results = Arrays.stream(xExpression.getTerms())
                        .map(this::encodeAssignmentValue)
                        .collect(Collectors.toCollection(LinkedList::new));
        return asEnvArithmeticExpressionMult(results, MultiplicationOp.DIV);
      case BIT_NEGATE:
        EnvBitExpressionNegated neg = factory.createEnvBitExpressionNegated();
        neg.setOperand(encodeAssignmentValue(((B2IUnaryExpression) assignment).getTerm()));
        return neg;
      case BIT_AND:
        xExpression = (B2INaryExpression) assignment;
        results = Arrays.stream(xExpression.getTerms())
                        .sorted()
                        .map(this::encodeAssignmentValue)
                        .collect(Collectors.toCollection(LinkedList::new));
        return asEnvBitExpressionAnd(results);
      case BIT_OR:
        xExpression = (B2INaryExpression) assignment;
        results = Arrays.stream(xExpression.getTerms())
                        .sorted()
                        .map(this::encodeAssignmentValue)
                        .collect(Collectors.toCollection(LinkedList::new));
        return asEnvBitExpressionOr(results);
      case VALUE:
      case TERM:
        return asEnvLiteral(assignment);
      case VARIABLE_REF:
        return asEnvLiteral(assignment); //the varName is a QName, so it will be a EnvironmentRef here
      default:
        throw new NotImplementedException("unhandled Assignment.Type: " + assignment.getType());
    }
    return null;
  }

  private EnvEvoBoolResult encodeAssignments(final Map<QName, B2IExpression> assignments) {
    final EnvEvoBoolResult agentBoolResult;

    final LinkedList<EnvEvoBoolResult> results = new LinkedList<>();
    if (assignments.isEmpty()) {
      throw new IllegalStateException("StateEvolutionStatement.assignments should never be empty.");
    }
    assignments
        .keySet()
        .stream()
        .sorted()
        .forEachOrdered(
            (varName) -> {
              final B2IExpression        assignment       = assignments.get(varName);
              EnvEvoBoolResultAssignment resultAssignment = factory.createEnvEvoBoolResultAssignment();

              if (false == varName.belongsToAgent(currentAgent.getName())) {
                throw new IllegalStateException(varName + " should be variable of agent " + currentAgent.getName());
              }
              resultAssignment.setVarName(varName.getName());
              resultAssignment.setValue(encodeAssignmentValue(assignment));

              results.add(resultAssignment);
            }
        );

    if (results.size() == 1) {
      agentBoolResult = results.get(0);
    } else {

      EnvEvoBoolResultAnd rootAnd = factory.createEnvEvoBoolResultAnd();
      EnvEvoBoolResultAnd iter    = rootAnd;
      iter.setLeft(results.removeFirst());
      EnvEvoBoolResult last = results.removeLast();

      for (EnvEvoBoolResult isBoolCondition: results) {
        EnvEvoBoolResultAnd tmp = factory.createEnvEvoBoolResultAnd();
        tmp.setLeft(isBoolCondition);

        iter.setRight(asParenthesized(tmp));
        iter = tmp;
      }
      iter.setRight(last);

      agentBoolResult = rootAnd;
    }

    return agentBoolResult;
  }

  private EnvEvoBoolCondition encodeCompareConditionForEvolution(final B2ICompareExpression compareCondition) {
    switch (compareCondition.getType()) {
      default:
        final EnvEvoBoolConditionCompare agentEvoBoolConditionCompare = factory.createEnvEvoBoolConditionCompare();
        agentEvoBoolConditionCompare.setLogicop(isplWriter.getLogicOp(compareCondition.getOperator()));

        agentEvoBoolConditionCompare.setLeft(asEnvLiteral(compareCondition.getLeft()));
        agentEvoBoolConditionCompare.setRight(asEnvLiteral(compareCondition.getRight()));
        return agentEvoBoolConditionCompare;
    }
  }

  private EnvEvoBoolCondition encodeConditionTermForEvolution(@NonNull final B2IExpression o) {
    final LinkedList<EnvEvoBoolCondition> results = new LinkedList<>();
    switch (o.getType()) {
      case AND:
        for (final B2IExpression term: ((B2INaryExpression) o).getTerms()) {
          results.add(encodeConditionTermForEvolution(term));
        }

        EnvEvoBoolCondition rootAnd = asEnvEvoBoolConditionAnd(results);

        return asParenthesized(rootAnd);
      case OR:
        for (final B2IExpression term: ((B2INaryExpression) o).getTerms()) {
          results.add(encodeConditionTermForEvolution(term));
        }

        EnvEvoBoolCondition rootOr = asEnvEvoBoolConditionOr(results);

        return asParenthesized(rootOr);
      case NOT:
        EnvEvoBoolConditionNegated negated = factory.createEnvEvoBoolConditionNegated();
        negated.setCondition(asParenthesized(encodeConditionTermForEvolution(((B2IUnaryExpression) o).getTerm())));

        return negated;
      case PARENTHESIZED:
        return asParenthesized(encodeConditionTermForEvolution(((B2IUnaryExpression) o).getTerm()));
      case ACTION_COMPARE:
        final B2IActionExpression actionExpression = (B2IActionExpression) o;
        final QName action = actionExpression.getTerm();

        final EnvEvoBoolConditionAction agentEvoBoolConditionAction = factory.createEnvEvoBoolConditionAction();

        agentEvoBoolConditionAction.setActionName(isplWriter.translateToIsplName(action.getName()));
        if (null != action.getAgent()
            && false == action.belongsToAgent(currentAgent.getName()))
        {
          agentEvoBoolConditionAction.setAgentName(isplWriter.translateToIsplName(action.getAgent()));
        }
        return agentEvoBoolConditionAction;
      case COMPARE:
        if (o instanceof B2ICompareExpression) {
          return encodeCompareConditionForEvolution((B2ICompareExpression) o);
        }
      case TERM:
      case NUM_ADD:
      case NUM_SUB:
      case NUM_MUL:
      case NUM_DIV:
      case VALUE:
      case BIT_NEGATE:
      case BIT_AND:
      case BIT_OR:
      case VARIABLE_REF:
      default:
        throw new NotImplementedException("unhandled B2IExpression.type: " + o.getType());
    }
  }

  private EnvBoolCondition getAgentProtocolCondition(
      final AgentAutomaton agent, final B2INaryExpression actionConditions
  ) {

    final EnvBoolConditionParenthesized ret   = factory.createEnvBoolConditionParenthesized();
    int                                 count = actionConditions.getTerms().length;
    logger.debug(LogMarker.ISPL, "Env Protocol Condition");

    if (actionConditions.getTerms().length == 0) {
      return null;
    } else if (count == 1) {
      return asParenthesized(getEnvBoolConditionCompare((B2ICompareExpression) actionConditions.getTerms()[0]));
    } else {
      LinkedList<EnvBoolConditionCompare> results = new LinkedList<>();

      for (final B2IExpression compare: actionConditions.getTerms()) {
        if (compare instanceof B2ICompareExpression) {
          final EnvBoolConditionCompare conditionCompare = getEnvBoolConditionCompare((B2ICompareExpression) compare);
          results.add(conditionCompare);
        }
        //FIXME right condition of AND and recursive
      }

      ret.setCondition(asAgentProtocolBoolConditionAnd(results));
    }
    logger.debug(LogMarker.ISPL, "ProtocolConditions: {}", ret.getCondition());
    return ret;
  }

  private EnvBoolConditionCompare getEnvBoolConditionCompare(final B2ICompareExpression compare) {
    final EnvBoolConditionCompare conditionCompare = factory.createEnvBoolConditionCompare();
    conditionCompare.setLogicop(isplWriter.getLogicOp(compare.getOperator()));

    conditionCompare.setLeft(asEnvLiteral(compare.getLeft()));

    conditionCompare.setRight(asEnvLiteral(compare.getRight()));

    return conditionCompare;
  }
}
