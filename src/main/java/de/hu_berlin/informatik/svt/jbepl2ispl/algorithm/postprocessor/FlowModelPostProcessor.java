package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Christoph Graupner on 2018-06-27.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public abstract class FlowModelPostProcessor {
  @Getter
  protected final FlowModel flowModel;
  protected final Logger    logger = LoggerFactory.getLogger(getClass());

  protected FlowModelPostProcessor(final FlowModel flowModel) {
    this.flowModel = flowModel;
  }

  public abstract void finish();
}
