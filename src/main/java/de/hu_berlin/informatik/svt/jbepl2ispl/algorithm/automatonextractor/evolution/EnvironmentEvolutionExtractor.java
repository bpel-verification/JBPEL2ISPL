package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor.evolution;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentVariableDefinition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2INaryExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.XPathConditionStatement;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowMessageSend;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import org.apache.commons.lang3.StringUtils;

import javax.xml.xpath.XPathExpressionException;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class EnvironmentEvolutionExtractor extends BaseEvolutionExtractor {

  public EnvironmentEvolutionExtractor(
      final FlowModel flowModel, final AutomatonModel model,
      final VariableTargetServiceResolver resolver,
      final StateInformationResolver stateResolver
  ) {
    super(flowModel, model, resolver, stateResolver);
  }

  @Override
  protected void assignDirect_generateConditions(
      final AgentState agentState, final QName actionName, final B2INaryExpression condition
  ) {
    condition.addTerm(
        B2IExpression.actionOtherAgent(actionName)
    );
  }

  @Override
  protected void assignDirect_setVariableValues(
      final AgentState agentState,
      final AgentAutomaton srcAgentAutomaton, final AgentAutomaton trgtAgentAutomaton,
      final String symbolicVariableValueAt
  ) {
    AgentVariableDefinition commChannelVarDef = agent.getVariableDefinition(getCommChannel(agentState));

    if (logger.isDebugEnabled()) {
      logger.debug(LogMarker.DEBUG_VALUES, "evolution {}\nJJ {}\nKK {}\nLL {}\nII {}",
                   agent,
                   StringUtils.join(agent.getVariables(), "\n--"),
                   trgtAgentAutomaton,
                   commChannelVarDef,
                   Naming.getBufferName(srcAgentAutomaton.getName(), trgtAgentAutomaton.getName())
      );
    }
    if (commChannelVarDef == null) {
      throw new IllegalStateException(
          "There should be a commChannel for " + srcAgentAutomaton + " -> " + trgtAgentAutomaton + " communication.");
    }

    if (symbolicVariableValueAt == null) {
      logger.error(
          LogMarker.TODO_EXCEPTION,
          "EnvironmentEvolutionExtractor.extractEvolutionAssignDirect: prevent null value for {}",
          agentState
      ); //FIXME prevent null value
      agentState.setVariableValue(
          commChannelVarDef.getQName(),
          "TBD_symbolicVariableValueAt"
      );
    } else {
      agentState.setVariableValue(
          commChannelVarDef.getQName(),
          symbolicVariableValueAt
      );
    }
  }

  @Override
  protected AgentState extractEvolutionAssignLocal(
      final AgentState agentState
  ) {
    logger.error(
        LogMarker.TODO_EXCEPTION,
        "EnvironmentEvolutionExtractor.extractEvolutionAssignLocal: IMPLEMENT"
    ); //FIXME IMPLEMENT
    //FIXME differentiate between LITERAL assignment and calculation via XPATH
    final FlowState flowState = flowModel.findStateByFlowId(agentState.getOriginStateId());
    flowModel.findAllVarWriteAccessRelationsForState(flowState)
             .forEach(
                 relation -> {
                   switch (relation.getValueSourceType()) {
                     case VARIABLE:
                     case LITERAL:
                     case LITERAL_XML:
                     case TEXT:
                     case RECEIVE:
                     case INVOKE:
                     case REPLY:
                       final String symbolicValue = relation.getSymbolicValue();
                       agentState.setVariableValue(
                           relation.getVariableName(),
                           symbolicValue == null ? "novalue" : symbolicValue
                       );
                       break;
                     case CONDITION:
                     case XPATH:

                       try {
                         agentState.setAssignment(
                             relation.getVariableName(),
                             new XPathConditionStatement(relation.getValue()).asCondition()
                         );
                       } catch (XPathExpressionException e) {
                         logger.error(LogMarker.EXCEPTION, e.getLocalizedMessage(), e);
                       }
                       break;
                   }
                 }

             );

    //define conditions

    QName actionName = flowModel.findAllIncomingTransitions(flowModel.findStateByFlowId(agentState.getOriginStateId()))
                                .iterator()
                                .next()
                                .getIsplActionName();

    final B2INaryExpression conditions = agentState.getStateEntryConditions();
    conditions.addTerm(B2IExpression.action(actionName));
    agentState.setStateEntryConditions(conditions);
    return agentState;
  }

  @Override
  protected void invoke_setVariablesAndAction(
      final AgentState agentState, final FlowState flowState, final FlowMessageSend messageSend,
      final String bufferVarName, final String commChannelVarName,
      final QName actionName
  ) {
    //In ENV
    //set commChannel to <free>
    agentState.setVariableValue(new QName(agent.getName(), commChannelVarName), Consts.ISPL_VAR_ENUM_NONE);
  }
}
