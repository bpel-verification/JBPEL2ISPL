package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import org.apache.ode.bpel.compiler.bom.Activity;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.apache.ode.bpel.compiler.bom.FlowActivity;
import org.apache.ode.bpel.compiler.bom.SequenceActivity;

import java.util.*;

/**
 * Created by Christoph Graupner on 2018-02-26.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public class FlowHandler extends HandlerBase {
  private Map<FlowState, List<FlowState>> mapBranches = new HashMap<>();
  private Deque<FlowState>                stack       = new LinkedList<>();

  public FlowHandler() {
    super(
        new Interest(
            Arrays.asList(FlowActivity.class, SequenceActivity.class, BpelObject.class),
            -1,
            -1
        )
    );
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    final FlowModel        model   = compilerState.getFlowModel();
    final StateFlowFactory factory = model.factory();
    if (currentBpelObject instanceof FlowActivity) {
      FlowActivity flowActivity = ((FlowActivity) currentBpelObject);

      FlowState beginState = factory.createState(
          flowActivity.getName(),
          "FLOW_BEGIN__" + flowActivity.getName(),
          de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity.FLOW,
          BeginEndType.BEGIN,
          compilerState.getCurrentBpelPath()
      );
      model.save(beginState);
      stack.push(beginState);
      mapBranches.put(beginState, new LinkedList<>());
    } else if (stack.size() > 0 && currentBpelObject instanceof Activity) {
      final FlowState flowBeginState = stack.peekFirst();
      if (new BpelPath(compilerState.getCurrentBpelPath()).isDirectChildOf(
          flowBeginState.getBpelPath()))
      {
        final String originalBpelName = ((Activity) currentBpelObject).getName();
        FlowState branchBeginState = factory.createState(
            originalBpelName,
            "FLOWBRANCH_BEGIN__" + originalBpelName,
            de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity.FLOW_BRANCH,
            BeginEndType.BEGIN,
            compilerState.getCurrentBpelPath()
        );
        model.save(branchBeginState);

        FlowTransition transition = factory.createTransition(
            flowBeginState,
            branchBeginState,
            null,
            null,
            compilerState.getCurrentBpelPath()
        );
        model.save(transition);
      }
      // we are inside flow and need to track the direct activity child states
    }
    return (T) this;
  }

  @Override
  public <T extends HandlerBase> T handleEnd(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    final FlowModel        model   = compilerState.getFlowModel();
    final StateFlowFactory factory = model.factory();
    if (currentBpelObject instanceof FlowActivity) {
      FlowActivity flowActivity = ((FlowActivity) currentBpelObject);
      FlowState endState = factory.createState(
          flowActivity.getName(),
          "FLOW_END__" + flowActivity.getName(),
          de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity.FLOW,
          BeginEndType.END,
          compilerState.getCurrentBpelPath()
      );
      model.save(endState);
      final FlowState beginState = stack.pop();
      mapBranches.get(beginState).forEach(
          flowState -> {
            FlowTransition transition = factory.createTransition(
                flowState,
                endState,
                null,
                null,
                compilerState.getCurrentBpelPath()
            );
            model.save(transition);
          }
      );
      //TODO evaluate all states between FLOW_BEGIN and FLOW_END and connect direct children

    } else if (stack.size() > 0) {
      // we are inside flow and need to track the direct activity child states
      final FlowState flowBeginState = stack.peekFirst();
      if (new BpelPath(compilerState.getCurrentBpelPath()).isDirectChildOf(
          flowBeginState.getBpelPath()))
      {
        final String originalBpelName = ((Activity) currentBpelObject).getName();
        FlowState state = factory.createState(
            originalBpelName,
            "FLOWBRANCH_END__" + originalBpelName,
            de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity.FLOW_BRANCH,
            BeginEndType.END,
            compilerState.getCurrentBpelPath()
        );
        model.save(state);

        final FlowState prevState = model.findPreviousState(state);
        final FlowTransition transition = factory.createTransition(
            prevState,
            state,
            null,
            null,
            compilerState.getCurrentBpelPath()
        );
        model.save(transition);
        // remember to connect later to FLOW.END state
        mapBranches.get(flowBeginState).add(state);
      }
    }
    return super.handleEnd(currentBpelObject, compilerState);
  }
}
