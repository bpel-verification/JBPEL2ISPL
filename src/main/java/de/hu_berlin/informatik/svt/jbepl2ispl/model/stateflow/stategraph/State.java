package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowServiceOperation;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.QNameNeo4jConverter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.ogm.annotation.*;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import java.util.Objects;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NodeEntity
@NoArgsConstructor
public class State extends FlowBaseBpel implements FlowState {
  @Getter
  @Property
  private BeginEndType beginEndType = BeginEndType.BOTH;
  @Getter
  @Property
  private Activity causingActivity;
  @Getter
  @Setter
  @Convert(QNameNeo4jConverter.class)
  private QName entryActionName;
  @Getter
  @Setter
  @Convert(QNameNeo4jConverter.class)
  private QName exitActionName;
  @Getter
  @Id
  @GeneratedValue
  private Long id;
  @Getter
  @Setter
  private boolean initialState;
  private String inputVariable;
  private String originalBpelName;
  private String outputVariable;
  @Relationship(type = "SOURCE_SERVICE")
  private Service sourceService;
  @Relationship(type = "TARGET_SERVICE")
  private Service targetService;
  @Relationship(type = "EXECUTED_OP", direction = Relationship.OUTGOING)
  private ServiceOperation targetServiceOperation;

  State(
      final String originalBpelName,
      final String stateName, final Activity causingActivity, final BeginEndType beginEndType, final int elementId,
      final String currentBpelPath
  ) {
    super(elementId, currentBpelPath);
    setName(stateName);
    this.causingActivity = causingActivity;
    this.beginEndType = beginEndType;
    this.originalBpelName = originalBpelName;
  }

  State(final String newName, final State state, final int alternativeCount) {
    this(
        state.originalBpelName, newName, state.causingActivity, state.beginEndType, state.elementId,
        state.bpelPathAsString
    );
    super.cloneBase(state);
    setName(newName);
    inputVariable = state.inputVariable;
    outputVariable = state.outputVariable;
    sourceService = state.sourceService;
    targetService = state.targetService;
    targetServiceOperation = state.targetServiceOperation;
    entryActionName = state.entryActionName;
    exitActionName = state.exitActionName;
    this.alternativeCount = alternativeCount;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof State)) {
      return false;
    }
    final State state = (State) o;
    return getBeginEndType() == state.getBeginEndType() &&
           getCausingActivity() == state.getCausingActivity() &&
           Objects.equals(getName(), state.getName())
           && Objects.equals(this.getBpelPathAsString(), state.getBpelPathAsString());
  }

  @Override
  public String getDisplayName() {
    return getName();
  }

  @Override
  public String getInputVariable() {
    return inputVariable;
  }

  @Override
  public void setInputVariable(final String variable) {
    inputVariable = variable;
  }

  @Override
  public String getOriginalBpelNameAttribute() {
    return originalBpelName;
  }

  @Override
  public String getOutputVariable() {
    return outputVariable;
  }

  @Override
  public void setOutputVariable(final String variable) {
    outputVariable = variable;
  }

  @Override
  public String getPlainName() {
    return getProperty("originalName");
  }

  @Override
  public Service getSourceService() {
    return sourceService;
  }

  @Override
  public void setSourceService(final FlowService sourceService) {
    this.sourceService = (Service) sourceService;
  }

  @Override
  public Service getTargetService() {
    return targetService;
  }

  @Override
  public void setTargetService(final FlowService targetService) {
    this.targetService = (Service) targetService;
  }

  @Override
  public ServiceOperation getTargetServiceOperation() {
    return targetServiceOperation;
  }

  @Override
  public void setTargetServiceOperation(
      final FlowServiceOperation targetServiceOperation
  ) {
    this.targetServiceOperation = (ServiceOperation) targetServiceOperation;
  }

  @Override
  public int hashCode() {

    return Objects.hash(
        getBeginEndType(), getCausingActivity(), getName(), this.getBpelPathAsString());
  }

  @Override
  public boolean isStartState() {
    return elementId == 0; //"INITIAL_STATE".equals(name);
  }

  @Override
  public void setName(final String stateName) {
    setProperty("originalName", stateName);
    super.setName(String.format(Consts.STATE_NAME_PATTERN, getElementId(), stateName));
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("FlowState{");
    sb.append("'").append(name).append('\'');
    sb.append("@").append(elementId).append("/").append(id);
    sb.append("}");
    return sb.toString();
  }
}
