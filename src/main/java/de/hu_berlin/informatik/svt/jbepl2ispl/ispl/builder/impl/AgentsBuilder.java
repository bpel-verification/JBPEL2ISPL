package de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.impl;

import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplAgentBuilder;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplAgentsBuilder;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplBuilder;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.ISPLFactory;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.SimpleAgent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christoph Graupner on 2018-03-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class AgentsBuilder extends BuilderBase<IsplBuilder> implements IsplAgentsBuilder {
  List<SimpleAgentBuilder> agentBuilders = new ArrayList<>();

  public AgentsBuilder(
      final SimpleIsplBuilder parent,
      final ISPLFactory factory
  ) {
    super(factory, parent);
  }

  @Override
  public IsplAgentBuilder beginAgent() {
    final SimpleAgentBuilder simpleAgentBuilder = new SimpleAgentBuilder(getFactory(), this);
    agentBuilders.add(simpleAgentBuilder);
    return simpleAgentBuilder;
  }

  @Override
  public IsplBuilder leaveAgents() {
    return end();
  }

  @Override
  public Object build() {
    final List<SimpleAgent> ret = new ArrayList<>(agentBuilders.size());
    agentBuilders.forEach(simpleAgentBuilder -> ret.add((SimpleAgent) simpleAgentBuilder.build()));
    return ret;
  }
}
