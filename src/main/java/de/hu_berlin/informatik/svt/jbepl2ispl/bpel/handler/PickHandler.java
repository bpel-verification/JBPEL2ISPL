package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.StateFlowFactory;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.apache.ode.bpel.compiler.bom.OnAlarm;
import org.apache.ode.bpel.compiler.bom.OnMessage;
import org.apache.ode.bpel.compiler.bom.PickActivity;

import java.util.Arrays;

/**
 * Created by Christoph Graupner on 2018-03-05.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public class PickHandler extends HandlerBase {
  public PickHandler(
  ) {
    super(Arrays.asList(PickActivity.class, OnAlarm.class, OnMessage.class));
  }

  @Override
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    final FlowModel        model   = compilerState.getFlowModel();
    final StateFlowFactory factory = model.factory();
    if (currentBpelObject instanceof PickActivity) {
      PickActivity pickActivity = (PickActivity) currentBpelObject;
      FlowState state = factory.createState(
          pickActivity.getName(),
          "PICK__" + pickActivity.getName() + "__BEGIN",
          Activity.PICK,
          BeginEndType.BEGIN,
          compilerState.getCurrentBpelPath()
      );
      model.save(state);
      //inbound transition
//      FlowTransition transition = factory.createTransition(
//          null,
//          state,
//          null,
//          "null->PICK",
//          compilerState.getCurrentBpelPath()
//      );
//      model.save(transition);
    } else if (currentBpelObject instanceof OnAlarm) {
      OnAlarm      onAlarm = (OnAlarm) currentBpelObject;
      final String name    = onAlarm.getFor() == null ? "" : onAlarm.getFor().getTextValue();
      FlowState state = factory.createState(
          name,
          "PICK_ONALARM__" + name +
          (onAlarm.getUntil() == null ? "" : onAlarm.getUntil().getTextValue()) + "__BEGIN",
          Activity.PICK_ONALARM,
          BeginEndType.BEGIN,
          compilerState.getCurrentBpelPath()
      );
      model.save(state);

      //inbound transition from PICK BEGIN
//      FlowTransition transition = factory.createTransition(
//          null,
//          state,
//          name +
//          (onAlarm.getUntil() == null ? "" : onAlarm.getUntil().getTextValue()),
//          name, compilerState.getCurrentBpelPath()
//      );
//      model.save(transition);
    } else if (currentBpelObject instanceof OnMessage) {
      OnMessage    onMessage = (OnMessage) currentBpelObject;
      final String name      = onMessage.getPartnerLink() + "::" + onMessage.getOperation();
      FlowState state = factory.createState(
          name,
          "PICK_ONMSG__" + name + "__BEGIN",
          Activity.PICK_ONMSG,
          BeginEndType.BEGIN,
          compilerState.getCurrentBpelPath()
      );
      model.save(state);

      //inbound transition from PICK BEGIN
//      FlowTransition transition = factory.createTransition(
//          null,
//          state,
//          onMessage.getPartnerLink() + "::" + onMessage.getOperation() + onMessage.getMessageExchangeId(),
//          name, compilerState.getCurrentBpelPath()
//      );
//      model.save(transition);
    }
    return super.handleBegin(currentBpelObject, compilerState);
  }

  @Override
  public <T extends HandlerBase> T handleEnd(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    final FlowModel        model   = compilerState.getFlowModel();
    final StateFlowFactory factory = model.factory();
    if (currentBpelObject instanceof PickActivity) {
      PickActivity pickActivity = (PickActivity) currentBpelObject;
      FlowState state = factory.createState(
          pickActivity.getName(),
          "PICK__" + pickActivity.getName() + "__END",
          Activity.PICK,
          BeginEndType.END,
          compilerState.getCurrentBpelPath()
      );
      model.save(state);
    } else if (currentBpelObject instanceof OnAlarm) {
      OnAlarm      onAlarm = (OnAlarm) currentBpelObject;
      final String name    = onAlarm.getFor().getTextValue() + "--" + onAlarm.getUntil();
      FlowState state = factory.createState(
          name,
          "PICK_ONALARM__" + name + "__END",
          Activity.PICK_ONALARM,
          BeginEndType.END,
          compilerState.getCurrentBpelPath()
      );
      model.save(state);
    } else if (currentBpelObject instanceof OnMessage) {
      OnMessage    onMessage = (OnMessage) currentBpelObject;
      final String name      = onMessage.getPartnerLink() + "::" + onMessage.getOperation();
      FlowState state = factory.createState(
          name,
          "PICK_ONMSG__" + name + "__END",
          Activity.PICK_ONMSG,
          BeginEndType.END,
          compilerState.getCurrentBpelPath()
      );
      model.save(state);
    }
    return super.handleEnd(currentBpelObject, compilerState);
  }
}
