package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.ExtraInformation;

/**
 * Created by Christoph Graupner on 2018-03-06.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public interface FlowAdditional extends ExtraInformation {
  int getAlternativeCount();

  FlowId getFlowId();
}
