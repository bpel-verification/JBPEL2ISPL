package de.hu_berlin.informatik.svt.jbepl2ispl.visualize;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph.AnalysisGraph;
import org.jgrapht.io.ExportException;

import java.io.IOException;

/**
 * Created by Christoph Graupner on 2018-03-06.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public interface VisualOutputAutomaton {
  void render(final AnalysisGraph model, final String filename)
      throws IOException, ExportException;
}
