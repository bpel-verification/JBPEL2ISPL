package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.ExtraInformation;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;

/**
 * Created by Christoph Graupner on 2018-04-21.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AgentTransition extends Identifiable, ExtraInformation {
  QName getAction();

  void setAction(QName name);

  String getGuard();

  void setGuard(String guard);

  AgentState getSourceState();

  AgentState getTargetState();

  /**
   * Indicate whether the guard expression should be negated (for ELSE) or not.
   *
   * @return
   */
  boolean invertGuard();

  void setInvertGuard(boolean value);

  void setName(String name);
}
