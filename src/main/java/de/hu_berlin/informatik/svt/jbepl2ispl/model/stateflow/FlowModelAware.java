package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

/**
 * Created by Christoph Graupner on 2018-06-13.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface FlowModelAware {
  FlowModel getModel();

  void setModel(FlowModel model);
}
