package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import lombok.NonNull;

import java.util.Collection;
import java.util.List;

/**
 * Created by Christoph Graupner on 2018-06-15.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface FlowModelQueries {
  FlowService findAgentWithVariable(String variableName);

  /**
   * @param <T>
   *
   * @return FlowScCommitment node loaded till depth(3)
   */

  <T extends FlowState> List<T> findAllConnectedFollowingStates(FlowState state);

  <T extends FlowState> List<T> findAllConnectedPrecedingStates(FlowState state);

  <T extends FlowScFulfillment> Collection<T> findAllFulfillmentNodesFor(FlowScCommitment commitment);

  <T extends FlowState> Collection<Collection<T>> findAllFulfillmentPathsFor(
      FlowScCommitment commitment, FlowScFulfillment fulfillment
  );

  <T extends FlowTransition> List<T> findAllIncomingTransitions(FlowState state);

  <T extends FlowState> Collection<T> findAllInvokeStatesWithService(FlowService flowService);

  <T extends FlowState> Collection<T> findAllMessagingSendForService(String serviceName);

  /**
   * ATTENTION: it does already a refreshAll(..., 2);
   *
   * @param startState
   * @param <T>
   *
   * @return
   */
  <T extends FlowMessageSend> Collection<T> findAllMessagingSendForState(FlowState startState);

  <T extends FlowMessageSend> Collection<T> findAllMessagingSendForStateAndService(FlowState state, String forService);

  <T extends FlowTransition> List<T> findAllOutgoingTransitions(FlowState state);

  Collection<FlowState.PathReturn> findAllPathsBetween(FlowState sourceState, FlowState targetState);

  Collection<FlowState.PathReturn> findAllPathsBetweenExcluding(
      FlowState startStart, FlowState endState, List<FlowState> excludedStates
  );

  <T extends FlowService> Collection<T> findAllServicesItTalksToForService(String serviceName);

  <T extends FlowState> Collection<T> findAllStatesConnectedByInvolvingLinkService(FlowService flowService);

  Collection<Collection<FlowState>> findAllStatesWithSameVarWriteAndSameOrLiteralRead();

  <T extends FlowVarReadAccessRelation> Collection<T> findAllVarReadAccessRelationsForState(FlowState state);

  <T extends FlowVarReadAccessRelation> List<T> findAllVarReadAccessRelationsForVariable(String variableName);

  /**
   * Get all writes access for state.
   *
   * @param state
   * @param <T>
   *
   * @return
   */
  <T extends FlowVarWriteAccessRelation> Collection<T> findAllVarWriteAccessRelationsForState(FlowState state);

  <T extends FlowState> Collection<T> findAllWithSourceInAssignState(FlowService service);

  <T extends FlowState> Collection<T> findAllWithTargetInAssignState(FlowService targetService);

  /**
   * @param variableName
   * @param <T>
   *
   * @return
   *
   * @see #findAllVarWriteAccessRelationsForState(FlowState)
   */
  <T extends FlowVarWriteAccessRelation> Collection<T> findAllWriteAccessesForVariable(String variableName);

  FlowScCommitment findCommitmentStateById(int commitmentID);

  FlowState findFirstInvokeOrReplyTargetForVariableForwardStartingFrom(String variableName, FlowState startingState);

  FlowState findFirstServiceValueGoesTo(final FlowState flowState);

  FlowState findFirstWriteSourceForVariableBackwardStartingFrom(String variableName, FlowState startingState);

  FlowState findFollowingState(@NonNull FlowState state);

  FlowState findFollowingStateNotOfActivity(FlowState flowState, Activity... excludeActivities);

  <T extends FlowState> List<T> findIfStatesWithVariableInConditionFrom(
      FlowState state, final String variableName
  );

  FlowVarWriteAccessRelation findLastInitialWriteForVariable(String varName);

  FlowVarWriteAccessRelation findLastWriteAccessBeforeRead(
      @NonNull final String varName, @NonNull FlowVarReadAccessRelation readRelation
  );

  FlowState findPreviousState(@NonNull FlowState state);

  FlowService findServiceByName(@NonNull String agentName);

  FlowServiceOperation findServiceOperation(
      @NonNull FlowService flowService, @NonNull String actionName, String inputVariableName, String outputVariable
  );

  FlowState findStartState();

  FlowState findStateByFlowId(FlowId stateId);

  <T extends FlowState> List<T> findStatesBetween(FlowState stateBegin, FlowState stateEnd);

  <T extends FlowState> Collection<T> findStatesWithActivity(@NonNull Activity activityType);

  FlowVarReadAccessRelation findVarReadAccessRelationsByStateAndVariable(
      @NonNull FlowState state, @NonNull String variable
  );

  FlowVariable findVariableByName(@NonNull String variableName);
}
