package de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder;

/**
 * Created by Christoph Graupner on 2018-03-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface IsplBuilderBase<PARENT extends IsplBuilderBase> {
  PARENT end();
}
