package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver;

import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import lombok.NonNull;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.LinkedList;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
public class AllKnowing implements AllKnowingFacade {
  private final AutomatonModel automatonModel;
  private final FlowModel      flowModel;
  private final Logger         logger = LoggerFactory.getLogger(getClass());

  public AllKnowing(
      final AutomatonModel automatonModel,
      final FlowModel flowModel
  ) {
    this.automatonModel = automatonModel;
    this.flowModel = flowModel;
  }

  @Override
  public <T extends FlowMessageSend> Collection<T> findAllMessagingSendForState(
      @NonNull final FlowState startState
  ) {
    return flowModel.findAllMessagingSendForState(startState);
  }

  @Override
  public <T extends FlowMessageSend> Collection<T> findAllRelevantMessagingSendForStateAndService(
      @NonNull final FlowState state,
      @NonNull final String forService
  ) {
    switch (state.getCausingActivity()) {
      case INVOKE:
        return findAllRelevantMessagingSendForInvokeState(state, forService);
      case RECEIVE:
      case ASSIGN:
      case REPLY:
//        break;
      case IFCASE_ELSEIF:
      case IF:
      case WHILE:
        throw new NotImplementedException("For state with activity: " + state);
    }
    return null;
  }

  private <T extends FlowMessageSend> Collection<T> findAllRelevantMessagingSendForInvokeState(
      @NonNull final FlowState state, @NonNull final String forService
  ) {
    boolean isSender = state.getSourceService().getName().equals(forService);

    final Collection<FlowMessageSend> ret = new LinkedList<>();
    for (final FlowMessageSend flowMessageSend: flowModel.findAllMessagingSendForStateAndService(state, forService)) {
      if (isSender) {
        if (forService.equals(flowMessageSend.getSendingService().getName())) {
          ret.add(flowMessageSend);
        }
      } else {
        if (forService.equals(flowMessageSend.getReceivingService().getName())) {
          ret.add(flowMessageSend);
        }
      }
    }
    return (Collection<T>) ret;
  }

  @Override
  public FlowState findIndirectSourceStateOfVariableValueOf(
      @NonNull final FlowState state
  ) {
    return null;
  }

  @Override
  public FlowState findIndirectTargetStateOfVariableValueOf(
      @NonNull final FlowState state
  ) {
    return null;
  }

  @Override
  public FlowState findSourceStateOfVariableValueOf(
      @NonNull final FlowState state
  ) {

    FlowState from = flowModel.findFirstWriteSourceForVariableBackwardStartingFrom(
        state.getInputVariable(),
        state
    );
    return from;
  }

  @Override
  public FlowState findTargetStateOfVariableValueOf(
      @NonNull final FlowState state
  ) {
    return flowModel.findFirstInvokeOrReplyTargetForVariableForwardStartingFrom(
        state.getOutputVariable(),
        state
    );
  }

  @Override
  public int getFlowBranchNumber(@NonNull final FlowState state) {
    return 0;
  }

  /**
   * @param startState
   *
   * @return
   *
   * @see de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor.MarkMessageSendingStatesPostProcessor
   */
  @Override
  public String getServiceValueComesFrom(@NonNull final FlowState startState) {
    final Collection<FlowMessageSend> flowMessages = findAllMessagingSendForState(startState);

    if (false == flowMessages.isEmpty()) { //we have already a message flow defined
      if (logger.isDebugEnabled()) {
        logger.debug(LogMarker.DEBUG_VALUES, "getServiceValueComesFrom: flowMessages \n-- {}",
                     StringUtils.join(flowMessages, "\n-- ")
        );
      }
      return flowMessages.iterator().next().getSendingService().getName();
    }
    return null;
  }

  @Override
  public String getServiceValueGoesTo(@NonNull final FlowState startState) {
    final Collection<FlowMessageSend> flowMessages = findAllMessagingSendForState(startState);

    if (false == flowMessages.isEmpty()) { //we have already a message flow defined
      if (logger.isDebugEnabled()) {
        logger.debug(LogMarker.DEBUG_VALUES, "getServiceValueComesFrom: flowMessages{}",
                     StringUtils.join(flowMessages, "\n--")
        );
      }

      return flowMessages.iterator().next().getReceivingService().getName();
    }
    return null;
  }

  @Override
  public B2IExpression getSymbolicValueAt(
      @NonNull final FlowState state
  ) {
    //    return flowModel.findLastWriteAccessBeforeState(varName,state);
    switch (state.getCausingActivity()) {
      case INVOKE:
        final FlowVarReadAccessRelation read =
            flowModel.findVarReadAccessRelationsByStateAndVariable(state, state.getInputVariable());
        if (read != null) {
          return B2IExpression.term(read.getSymbolicValue());
        }
        throw new IllegalStateException("There is no read relation for INVOKE state " + state);
      case RECEIVE:
      case REPLY:
        throw new NotImplementedException("IMPLEMENT " + state); //FIXME implemnet
    }

    throw new NotImplementedException("IMPLEMENT " + state); //FIXME implemnet
  }

  @Override
  public String getSymbolicVariableValueAt(@NonNull final FlowState state) {
    logger.error(LogMarker.TODO_EXCEPTION, "Neo4jVariableAccessGraph.getSymbolicVariableValueAt: implement for {}",
                 state
    ); //FIXME implement for {}
// FIXME   throw new IllegalStateException("Implement");
    //input variable des states
    return flowModel.findAllVarWriteAccessRelationsForState(state).iterator().next().getSymbolicValue();
  }

  @Override
  public String getSymbolicVariableValueAt(
      @NonNull final String varName, @NonNull final FlowState state
  ) {

    for (FlowVarWriteAccessRelation writeAccessRelation: flowModel.findAllVarWriteAccessRelationsForState(
        state)) {
      //FIXME write access is maybe wrong as a result of the state the value is set
      if (writeAccessRelation.getVariableName().getName().equals(varName)) {
        return writeAccessRelation.getSymbolicValue();
      }
    }
    for (FlowVarReadAccessRelation readAccessRelation: flowModel.findAllVarReadAccessRelationsForState(
        state)) {
      if (readAccessRelation.getVariableName().getName().equals(varName)) {
        return readAccessRelation.getSymbolicValue();
      }
    }
    logger.error(LogMarker.TODO_EXCEPTION, "AllKnowing.getSymbolicVariableValueAt: implement" +
                                           " calculation for var {} at {}",
                 varName, state
    ); //FIXME implement for {} at {}

    return null;
  }
}
