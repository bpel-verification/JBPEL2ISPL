package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae;

import lombok.Getter;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public class B2IFormulaStatesRef implements B2IFormula {
  public enum StatesType {
    DESIRED, UN_DESIRED
  }

  @Getter final String     agent;
  @Getter final StatesType type;

  public B2IFormulaStatesRef(
      final StatesType type,
      final String agent
  ) {
    this.type = type;
    this.agent = agent;
  }

  @Override
  public Function getFunction() {
    return Function.TERM;
  }
}
