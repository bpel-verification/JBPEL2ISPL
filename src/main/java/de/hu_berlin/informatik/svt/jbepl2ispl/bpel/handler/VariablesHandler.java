package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVariable;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.apache.ode.bpel.compiler.bom.Variable;
import org.apache.ode.bpel.compiler.bom.Variables;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Christoph Graupner on 2018-02-26.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public class VariablesHandler extends HandlerBase {
  private static final List<Class<? extends BpelObject>> INTERESTS = Arrays.asList(
      Variable.class,
      Variables.class
  );

  public VariablesHandler() {
    super(INTERESTS);
  }

  @Override
  @SuppressWarnings("unchecked")
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    if (currentBpelObject instanceof Variable) {
      final Variable    variable = (Variable) currentBpelObject;
      final FlowModel   model    = compilerState.getFlowModel();
      final FlowService envAgent = model.findServiceByName(FlowService.ENV);

      FlowVariable flowVariable = model.factory().createVariable(
          variable.getName(),
          variable.getTypeName().toString(),
          compilerState.getCurrentBpelPath()
      );
      flowVariable.setProperty(Consts.PROPERTY_NAME_BPEL_VAR_DEFINITION, variable.toString());
      flowVariable.setBpelType(FlowVariable.BpelType.valueOf(variable.getKind()));
      flowVariable.setBelongsTo(envAgent);
      model.save(flowVariable);
      envAgent.addVariable(flowVariable.getName());
      model.save(envAgent);
      model.save(flowVariable);
    }
    return (T) this;
  }
}
