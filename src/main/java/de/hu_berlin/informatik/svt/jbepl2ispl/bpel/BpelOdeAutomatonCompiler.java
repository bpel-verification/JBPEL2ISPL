package de.hu_berlin.informatik.svt.jbepl2ispl.bpel;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler.*;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVariable;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.apache.ode.bpel.compiler.bom.InvokeActivity;
import org.apache.ode.bpel.compiler.bom.Process;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Created by Christoph Graupner on 2018-02-09.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
@Service
public class BpelOdeAutomatonCompiler {
  private final ApplicationContext                                  applicationContext;
  private final Logger                                              logger = LoggerFactory.getLogger(getClass());
  private       Process                                             bpelProcess;
  private       CompilerState                                       compilerState;
  private       Map<Class<? extends BpelObject>, List<HandlerBase>> handlerEndRegistry;
  private       Map<Class<? extends BpelObject>, List<HandlerBase>> handlerRegistry;
  private       HandlerBase[]                                       handlers;

  public BpelOdeAutomatonCompiler(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
    registerHandlers();
  }

  public FlowModel getResult() {
    return compilerState.getFlowModel();
  }

  public BpelOdeAutomatonCompiler run() {
    logger.trace("Transforming the read BPEL process into an automaton...");
    try {
      compilerState = new CompilerState(
          getBpelProcess(),
          applicationContext.getBean(FlowModel.class)
      );
    } catch (Exception e) {
      throw new IllegalStateException(e);
    }
//    logger.debug(bpelProcess.toString());
    checkBpelFileVersionForSupport();

    initCompositionModel();

    try {
      logger.info("Reading variable information...");
      parseVariableInformation(bpelProcess);
      logger.trace(String.valueOf(compilerState.getVarOwningMap()));
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      logger.error(LogMarker.EXCEPTION, e.getLocalizedMessage(), e);
    }

    try {
      iterateThroughBpel(bpelProcess);
      for (final HandlerBase handler: handlers) {
        handler.finish(compilerState);
      }
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException pE) {
      logger.error(pE.toString(), pE);
    }

    logger.trace("DONE Transforming the read BPEL process into an automaton...");

    return this;
  }

  public BpelOdeAutomatonCompiler with(final Process process) {
    setBpelProcess(process);
    return this;
  }

  protected void registerHandlers() {
    handlerRegistry = new HashMap<>();
    handlerEndRegistry = new HashMap<>();
    handlers = new HandlerBase[]{
        applicationContext.getBean(CommitmentHandler.class),
        new VariablesHandler(),
        new PartnerLinkHandler(),
        new FlowHandler(),
        new AssignHandler(),
        new InvokeHandler(),
        new ReceiveHandler(),
        new ReplyHandler(),
        new IfHandler(),
        new WhileHandler(),
        new PickHandler(),
        new SequenceHandler()
    };
    initHandlersCallList();
  }

  /**
   * Checks we just can process BPEL2.0 files
   *
   * @throws IllegalArgumentException if file is not BPEL2.0
   */
  private void checkBpelFileVersionForSupport() {
    if (getBpelProcess().getBpelVersion() != Process.Version.BPEL20) {
      throw new IllegalArgumentException("The provided BPEL file is not of version " + Process.Version.BPEL20);
    }
  }

  private Process getBpelProcess() {
    return bpelProcess;
  }

  private void setBpelProcess(final Process bpelProcess) {
    this.bpelProcess = bpelProcess;
  }

  private void initCompositionModel() {
    final FlowModel model = compilerState.getFlowModel();
    model.clear();

    final FlowState initialState = model.factory().createState(
        null,
        Consts.AGENT_STATE_INITIAL_NAME,
        Activity.__INITIAL_STATE,
        BeginEndType.BOTH,
        "/"
    );
    initialState.setInitialState(true);
    model.save(initialState);
    FlowService envAgent = model.factory().createService(FlowService.ENV, "/");
    model.save(envAgent);

    FlowVariable literalVar = model.factory().createVariable(Consts.VAR_NAME_LITERAL, "STRING", "/");
    model.save(literalVar);

    FlowVariable commitmentID = model.factory().createVariable(Consts.VAR_NAME_SC_COMMITMENT_ID, "INT", "/");
    commitmentID.setPurposeType(FlowVariable.PurposeType.OPERATIONAL);
    commitmentID.setBelongsTo(envAgent);
    model.save(commitmentID);

    FlowVariable fulfillmentID = model.factory().createVariable(Consts.VAR_NAME_SC_FULFILLMENT_ID, "INT", "/");
    fulfillmentID.setPurposeType(FlowVariable.PurposeType.OPERATIONAL);
    commitmentID.setBelongsTo(envAgent);
    model.save(fulfillmentID);
  }

  private void initHandlersCallList() {
    for (final HandlerBase handler: handlers) {
      for (final Class<? extends BpelObject> bpelObjectClass: handler.getClassInterests().classInterests) {
        List<HandlerBase> currentHandlers;
        List<HandlerBase> currentEndHandlers;
        if (handlerRegistry.containsKey(bpelObjectClass)) {
          currentHandlers = handlerRegistry.get(bpelObjectClass);
          currentEndHandlers = handlerEndRegistry.get(bpelObjectClass);
        } else {
          currentHandlers = new ArrayList<>();
          currentEndHandlers = new ArrayList<>();
          handlerRegistry.put(bpelObjectClass, currentHandlers);
          handlerEndRegistry.put(bpelObjectClass, currentEndHandlers);
        }
        currentHandlers.add(handler);
        currentEndHandlers.add(handler);
      }
    }
    //Interest on BpelObject.class indicates interests on all objects -> spread in every list
    if (handlerRegistry.containsKey(BpelObject.class)) {
      final List<HandlerBase> allInterestHandlers = handlerRegistry.get(BpelObject.class);
      for (final List<HandlerBase> handlerBases: handlerRegistry.values()) {
        for (final HandlerBase handlerBase: allInterestHandlers) {
          if (handlerBases.contains(handlerBase)) {
            continue;
          }
          handlerBases.add(handlerBase);
        }
      }

      final List<HandlerBase> allInterestHandlersEnd = handlerEndRegistry.get(BpelObject.class);
      for (final List<HandlerBase> handlerBases: handlerEndRegistry.values()) {
        for (final HandlerBase handlerBase: allInterestHandlersEnd) {
          if (handlerBases.contains(handlerBase)) {
            continue;
          }
          handlerBases.add(handlerBase);
        }
      }
    }
    for (final List<HandlerBase> handlerBases: handlerRegistry.values()) {
      handlerBases.sort(Comparator.comparingInt(o -> o.getClassInterests().beginPriority));
    }
    for (final List<HandlerBase> handlerBases: handlerEndRegistry.values()) {
      handlerBases.sort(Comparator.comparingInt(o -> o.getClassInterests().endPriority));
      Collections.reverse(handlerBases);
    }
  }

  private void iterateThroughBpel(final BpelObject pBpelObject)
      throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
    compilerState.getContext().push(pBpelObject);
    logger.debug("{}", compilerState.getContext());
    if (handlerRegistry.containsKey(pBpelObject.getClass())) {
      handlerRegistry
          .get(pBpelObject.getClass())
          .forEach(
              handlerBase -> handlerBase.handleBegin(pBpelObject, compilerState)
          );
    }
    for (BpelObject child: BpelUtil.getChildren(pBpelObject)) {
      compilerState.getContext().incCounter();
      iterateThroughBpel(child);
    }
    if (handlerEndRegistry.containsKey(pBpelObject.getClass())) {
      handlerEndRegistry
          .get(pBpelObject.getClass())
          .forEach(
              handlerBase -> handlerBase.handleEnd(pBpelObject, compilerState)
          );
    }
    compilerState.getContext().pop();
  }

  private void parseVariableInformation(final BpelObject pBpelObject)
      throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
    for (BpelObject child: BpelUtil.getChildren(pBpelObject)) {
      if (child instanceof InvokeActivity) {
        final InvokeActivity invokeActivity = (InvokeActivity) child;
        compilerState.setVariableOwner(invokeActivity.getInputVar(), invokeActivity.getPartnerLink());
        compilerState.setVariableOwner(invokeActivity.getOutputVar(), invokeActivity.getPartnerLink());
      }
      parseVariableInformation(child);
    }
  }
}
