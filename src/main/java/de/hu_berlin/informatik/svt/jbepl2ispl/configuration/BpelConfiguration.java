package de.hu_berlin.informatik.svt.jbepl2ispl.configuration;

import org.apache.ode.bpel.compiler.bom.BpelObjectFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Christoph Graupner on 2018-04-23.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Configuration
public class BpelConfiguration {
  @Bean
  BpelObjectFactory beanBpelObjectFactory() {
    return BpelObjectFactory.getInstance();
  }
}
