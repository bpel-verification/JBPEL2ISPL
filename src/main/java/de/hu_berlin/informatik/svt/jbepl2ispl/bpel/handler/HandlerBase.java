package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.ConditionStatement;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.XPathConditionStatement;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import lombok.Getter;
import org.apache.ode.bpel.compiler.bom.Activity;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.xpath.XPathExpressionException;
import java.util.List;

/**
 * Created by Christoph Graupner on 2018-02-26.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public abstract class HandlerBase {
  public static class Interest {
    public final int                               beginPriority;
    public final List<Class<? extends BpelObject>> classInterests;
    public final int                               endPriority;

    public Interest(
        final List<Class<? extends BpelObject>> classInterests,
        final int beginPriority,
        final int endPriority
    ) {
      this.beginPriority = beginPriority;
      this.classInterests = classInterests;
      this.endPriority = endPriority;
    }
  }

  final         Logger   logger = LoggerFactory.getLogger(getClass());
  @Getter
  private final Interest classInterests;

  HandlerBase(final List<Class<? extends BpelObject>> classInterests) {
    this.classInterests = new Interest(classInterests, 0, 0);
  }

  HandlerBase(final Interest classInterests) {
    this.classInterests = classInterests;
  }

  public void finish(final CompilerState compilerState) {
    logger.debug("{} is finishing", getClass());
  }

  @SuppressWarnings("unchecked")
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    return (T) this;
  }

  @SuppressWarnings("unchecked")
  public <T extends HandlerBase> T handleEnd(final BpelObject currentBpelObject, final CompilerState compilerState) {
    return (T) this;
  }

  protected void createReadAccessForCondition(
      final String conditionText, final FlowState state, final FlowModel model, final CompilerState compilerState
  ) {
    try {
      final ConditionStatement x = new XPathConditionStatement(conditionText);
      x.getVariables().forEach(
          varName -> model.save(
              model.factory().createVarReadAccessRelation(
                  state,
                  model.findVariableByName(varName),
                  x.getConditionForVariable(varName),
                  conditionText,
                  VarValueSourceType.CONDITION,
                  compilerState.getCurrentBpelPath()
              )
          )
      );
    } catch (XPathExpressionException e) {
      logger.error(LogMarker.EXCEPTION, e.getLocalizedMessage(), e);
    }
  }

  String getActivityName(Activity activity) {
    return activity.getName() + "@" + activity.getLineNo();
  }
}
