package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.*;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentTransitionModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import lombok.NonNull;
import org.neo4j.ogm.annotation.Transient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Christoph Graupner on 2018-05-29.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
@Primary
@Scope("prototype")
@Transient
public class Neo4jAgentAutomationFactory implements AgentAutomatonFactory {
  private final Map<Class<?>, Integer> alternativeCount = new HashMap<>();
  private final Map<Class<?>, Integer> elementIdCount   = new HashMap<>();
  @Autowired
  ApplicationContext applicationContext;

  @Override
  public <T extends AgentAutomaton> T createAgentAutomaton(
      @NonNull final FlowService originService, final boolean modifyable
  ) {
    return (T) applicationContext.getBean(Neo4jAgentAutomaton.class, originService.getName(), originService);
  }

  @Override
  public <T extends AgentAutomaton> T createAgentAutomaton(final String agentName, final boolean modifyable) {
    return (T) applicationContext.getBean(Neo4jAgentAutomaton.class, agentName);
  }

  @Override
  public AgentState createAgentState(@NonNull final FlowState flowState) {
    return new Neo4jAgentState(flowState, getElementId(AgentState.class));
  }

  @Override
  public AgentTransitionModifiable createAgentTransition(
      @NonNull final AgentState sourceState, @NonNull final AgentState targetState,
      final String guard,
      final String name
  ) {
    return new Neo4jAgentTransition(
        (Neo4jAgentState) sourceState,
        (Neo4jAgentState) targetState,
        guard,
        name,
        getElementId(AgentTransition.class)
    );
  }

  @Override
  public AgentVariableDefinition createAgentVariableDefinition(@NonNull final String name) {
    return new Neo4jVariableDefinition(name, getElementId(AgentVariableDefinition.class));
  }

  private int getAlternativeCount(final Class<?> aClass, final int elementId) {
    final Integer ret = alternativeCount.getOrDefault(aClass, 0);
    alternativeCount.put(aClass, ret + 1);
    return ret;
  }

  private int getElementId(final Class<?> aClass) {
    final Integer ret = elementIdCount.getOrDefault(aClass, 0);
    elementIdCount.put(aClass, ret + 1);
    return ret;
  }
}
