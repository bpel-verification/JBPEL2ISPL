package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.BpelPath;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowId;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowTransition;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.QNameNeo4jConverter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.neo4j.ogm.annotation.*;
import org.neo4j.ogm.annotation.Properties;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import java.util.*;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@RelationshipEntity(type = Naming.RELATION_FOLLOWED_BY)
@NoArgsConstructor
public class Transition implements FlowTransition {
  @Getter
  @Setter
  @Property
  String guard;
  @Getter
  @Setter
  @Property
  @Convert(QNameNeo4jConverter.class)
  QName  isplActionName;
  private String               bpelObjectPath;
  private int                  elementId;
  @Transient
  private FlowId               flowId;
  @Property
  private List<String>         guardVariables = new ArrayList<>();
  private boolean              invertGuard    = false;
  @Getter
  @Property
  private String               name;
  @Properties(prefix = "processed")
  private Map<String, Boolean> processed      = new HashMap<>();
  @Properties(prefix = "stringProperty")
  private Map<String, String>  properties     = new HashMap<>();
  @Id
  @GeneratedValue
  private Long                 relationshipId;
  @StartNode
  private State                sourceState;
  @EndNode
  private State                targetState;

  public Transition(
      final State fromState, final State toState, final String guard, final String name, final int elementId,
      final String currentBpelPath
  ) {
    this.bpelObjectPath = currentBpelPath;
    this.elementId = elementId;
    this.sourceState = fromState;
    this.targetState = toState;
    this.guard = guard;
    this.name = name;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Transition)) {
      return false;
    }
    final Transition that = (Transition) o;
    return Objects.equals(getGuard(), that.getGuard()) &&
           Objects.equals(getIsplActionName(), that.getIsplActionName()) &&
           Objects.equals(getSourceState(), that.getSourceState()) &&
           Objects.equals(getTargetState(), that.getTargetState()) &&
           Objects.equals(getName(), that.getName());
  }

  @Override
  public int getAlternativeCount() {
    return 0;
  }

  @Override
  public BpelPath getBpelPath() {
    return null;
  }

  @Override
  public void setBpelPath(final String bpelObjectPath) {
    this.bpelObjectPath = bpelObjectPath;
  }

  @Override
  public String getBpelPathAsString() {
    return bpelObjectPath;
  }

  @Override
  public int getElementId() {
    return elementId;
  }

  @Override
  public FlowId getFlowId() {
    if (flowId == null) {
      flowId = new FlowId(elementId, getAlternativeCount());
    }
    return flowId;
  }

  @Override
  public Long getId() {
    return relationshipId;
  }

  @Override
  public String getProperty(final String propertyName) {
    return properties.getOrDefault(propertyName, null);
  }

  @Override
  public FlowState getSourceState() {
    return sourceState;
  }

  @Override
  public void setSourceState(@NonNull final FlowState sourceState) {
    this.sourceState = (State) sourceState;
  }

  @Override
  public FlowState getTargetState() {
    return targetState;
  }

  @Override
  public void setTargetState(@NonNull final FlowState targetState) {
    this.targetState = (State) targetState;
  }

  @Override
  public int hashCode() {

    return Objects.hash(getGuard(), getIsplActionName(), getSourceState(), getTargetState(), getName());
  }

  @Override
  public boolean invertGuard() {
    return invertGuard;
  }

  @Override
  public boolean isFlagged(final String identifier) {
    return processed.getOrDefault(identifier, false);
  }

  @Override
  public void setFlagged(final String identifier, final boolean state) {
    processed.put(identifier, state);
  }

  @Override
  public void setInvertGuard(final boolean value) {
    invertGuard = value;
  }

  @Override
  public void setProperty(final String propertyName, final String value) {
    properties.put(propertyName, value);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("TransitionEntity{");
    sb.append(sourceState);
    if (guard != null || isplActionName != null) {
      sb.append(" -[").append(guard).append(", ").append(isplActionName).append("]-> ");
    } else {
      sb.append(" -> ");
    }
    sb.append(targetState);
    sb.append('}');
    return sb.toString();
  }
}
