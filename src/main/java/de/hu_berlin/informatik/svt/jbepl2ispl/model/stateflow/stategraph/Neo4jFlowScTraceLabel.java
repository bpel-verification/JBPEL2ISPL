package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowScTraceLabel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NodeEntity(label = "ScTraceLabel")
@NoArgsConstructor
public class Neo4jFlowScTraceLabel extends FlowBase implements FlowScTraceLabel {
  @Getter
  private Type    behaviourType;
  @Getter
  @Id
  @GeneratedValue
  private Long    id;
  private int     originalElementId;
  @Getter
  @Relationship(type = Naming.RELATION_FLOW_SC_TARGET)
  private State   targetState;
  @Getter
  @Relationship(type = Naming.RELATION_FLOW_SC_TRACED_SERVICE)
  private Service tracedService;

  public Neo4jFlowScTraceLabel(
      final String nameAttribute, @NonNull final FlowService tracedService, @NonNull final FlowState targetState,
      @NonNull Type behaviourType,
      final int originalElementId, final int elementId
  ) {
    super(nameAttribute, elementId);
    this.originalElementId = originalElementId;
    this.tracedService = (Service) tracedService;
    this.targetState = (State) targetState;
    this.behaviourType = behaviourType;
  }
}
