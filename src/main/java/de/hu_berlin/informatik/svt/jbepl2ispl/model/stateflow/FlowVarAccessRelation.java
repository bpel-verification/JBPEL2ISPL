package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface FlowVarAccessRelation extends BpelIdentifiable {
  String getSymbolicValue();

  void setSymbolicValue(String value);

  String getValue();

  void setValue(String value);

  String getValueSource();

  VarValueSourceType getValueSourceType();

  FlowVariable getVariable();

  QName getVariableName();
}
