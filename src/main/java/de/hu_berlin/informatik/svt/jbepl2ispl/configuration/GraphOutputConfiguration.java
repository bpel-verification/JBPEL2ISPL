package de.hu_berlin.informatik.svt.jbepl2ispl.configuration;

import de.hu_berlin.informatik.svt.jbepl2ispl.visualize.VisualOutputAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.visualize.graph.GraphOutput;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Christoph Graupner on 2018-04-23.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Configuration
public class GraphOutputConfiguration {
  @Bean
  VisualOutputAutomaton[] beanUmlOutput(GraphOutput graphOutput) {

    return new VisualOutputAutomaton[]{
        //        new UmlOutput(),

        graphOutput
    };
  }
}
