package de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder;

import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.*;

/**
 * Created by Christoph Graupner on 2018-03-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface IsplEnvironmentBuilder
    extends IsplAgentBuilderBase<IsplBuilder, VarDef, EnvActionDef, EnvProtocolDef, EnvEvolutionDef, EnvRedStatesDef>
{
  <T extends IsplEnvironmentBuilder> T obsVars(EnvObsVarDef envObsVars);
}
