package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Deque;
import java.util.LinkedList;

import static de.hu_berlin.informatik.svt.jbepl2ispl.model.MessagingType.*;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
public class MarkMessageSendingStatesPostProcessor extends FlowModelPostProcessor {
  /**
   * Created by Christoph Graupner on 2018-06-12.
   *
   * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
   */
  class CommunicationTypeDetector {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    public void detectAssignmentType(FlowState state) {
      if (state.isFlagged(Consts.PROPERTY_MESSAGING_TYPE)) {
        //already done, nothing to do
        return;
      }
      logger.debug(LogMarker.POSTPROCESSING, "Detecting AssignmentType of {}", state);

      if (state.isInitialState()) {
        state.setProperty(Consts.PROPERTY_MESSAGING_TYPE, INITIALISE.name());
      } else if (isDirectSendAttempt(state)) {
        state.setProperty(Consts.PROPERTY_MESSAGING_TYPE, DIRECT.name());
      } else if (isIndirectSendAttempt(state)) {
        //FIXME: that should not
        if (state.isInitialState()) {
          logger.warn(
              LogMarker.MODEL_PROBLEM,
              "Seems like an initial state {} appears like an indirect send attempt, but that is illogical.", state
          );
        }
        state.setProperty(Consts.PROPERTY_MESSAGING_TYPE, INDIRECT.name());
      } else if (state.getInputVariable() != null) {

        final FlowVariable outputVariable = flowModel.findVariableByName(state.getOutputVariable());
        if (outputVariable.getPurposeType() == FlowVariable.PurposeType.OPERATIONAL) {
          state.setProperty(Consts.PROPERTY_MESSAGING_TYPE, LOCAL.name());
        } else {
          final FlowVariable inputVariable = flowModel.findVariableByName(state.getInputVariable());

          final String property = inputVariable.getProperty(Consts.PROPERTY_VAR_IS_EXTERNAL_BPEL_CLIENT_INPUT);
          if (property != null && Boolean.valueOf(property)) {
            state.setProperty(
                Consts.PROPERTY_MESSAGING_TYPE,
                LOCAL.name()
                //FIXME maybe there is a client partnerlink(service) given -> connect var with it and set this to direct
            );
          } else {
            state.setProperty(Consts.PROPERTY_MESSAGING_TYPE, LOCAL.name());
          }
        }
      } else {
        state.setProperty(Consts.PROPERTY_MESSAGING_TYPE, LOCAL.name());
      }
      state.setFlagged(Consts.PROPERTY_MESSAGING_TYPE, true);
      getFlowModel().save(state);

      logger.debug(LogMarker.POSTPROCESSING, "DONE Detecting AssignmentType of {}", state);
    }

    public boolean isDirectSendAttempt(final FlowState state) {
      final String inputVariable  = state.getInputVariable();
      final String outputVariable = state.getOutputVariable();
      if (outputVariable != null && inputVariable != null) {
        logger.error(
            LogMarker.TODO_EXCEPTION,
            "CommunicationTypeDetector.isDirectSendAttempt: check if variable is send to service later"
        ); //FIXME check if variable is send to service later
        if (false == ((flowModel.findVariableByName(inputVariable).getPurposeType() == FlowVariable.PurposeType.MESSAGE)
                      &&
                      (flowModel.findVariableByName(outputVariable).getPurposeType() ==
                       FlowVariable.PurposeType.MESSAGE)))
        {
          return false;
        }
        final FlowState possibleInvoke =
            flowModel.findFirstInvokeOrReplyTargetForVariableForwardStartingFrom(outputVariable, state);
        return possibleInvoke != null;
      }
      return false;
    }

    public boolean isIndirectSendAttempt(final FlowState state) {
      if (state.getCausingActivity() != Activity.ASSIGN //we are detecting only for <assign> states
          || state.getOutputVariable() == null //should always false as <assign> output is always a variable
          || state.getInputVariable() == null //if != null, there was a variable involved
          || (
              //maybe FIXME for initial states
              state.getSourceService() != null && FlowService.ENV.equals(
                  state.getSourceService().getName())  //if assign from ENV it is not indirect
          )
      )
      {
        return false;
      }

      return flowModel.findVariableByName(state.getInputVariable()).getPurposeType() == FlowVariable.PurposeType.MESSAGE
             &&
             flowModel.findVariableByName(state.getOutputVariable()).getPurposeType() ==
             FlowVariable.PurposeType.CONTAINER;

//      List<FlowState> ifStates = getModel().findIfStatesWithVariableInConditionFrom(
//          state,
//          state.getOutputVariable()
//      );
//
//      if (logger.isDebugEnabled()) {
//        logger.debug("Found for state <{}> with variable name <{}>: {} IF states", state,
//                     state.getOutputVariable(), ifStates.size()
//        );
//      }
//
//      //FIXME implement
//      logger.warn(
//          LogMarker.TODO_EXCEPTION, "{}: Check if <assign> between [state] and <if>", Consts.NEEDS_IMPLEMENTATION
//      );
//      return !ifStates.isEmpty();
    }
  }

  @Getter
  private final CommunicationTypeDetector communicationTypeDetector;
  private final StateInformationResolver  resolver;

  MarkMessageSendingStatesPostProcessor(
      final StateInformationResolver resolver,
      final FlowModel flowModel
  ) {
    super(flowModel);
    this.communicationTypeDetector = new CommunicationTypeDetector();
    this.resolver = resolver;
  }

  @Override
  public void finish() {

    logger.trace(LogMarker.POSTPROCESSING, "Marking <assign>s and <invoke>s as belonging together in sending...");
    for (final FlowState state: flowModel.findStatesWithActivity(Activity.ASSIGN)) {
      communicationTypeDetector.detectAssignmentType(state);
      if (isLocalAssignment(state) || isInitialisingAssignment(state)) {

      } else {
        if (logger.isDebugEnabled()) {
          logger.debug(LogMarker.DEBUG_VALUES, "finish: doing for state: {}", state);
        }
        final FlowMessageSend msgSend = createMessageSend(state);
        if (msgSend != null) {
          flowModel.save(msgSend);
        }
      }
    }
    logger.trace(LogMarker.POSTPROCESSING, "DONE Marking <assign>s and <invoke>s as belonging together in sending...");
  }

  private FlowMessageSend createMessageSend(final FlowState state) {
    Deque<FlowState> sourceStates = findInvokeSourceStateOfVariableValueOf(state);
    if (logger.isDebugEnabled()) {
      logger.debug(
          LogMarker.DEBUG_VALUES, "createMessageSend: sourceStates {}", StringUtils.join(sourceStates, "\n--"));
    }
    Deque<FlowState> targetStates = findInvokeTargetStateOfVariableValueOf(state);
    if (logger.isDebugEnabled()) {
      logger.debug(
          LogMarker.DEBUG_VALUES, "createMessageSend: targetStates {}", StringUtils.join(targetStates, "\n--"));
    }
    if (!sourceStates.isEmpty() && !targetStates.isEmpty()) {
      final FlowState valueFromState = sourceStates.pollLast(); //retrieve+remove last(final) state
      final FlowState valueToState   = targetStates.pollLast();
      final FlowMessageSend messageSend = flowModel.factory().createFlowMessageSend(
          state,
          valueFromState,
          valueToState,
          DIRECT
      );

      assert valueToState != null;
      assert valueFromState != null;
      messageSend.setSendingService(valueFromState.getCausingActivity() == Activity.RECEIVE
                                    ? valueFromState.getSourceService()
                                    : valueFromState.getTargetService());
      messageSend.setReceivingService(valueToState.getTargetService());

      sourceStates.forEach(messageSend::addIntermediateSendingState);
      targetStates.forEach(messageSend::addIntermediateReceivingState);

      messageSend.setSendVariableName(valueFromState.getOutputVariable());
      messageSend.setBufferVariableName(
          Naming.getBufferName(
              messageSend.getSendingService().getName(),
              messageSend.getReceivingService().getName()
          )
      );

      messageSend.setCommChannelVariableName(
          determineCommChannelFor(messageSend.getValueSendingState(), messageSend.getValueReceivingState())
      );
      return messageSend;
    }
    return null;
  }

  private String determineCommChannelFor(final FlowState valueSendingState, final FlowState valueReceivingState) {
    logger.error(
        LogMarker.TODO_EXCEPTION,
        "MarkMessageSendingStatesPostProcessor.determineCommChannelFor:" +
        "IMPLEMENT; is it right to determine by sending or receiving state? maybe intermediates also involved"
    ); //FIXME is it right to determine by sending or receiving state? maybe intermediates also involved
    logger.error(
        LogMarker.TODO_EXCEPTION,
        "MarkMessageSendingStatesPostProcessor.determineCommChannelFor: IMPLEMENT determine by flow branch"
    ); //FIXME IMPLEMNET determine by flow branch

    return Naming.getCommChannelName(1);
  }

  private Deque<FlowState> findInvokeSourceStateOfVariableValueOf(final FlowState startState) {
    final LinkedList<FlowState> ret          = new LinkedList<>();
    FlowState                   intermediate = startState;
    do {
      //hopping of values through variables with different names
      intermediate = resolver.findSourceStateOfVariableValueOf(intermediate);
      if (intermediate != null) {
        ret.add(flowModel.refresh(intermediate, 2));
      }
    } while (intermediate != null && !Activity.VariableValueSourceActivity.contains(intermediate.getCausingActivity()));
    return ret;
  }

  private Deque<FlowState> findInvokeTargetStateOfVariableValueOf(final FlowState startState) {
    final LinkedList<FlowState> ret          = new LinkedList<>();
    FlowState                   intermediate = startState;
    do {
      //hopping of values through variables with different names
      intermediate = resolver.findTargetStateOfVariableValueOf(intermediate);
      if (intermediate != null) {
        ret.add(flowModel.refresh(intermediate, 2));
      }
    } while (intermediate != null && !Activity.VariableValueTargetActivity.contains(intermediate.getCausingActivity()));

    return ret;
  }

  private boolean isInitialisingAssignment(final FlowState state) {
    return state.isInitialState() || state.isStartState();
  }

  private boolean isLocalAssignment(final FlowState state) {
    return
        flowModel.findVariableByName(state.getOutputVariable()).getPurposeType() == FlowVariable.PurposeType.OPERATIONAL
        || state.getInputVariable() == null;
  }
}
