package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor;

import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomatonFactory;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */

public abstract class CommonExtractorBase {
  protected final VariableTargetServiceResolver resolver;
  protected final StateInformationResolver      stateResolver;
  final           AgentAutomatonFactory         factory;
  @Getter final   FlowModel                     flowModel;
  final           Logger                        logger = LoggerFactory.getLogger(getClass());
  @Getter final   AutomatonModel                model;

  protected CommonExtractorBase(
      final FlowModel flowModel, final AutomatonModel model, final StateInformationResolver stateResolver,
      final VariableTargetServiceResolver resolver
  ) {
    this.resolver = resolver;
    this.stateResolver = stateResolver;
    this.flowModel = flowModel;
    this.model = model;
    this.factory = model.factory();
  }

  public abstract void extract();
}
