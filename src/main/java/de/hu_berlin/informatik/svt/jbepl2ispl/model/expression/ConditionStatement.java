package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import java.util.Set;

/**
 * Created by Christoph Graupner on 2018-06-12.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface ConditionStatement {
  B2IExpression asCondition();

  String getConditionForVariable(String varName);

  String getConditionRaw();

  Set<String> getVariables();
}
