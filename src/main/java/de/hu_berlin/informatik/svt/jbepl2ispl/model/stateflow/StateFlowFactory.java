package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.MessagingType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import lombok.NonNull;

/**
 * Created by Christoph Graupner on 2018-03-01.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public interface StateFlowFactory {
  FlowMessageSend cloneMessageSend(FlowMessageSend messageSend);

  FlowState cloneState(FlowState state, final String cloneStateName);

  FlowAlternativeStateLink createAlternativeLink(FlowState state, FlowState altState);

  BpelFlowAggregateState createBpelFlowAggregateState(FlowState flowBeginState);

  FlowMessageSend createFlowMessageSend(
      final FlowState originState,
      final FlowState valueFromState,
      final FlowState valueToState,
      final MessagingType messagingType
  );

  FlowScCommitment createScCommitment(
      final String nameAttribute,
      final int commitmentID,
      final FlowService debtor,
      final FlowService creditor,
      final FlowState targetState,
      final int originalElementId
  );

  FlowScFulfillment createScFulfillment(
      final String nameAttribute,
      final FlowScCommitment commitmentID,
      final int fulfillmentID,
      final FlowState targetState,
      final int originalElementId
  );

  FlowScTraceLabel createScTraceLabel(
      final String nameAttribute,
      final FlowService tracedService,
      final FlowState targetState,
      final FlowScTraceLabel.Type behaviourType,
      final int originalElementId
  );

  FlowService createService(String name, final String bpelObjectPath);

  FlowServiceOperation createServiceOperation(
      final FlowService service, String action,
      String inputVariable, String outputVariable, String currentBpelPath
  );

  FlowState createState(
      final String originalBpelName,
      @NonNull final String stateName,
      @NonNull final Activity causingActivity,
      @NonNull final BeginEndType beginEndType,
      String currentBpelPath
  );

  FlowStateInvolvesServiceLink createStateInvolvesServiceLink(FlowState state, FlowService agent);

  FlowTransition createTransition(
      @NonNull FlowState sourceState, @NonNull FlowState targetState, String guard,
      final String name, String currentBpelPath
  );

  FlowTransition createTransition(
      @NonNull FlowState sourceState, @NonNull FlowState targetState, String guard,
      String name, String bpelPathAsString,
      int elementId
  );

  FlowVarReadAccessRelation createVarReadAccessRelation(
      final FlowState state,
      @NonNull final FlowVariable variable, final String value, final String valueSource,
      final VarValueSourceType varValueSourceType,
      final String currentBpelPath
  );

  FlowVarWriteAccessRelation createVarWriteAccessRelation(
      final FlowState state,
      @NonNull final FlowVariable variable, final String value, final String valueSource,
      final VarValueSourceType varValueSourceType,
      final String currentBpelPath
  );

  FlowVariable createVariable(
      @NonNull String name, String variableOriginalType, final String currentBpelPath
  );
}
