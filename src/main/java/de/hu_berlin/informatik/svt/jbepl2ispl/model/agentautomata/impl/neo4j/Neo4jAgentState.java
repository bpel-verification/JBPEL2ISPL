package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.MessagingType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentStateModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2INaryExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowId;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.FlowIdNeo4jConverter;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.Jb2iUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.neo4j.ogm.annotation.*;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Christoph Graupner on 2018-05-29.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NodeEntity(label = "AgentState")
@NoArgsConstructor
public class Neo4jAgentState extends BaseAgentElement implements AgentStateModifiable {
  @Relationship(type = Naming.RELATION_AUTOMATON_STATE, direction = Relationship.INCOMING)
  @Getter
  private Neo4jAgentAutomaton agentAutomaton;

  private String                    agentName;
  @Transient
  @Setter
  private Map<QName, B2IExpression> assignmentMap;
  @Properties(prefix = "asn", delimiter = "#")
  private Map<String, String>       assignments      = new HashMap<>();
  @Getter
  private Activity                  causingActivity;
  @Getter
  @Id
  @GeneratedValue
  private Long                      id;
  @Getter
  private boolean                   initialState;
  @Getter
  private MessagingType             messagingType;
  @Convert(FlowIdNeo4jConverter.class)
  private FlowId                    originStateId;
  @Getter
  private int                       ownElementId;
  private String                    stateEntryCondition;
  @Properties(prefix = "var", delimiter = "#")
  private Map<String, String>       variableValueMap = new HashMap<>();

  Neo4jAgentState(final FlowState flowState, final int ownElementId) {
    super(flowState.getElementId(), flowState.getAlternativeCount());
    this.name = String.format(Consts.STATE_NAME_PATTERN, flowState.getElementId(), flowState.getPlainName());
    this.ownElementId = ownElementId;
    this.causingActivity = flowState.getCausingActivity();
    this.messagingType = getMessagingType(flowState);
    this.initialState = flowState.isInitialState();
    this.originStateId = new FlowId(flowState.getFlowId());
  }

//  Neo4jAgentState(final String name) {
//    this.originStateId = null;
//    this.name = name;
//    this.elementId = -1;
//    messagingType = null;
//  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Neo4jAgentState)) {
      return false;
    }
    final Neo4jAgentState that = (Neo4jAgentState) o;
    return Objects.equals(getId(), that.getId());
  }

  @Override
  public B2IExpression getAssignment(final String variableName) {
    if (!assignments.containsKey(variableName)) {
      return null;
    }
    try {
      return (B2INaryExpression) Jb2iUtils.fromBase64StringToObject(assignments.get(variableName));
    } catch (IOException | ClassNotFoundException e) {
      throw new IllegalStateException("assignments entry for " + variableName + " is not an B2IExpression object", e);
    }
  }

  @Override
  public Map<QName, B2IExpression> getAssignmentMap() {
    if (assignmentMap == null) {
      assignmentMap = new HashMap<>();

      variableValueMap
          .keySet()
          .stream()
          .sorted()
          .forEachOrdered(
              (var) -> {
                final String variableValue = getVariableValue(var);
                if (Jb2iUtils.isVariableReference(variableValue)) {
                  assignmentMap.put(
                      new QName(var),
                      B2IExpression.variableReference(Jb2iUtils.getVariableOfVariableReference(variableValue))
                  );
                } else {
                  assignmentMap.put(
                      new QName(var), B2IExpression.term(variableValue));
                }
              }
          );

      assignments
          .keySet()
          .stream()
          .sorted()
          .forEachOrdered(
              varName -> {
                final QName qName = new QName(varName);

                final B2IExpression variableValue = getAssignment(varName);
                if (assignmentMap.containsKey(qName)) {
                  assignmentMap.put(
                      qName,
                      B2IExpression.and(
                          assignmentMap.get(qName),
                          variableValue
                      )
                  );
                } else {
                  assignmentMap.put(
                      qName,
                      variableValue
                  );
                }
              }
          );
    }

    return assignmentMap;
  }

  @Override
  public String getIsplName() {
    return getName();
  }

  public MessagingType getMessagingType(FlowState originState) {
    if (logger.isDebugEnabled()) {
      logger.debug(LogMarker.DEBUG_VALUES, "originState: {}", originState);
    }
    if (originState == null) {
      return null;
    }
    final String property = originState.getProperty(Consts.PROPERTY_MESSAGING_TYPE);
    if (property == null) {
      if (originState.isInitialState()) {
        return MessagingType.INITIALISE;
      }
      return null;
    }

    return MessagingType.valueOf(property);
  }

  public FlowId getOriginStateId() {
    if (originStateId == null) {
      originStateId = new FlowId(getElementId(), alternativeCount);
    }
    return originStateId;
  }

  @Override
  public B2INaryExpression getStateEntryConditions() {
    if (stateEntryCondition == null) {
      return B2IExpression.and();
    }
    try {
      return (B2INaryExpression) Jb2iUtils.fromBase64StringToObject(stateEntryCondition);
    } catch (IOException | ClassNotFoundException e) {
      throw new IllegalStateException("stateEntryCondition is not an B2IExpression object", e);
    }
  }

  @Override
  public void setStateEntryConditions(
      final B2IExpression condition
  ) {
    try {
      stateEntryCondition = Jb2iUtils.toBase64String(condition);
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  @Override
  public String getVariableValue(final String variableName) {
    if (variableValueMap.containsKey(variableName)) {
      return Jb2iUtils.unwrapFromBase64AsString(variableValueMap.get(variableName));
    }
    return null;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public void setAgentAutomaton(@NonNull final AgentAutomaton agentAutomaton) {
    this.agentAutomaton = (Neo4jAgentAutomaton) agentAutomaton;
    this.agentName = agentAutomaton.getName();
  }

  @Override
  public void setAssignment(
      @NonNull final QName variableName, @NonNull final B2IExpression expression
  ) {
    if (!variableName.belongsToAgent(agentName))
      throw new IllegalStateException(agentName);
    try {
      assignments.put(variableName.getFullName(), Jb2iUtils.toBase64String(expression));
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  @Override
  public void setName(final String name) {
    super.setName(name);
  }

  @Override
  public void setVariableValue(@NonNull final QName varName, @NonNull final String value) {
    if (varName.hasAgent() == false) {
      throw new IllegalStateException("varName.agent must be non-null for " + varName);
    }
    if (!FlowService.ENV.equals(agentName) && !varName.belongsToAgent(agentName)) {
      throw new IllegalStateException(
          "Only in Environment a value for a variable of an other agent could be set. You try to set value for " +
          varName + " in agent " + agentAutomaton);
    }
    variableValueMap.put(varName.getFullName(), Jb2iUtils.wrapAsBase64(value));
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("AgentState{");
    sb.append("name='").append(getName()).append('\'');
    sb.append(", originID=").append(getOriginStateId());
    sb.append('}');
    return sb.toString();
  }
}
