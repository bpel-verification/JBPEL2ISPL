package de.hu_berlin.informatik.svt.jbepl2ispl.bpel;

import de.hu_berlin.informatik.svt.jbepl2ispl.utils.Stack;
import lombok.Getter;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Created by Christoph Graupner on 2018-02-26.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public class BpelObjectStack implements Stack<BpelObject> {
  public class StackElement {
    @Getter
    private final BpelObject element;
    @Getter
    private final int        lineNo;
    @Getter
    private final int        parentPosition;
    @Getter
    private       String     path;

    StackElement(final BpelObject pElement, final String pPath, final int lineNo) {
      element = pElement;
      path = pPath;
      this.lineNo = lineNo;
      this.parentPosition = getPositionInParent();
    }

    int getPositionInParent() {
      final Node     parentNode = element.getElement().getParentNode();
      final NodeList li         = parentNode.getChildNodes();
      for (int i = 0; i < li.getLength(); i++) {
        if (li.item(i).isSameNode(element.getElement())) {
          return i;
        }
      }
      return -1;
    }
  }

  private Deque<StackElement>        stack   = new ArrayDeque<>();

  BpelObjectStack() {
    super();
  }

  public void incCounter() {
    final StackElement peekFirst = stack.peekFirst();
  }

  @Override
  public boolean isEmpty() {
    return stack.isEmpty();
  }

  @Override
  public BpelObject peek() {
    return stack.isEmpty() ? null : stack.peekFirst().element;
  }

  public StackElement peekElement() {
    return stack.peekFirst();
  }

  @Override
  public BpelObject pop() {
    if (stack.isEmpty()) {
      return null;
    }
    final StackElement pop = stack.pop();
    return pop.element;
  }

  @Override
  public Stack<BpelObject> push(final BpelObject element) {

    final StackElement e = new StackElement(element, null, element.getLineNo());
    stack.push(e);
    e.path = getCurrentPath();
    return this;
  }

  @Override
  public String toString() {
    return getCurrentPath();
  }

  private String getCurrentPath() {
    final StringBuilder sb = new StringBuilder();
    stack.descendingIterator()
         .forEachRemaining(
             pStackElement -> sb.append('/').append(pStackElement.element.getElement().getTagName())
                                .append('[').append(pStackElement.parentPosition)
                                .append(':').append(pStackElement.lineNo)
                                .append(']')
         );

    return sb.toString();
  }
}
