package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;

import java.util.List;

/**
 * Created by Christoph Graupner on 2018-03-01.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public interface FlowState extends BpelIdentifiable, FlowAdditional, Cloneable {
  class PathReturn {
    public List<FlowState>      states;
    public List<FlowTransition> transitions;

    public PathReturn(
        final List<FlowState> states,
        final List<FlowTransition> transitions
    ) {
      this.states = states;
      this.transitions = transitions;
    }
  }

  BeginEndType getBeginEndType();

  Activity getCausingActivity();

  String getDisplayName();

  /**
   * Use {@link FlowTransition#getIsplActionName()}
   *
   * @return
   */
  @Deprecated
  QName getEntryActionName();

  /**
   * Use {@link FlowTransition#setIsplActionName(QName)}
   *
   * @return
   */
  @Deprecated
  void setEntryActionName(QName entryActionName);

  /**
   * Not used ATM
   */
  @Deprecated
  QName getExitActionName();

  /**
   * Not used ATM
   */
  @Deprecated
  void setExitActionName(QName exitActionName);

  /**
   * For an <strong>&lt;assign&gt;</strong> activity: the variable use in the &lt;from&gt; tag. If &lt;from&gt; is not a variable, it is returning <em>null</em>.
   * <p>
   * For an <strong>&lt;invoke&gt;</strong> activity: the variable mentioned in the "<em>inputVariable</em>" attribute.
   *
   * @return
   */
  String getInputVariable();

  void setInitialState(boolean value);

  void setInputVariable(String variable);

  String getName();

  void setName(String stateName);

  String getOriginalBpelNameAttribute();

  /**
   * For an <strong>&lt;assign&gt;</strong> activity: the variable use in the &lt;to&gt; tag.
   * <p>
   * For an <strong>&lt;invoke&gt;</strong> activity: the variable mentioned in the "<em>outputVariable</em>" attribute.
   *
   * @return <em>null</em>, if there was no variable involved in activity.
   */
  String getOutputVariable();

  void setOutputVariable(String variable);

  String getPlainName();

  FlowService getSourceService();

  void setSourceService(FlowService sourceService);

  FlowService getTargetService();

  void setTargetService(FlowService targetService);

  FlowServiceOperation getTargetServiceOperation();

  void setTargetServiceOperation(FlowServiceOperation targetServiceOperation);

  boolean isInitialState();

  boolean isStartState();
}
