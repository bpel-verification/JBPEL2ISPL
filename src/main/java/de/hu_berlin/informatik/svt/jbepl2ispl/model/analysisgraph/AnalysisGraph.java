package de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowTransition;
import lombok.Getter;
import org.jgrapht.Graph;
import org.jgrapht.graph.DirectedPseudograph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Wraps a JGraph instance around the original graph. so it can be manipulated (the transitions and containing nodes,
 * but you should not alter the properties of transition or a node)
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class AnalysisGraph {
  @Getter
  private final Graph<FlowStateWrapper, FlowTransitionWrapper> graph;
  private final Logger                                         logger = LoggerFactory.getLogger(getClass());
  @Getter
  private       HashMap<FlowState, FlowStateWrapper>           stateMap;

  public AnalysisGraph(
      FlowModel model
  ) {
    this.graph = generateGraph(model);
  }

  public void removeState(final FlowStateWrapper stateWrapper) {
    final Set<FlowTransitionWrapper> incomingEdges = new HashSet<>(graph.incomingEdgesOf(stateWrapper));
    final Set<FlowTransitionWrapper> outgoingEdges = new HashSet<>(graph.outgoingEdgesOf(stateWrapper));
    outgoingEdges.forEach(
        outgoing -> incomingEdges.forEach(
            incoming -> {
              final FlowStateWrapper      newSourceState = graph.getEdgeSource(incoming);
              final FlowStateWrapper      newTargetState = graph.getEdgeTarget(outgoing);
              final FlowTransitionWrapper existsEdge     = graph.getEdge(newSourceState, newTargetState);
              if (existsEdge == null) {
                if (outgoing.getGuard() != null && incoming.getGuard() != null) {
                  logger.error(
                      LogMarker.TODO_EXCEPTION,
                      "AnalysisGraph.removeState: guard is set on both transitions!"
                  ); //FIXME guard is set on both transitions!
                }
                if (outgoing.getIsplActionName() != null && incoming.getIsplActionName() != null) {
                  logger.error(
                      LogMarker.TODO_EXCEPTION,
                      "AnalysisGraph.removeState: actionName is set on both transitions!"
                  ); //FIXME actionName is set on both transitions!
                }

                graph.addEdge(
                    newSourceState,
                    newTargetState,
                    new OverlayFlowTransition(
                        incoming.getSourceState(),
                        outgoing.getTargetState(),
                        outgoing //preserve values of following state
                    )
                );
              }
            }
        )
    );
    graph.removeVertex(stateWrapper);
    getStateMap().remove(stateWrapper.wrapped());
  }

  private Graph<FlowStateWrapper, FlowTransitionWrapper> generateGraph(
      final FlowModel model
  ) {
    logger.trace("Generate graph... BEGIN");

    Graph<FlowStateWrapper, FlowTransitionWrapper> graph = new DirectedPseudograph<>(FlowTransitionWrapper.class);

    final List<FlowState> states = new LinkedList<>(model.getStates());
    stateMap = new HashMap<>(states.size());
    states.sort(Comparator.comparingInt(FlowState::getElementId));

    FlowStateWrapper nodeInitial = new FlowStateDelegate(states.get(0));
    stateMap.put(states.get(0), nodeInitial);
    graph.addVertex(nodeInitial);

    states.remove(0);

    for (FlowState aStateEntity: states) {
      final FlowStateWrapper nodeState = new FlowStateDelegate(aStateEntity);
      if (stateMap.containsKey(aStateEntity)) {
        FlowState other = null;
        for (FlowState flowState: stateMap.keySet()) {
          if (flowState.equals(aStateEntity)) {
            other = flowState;
            break;
          }
        }
        logger.error("State already mapped: {} <-> {}", aStateEntity, other);
      }
      if (null != stateMap.putIfAbsent(aStateEntity, nodeState)) {
        logger.error("Hashclash in statemap: {} <-> {}", aStateEntity, nodeState);
      }
      graph.addVertex(nodeState);
    }

    Iterable<? extends FlowTransition> transitions = model.getTransitions();
    int                                countT      = 0;
    for (final FlowTransition transition: transitions) {
      countT++;
      FlowStateWrapper from = stateMap.get(transition.getSourceState());
      FlowStateWrapper to   = stateMap.get(transition.getTargetState());
      if (from != null && to != null) {
        graph.addEdge(from, to, new FlowTransitionDelegate(transition));
      } else {
        logger.warn("No vertex: {}: {}, {} ", transition, from, to);
      }
    }
    logger.trace("Generate graph... END; transitions: {}", countT);

    return graph;
  }
}
