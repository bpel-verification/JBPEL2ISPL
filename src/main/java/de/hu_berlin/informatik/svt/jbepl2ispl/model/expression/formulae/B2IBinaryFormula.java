package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public interface B2IBinaryFormula extends B2IFormula {
  B2IFormula getLeft();

  B2IFormula getRight();
}
