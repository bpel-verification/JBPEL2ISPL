package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowScCommitment;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowScFulfillment;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NodeEntity(label = "ScFulfillment")
@NoArgsConstructor

public class Neo4jFlowScFulfillment extends FlowBase implements FlowScFulfillment {
  @Getter
  @Relationship(type = Naming.RELATION_FLOW_SC_FULFILLMENT_OF)
  private Neo4jFlowScCommitment commitment;
  @Getter
  private int                   fulfillmentId;
  @Getter
  @Id
  @GeneratedValue
  private Long                  id;
  @Getter
  private int                   originalElementId;
  @Relationship(type = Naming.RELATION_FLOW_SC_TARGET)
  @Getter
  private State                 targetState;

  public Neo4jFlowScFulfillment(
      final String name,
      @NonNull final FlowScCommitment commitment,
      final int fulfillmentId,
      @NonNull final FlowState targetState,
      final int originalElementId,
      final int elementId
  ) {
    super(name, elementId);
    this.commitment = (Neo4jFlowScCommitment) commitment;
    this.fulfillmentId = fulfillmentId;
    this.originalElementId = originalElementId;
    this.targetState = (State) targetState;
  }
}
