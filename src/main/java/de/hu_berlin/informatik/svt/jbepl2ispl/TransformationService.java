package de.hu_berlin.informatik.svt.jbepl2ispl;

import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor.AutomatonModelExtractor;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor.MainFlowModelPostProcessor;
import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.BpelOdeAutomatonCompiler;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.IsplWriter;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.IsplWriterFactory;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph.AnalysisGraph;
import de.hu_berlin.informatik.svt.jbepl2ispl.visualize.VisualOutputAutomaton;
import lombok.Getter;
import lombok.Setter;
import org.apache.ode.bpel.compiler.bom.BpelObjectFactory;
import org.apache.ode.bpel.compiler.bom.Process;
import org.jgrapht.io.ExportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.Callable;

/**
 * Created by Christoph Graupner on 2018-02-08.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
@Service
public class TransformationService implements Callable<FlowModel> {
  final         Config                     config;
  private final ApplicationContext         applicationContext;
  private final AutomatonModel             automatonModel;
  private final BpelOdeAutomatonCompiler   bpelHandler;
  private final BpelObjectFactory          bpelObjectFactory;
  private final MainFlowModelPostProcessor finisher;
  private final IsplWriterFactory          ispl;
  private final Logger                     logger = LoggerFactory.getLogger(getClass());
  private final AutomatonModelExtractor    modelExtractor;
  private final VisualOutputAutomaton[]    visualOutput;
  @Setter
  File outputIspl;
  @Getter
  private FlowModel   flowModel;
  @Getter
  private InputStream inputBpel;
  private URL         inputBpelUrl;

  public TransformationService(
      final ApplicationContext applicationContext,
      final BpelOdeAutomatonCompiler bpelHandler,
      final BpelObjectFactory bpelObjectFactory,
      final IsplWriterFactory isplFactory,
      final VisualOutputAutomaton[] visualOutput,
      final MainFlowModelPostProcessor finisher,
      final AutomatonModel automatonModel,
      final Config config,
      final AutomatonModelExtractor modelExtractor
  ) {
    this.bpelHandler = bpelHandler;
    this.bpelObjectFactory = bpelObjectFactory;
    this.ispl = isplFactory;
    this.visualOutput = visualOutput;
    this.applicationContext = applicationContext;
    this.finisher = finisher;
    this.automatonModel = automatonModel;
    this.config = config;
    this.modelExtractor = modelExtractor;
  }

  @Override
  public FlowModel call() throws Exception {
    logger.trace("BEGIN Processing BPEL -> ISPL ...");

    parseBpelToCompositionAutomaton();
    finisher.finish();
    printFlowModel();

    extractAutomatonModel();
    writeToIspl();

    logger.trace("FINISH Processing BPEL -> ISPL.");
    return flowModel;
  }

  public void setInputBpel(File filename) throws FileNotFoundException, MalformedURLException {
    inputBpelUrl = filename.toURI().toURL();
    inputBpel = new FileInputStream(filename);
  }

  private void extractAutomatonModel() {
    modelExtractor.extract();
  }

  private File getOutputIspl() {
    if (outputIspl == null) {
      outputIspl = new File(inputBpelUrl.getFile() + Consts.FILE_EXTENSION_ISPL);
    }

    return outputIspl;
  }

  private void parseBpelToCompositionAutomaton()
      throws URISyntaxException, IOException, SAXException {
    final InputSource inputSource = new InputSource(getInputBpel());
    inputSource.setSystemId(inputBpelUrl.toString());
    final Process process = bpelObjectFactory.parse(inputSource, inputBpelUrl.toURI());
    flowModel = bpelHandler
        .with(process)
        .run()
        .getResult();
  }

  private void printFlowModel() {
    if (config.isPrintModelGraph()) {
      logger.trace("Printing FlowModel...");
      for (final VisualOutputAutomaton visualOutputAutomaton: visualOutput) {
        try {
          visualOutputAutomaton.render(
              new AnalysisGraph(flowModel),
              MainApp.getConfig().getDebugPathForAgentGraphs() + "/" +
              new File(inputBpelUrl.getFile()).getName()
          );
        } catch (IOException | ExportException aE) {
          logger.error(LogMarker.APPLICATION, aE.getLocalizedMessage(), aE);
        }
      }
      logger.trace("DONE Printing FlowModel...");
    }
  }

  private void writeToIspl() throws IOException {
    IsplWriter writer = ispl.createWriter(automatonModel);
    writer.write(getOutputIspl());
  }
}
