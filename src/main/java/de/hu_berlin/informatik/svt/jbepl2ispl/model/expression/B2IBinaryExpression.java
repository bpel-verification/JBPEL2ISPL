package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public interface B2IBinaryExpression extends B2IExpression {

  B2IExpression getLeft();

  B2IExpression getRight();
}
