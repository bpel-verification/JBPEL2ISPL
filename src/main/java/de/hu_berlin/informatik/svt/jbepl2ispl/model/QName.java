package de.hu_berlin.informatik.svt.jbepl2ispl.model;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import lombok.Getter;
import lombok.NonNull;

import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class QName implements Comparable<QName>, Serializable {
  /**
   * It must not be the '.' (dot) as it used by neo4j for hashMap separation
   */
  public static final String SEPARATOR = ".";
  @Getter
  private final       String agent;
  @Getter
  private final       String name;

  public QName(final String agent, @NonNull final String name) {
    this.name = name;
    this.agent = agent;
  }

  public QName(@NonNull final String encoded) {
    final String[] split = encoded.split(Pattern.quote(SEPARATOR));
    if (split.length == 1) {
      this.name = encoded;
      this.agent = null;
    } else if (split.length == 2) {
      this.agent = split[0];
      this.name = split[1];
    } else {
      throw new IllegalArgumentException("encoded needs to be of form '<agent>.<name>' but was " + encoded);
    }
  }

  public static QName environmentRef(final String envVarName) {
    return new QName(FlowService.ENV, envVarName);
  }

  public boolean belongsToAgent(String agentName) {
    return getAgent() == null || Objects.equals(agentName, getAgent());
  }

  @Override
  public int compareTo(final QName other) {
    if (agent != null && other.agent != null) {
      final int agentCompare = this.agent.compareToIgnoreCase(other.agent);
      if (agentCompare != 0) {
        return agentCompare;
      }
    } else if (agent == null) {
      //this has no agent, other has one
      return -1;
    } else {
      //this has agent, other has none
      return 1;
    }
    return name.compareToIgnoreCase(other.name);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof QName)) {
      return false;
    }
    final QName qName = (QName) o;
    return Objects.equals(getAgent(), qName.getAgent()) &&
           Objects.equals(getName(), qName.getName());
  }

  public String getFullName() {
    return agent + SEPARATOR + name;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getAgent(), getName());
  }

  public boolean isInEnvironment() {
    return FlowService.ENV.equals(getAgent());
  }

  @Override
  public String toString() {
    return getFullName();
  }

  public boolean hasAgent() {
    return agent != null;
  }
}
