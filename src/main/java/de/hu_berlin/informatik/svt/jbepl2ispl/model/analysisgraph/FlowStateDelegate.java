package de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import lombok.NonNull;

import java.util.Objects;

/**
 * Created by Christoph Graupner on 2018-06-18.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class FlowStateDelegate implements FlowStateWrapper {
  private final FlowState delegate;

  @Override
  public void setInitialState(final boolean value) {
    delegate.setInitialState(value);
  }

  FlowStateDelegate(@NonNull final FlowState delegate) {
    this.delegate = delegate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof FlowStateDelegate)) {
      return false;
    }
    final FlowStateDelegate that = (FlowStateDelegate) o;
    return Objects.equals(delegate, that.delegate);
  }

  @Override
  public int getAlternativeCount() {
    return delegate.getAlternativeCount();
  }

  @Override
  public BeginEndType getBeginEndType() {
    return delegate.getBeginEndType();
  }

  @Override
  public BpelPath getBpelPath() {
    return delegate.getBpelPath();
  }

  @Override
  public void setBpelPath(final String bpelObjectPath) {
    delegate.setBpelPath(bpelObjectPath);
  }

  @Override
  public String getBpelPathAsString() {
    return delegate.getBpelPathAsString();
  }

  @Override
  public Activity getCausingActivity() {
    return delegate.getCausingActivity();
  }

  @Override
  public String getDisplayName() {
    return delegate.getDisplayName();
  }

  @Override
  public int getElementId() {
    return delegate.getElementId();
  }

  @Override
  public QName getEntryActionName() {
    return delegate.getEntryActionName();
  }

  @Override
  public void setEntryActionName(final QName entryActionName) {
    delegate.setEntryActionName(entryActionName);
  }

  @Override
  public QName getExitActionName() {
    return delegate.getExitActionName();
  }

  @Override
  public void setExitActionName(final QName exitActionName) {
    delegate.setExitActionName(exitActionName);
  }

  @Override
  public FlowId getFlowId() {
    return delegate.getFlowId();
  }

  @Override
  public Long getId() {
    return delegate.getId();
  }

  @Override
  public String getInputVariable() {
    return delegate.getInputVariable();
  }

  @Override
  public void setInputVariable(final String variable) {
    delegate.setInputVariable(variable);
  }

  @Override
  public String getName() {
    return delegate.getName();
  }

  @Override
  public void setName(final String stateName) {
    delegate.setName(stateName);
  }

  @Override
  public String getOriginalBpelNameAttribute() {
    return delegate.getOriginalBpelNameAttribute();
  }

  @Override
  public String getOutputVariable() {
    return delegate.getOutputVariable();
  }

  @Override
  public void setOutputVariable(final String variable) {
    delegate.setOutputVariable(variable);
  }

  @Override
  public String getPlainName() {
    return delegate.getPlainName();
  }

  @Override
  public String getProperty(final String propertyName) {
    return delegate.getProperty(propertyName);
  }

  @Override
  public FlowService getSourceService() {
    return delegate.getSourceService();
  }

  @Override
  public void setSourceService(final FlowService sourceService) {
    delegate.setSourceService(sourceService);
  }

  @Override
  public FlowService getTargetService() {
    return delegate.getTargetService();
  }

  @Override
  public void setTargetService(final FlowService targetService) {
    delegate.setTargetService(targetService);
  }

  @Override
  public FlowServiceOperation getTargetServiceOperation() {
    return delegate.getTargetServiceOperation();
  }

  @Override
  public void setTargetServiceOperation(
      final FlowServiceOperation targetServiceOperation
  ) {
    delegate.setTargetServiceOperation(targetServiceOperation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(delegate);
  }

  @Override
  public boolean isInitialState() {
    return delegate.isInitialState();
  }

  @Override
  public boolean isFlagged(final String identifier) {
    return delegate.isFlagged(identifier);
  }

  @Override
  public boolean isStartState() {
    return delegate.isStartState();
  }

  @Override
  public void setFlagged(final String identifier, final boolean state) {
    delegate.setFlagged(identifier, state);
  }

  @Override
  public void setProperty(final String propertyName, final String value) {
    delegate.setProperty(propertyName, value);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("FlowStateDelegate{");
    sb.append("delegate=").append(delegate);
    sb.append('}');
    return sb.toString();
  }

  @Override
  public FlowState wrapped() {
    return delegate;
  }
}
