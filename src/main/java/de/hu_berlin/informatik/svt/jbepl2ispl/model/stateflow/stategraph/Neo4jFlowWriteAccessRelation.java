package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVarWriteAccessRelation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.ogm.annotation.*;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@RelationshipEntity(type = Naming.RELATION_FLOW_VAR_WRITE_ACCESS)
@NoArgsConstructor
public class Neo4jFlowWriteAccessRelation extends Neo4jFlowAccessRelation implements FlowVarWriteAccessRelation {
  @Id
  @GeneratedValue
  @Getter
  Long     id;
  @StartNode
  @Setter
  @Getter
  State    state;
  @EndNode
  @Setter
  @Getter
  Variable variable;

  Neo4jFlowWriteAccessRelation(
      final State state,
      final Variable variable,
      final VarValueSourceType varValueSourceType,
      final String value,
      final String valueSource,
      final int elementId,
      final String bpelPathAsString

  ) {
    super(
        state.getDisplayName() + ">" + variable.getName(), state, variable, varValueSourceType, value, valueSource,
        elementId, bpelPathAsString, 0
    );
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Neo4jFlowWriteAccessRelation{");
    sb.append("id=").append(id);
    sb.append(", variable=").append(variable);
    sb.append(", state=").append(state);
    sb.append('}');
    return sb.toString();
  }
}
