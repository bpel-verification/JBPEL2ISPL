package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVariable;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
@Service
public class CommChannelPostProcessor extends FlowModelPostProcessor {
  protected CommChannelPostProcessor(final FlowModel flowModel) {
    super(flowModel);
  }

  @Override
  public void finish() {
    final Collection<FlowState> flows =
        flowModel.findStatesWithActivity(Activity.FLOW);
    int    maxBranchesInFlow    = 0;
    String savedCurrentBpelPath = "/";
    for (FlowState flowState: flows) {
      final int size = flowModel.findAllOutgoingTransitions(flowState).size();
      if (size > maxBranchesInFlow) {
        maxBranchesInFlow = size;
        savedCurrentBpelPath = flowState.getBpelPathAsString();
      }
    }

    for (int i = 0; i < Math.max(maxBranchesInFlow, 1); i++) {
      // at least one comChannel must be there, as there is always at least one thread running
      final FlowVariable variable = flowModel.factory().createVariable(
          Naming.getCommChannelName(i + 1),
          null,
          savedCurrentBpelPath
      );
      variable.setPurposeType(FlowVariable.PurposeType.COMM_CHANNEL);
      flowModel.save(variable);
    }
  }
}
