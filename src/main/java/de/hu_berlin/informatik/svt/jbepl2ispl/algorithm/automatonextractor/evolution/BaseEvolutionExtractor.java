package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor.evolution;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.*;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.Jb2iUtils;
import lombok.NonNull;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.xpath.XPathExpressionException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
abstract class BaseEvolutionExtractor {
  protected final VariableTargetServiceResolver resolver;
  protected final StateInformationResolver      stateResolver;
  final           FlowModel                     flowModel;
  final           Logger                        logger = LoggerFactory.getLogger(getClass());
  final           AutomatonModel                model;
  protected       AgentAutomaton                agent;

  BaseEvolutionExtractor(
      final FlowModel flowModel, final AutomatonModel model,
      final VariableTargetServiceResolver resolver,
      final StateInformationResolver stateResolver
  ) {
    this.flowModel = flowModel;
    this.model = model;
    this.resolver = resolver;
    this.stateResolver = stateResolver;
  }

  public void extract(AgentAutomaton agentToDo) {
    this.agent = agentToDo;

    for (AgentState agentState: agent.getStates()) {
      if (false == agentState.isFlagged(Consts.PROPERTY_AGENT_STATE_CONDITION)) {
        agentState.setStateEntryConditions(
            appendAgentStateVariableCondition(agentState, agentState.getStateEntryConditions())
        );
        agentState.setFlagged(Consts.PROPERTY_AGENT_STATE_CONDITION, true);
      }
      if (agent.isEnvironment()) {
        agentState.setVariableValue(agent.getStateVariableDef().getQName(), agentState.getNameAsIsplVarValue());
      }
      model.save(agentState);
    }

    for (AgentState agentState: agent.getStates()) {
      agentState = model.refresh(agentState, 4); //necessary as a previous loop could have changed it
      switch (agentState.getCausingActivity()) {
        case __INITIAL_STATE:
          //here will be nothing as condition or assignment
          break;
        case ASSIGN:
          switch (agentState.getMessagingType()) {
            case INITIALISE:
            case LOCAL:
              model.save(extractEvolutionAssignLocal(agentState));
              break;
            case DIRECT:
              model.save(extractEvolutionAssignDirect(agentState));
              break;
            case INDIRECT:
              model.save(extractEvolutionAssignInDirect(agentState));
              break;
          }
          break;
        case INVOKE:
          model.save(extractEvolutionInvoke(agentState));
          break;
        case IF:
        case IFCASE_ELSEIF:
        case IFCASE_ELSE:
          extractEvolutionIf(agentState);
          break;
        case WHILE:
          extractEvolutionWhile(agentState);
        case FLOW:
          //define conditions
          break;
        case RECEIVE:
          model.save(extractEvolutionReceive(agentState));
          break;
        case REPLY:
          model.save(extractEvolutionReply(agentState));
          break;
        case SC_FULFILLMENT:
        case SC_COMMITMENT:
        case SC_TRACE_LABEL:
        case SC_RECOVERY:
          break;
      }
    }
  }

  protected abstract void assignDirect_generateConditions(
      AgentState agentState, QName actionName,
      B2INaryExpression condition
  );

  protected abstract void assignDirect_setVariableValues(
      @NonNull AgentState agentState, final AgentAutomaton srcAgentAutomaton, final AgentAutomaton trgtAgentAutomaton,
      String symbolicVariableValueAt
  );

  /**
   * <ol>
   * <li>find target service, value goes to</li>
   * <li>find source service, value comes from</li>
   * <li>set value that is assigned to commChannel(x)</li>
   * <li>ste action-name on inbound transitions to &lt;source-agent&gt;.send__XXX</li>
   * <li>generate state-conditions</li>
   * </ol>
   *
   * @param agentState
   */
  protected AgentState extractEvolutionAssignDirect(final AgentState agentState) {
    //buffer muss im environment gefüllt werden
    if (logger.isDebugEnabled()) {
      logger.debug(LogMarker.DEBUG_VALUES, "extractEvolutionAssignDirect: agentState {}", agentState);
    }

    final String serviceValueComesFrom =
        resolver.getServiceValueComesFrom(flowModel.findStateByFlowId(agentState.getOriginStateId()));
    final String serviceValueGoesTo =
        resolver.getServiceValueGoesTo(flowModel.findStateByFlowId(agentState.getOriginStateId()));

    if (serviceValueComesFrom == null) {
      throw new IllegalStateException(
          "We have a DIRECT messaging in " + agentState + ", but can not find the source service."
      );
    }
    if (serviceValueGoesTo == null) {
      throw new IllegalStateException(
          "We have a DIRECT messaging in " + agentState + ", but can not find the target service."
      );
    }

    if (serviceValueComesFrom.equals(serviceValueGoesTo)) {
      throw new IllegalStateException(
          "Source service '" + serviceValueComesFrom + "' and target service '" + serviceValueGoesTo +
          "' should never be the same for state: " + agentState);
    }

    if (logger.isDebugEnabled()) {
      logger.debug(LogMarker.DEBUG_VALUES, "extractEvolutionAssignDirect: valuesComes From: {}", serviceValueComesFrom);
    }
    AgentAutomaton srcAgentAutomaton  = model.getAgentAutomaton(serviceValueComesFrom);
    AgentAutomaton trgtAgentAutomaton = model.getAgentAutomaton(serviceValueGoesTo);

    final String symbolicVariableValueAt =
        resolver.getSymbolicVariableValueAt(flowModel.findStateByFlowId(agentState.getOriginStateId()));
    final FlowMessageSend flowMessageSend =
        resolver.findAllMessagingSendForState(flowModel.findStateByFlowId(agentState.getOriginStateId()))
                .iterator()
                .next(); //FIXME it can be more states

    flowMessageSend.setCommChannelValue(symbolicVariableValueAt);
    flowModel.save(flowMessageSend);

    assignDirect_setVariableValues(agentState, srcAgentAutomaton, trgtAgentAutomaton, symbolicVariableValueAt);

    QName actionName = flowModel.findAllIncomingTransitions(flowMessageSend.getOriginAssignState())
                                .iterator()
                                .next()
                                .getIsplActionName();

    //FIXME now derive the conditions for the transitions

    final B2INaryExpression condition = agentState.getStateEntryConditions();

    assignDirect_generateConditions(agentState, actionName, condition);
    agentState.setStateEntryConditions(condition);

    return agentState;
  }

  protected AgentState extractEvolutionAssignInDirect(final AgentState agentState) {
    logger.error(
        LogMarker.TODO_EXCEPTION, "BaseEvolutionExtractor.extractEvolutionAssignInDirect: IMPLEMENT"); //FIXME IMPLEMENT
    //define conditions
    return agentState;
  }

  protected abstract AgentState extractEvolutionAssignLocal(final AgentState agentState);

  /**
   * //In ENV
   * //set commChannel to <free>
   * //set container to <free>
   * //invoke_setVariables(agentState);
   *
   * @param agentState
   */
  protected AgentState extractEvolutionInvoke(final AgentState agentState) {

    final FlowState flowState = flowModel.findStateByFlowId(agentState.getOriginStateId());

    final Collection<FlowMessageSend> messagings = resolver.findAllRelevantMessagingSendForStateAndService(
        flowState, agent.getName()
    );

    if (logger.isDebugEnabled()) {
      logger.debug(LogMarker.DEBUG_VALUES, "extractEvolutionInvoke: Agent: {}, state: {}", agent, agentState);
    }

    if (messagings.isEmpty()) {
      logger.error(
          LogMarker.TODO_EXCEPTION,
          "BaseEvolutionExtractor.extractEvolutionInvoke: messagings.isEmpty() should not happen"
      ); //FIXME messagings.isEmpty() should not happen

      return agentState;
    }

    if (messagings.size() > 1) {
      logger.error(
          LogMarker.TODO_EXCEPTION,
          "BaseEvolutionExtractor.extractEvolutionInvoke: if messagings.size > 1"
      ); //FIXME if messagings.size > 1
      throw new NotImplementedException(
          "BaseEvolutionExtractor.extractEvolutionInvoke: with messagings.size > 1: got:" +
          StringUtils.join(messagings, "\n--"));
    }

    //in receive agent
    //determine the buffer variable
    final FlowMessageSend messageSend   = messagings.iterator().next();
    final String          bufferVarName = messageSend.getBufferVariableName();
    //determine the comm channel
    final String commChannelVarName = messageSend.getCommChannelVariableName();

    assert bufferVarName != null;
    assert commChannelVarName != null;

    final List<FlowTransition> allIncomingTransitions =
        flowModel.findAllIncomingTransitions(messageSend.getValueReceivingState());

    if (allIncomingTransitions.size() > 1) {
      throw new NotImplementedException("BaseEvolutionExtractor.extractEvolutionInvoke: IMPLEMENT multi incoming");
      //FIXME IMPLEMENT multi incoming
    }
    final FlowTransition transition =
        allIncomingTransitions.iterator().next();

    final QName actionName = transition.getIsplActionName();
    assert Jb2iUtils.isNullOrEmpty(actionName) == false;

    invoke_setVariablesAndAction(
        agentState, flowState, messageSend, bufferVarName, commChannelVarName,
        actionName
    );

    //define conditions
    final B2INaryExpression condition = agentState.getStateEntryConditions();
    invoke_generateConditions(agentState, actionName, condition);
    agentState.setStateEntryConditions(condition);
    return agentState;
  }

  protected AgentState extractEvolutionReceive(final AgentState agentState) {
    logger.error(
        LogMarker.TODO_EXCEPTION, "BaseEvolutionExtractor.extractEvolutionReceive: IMPLEMENT"); //FIXME IMPLEMENT

    //define conditions
    return agentState;
  }

  protected AgentState extractEvolutionReply(final AgentState agentState) {
    logger.error(LogMarker.TODO_EXCEPTION, "BaseEvolutionExtractor.extractEvolutionReply: IMPLEMENT"); //FIXME IMPLEMENT
    //define conditions
    return agentState;
  }

  protected AgentState invoke_generateConditions(
      final AgentState agentState,
      final QName actionName,
      final B2INaryExpression condition
  ) {
    if (agent.getName().equals(actionName.getAgent())) {
      condition.addTerm(B2IExpression.actionSelf(actionName.getName()));
    } else {
      condition.addTerm(B2IExpression.actionOtherAgent(actionName));
    }
    return agentState;
  }

  abstract protected void invoke_setVariablesAndAction(
      final AgentState agentState, final FlowState flowState, final FlowMessageSend messageSend,
      final String bufferVarName,
      final String commChannelVarName,
      final QName actionName
  );

  B2INaryExpression appendAgentStateVariableCondition(
      @NonNull AgentState agentState, @NonNull B2INaryExpression condition
  ) {
    if (logger.isDebugEnabled()) {
      logger.debug(LogMarker.DEBUG_VALUES, "appendAgentStateVariableCondition: agentState: {}", agentState);
    }
    LinkedList<B2ICompareExpression> statesFrom = new LinkedList<>();
    model.getAllInboundTransitions(agentState).forEach(
        agentTransition -> statesFrom.add(
            B2IExpression.equal(
                B2IExpression.variableReference(
                    FlowService.ENV,
                    Consts.ISPL_VAR_NAME_STATEVAR_ENV
                ),
                B2IExpression.term(agentTransition.getSourceState().getName())
            )
        )
    );
    if (statesFrom.size() > 1) {
      condition.addTerm(
          B2IExpression.or(statesFrom)
      );
    } else if (statesFrom.size() == 1) {
      condition.addTerm(statesFrom.element());
    } else if (agentState.isInitialState()) {

    } else {
      logger.error(LogMarker.MODEL_PROBLEM, "No inbound transitions for {}", agentState);
      throw new IllegalStateException("No inbound transitions for " + agentState);
    }
    return condition;
  }

  String getCommChannel(final AgentState agentState) {
    int branchNo = stateResolver.getFlowBranchNumber(flowModel.findStateByFlowId(agentState.getOriginStateId()));
    if (branchNo < 1) {
      return Naming.getCommChannelName(1);
    } else {
      return Naming.getCommChannelName(branchNo);
    }
  }

  private AgentState extractEvolutionIf(final AgentState agentState) {
    model.getAllOutboundTransitions(agentState).forEach(
        agentTransition -> {
          final String guard = agentTransition.getGuard();
          if (logger.isDebugEnabled()) {
            logger.debug(LogMarker.DEBUG_VALUES, "extractEvolutionIf: trans: {}\nguard: {}", agentTransition, guard);
          }
          if (guard == null) {
            return;
          }
          if (Consts.IF_BRANCH_HELPER_GUARD.equals(guard)) {
            logger.error(
                LogMarker.TODO_EXCEPTION, "BaseEvolutionExtractor.extractEvolutionIf: something to think of {}-[{}]->",
                agentState, agentTransition
            ); //FIXME something to think of
          } else {
            try {
              ConditionStatement  guardStatement = new XPathConditionStatement(guard);
              final B2IExpression condition      = guardStatement.asCondition();
              if (condition != null) {
                final AgentState  targetState     = model.refresh(agentTransition.getTargetState(), 2);
                B2INaryExpression entryConditions = targetState.getStateEntryConditions();
                if (agentTransition.invertGuard()) {
                  entryConditions.addTerm(B2IExpression.negate(condition));
                } else {
                  entryConditions.addTerm(condition);
                }
                targetState.setStateEntryConditions(entryConditions);
                model.save(targetState);
              }
            } catch (XPathExpressionException e) {
              logger.error(LogMarker.EXCEPTION, e.getLocalizedMessage(), e);
            }
          }
        }
    );
    return agentState;
  }

  private AgentState extractEvolutionWhile(final AgentState agentState) {
    model.getAllOutboundTransitions(agentState).forEach(
        agentTransition -> {
          final String guard = agentTransition.getGuard();
          if (logger.isDebugEnabled()) {
            logger.debug(LogMarker.DEBUG_VALUES, "extractEvolutionWhile: trans: {}\nguard: {}", agentTransition, guard);
          }
          if (guard == null) {
            return;
          }
          if (Consts.IF_BRANCH_HELPER_GUARD.equals(guard)) {
            logger.error(
                LogMarker.TODO_EXCEPTION,
                "BaseEvolutionExtractor.extractEvolutionWhile: something to think of {}-[{}]->",
                agentState, agentTransition
            ); //FIXME something to think of
          } else {
            try {
              ConditionStatement  guardStatement = new XPathConditionStatement(guard);
              final B2IExpression condition      = guardStatement.asCondition();
              if (condition != null) {
                final AgentState  targetState     = model.refresh(agentTransition.getTargetState(), 2);
                B2INaryExpression entryConditions = targetState.getStateEntryConditions();
                if (agentTransition.invertGuard()) {
                  entryConditions.addTerm(B2IExpression.negate(condition));
                } else {
                  entryConditions.addTerm(condition);
                }
                targetState.setStateEntryConditions(entryConditions);
                model.save(targetState);
              }
            } catch (XPathExpressionException e) {
              logger.error(LogMarker.EXCEPTION, e.getLocalizedMessage(), e);
            }
          }
        }
    );
    return agentState;
  }
}
