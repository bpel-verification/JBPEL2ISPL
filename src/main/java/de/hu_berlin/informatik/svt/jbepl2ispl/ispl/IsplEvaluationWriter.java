package de.hu_berlin.informatik.svt.jbepl2ispl.ispl;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.*;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.*;
import org.apache.commons.lang3.NotImplementedException;

import java.util.LinkedList;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
class IsplEvaluationWriter extends IsplSubWriterBase {
  private AutomatonModel model;

  IsplEvaluationWriter(
      final ISPLFactory factory,
      final IsplWriter isplWriter
  ) {
    super(factory, isplWriter);
    model = isplWriter.model;
  }

  public Evaluation createEvaluation() {
    final Evaluation evaluation = factory.createEvaluation();
    model.getAllAgentAutomatons().forEach(
        agentAutomaton -> {
          agentAutomaton.getDesiredStates().forEach(
              greenStateName -> {
                final GlobalEvalStatement evalStatement = factory.createGlobalEvalStatement();
                evalStatement.setEvalResult(isplWriter.translateToIsplName(greenStateName));

                evalStatement.setEvalCondition(
                    encodeAsGlobalEvalBoolCondition(agentAutomaton.getDesiredStateDefinition(greenStateName))
                );

                evaluation.getEvaluationStatements().add(evalStatement);
              }
          );
          if (!agentAutomaton.isEnvironment()) {
            agentAutomaton.getUnDesiredStates().forEach(
                redStateName -> {
                  final GlobalEvalStatement evalStatement = factory.createGlobalEvalStatement();
                  evalStatement.setEvalResult(isplWriter.translateToIsplName(redStateName));

                  evalStatement.setEvalCondition(
                      encodeAsGlobalEvalBoolCondition(agentAutomaton.getUnDesiredStateDefinition(redStateName))
                  );

                  evaluation.getEvaluationStatements().add(evalStatement);
                }
            );
          }
        }
    );
    model.getPropositionNames()
         .stream()
         .sorted()
         .forEachOrdered(
             name -> {
               final GlobalEvalStatement evalStatement = factory.createGlobalEvalStatement();
               evalStatement.setEvalResult(isplWriter.translateToIsplName(name));
               evalStatement.setEvalCondition(
                   encodeAsGlobalEvalBoolCondition(model.getPropositionDefinition(name))
               );
               evaluation.getEvaluationStatements().add(evalStatement);
             }
         );
    return evaluation;
  }

  private GlobalEvalBoolCondition asGlobalEvalBoolConditionAnd(final LinkedList<GlobalEvalBoolCondition> results) {
    if (results.size() == 1) {
      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }

    GlobalEvalBoolConditionAnd rootAnd = factory.createGlobalEvalBoolConditionAnd();
    GlobalEvalBoolConditionAnd iterAnd = rootAnd;

    iterAnd.setLeft(results.removeFirst());
    GlobalEvalBoolCondition lastAnd = results.removeLast();

    for (GlobalEvalBoolCondition isBoolCondition : results) {
      GlobalEvalBoolConditionAnd tmp = factory.createGlobalEvalBoolConditionAnd();
      tmp.setLeft(isBoolCondition);

      GlobalEvalBoolConditionParenthesized parenthesizedAnd = asParenthesized(tmp);

      iterAnd.setRight(parenthesizedAnd);
      iterAnd = tmp;
    }
    iterAnd.setRight(lastAnd);
    return rootAnd;
  }

  private GlobalEvalBoolCondition asGlobalEvalBoolConditionOr(final LinkedList<GlobalEvalBoolCondition> results) {
    if (results.size() == 1) {
      return results.get(0);
    } else if (results.isEmpty()) {
      throw new IllegalArgumentException("[results] should never be empty.");
    }

    GlobalEvalBoolConditionOr rootOr = factory.createGlobalEvalBoolConditionOr();
    GlobalEvalBoolConditionOr iterOr = rootOr;

    iterOr.setLeft(results.removeFirst());
    GlobalEvalBoolCondition lastOr = results.removeLast();

    for (GlobalEvalBoolCondition isBoolCondition : results) {
      GlobalEvalBoolConditionOr tmp = factory.createGlobalEvalBoolConditionOr();
      tmp.setLeft(isBoolCondition);

      GlobalEvalBoolConditionParenthesized parenthesizedOr = asParenthesized(tmp);

      iterOr.setRight(parenthesizedOr);
      iterOr = tmp;
    }
    iterOr.setRight(lastOr);
    return rootOr;
  }

  private GlobalEvalLiteral asGlobalEvalLiteral(final Object obj) {
    if (obj instanceof B2IExpression) {
      switch (((B2IExpression) obj).getType()) {
        case VARIABLE_REF:
          QName qName = ((B2IVarRefExpression) obj).getTerm();
          final GlobalEvalAgentVariableLiteral environmentRef = factory.createGlobalEvalAgentVariableLiteral();
          environmentRef.setVarName(qName.getName());
          environmentRef.setAgentName(qName.getAgent());
          return environmentRef;
      }

      return isplWriter.getLiteral(((B2IExpressionTerm) obj).getTerm());
    }
    return isplWriter.getLiteral(obj);
  }

  private GlobalEvalBoolConditionParenthesized asParenthesized(final GlobalEvalBoolCondition condition) {
    final GlobalEvalBoolConditionParenthesized ret = factory.createGlobalEvalBoolConditionParenthesized();
    ret.setCondition(condition);
    return ret;
  }

  private GlobalEvalBoolConditionCompare encodeAsGlobalEvalBoolCompare(final B2ICompareExpression compareExpression) {
    switch (compareExpression.getType()) {
      case COMPARE:
      default:
        final GlobalEvalBoolConditionCompare agentEvoBoolConditionCompare =
            factory.createGlobalEvalBoolConditionCompare();
        agentEvoBoolConditionCompare.setLogicop(isplWriter.getLogicOp(compareExpression.getOperator()));

        agentEvoBoolConditionCompare.setLeft(asGlobalEvalLiteral(compareExpression.getLeft()));
        agentEvoBoolConditionCompare.setRight(asGlobalEvalLiteral(compareExpression.getRight()));
        return agentEvoBoolConditionCompare;
    }
  }

  private GlobalEvalBoolCondition encodeAsGlobalEvalBoolCondition(final B2IExpression o) {
    final GlobalEvalBoolConditionParenthesized ret     = factory.createGlobalEvalBoolConditionParenthesized();
    final LinkedList<GlobalEvalBoolCondition>  results = new LinkedList<>();
    switch (o.getType()) {
      case AND:
        for (final B2IExpression term : ((B2INaryExpression) o).getTerms()) {
          results.add(encodeAsGlobalEvalBoolCondition(term));
        }

        GlobalEvalBoolCondition rootAnd = asGlobalEvalBoolConditionAnd(results);

        return asParenthesized(rootAnd);
      case OR:
        for (final B2IExpression term : ((B2INaryExpression) o).getTerms()) {
          results.add(encodeAsGlobalEvalBoolCondition(term));
        }

        GlobalEvalBoolCondition rootOr = asGlobalEvalBoolConditionOr(results);

        return asParenthesized(rootOr);
      case NOT:
        GlobalEvalBoolConditionNegated negated = factory.createGlobalEvalBoolConditionNegated();
        negated.setCondition(encodeAsGlobalEvalBoolCondition(((B2IUnaryExpression) o).getTerm()));

        return negated;
      case PARENTHESIZED:
        return asParenthesized(encodeAsGlobalEvalBoolCondition(((B2IUnaryExpression) o).getTerm()));
      case TERM:
      case NUM_ADD:
      case NUM_SUB:
      case NUM_MUL:
      case NUM_DIV:
      case VALUE:
      case BIT_NEGATE:
      case BIT_AND:
      case BIT_OR:
      case VARIABLE_REF:
        throw new NotImplementedException("Not Implement yet");
      case ACTION_COMPARE:
//        final B2IActionExpression actionExpression = (B2IActionExpression) o;
//        final QName action = actionExpression.getTerm();
//
//        final GlobalEvalBoolConditionAction agentEvoBoolConditionAction = factory.createAgentEvoBoolConditionAction();
//
//        agentEvoBoolConditionAction.setActionName(translateToIsplName(action.getName()));
//        if (null != action.getAgent()) {
//          agentEvoBoolConditionAction.setAgentName(translateToIsplName(action.getAgent()));
//        }
//        return agentEvoBoolConditionAction;
        return null;
      case COMPARE:
        if (o instanceof B2ICompareExpression) {
          return encodeAsGlobalEvalBoolCompare(((B2ICompareExpression) o));
        }
      default:
        throw new IllegalStateException("unhandled B2IExpression.type: " + o.getType());
    }
  }
}
