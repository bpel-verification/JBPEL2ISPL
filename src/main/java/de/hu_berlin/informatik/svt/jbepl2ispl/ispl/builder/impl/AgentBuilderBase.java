package de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.impl;

import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplAgentBuilderBase;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplBuilderBase;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.ISPLFactory;

/**
 * Created by Christoph Graupner on 2018-03-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@SuppressWarnings("unchecked")
abstract class AgentBuilderBase<PARENT extends IsplBuilderBase, VARDEF, ACTION_DEF, PROTOCOL_DEF, EVOLUTION_DEF, RED_STATES_DEF, AGENT_TYPE>
    extends BuilderBase<PARENT>
    implements IsplAgentBuilderBase<PARENT, VARDEF, ACTION_DEF, PROTOCOL_DEF, EVOLUTION_DEF, RED_STATES_DEF>
{
  final AGENT_TYPE agent;

  public AgentBuilderBase(final ISPLFactory factory, PARENT parent, final AGENT_TYPE agent) {
    super(factory, parent);
    this.agent = agent;
  }
}
