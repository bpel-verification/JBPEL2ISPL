package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface StateInformationResolver {
  FlowState findIndirectSourceStateOfVariableValueOf(FlowState state);

  FlowState findIndirectTargetStateOfVariableValueOf(FlowState state);

  FlowState findSourceStateOfVariableValueOf(FlowState state);

  FlowState findTargetStateOfVariableValueOf(FlowState state);

  int getFlowBranchNumber(FlowState state);

  B2IExpression getSymbolicValueAt(FlowState state);
}
