package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae;

import lombok.Getter;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public class B2IFormulaSC implements B2IFormula {
  @Getter final String   creditor;
  @Getter final String   debtor;
  @Getter final Function function;
  final private String   commitmentContentName;

  public B2IFormulaSC(
      final Function function,
      final String debtor,
      final String creditor,
      final String commitmentContentName
  ) {
    this.debtor = debtor;
    this.creditor = creditor;
    this.commitmentContentName = commitmentContentName;
    this.function = function;
  }

  public String getCommitmentContent() {
    return commitmentContentName;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder(function.toString());
    sb.append("debtor='").append(debtor).append('\'');
    sb.append(", creditor='").append(creditor).append('\'');
    sb.append(", commitmentContentName='").append(commitmentContentName).append('\'');
    sb.append(')');
    return sb.toString();
  }
}
