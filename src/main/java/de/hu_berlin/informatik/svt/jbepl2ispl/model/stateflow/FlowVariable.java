package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import org.apache.ode.bpel.compiler.bom.Variable;

import java.util.EnumSet;

/**
 * Created by Christoph Graupner on 2018-03-01.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public interface FlowVariable extends BpelIdentifiable, FlowAdditional {
  enum ValueType {
    STRING, BOOLEAN, INT, UNKNOWN
  }

  /**
   * @see org.apache.ode.bpel.compiler.bom.Variable.Kind
   */
  enum BpelType {
    SCHEMA, ELEMENT, MESSAGE;

    public static BpelType valueOf(final Variable.Kind kind) {
      switch (kind) {

        case SCHEMA:
          return SCHEMA;
        case ELEMENT:
          return ELEMENT;
        case MESSAGE:
          return MESSAGE;
      }
      return null;
    }

  }

  /**
   * Created by Christoph Graupner on 2018-06-16.
   *
   * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
   */
  enum PurposeType {
    SHARED_BUFFER, COMM_CHANNEL, OPERATIONAL, CONTAINER, MESSAGE, TRACE_LABEL;
    public static EnumSet<PurposeType> COMM_CHANNEL_RELEVANT      = EnumSet.of(CONTAINER, MESSAGE);
    public static EnumSet<PurposeType> LOCAL_ASSIGNMENT_INDICATOR = EnumSet.of(OPERATIONAL);
  }

  BpelType getBpelType();

  void setBelongsTo(FlowService agent);

  void setBpelType(BpelType bpelType);

  QName getQName();

  PurposeType getPurposeType();

  void setPurposeType(PurposeType purposeType);

  ValueType getValueType();

  String getVariableType();

  boolean isInternalOnly();

  boolean isVirtual();
}
