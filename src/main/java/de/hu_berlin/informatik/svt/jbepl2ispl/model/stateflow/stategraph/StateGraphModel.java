package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.Config;
import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.repository.*;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.neo4j.ogm.annotation.Transient;
import org.neo4j.ogm.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@SuppressWarnings("unchecked")
@org.springframework.stereotype.Service
@Transient
@NoArgsConstructor
public class StateGraphModel implements FlowModel {
  @Transient
  private final Logger logger = LoggerFactory.getLogger(getClass());
  @Autowired
  AlternativeStateLinkRepository              alternativeStateLinkRepository;
  @Autowired
  Neo4jFlowScCommitmentRepository             commitmentRepository;
  @Autowired
  Config                                      config;
  @Autowired
  Neo4jBpelFlowAggregateStateRepository       flowAggregateStateRepository;
  @Autowired
  FlowMessageSendRepository                   flowMessageSendRepository;
  @Autowired
  Neo4jFlowScFulfillmentRepository            fulfillmentRepository;
  @Autowired
  Neo4jFlowStateInvolvesServiceLinkRepository involvesServiceLinkRepository;
  @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
  @Autowired
  Session                                     neo4jSession;
  @Autowired
  Neo4jFlowScRecoveryRepository               recoveryRepository;
  @Autowired
  ServiceOperationRepository                  serviceOperationRepository;
  @Autowired
  ServiceRepository                           serviceRepository;
  @Autowired
  StateRepository                             stateRepository;
  @Autowired
  Neo4jFlowScTraceLabelRepository             traceLabelRepository;
  @Autowired
  TransitionRepository                        transitionRepository;
  @Autowired
  VarReadAccessRepository                     varReadAccessRepository;
  @Autowired
  VarWriteAccessRepository                    varWriteAccessRepository;
  @Autowired
  VariableRepository                          variableRepository;
  private StateGraphFactory factory;

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public void clear() {
    neo4jSession.purgeDatabase();
    neo4jSession.clear();
  }

  @Override
  public StateFlowFactory factory() {
    if (factory == null) {
      factory = new StateGraphFactory();
    }
    return factory;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowService findAgentWithVariable(
      final String variableName
  ) {
    FlowService ret = null;
    for (final FlowService flowService: getServices()) {
      if (FlowService.ENV.equals(flowService.getName())) {
        //return ENV only if no other flowService owns this variable
        if (flowService.getVariables().contains(variableName)) {
          ret = flowService;
        }

        continue;
      }
      if (flowService.getVariables().contains(variableName)) {
        return flowService;
      }
    }
    return ret;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowState> List<T> findAllConnectedFollowingStates(
      final FlowState state
  ) {
    return (List<T>) stateRepository.findAllConnectedFollowingStates(state.getId());
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowState> List<T> findAllConnectedPrecedingStates(
      final FlowState state
  ) {
    return (List<T>) stateRepository.findAllConnectedPrecedingStates(state.getId());
  }

  @Override
  public <T extends FlowScFulfillment> Collection<T> findAllFulfillmentNodesFor(
      final FlowScCommitment commitment
  ) {
    return (Collection<T>) fulfillmentRepository.findAllByCommitment(commitment.getId());
  }

  @Override
  public Collection<Collection<State>> findAllFulfillmentPathsFor(
      final FlowScCommitment commitment, final FlowScFulfillment fulfillment
  ) {

    final Iterable<Map<String, Object>> allPathsForCommitment =
        stateRepository.findAllPathsBetween(
            commitment.getTargetState().getId(),
            fulfillment.getTargetState().getId()
        );
    final ArrayList<Collection<State>> ret = new ArrayList<>();
    allPathsForCommitment.forEach(
        stringObjectMap -> {
          if (logger.isDebugEnabled()) {
            logger.debug(LogMarker.DEBUG_VALUES, "findAllFulfillmentPathsFor: keys: {}", stringObjectMap.keySet());
          }
          ret.add(new ArrayList<>((Collection<? extends State>) stringObjectMap.get("nodes")));
        });

    return ret;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowTransition> List<T> findAllIncomingTransitions(
      final FlowState state
  ) {
    return (List<T>) transitionRepository.findByTargetState(state.getId());
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowState> Collection<T> findAllInvokeStatesWithService(
      final FlowService flowService
  ) {
    return (Collection<T>) refreshAll(
        State.class,
        stateRepository.findAllStatesByActivityAndSourceService(Activity.INVOKE, flowService.getId()), 3
    );
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public Collection<Neo4jFlowMessageSend> findAllMessagingSendForService(final String serviceName) {
    return flowMessageSendRepository.findAllMessagingForService(serviceName);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowMessageSend> Collection<T> findAllMessagingSendForState(
      @NonNull final FlowState startState
  ) {
    return (Collection<T>) refreshAll(
        Neo4jFlowMessageSend.class,
        flowMessageSendRepository.findMessagingForState(startState.getId()),
        2
    );
  }

  @Override
  public <T extends FlowMessageSend> Collection<T> findAllMessagingSendForStateAndService(
      final FlowState state, final String forService
  ) {
    return (Collection<T>) refreshAll(
        Neo4jFlowMessageSend.class,
        flowMessageSendRepository.findAllMessagingForStateAndService(state.getId(), forService),
        2
    );
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowTransition> List<T> findAllOutgoingTransitions(
      final FlowState state
  ) {
    return (List<T>) transitionRepository.findBySourceState(state.getId());
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public Collection<FlowState.PathReturn> findAllPathsBetween(
      final FlowState sourceState, final FlowState targetState
  ) {
    final Iterable<Map<String, Object>> allPathsForCommitment =
        stateRepository.findAllPathsBetween(
            sourceState.getId(),
            targetState.getId()
        );

    return getPathReturns(allPathsForCommitment);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public Collection<FlowState.PathReturn> findAllPathsBetweenExcluding(
      final FlowState sourceState, final FlowState targetState, final List<FlowState> excludedStates
  ) {

    final Iterable<Map<String, Object>> allPathsForCommitment =
        stateRepository.findAllStatesBetweenExcluding(
            sourceState.getId(),
            targetState.getId(),
            excludedStates.stream()
                          .map(Identifiable::getId)
                          .collect(Collectors.toSet())
        );

    return getPathReturns(allPathsForCommitment);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public Collection<Service> findAllServicesItTalksToForService(final String serviceName) {
    return serviceRepository.findAllServicesItTalksToForService(serviceName);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowState> Collection<T> findAllStatesConnectedByInvolvingLinkService(
      final FlowService flowService
  ) {
    return (Collection<T>) refreshAll(State.class, stateRepository.findAllStatesByInvolvingLink(flowService.getName()),
                                      3
    );
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public Collection<Collection<FlowState>> findAllStatesWithSameVarWriteAndSameOrLiteralRead() {
    final Collection<Collection<FlowState>> ret = new ArrayList<>();
    final Iterable<Map<String, Object>> allPathsForCommitment =
        stateRepository.findAllStatesWithSameVarWriteAndSameOrLiteralRead(
            Consts.VAR_NAME_LITERAL);
    allPathsForCommitment.forEach(
        stringObjectMap -> {
          if (logger.isDebugEnabled()) {
            logger.debug(LogMarker.DEBUG_VALUES, "findAllFulfillmentPathsFor: keys: {}", stringObjectMap.keySet());
          }
          ret.add(Arrays.asList((FlowState) stringObjectMap.get("s1"), (FlowState) stringObjectMap.get("s2")));
        });

    return ret;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowVarReadAccessRelation> Collection<T> findAllVarReadAccessRelationsForState(
      final FlowState state
  ) {
    return (Collection<T>) varReadAccessRepository.findAllByState(state.getId());
  }

  @Override
  public List<Neo4jFlowReadAccessRelation> findAllVarReadAccessRelationsForVariable(
      final String variableName
  ) {
    return varReadAccessRepository.findAllByVariable(variableName);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowVarWriteAccessRelation> Collection<T> findAllVarWriteAccessRelationsForState(
      final FlowState state
  ) {
    return (Collection<T>) varWriteAccessRepository.findAllByState(state.getId());
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowState> Collection<T> findAllWithSourceInAssignState(
      final FlowService service
  ) {
    return (Collection<T>) refreshAll(State.class, stateRepository.findAllBySourceService(service.getElementId()), 3);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowState> Collection<T> findAllWithTargetInAssignState(
      final FlowService targetService
  ) {
    return (Collection<T>) refreshAll(
        State.class, stateRepository.findAllByTargetService(targetService.getElementId()), 3);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public Collection<Neo4jFlowWriteAccessRelation> findAllWriteAccessesForVariable(
      final String variableName
  ) {
    return varWriteAccessRepository.findAllByVariable(variableName);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowScCommitment findCommitmentStateById(final int commitmentID) {
    return commitmentRepository.findByCommitmentId(commitmentID);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowState findFirstInvokeOrReplyTargetForVariableForwardStartingFrom(
      final String variableName, final FlowState startingState
  ) {
    final State firstState =
        stateRepository.findFirstInvokeOrReplyStateWhichReadsFromForwardFrom(
            variableName,
            startingState.getId()
        );
    return firstState;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowState findFirstServiceValueGoesTo(
      final FlowState flowState
  ) {
    return null;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowState findFirstWriteSourceForVariableBackwardStartingFrom(
      final String variableName, final FlowState startingState
  ) {
    final State firstState =
        stateRepository.findFirstStateWhichWritesToBackwardFrom(variableName, startingState.getId());
    return firstState;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowState findFollowingState(final FlowState state) {
    return stateRepository.findFollowingByElementId(state.getElementId());
  }

  @Override
  public FlowState findFollowingStateNotOfActivity(
      final FlowState state, final Activity... excludeActivities
  ) {
    return stateRepository.findFollowingByElementIdAndExcludeActivity(state.getElementId(), excludeActivities);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowState> List<T> findIfStatesWithVariableInConditionFrom(
      final FlowState state,
      final String variableName
  ) {
    return (List<T>) stateRepository
        .findIfStatesWithVariableInConditionFromElementId(
            state.getElementId(),
            ".*\\$" + variableName + "\\b.*"
        );
  }

  @Override
  public FlowVarWriteAccessRelation findLastInitialWriteForVariable(@NonNull final String varName) {
    return varWriteAccessRepository.findLastInitialWriteForVariable(varName);
  }

  @Override
  public FlowVarWriteAccessRelation findLastWriteAccessBeforeRead(
      @NonNull final String varName,
      @NonNull final FlowVarReadAccessRelation readRelation
  ) {
    return varWriteAccessRepository.findLastBeforeReadByVariable(varName, readRelation.getElementId());
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowState findPreviousState(final FlowState state) {
    return stateRepository.findPreviousByElementId(state.getElementId());
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowService findServiceByName(final String agentName) {

    return serviceRepository.findByName(agentName);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowServiceOperation findServiceOperation(
      final FlowService flowService, final String actionName, final String inputVariableName,
      final String outputVariable
  ) {
    return flowService
        .getOperations().stream().filter(
            operation -> Objects.equals(actionName, operation.getName())
                         && Objects.equals(inputVariableName, operation.getInputVariableName())
                         && Objects.equals(outputVariable, operation.getOutputVariableName())
        ).findFirst().orElse(null);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowState findStartState() {
    return stateRepository.findFirstByElementId(0);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowState findStateByFlowId(@NonNull final FlowId stateId) {
    return stateRepository.findFirstByElementIdAndAlternativeCount(stateId.getElementId(), stateId.getAlternateId());
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowState> List<T> findStatesBetween(
      final FlowState stateBegin, final FlowState stateEnd
  ) {
    if (stateBegin.getElementId() > stateEnd.getElementId()) {
      return (List<T>) stateRepository.findAllStatesBetween(stateEnd.getId(), stateBegin.getId());
    } else {
      return (List<T>) stateRepository.findAllStatesBetween(stateBegin.getId(), stateEnd.getId());
    }
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowState> Collection<T> findStatesWithActivity(
      final Activity activityType
  ) {
    return (Collection<T>) stateRepository.findAllByCausingActivity(activityType);
  }

  @Override
  public FlowVarReadAccessRelation findVarReadAccessRelationsByStateAndVariable(
      final FlowState state, final String variable
  ) {
    return varReadAccessRepository.findAllByStateAndVariable(state.getId(), variable);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowVariable findVariableByName(@NonNull final String variableName) {
    return variableRepository.findByName(variableName);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowScCommitment> Collection<T> getAllCommitmentNodes() {
    return (Collection<T>) neo4jSession.loadAll(Neo4jFlowScCommitment.class, 3);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowScFulfillment> Collection<T> getAllFulfillmentNodes() {
    return (Collection<T>) neo4jSession.loadAll(Neo4jFlowScFulfillment.class, 3);
  }

  @Override
  public <T extends FlowMessageSend> Collection<T> getAllMessageSend(final int depth) {
    return (Collection<T>) neo4jSession.loadAll(Neo4jFlowMessageSend.class, depth);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowScTraceLabel> Collection<T> getAllTraceLabelNodes() {
    return (Collection<T>) neo4jSession.loadAll(Neo4jFlowScTraceLabel.class, 3);
  }

  @Override
  public State getEndState() {
    return stateRepository.findLastState();
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowService> Collection<T> getServices() {
    return (Collection<T>) neo4jSession.loadAll(Service.class, 3);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowState> Collection<T> getStates() {
    return (Collection<T>) neo4jSession.loadAll(State.class, 3);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowTransition> Collection<T> getTransitions() {
    neo4jSession.clear();
    return (Collection<T>) neo4jSession.loadAll(Transition.class, 4);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends FlowVariable> Collection<T> getVariables() {
    return (Collection<T>) neo4jSession.loadAll(Variable.class, 3);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public FlowState refresh(final FlowState state, final int objLoadDepth) {
    if (state == null) {
      return null;
    }
    return neo4jSession.load(State.class, state.getId(), objLoadDepth);
  }

  @Override
  public FlowScFulfillment refresh(
      final FlowScFulfillment fulfillment,
      final int objLoadDepth
  ) {
    return neo4jSession.load(Neo4jFlowScFulfillment.class, fulfillment.getId(), objLoadDepth);
  }

  @SuppressWarnings("unchecked")
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends Identifiable> Collection<T> refreshAll(
      final Class<T> aClass, final Collection<T> entities, final int loadDepth
  ) {
    neo4jSession.clear();
    if (entities.isEmpty()) {
      return entities;
    }
    return neo4jSession.loadAll(
        getEntityClassOf(aClass),
        entities.stream()
                .map(Identifiable::getId)
                .collect(Collectors.toList()), loadDepth
    );
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public void removeState(@NonNull final FlowState state) {
//    if (false ==
//        EnumSet.of(Activity.SC_COMMITMENT, Activity.SC_FULFILLMENT, Activity.SC_TRACE_LABEL, Activity.SC_RECOVERY)
//               .contains(state.getCausingActivity()))
//    {
//      throw new NotImplementedException(state.getCausingActivity() + " is not yet supported for removal");
//    }

    final List<? extends FlowTransition> inbound  = transitionRepository.findByTargetState(state.getId());
    final List<? extends FlowTransition> outbound = transitionRepository.findBySourceState(state.getId());

    inbound.forEach(
        inTransition -> outbound.forEach(
            outTransition -> {
              inTransition.setTargetState(outTransition.getTargetState());
              save(inTransition);
              save(factory.createTransition(
                  inTransition.getSourceState(),
                  outTransition.getTargetState(),
                  inTransition.getGuard(),
                  inTransition.getName(),
                  inTransition.getBpelPathAsString(),
                  outTransition.getElementId()
              ));
            }
        )
    );

    stateRepository.delete((State) state);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowState save(@NonNull final FlowState state) {

    return stateRepository.save((State) state);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public BpelFlowAggregateState save(@NonNull final BpelFlowAggregateState state) {
    return flowAggregateStateRepository.save((Neo4jBpelFlowAggregateState) state);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowVariable save(@NonNull final FlowVariable variable) {
    return variableRepository.save((Variable) variable);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowTransition save(@NonNull final FlowTransition transition) {
    // refreshing the states objects
    final Optional<State> source = stateRepository.findById(transition.getSourceState().getId());
    final Optional<State> target = stateRepository.findById(transition.getTargetState().getId());
    if (!source.isPresent()) {
      throw new IllegalStateException("SOURCE state not existing");
    }
    if (!target.isPresent()) {
      throw new IllegalStateException("TARGET state not existing");
    }
    transition.setSourceState(source.get());
    transition.setTargetState(target.get());
    return transitionRepository.save((Transition) transition);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowService save(@NonNull final FlowService flowService) {
    return serviceRepository.save((Service) flowService);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowVarWriteAccessRelation save(
      final FlowVarWriteAccessRelation relation
  ) {
    return varWriteAccessRepository.save((Neo4jFlowWriteAccessRelation) relation);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowVarReadAccessRelation save(
      final FlowVarReadAccessRelation relation
  ) {
    return varReadAccessRepository.save((Neo4jFlowReadAccessRelation) relation);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowServiceOperation save(@NonNull final FlowServiceOperation agentOperation) {
    return serviceOperationRepository.save((ServiceOperation) agentOperation);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowAlternativeStateLink save(
      @NonNull final FlowAlternativeStateLink alternativeLink
  ) {
    alternativeStateLinkRepository.save((AlternativeStateLinkRelation) alternativeLink);
    return alternativeLink;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowStateInvolvesServiceLink save(
      @NonNull final FlowStateInvolvesServiceLink involvesServiceLink
  ) {
    return involvesServiceLinkRepository.save((Neo4jFlowStateInvolvesServiceLink) involvesServiceLink);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowMessageSend save(@NonNull final FlowMessageSend messageSend) {
    return flowMessageSendRepository.save((Neo4jFlowMessageSend) messageSend);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowScCommitment save(final FlowScCommitment commitment) {
    return commitmentRepository.save((Neo4jFlowScCommitment) commitment);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowScFulfillment save(final FlowScFulfillment fulfillment) {
    return fulfillmentRepository.save((Neo4jFlowScFulfillment) fulfillment);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowScRecovery save(final FlowScRecovery recovery) {
    return recoveryRepository.save((Neo4jFlowScRecovery) recovery);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public FlowScTraceLabel save(final FlowScTraceLabel traceLabel) {
    return traceLabelRepository.save((Neo4jFlowScTraceLabel) traceLabel);
  }

  private <T extends Identifiable> Class getEntityClassOf(final Class<T> aClass) {
    if (aClass.equals(FlowMessageSend.class)) {
      return Neo4jFlowMessageSend.class;
    }
    return aClass;
  }

  private ArrayList<FlowState.PathReturn> getPathReturns(final Iterable<Map<String, Object>> allPathsForCommitment) {
    final ArrayList<FlowState.PathReturn> ret = new ArrayList<>();
    allPathsForCommitment.forEach(
        stringObjectMap -> {
          if (logger.isDebugEnabled()) {
            logger.debug(LogMarker.DEBUG_VALUES, "findAllFulfillmentPathsFor: keys: {}", stringObjectMap.keySet());
          }
          final List<FlowState>      nodes     = (List<FlowState>) stringObjectMap.get("nodes");
          final List<FlowTransition> relations = (List<FlowTransition>) stringObjectMap.get("relations");
          ret.add(new FlowState.PathReturn(nodes, relations));
        });
    return ret;
  }
}
