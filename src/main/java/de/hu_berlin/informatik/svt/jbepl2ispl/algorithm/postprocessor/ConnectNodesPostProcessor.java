package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowTransition;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Christoph Graupner on 2018-06-27.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
class ConnectNodesPostProcessor extends FlowModelPostProcessor {

  protected ConnectNodesPostProcessor(final FlowModel model) {
    super(model);
  }

  @Override
  public void finish() {
    connectRemaining();
    removeUnnecessary();
  }

  /**
   */
  private void connectRemaining() {
    logger.trace(LogMarker.POSTPROCESSING, "Connect remaining unconnected states with the flow graph...");

    final Collection<FlowState> states = flowModel.getStates();
    states.forEach(
        state -> {
          if (false == state.isStartState()) {
            final List<? extends FlowTransition> inBoundTransitions = flowModel.findAllIncomingTransitions(state);
            if (inBoundTransitions.isEmpty() && shouldConnectToPrevious(state)) {
              FlowState prevState = flowModel.findPreviousState(state);
              assert prevState != null;
              FlowTransition transitionAlways = flowModel.factory().createTransition(
                  prevState,
                  state,
                  null,
                  Naming.RELATION_FOLLOWED_BY_LABEL_PREV,
                  state.getBpelPathAsString()
              );
              flowModel.save(transitionAlways);
            }
            for (FlowTransition transition: inBoundTransitions) {
              // check if source state is <null>
              if (transition.getSourceState() == null) {
                //connect to previous state
                FlowState prevState = flowModel.findPreviousState(state);
                assert prevState != null; //for every state except the start state(which is eliminated before)
                logger.info(LogMarker.POSTPROCESSING, "Connecting {} -> {}", state.toString(), prevState.toString());

                transition.setSourceState(prevState);
                flowModel.save(transition);
              }
            }
          }
        }
    );
    logger.trace(LogMarker.POSTPROCESSING, "DONE Connect remaining unconnected states with the flow graph...");
  }

  private void removeUnnecessary() {
    boolean isEmpty;

    do { //as long as we find some pair of nodes
      final Collection<Collection<FlowState>> allStatesWithSameVarWriteAndSameOrLiteralRead =
          flowModel.findAllStatesWithSameVarWriteAndSameOrLiteralRead();
      isEmpty = allStatesWithSameVarWriteAndSameOrLiteralRead.isEmpty();

      allStatesWithSameVarWriteAndSameOrLiteralRead.forEach(
          flowStateCollection -> {
            List<FlowState> toRemove = new ArrayList<>();
            FlowState       toKeep   = null;
            final List<FlowState> list = flowStateCollection.stream()
                                                            .sorted(Comparator.comparingInt(Identifiable::getElementId))
                                                            .collect(Collectors.toList());
            for (final FlowState flowState: list) {
              if (toKeep == null) {
                toKeep = flowState;
                continue;
              }

              if (false == Consts.VAR_NAME_LITERAL.equals(flowState.getInputVariable())) {
                if (false == Consts.VAR_NAME_LITERAL.equals(toKeep.getInputVariable())) {
                  toKeep = flowState;
                  continue;
                }
                if (toKeep.getElementId() < flowState.getElementId()) {
                  toKeep = flowState;
                }
              }
            }
            final FlowState toKeepFinal = toKeep;
            list.forEach(
                state -> {
                  if (toKeepFinal == state) {
                    return;
                  }
                  logger.trace(LogMarker.POSTPROCESSING, "Removing state {} in favor of {}", state, toKeepFinal);
                  flowModel.removeState(state);
                }
            );
          }
      );
    } while (false == isEmpty);
  }

  private boolean shouldConnectToPrevious(
      final FlowState state
  ) {
    return !(state.getCausingActivity() == Activity.WHILE
             && state.getBeginEndType() == BeginEndType.END)
//           && !(state.getCausingActivity() == Activity.FLOW_BRANCH
//        /*&& state.getBeginEndType() == FlowState.BeginEndType.BEGIN*/)
        ;
  }
}
