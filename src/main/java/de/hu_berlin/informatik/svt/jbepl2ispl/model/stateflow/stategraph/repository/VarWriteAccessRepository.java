package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.repository;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.Neo4jFlowWriteAccessRelation;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
public interface VarWriteAccessRepository extends Neo4jRepository<Neo4jFlowWriteAccessRelation, Long> {

  @Query(
      "MATCH (s:State)-[r:FLOW_WRITE_TO]->(v:Variable) WHERE id(s) = {eId} RETURN DISTINCT r,s,v ORDER BY r.elementId")
  Collection<Neo4jFlowWriteAccessRelation> findAllByState(@Param("eId") final long stateId);

  @Query(
      "MATCH (s:State)-[r:FLOW_WRITE_TO]->(v:Variable) WHERE v.name = {varName} RETURN DISTINCT r,s,v ORDER BY r.elementId")
  Collection<Neo4jFlowWriteAccessRelation> findAllByVariable(@Param("varName") final String varName);

  @Query("MATCH (s:State)-[r:FLOW_WRITE_TO]->(v:Variable) WHERE v.name = {varName} AND r.elementId < {readElementId}" +
         " RETURN r,s,v" +
         " ORDER BY r.elementId DESC" +
         " LIMIT 1")
  Neo4jFlowWriteAccessRelation findLastBeforeReadByVariable(
      @Param("varName") String varName, @Param("readElementId") int readElementId
  );

  @Query("MATCH (s:State)-[r:FLOW_WRITE_TO]->(v:Variable) WHERE s.initialState = true AND v.name = {varName} RETURN r,s,v ORDER BY r.elementId DESC LIMIT 1")
  Neo4jFlowWriteAccessRelation findLastInitialWriteForVariable(@Param("varName") String varName);
}
