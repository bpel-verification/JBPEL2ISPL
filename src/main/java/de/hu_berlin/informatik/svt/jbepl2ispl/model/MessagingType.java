package de.hu_berlin.informatik.svt.jbepl2ispl.model;

/**
 * Created by Christoph Graupner on 2018-03-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public enum MessagingType {
  DIRECT, INDIRECT, LOCAL, INITIALISE
}
