package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.ExtraInformation;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.Properties;
import org.neo4j.ogm.annotation.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NoArgsConstructor
abstract class BaseAgentElement implements ExtraInformation, Identifiable {

  @Transient
  protected final   Logger               logger           = LoggerFactory.getLogger(getClass());
  @Getter protected int                  alternativeCount = 0;
  @Getter
  protected         int                  elementId;
  @Getter
  protected         String               name;
  @Properties(prefix = "processed")
  private           Map<String, Boolean> processed        = new HashMap<>();
  @Properties(prefix = "stringProperty")
  private           Map<String, String>  properties       = new HashMap<>();

  BaseAgentElement(final int elementId, final int alternativeCount) {
    this.alternativeCount = alternativeCount;
    this.elementId = elementId;
  }

  @Override
  public String getProperty(final String propertyName) {
    return properties.getOrDefault(propertyName, null);
  }

  @Override
  public boolean isFlagged(final String identifier) {
    return processed.getOrDefault(identifier, false);
  }

  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public void setFlagged(final String identifier, final boolean state) {
    processed.put(identifier, state);
  }

  @Override
  public void setProperty(final String propertyName, final String value) {
    properties.put(propertyName, value);
  }

  protected void cloneBase(BaseAgentElement from) {
    this.processed.putAll(from.processed);
    this.properties.putAll(from.properties);
  }

  void setAlternativeCount(final int alternativeCount) {
    this.alternativeCount = alternativeCount;
  }

  void setElementId(final int elementId) {
    this.elementId = elementId;
  }
}
