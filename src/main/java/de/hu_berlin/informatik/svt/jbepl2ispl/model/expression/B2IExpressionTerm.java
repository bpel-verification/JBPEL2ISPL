package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import lombok.Getter;

import java.util.Objects;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class B2IExpressionTerm<T> implements B2IExpression {

  private static final long serialVersionUID = 1L;

  @Getter
  final Type type;

  @Getter
  T term;

  B2IExpressionTerm(
      final Type type, final T term
  ) {
    this.type = type;
    this.term = term;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof B2IExpressionTerm)) {
      return false;
    }
    final B2IExpressionTerm<?> that = (B2IExpressionTerm<?>) o;
    return getType() == that.getType() &&
           Objects.equals(getTerm(), that.getTerm());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getType(), getTerm());
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("B2ITerm{");
    sb.append(type);
    sb.append(", ").append(term);
    sb.append('}');
    return sb.toString();
  }
}
