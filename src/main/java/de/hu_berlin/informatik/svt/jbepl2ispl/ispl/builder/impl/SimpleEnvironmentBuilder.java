package de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.impl;

import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplAgentBuilderBase;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplBuilder;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplEnvironmentBuilder;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.*;

import java.util.Arrays;

/**
 * Created by Christoph Graupner on 2018-03-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@SuppressWarnings("unchecked")
public class SimpleEnvironmentBuilder
    extends
    AgentBuilderBase<IsplBuilder, VarDef, EnvActionDef, EnvProtocolDef, EnvEvolutionDef, EnvRedStatesDef, Environment>
    implements IsplEnvironmentBuilder
{
  SimpleEnvironmentBuilder(IsplBuilder parent, final ISPLFactory factory) {
    super(factory, parent, factory.createEnvironment());
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplBuilder, VarDef, EnvActionDef, EnvProtocolDef, EnvEvolutionDef, EnvRedStatesDef>> T actions(
      final String... actionNames
  ) {
    agent.getActions().getActionNames().addAll(Arrays.asList(actionNames));
    return (T) this;
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplBuilder, VarDef, EnvActionDef, EnvProtocolDef, EnvEvolutionDef, EnvRedStatesDef>> T actions(
      final EnvActionDef actionDefinition
  ) {
    agent.setActions(actionDefinition);
    return (T) this;
  }

  @Override
  public Object build() {
    return agent;
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplBuilder, VarDef, EnvActionDef, EnvProtocolDef, EnvEvolutionDef, EnvRedStatesDef>> T evolution(
      final EnvEvolutionDef evolutionDef
  ) {
    agent.setEvolution(evolutionDef);
    return (T) this;
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplBuilder, VarDef, EnvActionDef, EnvProtocolDef, EnvEvolutionDef, EnvRedStatesDef>> T name(
      final String name
  ) {
    agent.setName(name);
    return (T) this;
  }

  @Override
  public <T extends IsplEnvironmentBuilder> T obsVars(final EnvObsVarDef envObsVars) {
    agent.setObsVariabbles(envObsVars);
    return (T) this;
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplBuilder, VarDef, EnvActionDef, EnvProtocolDef, EnvEvolutionDef, EnvRedStatesDef>> T protocol(
      final EnvProtocolDef agentProtocolDefinition
  ) {
    agent.setProtocol(agentProtocolDefinition);
    return (T) this;
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplBuilder, VarDef, EnvActionDef, EnvProtocolDef, EnvEvolutionDef, EnvRedStatesDef>> T redStates(
      final EnvRedStatesDef agentRedStatesDefinition
  ) {
    agent.setRedStates(agentRedStatesDefinition);
    return (T) this;
  }

  @Override
  public <T extends IsplAgentBuilderBase<IsplBuilder, VarDef, EnvActionDef, EnvProtocolDef, EnvEvolutionDef, EnvRedStatesDef>> T variables(
      final VarDef variableDefinition
  ) {
    agent.setVariables(variableDefinition);
    return (T) this;
  }
}
