package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowVariable;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.QNameNeo4jConverter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.typeconversion.Convert;

import java.util.Objects;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NodeEntity
@NoArgsConstructor
public class Variable extends FlowBaseBpel implements FlowVariable {

  @Getter
  @Relationship(type = Naming.RELATION_FLOW_BELONGS_TO)
  private Service     belongsTo;
  @Getter
  @Setter
  private BpelType    bpelType;
  @Getter
  @Id
  @GeneratedValue
  private Long        id;
  @Getter
  @Setter
  private PurposeType purposeType;
  @Getter
  @Convert(QNameNeo4jConverter.class)
  private QName       qName;
  private ValueType   valueType;
  @Getter
  private String      variableType;
  @Getter
  @Setter
  private boolean     virtual;

  Variable(
      @NonNull final String aName, final String variableOriginalType, final int elementId,
      final String currentBpelPath
  ) {
    super(elementId, currentBpelPath);
    name = aName;
    variableType = variableOriginalType;
    valueType = variableType == null ? null : getValueType(variableType);
    virtual = Consts.VAR_NAME_LITERAL.equals(name)
              || Consts.ISPL_VAR_NAME_STATEVAR.equals(name)
              || Consts.ISPL_VAR_NAME_STATEVAR_ENV.equals(name)
              || Consts.VAR_NAME_SC_COMMITMENT_ID.equals(name)
              || Consts.VAR_NAME_SC_FULFILLMENT_ID.equals(name)
              || name.startsWith(Consts.ISPL_VAR_PREFIX_BUFFER + Consts.ISPL_VAR_NAME_SEPARATOR)
              || name.startsWith(Consts.VAR_NAME_PREFIX_COMM_CHANNEL);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Variable)) {
      return false;
    }
    final Variable variable = (Variable) o;
    return Objects.equals(getName(), variable.getName());
  }

  @Override
  public ValueType getValueType() {
    return getValueType(getVariableType());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getName());
  }

  @Override
  public boolean isInternalOnly() {
    return Consts.VAR_NAME_LITERAL.equals(name);
  }

  @Override
  public void setBelongsTo(final FlowService agent) {
    this.belongsTo = (Service) agent;
    this.qName = new QName(agent.getName(), getName());
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Var{");
    sb.append('\'').append(name).append('\'');
    sb.append(", purposeType=").append(purposeType);
    sb.append(", varType='").append(variableType).append('\'');
    sb.append('}');
    return sb.toString();
  }

  protected ValueType getValueType(String xmlValueType) {
    xmlValueType = xmlValueType.toLowerCase().trim();
    if (xmlValueType.matches(".*\\b(int|integer)\\b.*")) {
      return ValueType.INT;
    }
    if (xmlValueType.matches(".*\\b(bool|boolean)\\b.*")) {
      return ValueType.BOOLEAN;
    }
    if (xmlValueType.matches(".*\\b(string)\\b.*")) {
      return ValueType.STRING;
    }
    return ValueType.UNKNOWN;
  }
}
