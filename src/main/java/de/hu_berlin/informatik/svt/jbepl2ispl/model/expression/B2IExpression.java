package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import lombok.NonNull;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface B2IExpression extends Serializable {

  enum Type {
    AND, OR, NOT, PARENTHESIZED, TERM,

    NUM_ADD, NUM_SUB, NUM_MUL, NUM_DIV, VALUE, BIT_NEGATE, BIT_AND, BIT_OR,
    COMPARE, VARIABLE_REF,
    ACTION_COMPARE,
    FUNCTION
  }

  static B2IActionExpression action(@NonNull final QName qName) {
    return new B2IActionExpression(qName);
  }

  static B2IActionExpression action(@NonNull final String agent, @NonNull final String actionName) {
    return new B2IActionExpression(new QName(agent, actionName));
  }

  static B2IActionExpression actionEnv(@NonNull final String actionName) {
    return action(FlowService.ENV, actionName);
  }

  static B2IActionExpression actionOtherAgent(@NonNull final QName actionName) {
    return new B2IActionExpression(actionName);
  }

  static B2IActionExpression actionSelf(@NonNull String actionName) {
    return new B2IActionExpression(new QName(null, actionName));
  }

  static B2ICompareExpression alwaysFalse() {
    return new B2ICompareExpression(B2ICompareExpression.CompareType.LT, term(1), term(0));
  }

  static B2ICompareExpression alwaysTrue() {
    return new B2ICompareExpression(B2ICompareExpression.CompareType.EQ, term(0), term(1));
  }

  @SafeVarargs
  static <T extends B2IExpression> B2INaryExpression and(@NonNull T... terms) {
    return new DefaultB2INaryExpression(Type.AND, terms);
  }

  static B2INaryExpression arithmetic(@NonNull Type type, @NonNull B2IExpression left, @NonNull B2IExpression right) {
    return new DefaultB2INaryExpression(type, left, right);
  }

  static B2ICompareExpression compare(
      B2ICompareExpression.CompareType compareType, @NonNull B2IExpression left, @NonNull B2IExpression right
  ) {
    return new B2ICompareExpression(compareType, left, right);
  }

  static <C1 extends B2IExpression, C2 extends B2IExpression> B2ICompareExpression equal(
      @NonNull final C1 left, @NonNull final C2 right
  ) {
    return new B2ICompareExpression(B2ICompareExpression.CompareType.EQ, left, right);
  }

  static B2ICompareExpression equal(
      @NonNull final String left, @NonNull final String right
  ) {
    return new B2ICompareExpression(B2ICompareExpression.CompareType.EQ, term(left), term(right));
  }

  static <C1 extends B2IExpression, C2 extends B2IExpression> B2ICompareExpression greaterEqual(
      @NonNull final C1 left, @NonNull final C2 right
  ) {
    return new B2ICompareExpression(B2ICompareExpression.CompareType.GE, left, right);
  }

  static <C1 extends B2IExpression, C2 extends B2IExpression> B2ICompareExpression greaterThan(
      @NonNull final C1 left, @NonNull final C2 right
  ) {
    return new B2ICompareExpression(B2ICompareExpression.CompareType.GT, left, right);
  }

  static <C1 extends B2IExpression, C2 extends B2IExpression> B2ICompareExpression lessEqual(
      @NonNull final C1 left, @NonNull final C2 right
  ) {
    return new B2ICompareExpression(B2ICompareExpression.CompareType.LE, left, right);
  }

  static <C1 extends B2IExpression, C2 extends B2IExpression> B2ICompareExpression lessThan(
      @NonNull final C1 left, @NonNull final C2 right
  ) {
    return new B2ICompareExpression(B2ICompareExpression.CompareType.LT, left, right);
  }

  static B2IUnaryExpression negate(B2IExpression term) {
    return new DefaultB2IUnaryExpression(DefaultB2INaryExpression.Type.NOT, term);
  }

  @SafeVarargs
  static <T extends B2IExpression> B2INaryExpression or(T... terms) {
    return new DefaultB2INaryExpression(Type.OR, terms);
  }

  static <T extends B2IExpression> B2INaryExpression or(Collection<T> terms) {
    final B2INaryExpression condition = new DefaultB2INaryExpression(Type.OR);
    terms.forEach(condition::addTerm);
    return condition;
  }

  static B2IUnaryExpression parenthesized(B2IExpression term) {
    return new DefaultB2IUnaryExpression(Type.PARENTHESIZED, term);
  }

  static B2IExpressionTerm<Integer> term(int term) {
    return new B2IExpressionTerm<>(Type.TERM, term);
  }

  static B2IExpressionTerm<String> term(@NonNull String term) {
    return new B2IExpressionTerm<>(Type.TERM, term);
  }

  static B2IExpressionTerm<Boolean> term(boolean value) {
    return new B2IExpressionTerm<>(Type.TERM, value);
  }

  static B2IVarRefExpression variableReference(@NonNull QName varName) {
    return new B2IVarRefExpression(varName);
  }

  static B2IVarRefExpression variableReference(@NonNull String varName) {
    return new B2IVarRefExpression(new QName(null, varName));
  }

  static B2IVarRefExpression variableReference(@NonNull String agent, @NonNull String varName) {
    return new B2IVarRefExpression(new QName(agent, varName));
  }

  Type getType();

  @SuppressWarnings("unchecked")
  default <T extends B2IExpression> T sort() {
    return (T) this;
  }
}
