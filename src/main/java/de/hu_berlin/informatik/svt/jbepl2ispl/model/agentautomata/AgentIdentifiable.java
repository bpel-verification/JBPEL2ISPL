package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;

/**
 * Created by Christoph Graupner on 2018-06-21.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AgentIdentifiable extends Identifiable {
  String getIsplName();
}
