package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import lombok.Getter;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class B2IExpressionActionPair implements Serializable {

  private static final long serialVersionUID = 1L;

  @Getter final Set<QName>    actions;
  @Getter final B2IExpression expression;

  public B2IExpressionActionPair(
      final B2IExpression expression,
      final Set<QName> actions
  ) {
    this.actions = actions;
    this.expression = expression;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof B2IExpressionActionPair)) {
      return false;
    }
    final B2IExpressionActionPair that = (B2IExpressionActionPair) o;
    return Objects.equals(getActions(), that.getActions()) &&
           Objects.equals(getExpression(), that.getExpression());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getActions(), getExpression());
  }
}
