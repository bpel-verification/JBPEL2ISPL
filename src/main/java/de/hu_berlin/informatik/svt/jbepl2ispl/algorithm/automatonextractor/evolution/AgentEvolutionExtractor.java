package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.automatonextractor.evolution;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.StateInformationResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.resolver.VariableTargetServiceResolver;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentAutomaton;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AgentVariableDefinition;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.AutomatonModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentAutomatonModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2INaryExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowMessageSend;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class AgentEvolutionExtractor extends BaseEvolutionExtractor {

  public AgentEvolutionExtractor(
      final FlowModel flowModel, final AutomatonModel model,
      final VariableTargetServiceResolver resolver,
      final StateInformationResolver stateResolver
  ) {
    super(flowModel, model, resolver, stateResolver);
  }

  @Override
  protected void assignDirect_generateConditions(
      final AgentState agentState, final QName actionName,
      final B2INaryExpression condition
  ) {
    condition.addTerm(
        B2IExpression.equal(
            B2IExpression.variableReference(
                FlowService.ENV,
                getCommChannel(agentState)
            ),
            B2IExpression.term(Consts.ISPL_VAR_ENUM_NONE)
        )
    );
    if (agent.getName().equals(actionName.getAgent())) {
      condition.addTerm(B2IExpression.actionSelf(actionName.getName()));
    } else {
      condition.addTerm(B2IExpression.actionOtherAgent(actionName));
    }
  }

  @Override
  protected void assignDirect_setVariableValues(
      @NonNull final AgentState agentState, final AgentAutomaton srcAgentAutomaton,
      final AgentAutomaton trgtAgentAutomaton,
      @NonNull final String symbolicVariableValueAt
  ) {
    AgentVariableDefinition bufferVarSrcToTrgt = srcAgentAutomaton.getBufferVariableForAgent(trgtAgentAutomaton);
    if (logger.isDebugEnabled()) {

      logger.debug(LogMarker.DEBUG_VALUES, "assignDirect_setVariableValues: {}\nJJ {}\nKK {}\nLL {}\nII {}",
                   agent,
                   StringUtils.join(agent.getVariables(), "\n--"),
                   trgtAgentAutomaton,
                   bufferVarSrcToTrgt,
                   Naming.getBufferName(srcAgentAutomaton.getName(), trgtAgentAutomaton.getName())
      );
    }
    if (bufferVarSrcToTrgt == null) {
      throw new IllegalStateException(
          "There should be a buffervariable for " + srcAgentAutomaton + " -> " + trgtAgentAutomaton +
          " communication.");
    }
    //der buffer des service des invoke states davor muss mit dem wert gefüllt werden

    agentState.setVariableValue(
        bufferVarSrcToTrgt.getQName(),
        symbolicVariableValueAt
    );
  }

  @Override
  protected AgentState extractEvolutionAssignLocal(
      final AgentState agentState
  ) {
    //Assumption: <assign> is labeled with something containing "initial" if it is initializing the variables
    if (agentState.getName().toLowerCase().contains("initial")) {
      //FIXME possibly the initializing of the variable structure in BPEL
    } else {
      throw new IllegalStateException(
          "LOCAL non-initializing assign state '" + agentState +
          "' should not be part of the non-Environment-AgentAutomaton '" +
          agent.getName() + "'");
    }
    return agentState;
  }

  protected void invoke_setVariablesAndAction(
      final AgentState agentState, final FlowState flowState, final FlowMessageSend messageSend,
      final String bufferVarName,
      final String commChannelVarName,
      final QName actionName
  ) {
    //set buffer to value from commchannel as reaction on transmit action of send agent
    agentState.setVariableValue(
        new QName(messageSend.getReceivingService().getName(), bufferVarName),
        messageSend.getCommChannelValue()
    );

    //in send agent (if indirect: env)
    switch (messageSend.getType()) {
      case INDIRECT:
      case DIRECT:
        model.getAllInboundTransitions(agentState).forEach(
            agentTransition -> {
              //set action "transmit"
              agentTransition.setAction(
                  actionName
              );
              ((AgentAutomatonModifiable) agent).save(agentTransition);
            }
        );
        break;
//        break;
      case LOCAL:
        break;
      case INITIALISE:
        break;
    }
  }

  @Override
  B2INaryExpression appendAgentStateVariableCondition(
      final AgentState agentState, final B2INaryExpression condition
  ) {
    if (agentState.isInitialState()) {
      return condition;
    }
    // map the agent state into the ENV fsm to get their preconditions as we depend on that graph instead of the agent's
    if (logger.isDebugEnabled()) {
      logger.debug(LogMarker.DEBUG_VALUES, "appendAgentStateVariableCondition: originId: {}", agentState.getOriginStateId());
    }
    return super.appendAgentStateVariableCondition(
        model.getAgentAutomaton(FlowService.ENV).findCorrespondingStateForFlowState(agentState.getOriginStateId())
        , condition);
  }
}
