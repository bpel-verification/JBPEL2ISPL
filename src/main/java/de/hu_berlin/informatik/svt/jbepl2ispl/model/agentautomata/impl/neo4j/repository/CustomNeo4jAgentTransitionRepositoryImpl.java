package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j.repository;

import org.neo4j.ogm.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Christoph Graupner on 2018-06-05.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
@Transactional(readOnly = true)
@org.neo4j.ogm.annotation.Transient
public class CustomNeo4jAgentTransitionRepositoryImpl implements CustomNeo4jAgentTransitionRepository {
  private final Logger logger = LoggerFactory.getLogger(getClass());
  @Autowired
  Session session;

}
