package de.hu_berlin.informatik.svt.jbepl2ispl.utils;

/**
 * Created by Christoph Graupner on 2018-02-08.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public interface Stack<T> {
    boolean isEmpty();

    T peek();

    T pop();

    Stack<T> push(T element);
}
