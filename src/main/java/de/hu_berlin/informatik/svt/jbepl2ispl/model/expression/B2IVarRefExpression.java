package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import lombok.NonNull;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class B2IVarRefExpression extends B2IExpressionTerm<QName> {

  private static final long serialVersionUID = 1L;

  B2IVarRefExpression(@NonNull final QName term) {
    super(Type.VARIABLE_REF, term);
  }
}
