package de.hu_berlin.informatik.svt.jbepl2ispl.visualize.uml;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.analysisgraph.AnalysisGraph;
import de.hu_berlin.informatik.svt.jbepl2ispl.visualize.VisualOutputAutomaton;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.*;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.jgrapht.io.ExportException;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created by Christoph Graupner on 2018-03-05.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public class UmlOutput implements VisualOutputAutomaton {
  ResourceSet resourceSet;

  public void render(
      final AnalysisGraph model, final String filename
  ) throws IOException, ExportException {
    resourceSet = new ResourceSetImpl();
    registerUml2();

    final Model umlMetamodel = (Model) loadPackage(UMLResource.UML_METAMODEL_URI);

    final Model sampleModel = UMLFactory.eINSTANCE.createModel();
    sampleModel.setName("Bpel2Ispl Automaton Model");
//    UMLFactory.eINSTANCE.createPackage().
//        StateMachine myStateMachine =
//        (StateMachine)sampleModel.createOwnedBehavior("myStatemachine",
//                                                  UMLPackage.Literals.STATE_MACHINE);

//    final Profile sampleProfile = UMLFactory.eINSTANCE.createProfile();
//    sampleProfile.setName( "Sample Profile" );
//
//    final Stereotype ejbStereo = sampleProfile.createOwnedStereotype( "EJB", false);
//    extendMetaclass( umlMetamodel, sampleProfile, "Class", ejbStereo );
//
//    sampleProfile.define();
//
    final Package samplePackage = sampleModel.createNestedPackage("bpel2ispl");
    final Class   sampleClass   = samplePackage.createOwnedClass("BpelService", false);
    StateMachine stateMachine =
        (StateMachine) sampleClass.createOwnedBehavior("StateMachine", UMLPackage.Literals.STATE_MACHINE);
    final Region r = stateMachine.createRegion("d");
    r.createSubvertex("", UMLPackage.Literals.STATE);
//    sampleClass.applyStereotype( ejbStereo );

    final File     outputFile = new File(filename + "." + UMLResource.FILE_EXTENSION);
    final URI      outputUri  = URI.createFileURI(outputFile.getAbsolutePath());
    final Resource resource   = resourceSet.createResource(outputUri);
    resource.getContents().add(sampleModel);
//    resource.getContents().add( sampleProfile );
    resource.save(null);
  }

  void registerUml2() {
    resourceSet.getPackageRegistry().put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);
    resourceSet
        .getResourceFactoryRegistry()
        .getExtensionToFactoryMap()
        .put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
    Map<URI, URI> uriMap = resourceSet.getURIConverter().getURIMap();

//    String resourcesJarPath = Thread.currentThread().getContextClassLoader().getResource("org.eclipse.uml2.uml.resources_5.3.0.v20170605-1616").toExternalForm();
//    URI    uri              = URI.createURI(resourcesJarPath);

    URI uri = URI.createURI(
        "jar:file:./libs/mdt-uml2-SDK-5.3.0/eclipse/plugins/org.eclipse.uml2.uml.resources_5.3.0.v20170605-1616.jar!/"); // for example
    uriMap.put(URI.createURI(UMLResource.LIBRARIES_PATHMAP), uri.appendSegment("libraries").appendSegment(""));
    uriMap.put(URI.createURI(UMLResource.METAMODELS_PATHMAP), uri.appendSegment("metamodels").appendSegment(""));
    uriMap.put(URI.createURI(UMLResource.PROFILES_PATHMAP), uri.appendSegment("profiles").appendSegment(""));

    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(
        UMLResource.FILE_EXTENSION,
        UMLResource.Factory.INSTANCE
    );
  }

  private Package loadPackage(final String uri) {
//    System.out.println("uri = " + uri);
    final Resource resource = resourceSet.getResource(URI.createURI(uri), true);
    EcoreUtil.resolveAll(resource);
    return (org.eclipse.uml2.uml.Package) EcoreUtil.getObjectByType(
        resource.getContents(), UMLPackage.Literals.PACKAGE);
  }
}
