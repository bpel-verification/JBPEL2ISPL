package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j;

import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.*;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentAutomatonModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentStateModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2ICompareExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpressionActionPair;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2INaryExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowId;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowScTraceLabel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.FlowIdNeo4jConverter;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.Jb2iUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.apache.commons.compress.utils.Sets;
import org.neo4j.ogm.annotation.*;
import org.neo4j.ogm.annotation.typeconversion.Convert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Christoph Graupner on 2018-05-29.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@SuppressWarnings("unchecked")
@NodeEntity(label = Naming.AGENT_AUTOMATON)
@NoArgsConstructor
@Component
@Scope("prototype")
public class Neo4jAgentAutomaton implements AgentAutomatonModifiable {
  @Transient
  private final Logger logger = LoggerFactory.getLogger(getClass());

  @Getter
  @Id
  @GeneratedValue
  Long id;
  @Relationship(type = Naming.RELATION_TALKS_TO)
  private Set<Neo4jAgentAutomaton> connectedAgents = new HashSet<>();
  @Getter
  private int                      elementId;
  @Autowired
  @Transient
  private AutomatonModel           model;
  @Getter
  @Setter
  private String                   name;
  @Convert(FlowIdNeo4jConverter.class)
  private FlowId                   originService;

  private String                       protocolConditions;
  @Relationship(type = Naming.RELATION_AUTOMATON_STATE)
  private Set<Neo4jAgentState>         states    = new HashSet<>();
  @Relationship(type = Naming.RELATION_AGENT_BELONGS_TO, direction = Relationship.INCOMING)
  private Set<Neo4jVariableDefinition> variables = new HashSet<>();

  Neo4jAgentAutomaton(@NonNull final String agentName, int elementId) {
    this.name = agentName;
    this.elementId = elementId;
  }

  Neo4jAgentAutomaton(@NonNull final String agentName, @NonNull final FlowService originService) {
    this.name = agentName;
    this.elementId = originService.getElementId();
    setOriginFlowService(originService);
  }

  @Override
  public AgentAutomatonModifiable addState(@NonNull final AgentState state) {
    ((AgentStateModifiable) state).setAgentAutomaton(this);
    model.save(state);
    states.add((Neo4jAgentState) state);
    model.save(this);
    return this;
  }

  @Override
  public void addTalksTo(@NonNull final AgentAutomaton otherAgent) {
    connectedAgents.add((Neo4jAgentAutomaton) otherAgent);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Neo4jAgentAutomaton)) {
      return false;
    }
    final Neo4jAgentAutomaton that = (Neo4jAgentAutomaton) o;
    return Objects.equals(getName(), that.getName());
  }

  @Override
  public <T extends AgentState> Collection<T> findAllStatesWithAction(final String actionName) {

    return model.findAllStatesWithActionForAgent(actionName, this);
  }

  @Override
  public AgentState findCorrespondingStateForFlowState(
      final FlowId flowStateId
  ) {
    return model.findStateForAgentByOriginFlowId(this, flowStateId);
  }

  @Override
  public Collection<String> getActionOthers() {
    return Arrays.asList(Consts.ISPL_ACTION_NAME_NONE);
  }

  @Override
  public Set<QName> getActions() {
    Iterable<? extends AgentTransition> transitions = model.getAllTransitionsForAutomaton(FlowService.ENV);
    Set<QName>                          ret         = new HashSet<>();
    for (final AgentTransition transition: transitions) {
      final QName action = transition.getAction();
      if (action != null && (action.belongsToAgent(getName()))) {
        ret.add(action);
      }
    }
    return ret;
  }

  @Override
  public <T extends AgentTransition> Collection<T> getAllInboundTransitions(
      @NonNull final AgentState state
  ) {
    return model.getAllInboundTransitions(state);
  }

  @Override
  public <T extends AgentTransition> Collection<T> getAllOutboundTransitions(
      @NonNull final AgentState state
  ) {
    return model.getAllOutboundTransitions(state);
  }

  @Override
  public AgentVariableDefinition getBufferVariableForAgent(
      final AgentAutomaton agent
  ) {
    return getVariableDefinition(Naming.getBufferName(this.getName(), agent.getName()));
  }

  @Override
  public B2INaryExpression getDesiredStateDefinition(final String name) {

    return
        B2IExpression.and(
            B2IExpression.compare(
                B2ICompareExpression.CompareType.EQ,
                B2IExpression.variableReference(getTraceLabelName()),
                B2IExpression.term(FlowScTraceLabel.Type.DESIRED.getIsplCode())
            )
        );
  }

  @Override
  public Set<String> getDesiredStates() {
    if (isEnvironment()) {
      return new HashSet<>();
    }

    return Sets.newHashSet(Naming.getDesiredStateProposition(getName()));
  }

  @Override
  public String getIsplName() {
    return getName();
  }

  @Override
  public AgentState getLastState() {
    return model.getLastStateForAgent(this);
  }

  @Override
  public Set<B2IExpressionActionPair> getProtocolConditions() {
    try {
      return (Set<B2IExpressionActionPair>) Jb2iUtils.fromBase64StringToObject(protocolConditions);
    } catch (IOException | ClassNotFoundException e) {
      throw new IllegalStateException(e);
    }
  }

  @Override
  public void setProtocolConditions(
      final Set<B2IExpressionActionPair> actionPairs
  ) {
    try {
      this.protocolConditions = Jb2iUtils.toBase64String((Serializable) actionPairs);
    } catch (IOException | ClassCastException e) {
      throw new IllegalStateException(e);
    }
  }

  @Override
  public AgentVariableDefinition getStateVariableDef() {
    if (isEnvironment()) {
      return getVariableDefinition(Consts.ISPL_VAR_NAME_STATEVAR_ENV);
    }
    return getVariableDefinition(Consts.ISPL_VAR_NAME_STATEVAR);
  }

  @Override
  public <T extends AgentState> Collection<T> getStates() {
    return model.getAllStatesForAutomaton(this);
  }

  @Override
  public <T extends AgentAutomaton> Set<T> getTalksToAgents() {
    return (Set<T>) connectedAgents;
  }

  @Override
  public QName getTraceLabelName() {
    if (isEnvironment()) {
      return null;
    }
    return Naming.getTraceLabelName(getName());
  }

  @Override
  public Collection<? extends AgentTransition> getTransitions() {
    return null;
  }

  @Override
  public B2INaryExpression getUnDesiredStateDefinition(final String name) {
    if (isEnvironment()) {
      return null;
    }
    return B2IExpression.and(
        B2IExpression.compare(
            B2ICompareExpression.CompareType.EQ,
            B2IExpression.variableReference(getTraceLabelName()),
            B2IExpression.term(FlowScTraceLabel.Type.UNDESIRED.getIsplCode())
        )
    );
  }

  @Override
  public Set<String> getUnDesiredStates() {
    return Sets.newHashSet(Naming.getUnDesiredStateProposition(getName()));
  }

  @Override
  public AgentVariableDefinition getVariableDefinition(final String name) {
    return model.refresh(
        variables.stream()
                 .filter(neo4jVariableDefinition -> name.equals(neo4jVariableDefinition.name))
                 .findFirst()
                 .orElse(null),
        3
    );
  }

  @Override
  public <T extends AgentVariableDefinition> Collection<T> getVariableDefinitions() {
    return (Collection<T>) variables;
  }

  @Override
  public Set<String> getVariables() {

    final HashSet<String> ret = getVariableDefinitions()
        .stream()
        .map(AgentVariableDefinition::getName)
        .collect(Collectors.toCollection(HashSet::new));
    return ret;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getName());
  }

  public boolean isEnvironment() {
    return FlowService.ENV.equals(getName());
  }

  @Override
  public boolean ownsVariable(final String variableName) {
    return getVariables().contains(variableName);
  }

  @Override
  public AgentTransition save(final AgentTransition transition) {
    return model.save(transition);
  }

  @Override
  public AgentVariableDefinition save(final AgentVariableDefinition varDef) {
    varDef.setBelongsTo(this);
    final AgentVariableDefinition ret = model.save(varDef);
    variables.remove(varDef);
    variables.add((Neo4jVariableDefinition) ret);
    return ret;
  }

  @Override
  public AgentState save(final AgentState state) {
    return model.save(state);
  }

  public void setOriginFlowService(final FlowService flowService) {
    originService = new FlowId(flowService.getFlowId());
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("AgentAutomaton{");
    sb.append("'").append(name).append('\'');
    sb.append(", elementId=").append(elementId);
    sb.append(", connectedStates=")
      .append(connectedAgents.stream().map(Neo4jAgentAutomaton::getId).collect(Collectors.toList()));
    sb.append(", originService=").append(originService);
    sb.append(", states=").append(states.size());
    sb.append(", variables=")
      .append(variables.stream().map(Neo4jVariableDefinition::getName).collect(Collectors.toList()));
    sb.append('}');
    return sb.toString();
  }

  void setModel(final AutomatonModel model) {
    this.model = model;
  }
}
