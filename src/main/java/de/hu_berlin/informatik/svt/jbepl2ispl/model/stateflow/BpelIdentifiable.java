package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;

/**
 * Created by Christoph Graupner on 2018-03-01.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public interface BpelIdentifiable extends Identifiable {
  BpelPath getBpelPath();

  void setBpelPath(String bpelObjectPath);

  String getBpelPathAsString();

  FlowId getFlowId();
}
