package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Naming;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowServiceOperation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NodeEntity
@NoArgsConstructor
public class Service extends FlowBaseBpel implements FlowService {
  @Getter
  @Id
  @GeneratedValue
  private Long id;

  @Getter
  @Setter
  private String                name;
  @Relationship(type = Naming.RELATION_PROVIDES_OP)
  private Set<ServiceOperation> operations = new HashSet<>();
  @Getter
  private Set<String>           variables  = new HashSet<>();

  Service(final String aName, final int elementId, final String bpelObjectPath) {
    super(elementId, bpelObjectPath);
    name = aName;
  }

  @Override
  public FlowService addOperation(final FlowServiceOperation operation) {
    operations.add((ServiceOperation) operation);
    return this;
  }

  @Override
  public void addVariable(final String variable) {
    variables.add(variable);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Service)) {
      return false;
    }
    final Service service = (Service) o;
    return Objects.equals(getName(), service.getName());
  }

  @Override
  public Set<? extends FlowServiceOperation> getOperations() {
    return operations;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getName());
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Service{");
    sb.append("name='").append(name).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
