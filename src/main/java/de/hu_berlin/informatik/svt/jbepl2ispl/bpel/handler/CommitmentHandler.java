package de.hu_berlin.informatik.svt.jbepl2ispl.bpel.handler;

import de.hu_berlin.informatik.svt.jbepl2ispl.Config;
import de.hu_berlin.informatik.svt.jbepl2ispl.Consts;
import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.CompilerState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.Activity;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.BeginEndType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowScTraceLabel;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import org.apache.ode.bpel.compiler.bom.BpelObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * Created by Christoph Graupner on 2018-05-10.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
public class CommitmentHandler extends HandlerBase {

  @Autowired
  Config config;

  public CommitmentHandler() {
    super(Arrays.asList(BpelObject.class));
  }

  @Override
  public <T extends HandlerBase> T handleBegin(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
    final String bpelObjectLocalPart = currentBpelObject.getType().getLocalPart();
    if (config.getScTraceLabelActivityName().equals(bpelObjectLocalPart)) {
      // tracelabel
      handleTracelabel(currentBpelObject, compilerState);
    } else if (config.getScFulfillmentActivityName().equals(bpelObjectLocalPart)) {
      // fulfillment
      handleFulfillmentLabel(currentBpelObject, compilerState);
    } else if (config.getScCommitmentActivityName().equals(bpelObjectLocalPart)) {
      // commiment
      handleCommitmentLabel(currentBpelObject, compilerState);
    } else if (config.getScRecoveryLabelActivityName().equals(bpelObjectLocalPart)) {
      // recovery
      handleRecoveryLabel(currentBpelObject, compilerState);
    }

    return super.handleBegin(currentBpelObject, compilerState);
  }

  @Override
  public <T extends HandlerBase> T handleEnd(
      final BpelObject currentBpelObject, final CompilerState compilerState
  ) {
//    final String bpelOriginalName = currentBpelObject.getElement().getAttribute("name");
//    if (config.getFulfillmentActivityName().equals(currentBpelObject.getType().getLocalPart())) {
//      // fulfActivity
//      final FlowModel model = compilerState.getFlowModel();
//      logger.info(
//          "END FULFILLMENT: {}: {}", currentBpelObject.getClass().getSimpleName(), currentBpelObject.getType());
//      FlowState state = model.factory().createState(
//          bpelOriginalName,
//          "FULFILLMENT_END__" + bpelOriginalName,
//          Activity.FULFILLMENT,
//          BeginEndType.END,
//          compilerState.getCurrentBpelPath()
//      );
//      model.save(state);
//    } else if (config.getCommitmentActivityName().equals(currentBpelObject.getType().getLocalPart())) {
//      // CommActivity
//      logger.info(
//          "END COMMITMENT: {}: {}", currentBpelObject.getClass().getSimpleName(), currentBpelObject.getType());
//      final FlowModel model = compilerState.getFlowModel();
//      FlowState state = model.factory().createState(
//          bpelOriginalName,
//          "COMMITMENT_END__" + bpelOriginalName,
//          Activity.COMMITMENT,
//          BeginEndType.END,
//          compilerState.getCurrentBpelPath()
//      );
//      model.save(state);
//    }
    return super.handleEnd(currentBpelObject, compilerState);
  }

  private void handleCommitmentLabel(final BpelObject currentBpelObject, final CompilerState compilerState) {
    logger.info(
        "BEGIN COMMITMENT: {}: {}", currentBpelObject.getClass().getSimpleName(), currentBpelObject.getType());
    final FlowModel model = compilerState.getFlowModel();

    FlowState state = model.factory().createState(
        currentBpelObject.getElement().getAttribute("name"),
        "COMMITMENT__" + currentBpelObject.getElement().getAttribute("name"),
        Activity.SC_COMMITMENT,
        BeginEndType.BOTH,
        compilerState.getCurrentBpelPath()
    );
    final String commitmentID = currentBpelObject.getElement().getAttribute("commitmentID");
    final String debtor       = currentBpelObject.getElement().getAttribute("debtor");
    final String creditor     = currentBpelObject.getElement().getAttribute("creditor");

    if (commitmentID == null || "".equals(commitmentID)) {
      throw new IllegalStateException(
          config.getScCommitmentActivityName() + " activity's attribute '" + "commitmentID" + "' must not be empty."
      );
    }
    if (debtor == null || "".equals(debtor)) {
      throw new IllegalStateException(
          config.getScCommitmentActivityName() + " activity's attribute '" + "debtor" + "' must not be empty."
      );
    }
    if (creditor == null || "".equals(creditor)) {
      throw new IllegalStateException(
          config.getScCommitmentActivityName() + " activity's attribute '" + "creditor" + "' must not be empty."
      );
    }

    state.setProperty(Consts.PROPERTY_SC_COMMITMENT_ID, commitmentID);
    state.setProperty(Consts.PROPERTY_SC_DEBTOR, debtor);
    state.setProperty(Consts.PROPERTY_SC_CREDITOR, creditor);

    model.save(state);
  }

  private void handleFulfillmentLabel(final BpelObject currentBpelObject, final CompilerState compilerState) {
    final FlowModel model = compilerState.getFlowModel();
    logger.info(
        "BEGIN FULFILLMENT: {}: {}", currentBpelObject.getClass().getSimpleName(), currentBpelObject.getType()
    );
    FlowState state = model.factory().createState(
        currentBpelObject.getElement().getAttribute("name"),
        "FULFILLMENT__" + currentBpelObject.getElement().getAttribute("name"),
        Activity.SC_FULFILLMENT,
        BeginEndType.BOTH,
        compilerState.getCurrentBpelPath()
    );
    final String commitmentID  = currentBpelObject.getElement().getAttribute("commitmentID");
    final String fulfillmentID = currentBpelObject.getElement().getAttribute("fulfillmentID");

    if (commitmentID == null || "".equals(commitmentID)) {
      throw new IllegalStateException(
          config.getScFulfillmentActivityName() + " activity's attribute '" + "commitmentID" + "' must not be empty."
      );
    }
    if (fulfillmentID == null || "".equals(fulfillmentID)) {
      throw new IllegalStateException(
          config.getScFulfillmentActivityName() + " activity's attribute '" + "fulfillmentID" + "' must not be empty."
      );
    }

    state.setProperty(Consts.PROPERTY_SC_COMMITMENT_ID, commitmentID);
    state.setProperty(Consts.PROPERTY_SC_FULFILLMENT_ID, fulfillmentID);
    model.save(state);
  }

  private void handleRecoveryLabel(final BpelObject currentBpelObject, final CompilerState compilerState) {
    final FlowModel model = compilerState.getFlowModel();
    logger.info(
        "BEGIN RECOVERY: {}: {}", currentBpelObject.getClass().getSimpleName(), currentBpelObject.getType()
    );
    FlowState state = model.factory().createState(
        currentBpelObject.getElement().getAttribute("name"),
        "RECOVERY__" + currentBpelObject.getElement().getAttribute("name"),
        Activity.SC_RECOVERY,
        BeginEndType.BOTH,
        compilerState.getCurrentBpelPath()
    );
    final String commitmentID = currentBpelObject.getElement().getAttribute("commitmentID");
    final String recoveryID   = currentBpelObject.getElement().getAttribute("recoveryID");

    if (commitmentID == null || "".equals(commitmentID)) {
      throw new IllegalStateException(
          config.getScRecoveryLabelActivityName() + " activity's attribute '" + "commitmentID" +
          "' must not be empty."
      );
    }
    if (recoveryID == null || "".equals(recoveryID)) {
      throw new IllegalStateException(
          config.getScRecoveryLabelActivityName() + " activity's attribute '" + "recoveryID" + "' must not be empty."
      );
    }

    state.setProperty(Consts.PROPERTY_SC_COMMITMENT_ID, commitmentID);
    state.setProperty(Consts.PROPERTY_SC_RECOVERY_ID, recoveryID);
    model.save(state);
  }

  private void handleTracelabel(final BpelObject currentBpelObject, final CompilerState compilerState) {
    final FlowModel model = compilerState.getFlowModel();
    logger.info(
        "BEGIN TRACELABEL: {}: {}", currentBpelObject.getClass().getSimpleName(), currentBpelObject.getType());
    FlowState state = model.factory().createState(
        currentBpelObject.getElement().getAttribute("name"),
        "TRACE_LABEL__" + currentBpelObject.getElement().getAttribute("name"),
        Activity.SC_TRACE_LABEL,
        BeginEndType.BOTH,
        compilerState.getCurrentBpelPath()
    );
    final String serviceName   = currentBpelObject.getElement().getAttribute("serviceName");
    final String behaviourType = currentBpelObject.getElement().getAttribute("behaviourType");

    if (serviceName == null || "".equals(serviceName)) {
      throw new IllegalStateException(
          config.getScTraceLabelActivityName() + " activity's attribute '" + "serviceName" + "' must not be empty."
      );
    }
    if (behaviourType == null || "".equals(behaviourType)) {
      throw new IllegalStateException(
          config.getScTraceLabelActivityName() + " activity's attribute '" + "behaviourType" + "' must not be empty."
      );
    }

    state.setProperty(Consts.PROPERTY_SC_TRACE_SERVICE_NAME, serviceName);
    state.setProperty(
        Consts.PROPERTY_SC_TRACE_BEHAVIOUR_TYPE, FlowScTraceLabel.Type.valueOf(behaviourType.toUpperCase()).toString());
    model.save(state);
  }
}
