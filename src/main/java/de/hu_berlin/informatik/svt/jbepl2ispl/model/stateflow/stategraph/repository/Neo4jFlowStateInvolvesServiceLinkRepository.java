package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.repository;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph.Neo4jFlowStateInvolvesServiceLink;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Christoph Graupner on 2018-06-18.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
public interface Neo4jFlowStateInvolvesServiceLinkRepository
    extends Neo4jRepository<Neo4jFlowStateInvolvesServiceLink, Long>
{
}
