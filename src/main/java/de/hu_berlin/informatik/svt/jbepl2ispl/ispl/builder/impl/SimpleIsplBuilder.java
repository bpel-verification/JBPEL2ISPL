package de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.impl;

import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplAgentsBuilder;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplBuilder;
import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplEnvironmentBuilder;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.Agent;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.ISPLFactory;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.InterpreterSystem;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.SimpleAgent;

import java.util.List;

/**
 * Created by Christoph Graupner on 2018-03-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class SimpleIsplBuilder implements IsplBuilder {
  private final ISPLFactory              factory;
  private final InterpreterSystem        interpreterSystem;
  private       AgentsBuilder            agentsBuilder;
  private       SimpleEnvironmentBuilder environmentBuilder;

  public SimpleIsplBuilder(final ISPLFactory isplFactory) {
    this.factory = isplFactory;
    interpreterSystem = isplFactory.createInterpreterSystem();
  }

  @Override
  public IsplAgentsBuilder agents() {
    if (agentsBuilder == null) {
      agentsBuilder = new AgentsBuilder(this, factory);
    }
    return agentsBuilder;
  }

  @Override
  public InterpreterSystem build() {
    interpreterSystem.getAgents().addAll((List<SimpleAgent>) agentsBuilder.build());
    interpreterSystem.setEnvironment((Agent) environmentBuilder.build());
    return interpreterSystem;
  }

  @Override
  public IsplEnvironmentBuilder environment() {
    if (environmentBuilder == null) {
      environmentBuilder = new SimpleEnvironmentBuilder(this, factory);
    }
    return environmentBuilder;
  }

  @Override
  public IsplBuilder end() {
    throw new IllegalStateException("Use build() instead.");
  }
}
