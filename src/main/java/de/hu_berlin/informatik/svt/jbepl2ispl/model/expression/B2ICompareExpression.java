package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import lombok.Getter;

import java.util.Objects;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class B2ICompareExpression extends DefaultB2IBinaryExpression implements B2IBinaryExpression {

  public enum CompareType {
    EQ, GT, GE, LT, LE, ACTION_COMPARE
  }

  private static final long serialVersionUID = 1L;

  @Getter
  CompareType operator;

  B2ICompareExpression(
      final CompareType type, final B2IExpression left, final B2IExpression right
  ) {
    super(Type.COMPARE, left, right);
    this.operator = type;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof B2ICompareExpression)) {
      return false;
    }
    final B2ICompareExpression that = (B2ICompareExpression) o;
    return getType() == that.getType() &&
           Objects.equals(getLeft(), that.getLeft()) &&
           getOperator() == that.getOperator() &&
           Objects.equals(getRight(), that.getRight());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getType(), getLeft(), getOperator(), getRight());
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("B2ICompareExpression{");
    sb.append("left=").append(left);
    sb.append(" op=").append(operator);
    sb.append(" right=").append(right);
    sb.append('}');
    return sb.toString();
  }
}
