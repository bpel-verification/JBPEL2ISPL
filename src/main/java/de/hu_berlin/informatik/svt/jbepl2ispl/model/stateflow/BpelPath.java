package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import lombok.NonNull;
import org.apache.ode.bpel.compiler.bom.Bpel20QNames;
import org.neo4j.ogm.annotation.Transient;
import org.springframework.util.StringUtils;

import java.util.Arrays;

/**
 * Created by Christoph Graupner on 2018-03-07.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
@Transient
public class BpelPath {
  public static final String ELEMENT_ASSIGN      = Bpel20QNames.FINAL_ASSIGN.getLocalPart();
  public static final String ELEMENT_FLOW        = Bpel20QNames.FINAL_FLOW.getLocalPart();
  public static final String ELEMENT_IF          = Bpel20QNames.FINAL_IF.getLocalPart();
  public static final String ELEMENT_IFCASE_ELSE = Bpel20QNames.FINAL_ELSE.getLocalPart();
  public static final String ELEMENT_WHILE       = Bpel20QNames.FINAL_WHILE.getLocalPart();

  private final String   bpelPath;
  private final String[] parts;

  public BpelPath(@NonNull final String bpelPath) {
    this(bpelPath.split("/"));
  }

  public BpelPath(@NonNull final String[] parts) {
    int i, j;
    for (i = 0; i < parts.length; i++) {
      if (!parts[i].isEmpty()) {
        break;
      }
    }
    for (j = parts.length - 1; j >= 0; j--) {
      if (!parts[j].isEmpty()) {
        break;
      }
    }

    this.parts = Arrays.copyOfRange(parts, i, j + 1);
    bpelPath = "/" + StringUtils.arrayToDelimitedString(this.parts, "/");
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof BpelPath) {
      BpelPath path = (BpelPath) obj;
      return bpelPath.equals(path.bpelPath);
    }
    return false;
  }

  public BpelPath getNearestElement(String element) {
    String search = element + "[";
    for (int i = parts.length - 1; i >= 0; i--) {
      if (parts[i].startsWith(search) || parts[i].contains(":" + search)) {
        return new BpelPath(Arrays.copyOfRange(parts, 0, i + 1));
      }
    }
    return null;
  }

  @Override
  public int hashCode() {
    return bpelPath.hashCode();
  }

  public boolean isDirectChildOf(final BpelPath parent) {
    if (parent.parts.length != parts.length - 1) {
      return false;
    }

    return parent.isSubElement(this);
  }

  public boolean isInside(final String elementFlow) {
    return getNearestElement(elementFlow) != null;
  }

  public boolean isSubElement(final BpelPath otherPath) {
    if (otherPath.parts.length - 1 < parts.length) {
      return false;
    }
    for (int i = 0; i < parts.length - 1; i++) {
      if (!parts[i].equals(otherPath.parts[i])) {
        return false;
      }
    }

    return true;
  }

  public boolean isSubElement(final String elementPath) {
    return isSubElement(new BpelPath(elementPath));
  }

  public int length() {
    return bpelPath.length();
  }

  @Override
  public String toString() {
    return bpelPath;
  }

  BpelPath getParent() {
    return new BpelPath(Arrays.copyOfRange(parts, 0, parts.length - 1));
  }
}
