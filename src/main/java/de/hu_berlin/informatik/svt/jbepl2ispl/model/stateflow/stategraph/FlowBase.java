package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowAdditional;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowId;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.Properties;
import org.neo4j.ogm.annotation.Transient;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Christoph Graupner on 2018-03-16.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@NoArgsConstructor
public abstract class FlowBase implements FlowAdditional {
  @Getter
  protected int                  alternativeCount = 0;
  /**
   * Hier durch wird es möglich, die Elemente untereinander in eine Reihenfolge zu bringen, wie sie im
   * ursprünglich BPEL Dokument auftauchen.
   */
  @Getter
  protected int                  elementId;
  @Getter
  protected String               name;
  @Properties(prefix = "flagged")
  private   Map<String, Boolean> flagged          = new HashMap<>();
  @Transient
  private   FlowId               flowId           = null;
  @Properties(prefix = "stringProperty")
  private   Map<String, String>  properties       = new HashMap<>();

  FlowBase(final int elementId) {
    this.elementId = elementId;
  }

  FlowBase(final String name, final int elementId) {
    this.elementId = elementId;
    this.name = name;
  }

  FlowBase(final String name, final int elementId, final int alternativeCount) {
    this.alternativeCount = alternativeCount;
    this.elementId = elementId;
    this.name = name;
  }

  @Override
  public FlowId getFlowId() {
    if (flowId == null) {
      flowId = new FlowId(elementId, alternativeCount);
    }
    return flowId;
  }

  @Override
  public String getProperty(final String propertyName) {
    return properties.getOrDefault(propertyName, null);
  }

  @Override
  public boolean isFlagged(final String identifier) {
    return flagged.getOrDefault(identifier, false);
  }

  @Override
  public void setFlagged(final String identifier, final boolean state) {
    flagged.put(identifier, state);
  }

  @Override
  public void setProperty(final String propertyName, final String value) {
    properties.put(propertyName, value);
  }

  protected void cloneBase(FlowBase from) {
    this.flagged.putAll(from.flagged);
    this.properties.putAll(from.properties);
  }

  void setAlternativeCount(final int alternativeCount) {
    this.alternativeCount = alternativeCount;
    flowId = null;
  }

  void setElementId(final int elementId) {
    this.elementId = elementId;
    flowId = null;
  }

  void setName(final String name) {
    this.name = name;
  }
}
