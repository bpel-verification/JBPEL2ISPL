package de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.impl;

import de.hu_berlin.informatik.svt.jbepl2ispl.ispl.builder.IsplBuilderBase;
import de.hu_berlin.informatik.svt.mcmas.ispl.iSPL.ISPLFactory;
import lombok.Getter;

/**
 * Created by Christoph Graupner on 2018-03-19.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
abstract class BuilderBase<PARENT extends IsplBuilderBase> implements IsplBuilderBase<PARENT>, BuilderInternal {
  @Getter
  private final ISPLFactory factory;
  @Getter
  private final PARENT      parent;

  public BuilderBase(final ISPLFactory factory, final PARENT parent) {
    this.factory = factory;
    this.parent = parent;
  }

  @Override
  public PARENT end() {
    return parent;
  }
}
