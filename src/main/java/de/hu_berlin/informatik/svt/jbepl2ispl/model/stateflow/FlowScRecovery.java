package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface FlowScRecovery extends Identifiable {
}
