package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j.repository;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j.Neo4jAgentAutomaton;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Christoph Graupner on 2018-05-29.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Repository
public interface Neo4jAgentAutomatonRepository extends Neo4jRepository<Neo4jAgentAutomaton, Long> {
  Neo4jAgentAutomaton findFirstByName(String agentName);
}
