package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpressionActionPair;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2INaryExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowId;

import java.util.Collection;
import java.util.Set;

/**
 * Created by Christoph Graupner on 2018-05-25.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AgentAutomatonAnalysis {
  <T extends AgentState> Collection<T> findAllStatesWithAction(String actionName);

  AgentState findCorrespondingStateForFlowState(FlowId flowStateId);

  Set<QName> getActions();

  B2INaryExpression getDesiredStateDefinition(String name);

  Set<String> getDesiredStates();

  /**
   * Returns the values for the ISPL-<em>Protocol</em> section of an agent
   *
   * @return <b>key</b>: condition when the actions (value of the map entry) are executable
   * <br/> <b>value</b>:
   */
  Set<B2IExpressionActionPair> getProtocolConditions();

  void setProtocolConditions(Set<B2IExpressionActionPair> actionPairs);

  B2INaryExpression getUnDesiredStateDefinition(String name);

  Set<String> getUnDesiredStates();

  /**
   * Returns the values for the ISPL-<em>Variables</em> section of an agent
   *
   * @return
   */
  <T extends AgentVariableDefinition> Collection<T> getVariableDefinitions();

  boolean ownsVariable(String variableName);
}
