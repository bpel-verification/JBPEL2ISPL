package de.hu_berlin.informatik.svt.jbepl2ispl.algorithm.postprocessor;

import de.hu_berlin.informatik.svt.jbepl2ispl.LogMarker;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.VarValueSourceType;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.*;
import org.springframework.stereotype.Service;

/**
 * Created by Christoph Graupner on 2018-06-27.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@Service
class SourceTargetServiceForStatesPostProcessor extends FlowModelPostProcessor {

  protected SourceTargetServiceForStatesPostProcessor(final FlowModel model) {
    super(model);
  }

  @Override
  public void finish() {
    deriveSourceTargetAgentsForStates();
  }

  private FlowStateInvolvesServiceLink createInvolvesLink(final FlowState flowState, final FlowService targetAgent) {
    final FlowStateInvolvesServiceLink link =
        flowModel.factory().createStateInvolvesServiceLink(flowState, targetAgent);
    return flowModel.save(link);
  }

  private void deriveSourceTargetAgentsForStates() {
    logger.trace(LogMarker.POSTPROCESSING, "Deriving source and target service for states...");
    final Iterable<? extends FlowService> allServices = flowModel.getServices();
    flowModel.getStates().forEach(
        flowState -> {
          if (flowState != null && flowState.getCausingActivity() != null) {
            switch (flowState.getCausingActivity()) {
              case ASSIGN:
                final FlowVarWriteAccessRelation variableValue =
                    flowModel.findAllVarWriteAccessRelationsForState(flowState).iterator().next();
                FlowVariable targetVariable = variableValue.getVariable();
                FlowService targetService = flowModel.findAgentWithVariable(targetVariable.getName());
                flowState.setTargetService(targetService);
//                createInvolvesLink(flowState, targetService);

                FlowService sourceService = null;
                if (variableValue.getValueSourceType() == VarValueSourceType.VARIABLE) {
                  sourceService = flowModel.findAgentWithVariable(variableValue.getValueSource());
                  flowState.setSourceService(sourceService);
//                  createInvolvesLink(flowState, sourceService);
                }
                if (targetService != null || sourceService != null) {
                  flowModel.save(flowState);
                }
                break;
              case RECEIVE:
                targetVariable = flowModel.findVariableByName(flowState.getOutputVariable());
                sourceService = flowModel.findAgentWithVariable(targetVariable.getName());
                targetService = flowModel.findServiceByName(FlowService.ENV);
                flowState.setSourceService(sourceService);
                flowState.setTargetService(targetService);
                flowModel.save(flowState);
                break;
              case REPLY:
                final String inputVariable = flowState.getInputVariable();
                if (inputVariable != null) {
                  FlowVariable sourceVariable = flowModel.findVariableByName(inputVariable);
                  targetService = flowModel.findAgentWithVariable(sourceVariable.getName());
                  sourceService = flowModel.findServiceByName(FlowService.ENV);
                  flowState.setSourceService(sourceService);
                  flowState.setTargetService(targetService);
                  flowModel.save(flowState);
                }
                break;
              case WHILE:
              case IF:
              case IFCASE_ELSEIF:
              case IFCASE_ELSE:
              case FLOW:
                flowState = flowModel.refresh(flowState, 3);
                for (FlowService flowService: allServices) {
                  createInvolvesLink(flowState, flowService);
                }
                break;
            }
          } else {
            logger.error(LogMarker.POSTPROCESSING, "That's wrong: {} ", flowState);
          }
        }
    );
    logger.trace(LogMarker.POSTPROCESSING, "DONE Deriving source and target service for states...");
  }
}
