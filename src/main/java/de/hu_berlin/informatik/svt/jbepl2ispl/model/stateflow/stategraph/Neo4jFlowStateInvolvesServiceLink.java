package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.stategraph;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowStateInvolvesServiceLink;
import lombok.Getter;
import org.neo4j.ogm.annotation.*;

/**
 * Created by Christoph Graupner on 2018-06-18.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@RelationshipEntity(type = "INVOLVES")
public class Neo4jFlowStateInvolvesServiceLink implements FlowStateInvolvesServiceLink {
  @Getter
  private int  elementId;
  @Id
  @GeneratedValue
  @Getter
  private Long neo4jId;

  @EndNode
  private Service service;
  @StartNode
  private State   state;

  Neo4jFlowStateInvolvesServiceLink(final State state, final Service service, final int elementId) {
    this.state = state;
    this.service = service;
    this.elementId = elementId;
  }

  @Override
  public FlowService getService() {
    return service;
  }

  @Override
  public void setService(final FlowService service) {
    this.service = (Service) service;
  }

  @Override
  public FlowState getState() {
    return state;
  }

  @Override
  public void setState(final FlowState state) {
    this.state = (State) state;
  }
}
