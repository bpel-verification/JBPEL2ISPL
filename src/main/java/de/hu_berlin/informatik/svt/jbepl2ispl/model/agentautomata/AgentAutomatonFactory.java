package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentTransitionModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import lombok.NonNull;

/**
 * Created by Christoph Graupner on 2018-05-05.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface AgentAutomatonFactory {
  <T extends AgentAutomaton> T createAgentAutomaton(@NonNull FlowService originService, final boolean modifyable);

  <T extends AgentAutomaton> T createAgentAutomaton(@NonNull String agentName, final boolean modifyable);

  AgentState createAgentState(@NonNull FlowState flowState);

  AgentTransitionModifiable createAgentTransition(
      @NonNull AgentState sourceState, @NonNull AgentState targetState, final String guard,
      final String name
  );

  AgentVariableDefinition createAgentVariableDefinition(@NonNull final String name);
}
