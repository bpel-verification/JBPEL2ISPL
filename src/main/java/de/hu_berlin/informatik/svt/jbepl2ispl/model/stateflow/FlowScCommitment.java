package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.Identifiable;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface FlowScCommitment extends Identifiable {
  int getCommitmentId();

  FlowService getCreditor();

  FlowService getDebtor();

  int getOriginalElementId();

  FlowState getTargetState();
}
