package de.hu_berlin.informatik.svt.jbepl2ispl;

/**
 * Created by Christoph Graupner on 2018-04-24.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public interface Consts {
  //development+debug information
  String NEEDS_IMPLEMENTATION = "NEEDS IMPLEMENTATION";

  //agent automaton model
  String AGENT_ENV_NAME                        = "Environment";
  String AGENT_STATE_FINAL_NAME                = "__FINAL__";
  String AGENT_STATE_INITIAL_NAME              = "__INITIAL__";
  String STATE_NAME_PATTERN                    = "S%03d#%s";
  String VAR_NAME_LITERAL                      = "___LITERAL__";
  String VAR_NAME_PREFIX_COMM_CHANNEL          = "commChannel";
  String VAR_VALUE_ENUM_PREFIX_MSG             = "Msg";
  String VAR_VALUE_REFERENCE_OTHER_VAR_PATTERN = "${{%s}}";

  //BPEL
  String IF_BRANCH_HELPER_GUARD              = "TRUE";
  String IF_ELSE_BRANCH_GUARD                = "ELSE";
  String INITIAL_STATE_NAME_IDENTIFIER_REGEX = ".*(initial(i[sz](ation|ing))?).*";

  //ISPL
  String FILE_EXTENSION_ISPL             = ".ispl";
  String ISPL_ACTION_LOCAL_ASSIGN_FORMAT = "asgnLocal__%s";
  String ISPL_ACTION_NAME_DELEMITTER     = "__";
  String ISPL_ACTION_NAME_NONE           = "def__none";
  String ISPL_ACTION_PREFIX_RECEIVE      = "receive";
  String ISPL_ACTION_PREFIX_SEND         = "send";
  String ISPL_ACTION_RECEIVE_FORMAT      = "receive__%s_%s";
  String ISPL_ACTION_SEND_FORMAT         = "send__%s_%s";
  String ISPL_STATE_NAME_DELEMITTER      = "__";
  String ISPL_VAR_ENUM_NONE              = "def__none";
  String ISPL_VAR_NAME_SEPARATOR         = "__";
  String ISPL_VAR_NAME_STATEVAR          = "agentState";
  String ISPL_VAR_NAME_STATEVAR_ENV      = "envAgentState";
  String ISPL_VAR_PREFIX_BUFFER          = "b";

  //neo4j node property names

  String ISPL_VAR_PREFIX_TRACE_LABEL       = "traceLabel";
  String PROPERTY_AGENT_STATE_CONDITION    = "agentStateCondition";
  String PROPERTY_IF_CONDITION             = "ifCondition";
  String PROPERTY_MESSAGING_TYPE           = "messagingType";
  String PROPERTY_NAME_BPEL_VAR_DEFINITION = "bpelVarDefinition";
  String PROPERTY_NAME_LOOP_CLOSE          = "loopClose";
  String PROPERTY_SC_COMMITMENT_ID         = "sc_commitmentID";
  String PROPERTY_SC_CREDITOR              = "sc_creditor";
  String PROPERTY_SC_DEBTOR                = "sc_debtor";
  String PROPERTY_SC_FULFILLMENT_ID        = "sc_fulfillmentID";
  String PROPERTY_SC_RECOVERY_ID           = "sc_recoveryID";
  String PROPERTY_SC_TRACE_BEHAVIOUR_TYPE  = "sc_traceBehaviour";
  String PROPERTY_SC_TRACE_SERVICE_NAME    = "sc_traceServiceName";
  /**
   * property that if is set marks a variable, that have &gt;receive&lt;d the input value of the BPEL process
   */
  String PROPERTY_VAR_IS_EXTERNAL_BPEL_CLIENT_INPUT  = "clientCallerVar";
  String PROPERTY_VAR_IS_EXTERNAL_BPEL_CLIENT_OUTPUT = "clientCallerVarOutput";

  // Propositions for Evaluation and Formulae
  String PROPOSITION_DESIRED_STATE_SUFFIX    = "DesiredState";
  String PROPOSITION_UN_DESIRED_STATE_SUFFIX = "UnDesiredState";
  String PROPOSITION_NULL_MSG                = "NullMsg";
  String VAR_NAME_SC_COMMITMENT_ID           = "commitmentID";
  String VAR_NAME_SC_FULFILLMENT_ID          = "fulfillmentID";
  String PROPERTY_SC_FULFILLMENT_DERIVED     = "fulfillmentDerived";
}
