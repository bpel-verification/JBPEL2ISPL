package de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j;

import de.hu_berlin.informatik.svt.jbepl2ispl.model.QName;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.*;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j.repository.Neo4jAgentAutomatonRepository;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j.repository.Neo4jAgentStateRepository;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j.repository.Neo4jAgentTransitionRepository;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.impl.neo4j.repository.Neo4jAgentVariableDefinitionRepository;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentAutomatonModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.agentautomata.modify.AgentTransitionModifiable;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.B2IExpression;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.expression.formulae.B2IFormula;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowId;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowState;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.Jb2iUtils;
import lombok.Getter;
import lombok.NonNull;
import org.neo4j.ogm.annotation.Transient;
import org.neo4j.ogm.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Christoph Graupner on 2018-05-29.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@SuppressWarnings("unchecked")
@Service
@Primary
@Transient
public class Neo4jAutomationModel implements AutomatonModel {
  private final Neo4jAgentAutomatonRepository          automatonRepository;
  private final AgentAutomatonFactory                  factory;
  private final Logger                                 logger       = LoggerFactory.getLogger(getClass());
  private final Session                                neo4jSession;
  private final Neo4jAgentStateRepository              stateRepository;
  private final Neo4jAgentTransitionRepository         transitionRepository;
  private final Neo4jAgentVariableDefinitionRepository variableDefinitionRepository;
  @Getter
  private       List<B2IFormula>                       formulas     = new LinkedList<>();
  private       Map<String, B2IExpression>             propositions = new HashMap<>();

  @Autowired
  public Neo4jAutomationModel(
      @NonNull final AgentAutomatonFactory factory,
      @NonNull final Neo4jAgentAutomatonRepository automatonRepository,
      @NonNull final Neo4jAgentStateRepository stateRepository,
      @NonNull final Neo4jAgentTransitionRepository transitionRepository,
      @NonNull final Neo4jAgentVariableDefinitionRepository variableDefinitionRepository,
      @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
      @NonNull final Session neo4jSession
  ) {
    this.factory = factory;
    this.automatonRepository = automatonRepository;
    this.stateRepository = stateRepository;
    this.transitionRepository = transitionRepository;
    this.variableDefinitionRepository = variableDefinitionRepository;
    this.neo4jSession = neo4jSession;
  }

  @Override
  public void addAgentAutomaton(
      @NonNull final AgentAutomaton agentAutomaton
  ) {
    save(agentAutomaton);
  }

  @Override
  public void addFormula(@NonNull final B2IFormula formula) {
    formulas.add(formula);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public boolean areNeighborStates(
      @NonNull final FlowState state1, @NonNull final FlowState state2
  ) {
    return stateRepository.areTheyNeighborStates(state1.getId(), state2.getId());
  }

  @Override
  public AgentAutomatonFactory factory() {
    return factory;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends AgentState> Collection<T> findAllStatesWithActionForAgent(
      @NonNull final String actionName, @NonNull final AgentAutomaton agentAutomaton
  ) {
    return (Collection<T>) stateRepository.findAllStatesByActionForAgent(agentAutomaton.getName(), actionName);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public Set<String> findAllVariableValuesFor(final QName varName) {
    final Set<String> ret = new HashSet<>();
    stateRepository.findAllValuesForVariable("var#" + varName.getFullName()).forEach(
        s -> ret.add(Jb2iUtils.unwrapFromBase64AsString(s))
    );
    return ret;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public AgentState findStateForAgentByOriginFlowId(
      @NonNull final AgentAutomaton agentAutomaton,
      @NonNull final FlowId flowStateId
  ) {
    return stateRepository.findExactlyOneByAgentAutomatonAndElementIdAndAlternativeCount(
        agentAutomaton.getName(),
        flowStateId.getElementId(),
        flowStateId.getAlternateId()
    );
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public AgentAutomaton getAgentAutomaton(@NonNull final String agentName) {
    final Neo4jAgentAutomaton automaton = refresh(automatonRepository.findFirstByName(agentName), 5);
    if (automaton != null) {
      automaton.setModel(this);
    }
    return automaton;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends AgentAutomaton> Collection<T> getAllAgentAutomatons() {
    final Collection<Neo4jAgentAutomaton> automata = neo4jSession.loadAll(Neo4jAgentAutomaton.class, 3);
    for (Neo4jAgentAutomaton neo4jAgentAutomaton: automata) {
      neo4jAgentAutomaton.setModel(this);
    }
    return (Collection<T>) automata;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends AgentTransition> Collection<T> getAllInboundTransitions(
      @NonNull final AgentState state
  ) {

    return (Collection<T>) transitionRepository.findAllByTargetState(state.getId());
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends AgentTransition> Collection<T> getAllOutboundTransitions(
      @NonNull final AgentState state
  ) {
    return (Collection<T>) transitionRepository.findAllBySourceState(state.getId());
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends AgentState> Collection<T> getAllStatesForAutomaton(
      @NonNull final AgentAutomaton agentAutomaton
  ) {
    return (Collection<T>) refreshAll(stateRepository.findAllForAgent(agentAutomaton.getName()));
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends AgentTransition> Collection<T> getAllTransitions() {
    return (Collection<T>) neo4jSession.loadAll(Neo4jAgentTransition.class, 3);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends AgentTransition> Collection<T> getAllTransitionsForAutomaton(
      @NonNull final String agentAutomatonName
  ) {
    Collection<Neo4jAgentTransition> ret = new HashSet<>();
    ret.addAll(transitionRepository.findAllForAutomaton(agentAutomatonName));
    return (Collection<T>) ret;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public AgentState getLastStateForAgent(@NonNull final AgentAutomaton neo4jAgentAutomaton) {
    return stateRepository.findLastStateForAgent(neo4jAgentAutomaton.getName());
  }

  @Override
  public B2IExpression getPropositionDefinition(@NonNull final String name) {
    if (propositions.containsKey(name)) {
      return propositions.get(name);
    }
    return null;
  }

  @Override
  public Set<String> getPropositionNames() {
    return propositions.keySet();
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public AgentVariableDefinition refresh(
      final AgentVariableDefinition variableDefinition, final int depth
  ) {
    if (variableDefinition == null) {
      return null;
    }
    neo4jSession.clear();
    return neo4jSession.load(Neo4jVariableDefinition.class, variableDefinition.getId(), depth);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends AgentAutomaton> T refresh(@NonNull final T agent, final int depth) {
    neo4jSession.clear();
    final Neo4jAgentAutomaton load = neo4jSession.load(Neo4jAgentAutomaton.class, agent.getId(), depth);
    load.setModel(this);
    return (T) load;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public AgentState refresh(@NonNull final AgentState agent, final int depth) {
    neo4jSession.clear();
    return neo4jSession.load(Neo4jAgentState.class, agent.getId(), depth);
  }

  @Transactional(transactionManager = "neo4jTransactionManager", readOnly = true)
  public <T extends AgentState> Collection<T> refreshAll(@NonNull final Collection<T> states) {
    return (Collection<T>) neo4jSession.loadAll(
        Neo4jAgentState.class,
        states.stream().map(AgentState::getId).collect(Collectors.toList()),
        3
    );
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public AgentState save(@NonNull final AgentState state) {
    return stateRepository.save((Neo4jAgentState) state, 1);
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public AgentAutomatonModifiable save(@NonNull final AgentAutomaton automaton) {
    final Neo4jAgentAutomaton neo4jAgentAutomaton = automatonRepository.save((Neo4jAgentAutomaton) automaton, 1);
    neo4jAgentAutomaton.setModel(this);
    return neo4jAgentAutomaton;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public AgentVariableDefinition save(
      @NonNull final AgentVariableDefinition variableDefinition
  ) {
    neo4jSession.clear();
    final Neo4jVariableDefinition ret = variableDefinitionRepository.save((Neo4jVariableDefinition) variableDefinition);
    return ret;
  }

  @Override
  @Transactional(transactionManager = "neo4jTransactionManager")
  public AgentTransitionModifiable save(@NonNull final AgentTransition agentTransition) {
    return transitionRepository.save((Neo4jAgentTransition) agentTransition, 1);
  }

  @Override
  public void setProposition(@NonNull final String name, @NonNull final B2IExpression definition) {
    propositions.put(name, definition);
  }
}
