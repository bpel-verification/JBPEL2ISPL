package de.hu_berlin.informatik.svt.jbepl2ispl.model;

import java.util.EnumSet;

/**
 * Created by Christoph Graupner on 2018-06-22.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public enum Activity {
  FLOW, IF, IFCASE_ELSE, IFCASE_ELSEIF, WHILE, INVOKE, PICK, PICK_ONALARM, PICK_ONMSG, ASSIGN, SEQUENCE, FLOW_BRANCH,
  RECEIVE,
  SC_FULFILLMENT, SC_COMMITMENT, SC_TRACE_LABEL, SC_RECOVERY,
  __INITIAL_STATE, EMPTY, REPLY, NONE;

  public static final EnumSet<Activity> VariableValueSourceActivity = EnumSet.of(INVOKE, RECEIVE);
  public static final EnumSet<Activity> VariableValueTargetActivity = EnumSet.of(INVOKE, REPLY);
}
