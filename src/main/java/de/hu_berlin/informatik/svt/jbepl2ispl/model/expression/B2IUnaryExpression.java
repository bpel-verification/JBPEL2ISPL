package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

/**
 * @author Christoph Graupner <graupner@informatik.hu-berlin.de>
 */
public interface B2IUnaryExpression extends B2IExpression {
  B2IExpression getTerm();
}
