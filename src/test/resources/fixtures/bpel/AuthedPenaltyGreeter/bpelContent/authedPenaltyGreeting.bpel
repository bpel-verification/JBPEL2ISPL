<bpel:process
    name="authedPenaltyGreeting"
    targetNamespace="http://sample.bpel.org/bpel/authedPenaltyGreeting"
    suppressJoinFailure="yes"
    xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:tns="http://sample.bpel.org/bpel/authedPenaltyGreeting"
    xmlns:authsrv="http://sample.bpel.org/wsdl/authService.wsdl"
    xmlns:greet="http://ode.example/bpel/greeter.wsdl"
    xmlns:ctlc="http://sample.bpel.org/bpel/ctlc"
    xmlns:accSrv="http://sample.bpel.org/wsdl/accountService/"
    xmlns:adtLog="http://www.example.org/auditLogService/"
>

  <!-- Import the client WSDL -->
  <bpel:import
      location="authedPenaltyGreetingArtifacts.wsdl"
      namespace="http://sample.bpel.org/bpel/authedPenaltyGreeting"
      importType="http://schemas.xmlsoap.org/wsdl/"
  />
  <bpel:import
      location="authService.wsdl"
      namespace="http://sample.bpel.org/wsdl/authService.wsdl"
      importType="http://schemas.xmlsoap.org/wsdl/"
  />
  <bpel:import
      location="accountService.wsdl"
      namespace="http://sample.bpel.org/wsdl/accountService/"
      importType="http://schemas.xmlsoap.org/wsdl/"
  />
  <bpel:import
      location="auditLogService.wsdl"
      namespace="http://www.example.org/auditLogService/"
      importType="http://schemas.xmlsoap.org/wsdl/"
  />
  <bpel:import
      location="Greeter.wsdl"
      namespace="http://ode.example/bpel/greeter.wsdl"
      importType="http://schemas.xmlsoap.org/wsdl/"
  />
  <!-- ================================================================= -->
  <!-- PARTNERLINKS                                                      -->
  <!-- List of services participating in this BPEL process               -->
  <!-- ================================================================= -->
  <bpel:partnerLinks>
    <!-- The 'client' role represents the requester of this service. -->
    <bpel:partnerLink
        name="client"
        partnerLinkType="tns:authedGreeting"
        myRole="authedGreetingProvider"
    />
    <bpel:partnerLink
        name="authService"
        partnerLinkType="tns:AuthServicePartnerLinkType"
        myRole="requester"
        partnerRole="authority"
    />
    <bpel:partnerLink
        name="accountService"
        partnerLinkType="tns:AccountServicePartnerLinkType"
        myRole="requester"
        partnerRole="accounter"
    />
    <bpel:partnerLink
        name="auditLogService"
        partnerLinkType="tns:AuditLogServicePartnerLinkType"
        myRole="requester"
        partnerRole="logger"
    />
    <bpel:partnerLink
        name="greetService"
        partnerLinkType="greet:GreetPartnerLinkType"
        myRole="me"
        partnerRole="you"
    />
  </bpel:partnerLinks>


  <bpel:extensions>
    <bpel:extension namespace="http://sample.bpel.org/bpel/ctlc" mustUnderstand="no"/>
  </bpel:extensions>
  <!-- ================================================================= -->
  <!-- VARIABLES                                                         -->
  <!-- List of messages and XML documents used within this BPEL process  -->
  <!-- ================================================================= -->
  <bpel:variables>
    <!-- Reference to the message passed as input during initiation -->
    <bpel:variable name="input" messageType="tns:authedGreetingRequestMessage"/>

    <!--
      Reference to the message that will be returned to the requester
      -->
    <bpel:variable name="output" messageType="tns:authedGreetingResponseMessage">
      <bpel:from>
        <bpel:literal xml:space="preserve"><tns:authedGreetingResponse xmlns:tns="http://sample.bpel.org/bpel/authedPenaltyGreeting" >
  <tns:greeting>tns:greeting</tns:greeting>
</tns:authedGreetingResponse>
</bpel:literal>
      </bpel:from>
    </bpel:variable>

    <bpel:variable name="authRequest" messageType="authsrv:AuthenticateRequestMessage">
      <bpel:from>
        <bpel:literal xml:space="preserve"><authsrv:AuthenticateRequest xmlns:authsrv="http://sample.bpel.org/wsdl/authService.wsdl" >
  <username>username</username>
  <password>password</password>
</authsrv:AuthenticateRequest>
</bpel:literal>
      </bpel:from>
    </bpel:variable>
    <bpel:variable name="authResponse" messageType="authsrv:AuthenticateResponseMessage"/>
    <bpel:variable name="authSessionResult" type="xsd:int"/>
    <bpel:variable name="authPenaltyResult" type="xsd:int"/>
    <bpel:variable name="authSuccessfulResult" type="xsd:boolean"/>

    <bpel:variable name="greetRequest" messageType="greet:GreetRequestMessage">
      <bpel:from>
        <bpel:literal xml:space="preserve"><tns:GreetRequest xmlns:tns="http://ode.example/bpel/greeter.wsdl" >
  <name>name</name>
  <authorized>true</authorized>
</tns:GreetRequest>
</bpel:literal>
      </bpel:from>
    </bpel:variable>
    <bpel:variable name="greetResponse" messageType="greet:GreetResponseMessage"/>
    <bpel:variable name="accountRequest" element="accSrv:GetFullName">
      <bpel:from>
        <bpel:literal xml:space="preserve"><accSrv:GetFullName xmlns:accSrv="http://sample.bpel.org/wsdl/accountService/" >
  <username>username</username>
</accSrv:GetFullName>
</bpel:literal>
      </bpel:from>
    </bpel:variable>
    <bpel:variable name="accountResponse" element="accSrv:GetFullNameResponse">
    </bpel:variable>
    <bpel:variable name="auditLogRequest" element="adtLog:logAuthRequest">
      <bpel:from>
        <bpel:literal xml:space="preserve"><adtLog:logAuthRequest xmlns:adtLog="http://www.example.org/auditLogService/" >
  <entry>entry</entry>
  <isAuthorized>true</isAuthorized>
</adtLog:logAuthRequest>
</bpel:literal>
      </bpel:from>
    </bpel:variable>
    <bpel:variable name="auditLogResponse" element="adtLog:logAuthResponse"/>
  </bpel:variables>

  <!-- ================================================================= -->
  <!-- ORCHESTRATION LOGIC                                               -->
  <!-- Set of activities coordinating the flow of messages across the    -->
  <!-- services integrated within this business process                  -->
  <!-- ================================================================= -->
  <bpel:sequence name="main">

    <!-- Receive input from requester.
         Note: This maps to operation defined in authedGreeting.wsdl
         -->
    <bpel:receive
        name="receiveInputFromClient"
        partnerLink="client"
        portType="tns:authedGreeting"
        operation="process"
        variable="input"
        createInstance="yes"
    />
    <bpel:assign name="initialize_variables">
      <bpel:copy>
        <bpel:from>
          <bpel:literal>
            <authsrv:AuthenticateRequest>
              <authsrv:username>To</authsrv:username>
              <authsrv:password>Text</authsrv:password>
            </authsrv:AuthenticateRequest>
          </bpel:literal>
        </bpel:from>
        <bpel:to variable="authRequest" part="parameter"/>
      </bpel:copy>
      <bpel:copy>
        <bpel:from>
          <bpel:literal>
            <greet:GreetRequest>
              <greet:name/>
              <greet:authorized>false</greet:authorized>
            </greet:GreetRequest>
          </bpel:literal>
        </bpel:from>
        <bpel:to variable="greetRequest" part="parameter"/>
      </bpel:copy>
    </bpel:assign>

    <!-- Auth -->
    <bpel:assign name="PrepareForAuth">
      <bpel:copy>
        <bpel:from variable="input" part="parameter">
          <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">tns:username</bpel:query>
        </bpel:from>
        <bpel:to variable="authRequest" part="parameter">
          <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">username</bpel:query>
        </bpel:to>
      </bpel:copy>
      <bpel:copy>
        <bpel:from variable="input" part="parameter">
          <bpel:query>tns:password</bpel:query>
        </bpel:from>
        <bpel:to variable="authRequest" part="parameter">
          <bpel:query>password</bpel:query>
        </bpel:to>
      </bpel:copy>
    </bpel:assign>

    <bpel:extensionActivity>
      <ctlc:commitment-label name="CommitmentAuthServiceAuditLog"
                             commitmentID="1"
                             debtor="authService"
                             creditor="auditLogService"
      />
    </bpel:extensionActivity>
    <bpel:invoke
        name="callAuthServiceForAuthenticateUser"
        partnerLink="authService"
        operation="authenticate"
        inputVariable="authRequest"
        outputVariable="authResponse"
    >
    </bpel:invoke>
    <bpel:assign name="PrepareAuthResultCheck">
      <bpel:copy>
        <bpel:from variable="authResponse" part="return">
          <bpel:query>authorized</bpel:query>
        </bpel:from>
        <bpel:to variable="authSuccessfulResult"/>
      </bpel:copy>
      <bpel:copy>
        <bpel:from variable="authResponse" part="return">
          <bpel:query>sessionId</bpel:query>
        </bpel:from>
        <bpel:to variable="authSessionResult"/>
      </bpel:copy>
    </bpel:assign>

    <bpel:if name="IfReturnValid">
      <bpel:condition><![CDATA[$authSessionResult < 0]]></bpel:condition>
      <bpel:sequence>
        <bpel:extensionActivity>
          <ctlc:trace-label name="Trace1"
                            serviceName="authService"
                            behaviourType="undesired"
          />
        </bpel:extensionActivity>
        <bpel:reply
            name="replyToClient"
            partnerLink="client"
            portType="tns:authedGreeting"
            operation="tryAgain"
        />
      </bpel:sequence>
      <bpel:else>
        <bpel:sequence>
          <bpel:if name="reactDifferentForAuthedAndNonAuthedUser">
            <!-- got session-id > 0 ? -->
            <bpel:condition><![CDATA[      $authSuccessfulResult = true()      ]]></bpel:condition>
            <bpel:sequence>
              <bpel:assign validate="no" name="PrepareAuditLogCommitAuthorized">
                <bpel:copy>
                  <bpel:from part="return" variable="authResponse">
                    <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                      <![CDATA[sessionId]]>
                    </bpel:query>
                  </bpel:from>
                  <bpel:to variable="auditLogRequest">
                    <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                      <![CDATA[entry]]></bpel:query>
                  </bpel:to>
                </bpel:copy>
              </bpel:assign>
              <bpel:extensionActivity>
                <ctlc:fulfillment-label name="FulfilmentAuthServiceAuditLog1" commitmentID="1" fulfillmentID="1">
                </ctlc:fulfillment-label>

              </bpel:extensionActivity>
              <bpel:extensionActivity>
                <ctlc:trace-label name="Trace2"
                                  serviceName="authService"
                                  behaviourType="desired"
                />
              </bpel:extensionActivity>
              <bpel:invoke name="LogAuthorizedEvent"
                           partnerLink="auditLogService"
                           operation="logAuth"
                           portType="adtLog:auditLogService"
                           inputVariable="auditLogRequest"
                           outputVariable="auditLogResponse"/>
              <bpel:assign validate="no" name="prepareGetFullname">
                <bpel:copy>
                  <bpel:from>
                    <bpel:literal><![CDATA[penaltyGreeting]]></bpel:literal>
                  </bpel:from>
                  <bpel:to variable="accountRequest">
                    <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                      <![CDATA[requester]]>
                    </bpel:query>
                  </bpel:to>
                </bpel:copy>
                <bpel:copy>
                  <bpel:from>
                    <bpel:literal><![CDATA[penaltyGreeting2]]></bpel:literal>
                  </bpel:from>
                  <bpel:to variable="accountRequest">
                    <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                      <![CDATA[requester]]>
                    </bpel:query>
                  </bpel:to>
                </bpel:copy>
                <bpel:copy>
                  <bpel:from part="parameter" variable="input">
                    <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                      <![CDATA[tns:username]]></bpel:query>
                  </bpel:from>
                  <bpel:to variable="accountRequest">
                    <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                      <![CDATA[username]]>
                    </bpel:query>
                  </bpel:to>
                </bpel:copy>
              </bpel:assign>
              <bpel:invoke name="getFullname"
                           partnerLink="accountService"
                           operation="GetFullName"
                           portType="accSrv:accountService"
                           inputVariable="accountRequest"
                           outputVariable="accountResponse"/>
              <bpel:assign name="prepareGreetRequestForAuthorized">
                <bpel:copy>
                  <bpel:from>$input.parameter/tns:username</bpel:from>
                  <!--<bpel:from variable="input" part="parameter">-->
                  <!--<bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">tns:username</bpel:query>-->
                  <!--</bpel:from>-->
                  <!--<bpel:to>$greetRequest.parameter/name</bpel:to>-->
                  <bpel:to variable="greetRequest" part="parameter">
                    <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">name</bpel:query>
                  </bpel:to>
                </bpel:copy>
                <bpel:copy>
                  <bpel:from variable="authSuccessfulResult">

                  </bpel:from>
                  <bpel:to variable="greetRequest" part="parameter">
                    <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">authorized</bpel:query>
                  </bpel:to>
                </bpel:copy>
              </bpel:assign>
              <bpel:invoke name="getGreetingTextFromGreetServiceForUser"
                           partnerLink="greetService"
                           operation="greet"
                           portType="greet:GreetPortType"
                           inputVariable="greetRequest"
                           outputVariable="greetResponse">
              </bpel:invoke>
            </bpel:sequence>
            <bpel:else>
              <bpel:sequence>
                <bpel:assign validate="no" name="PrepareAuditLogCommitUnauthorized">
                  <bpel:copy>
                    <bpel:from part="return" variable="authResponse">
                      <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                        <![CDATA[authorized]]>
                      </bpel:query>
                    </bpel:from>
                    <bpel:to variable="auditLogRequest">
                      <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                        <![CDATA[isAuthorized]]></bpel:query>
                    </bpel:to>
                  </bpel:copy>
                </bpel:assign>
                <!--<bpel:extensionActivity>-->
                <!--<ctlc:fulfillment-label name="FulfilmentAuthServiceAuditLog2" commitmentID="1" fulfillmentID="2">-->
                <!--</ctlc:fulfillment-label>-->

                <!--</bpel:extensionActivity>-->

                <bpel:invoke name="LogNonAuthorizedEvent"
                             partnerLink="auditLogService"
                             operation="logAuth"
                             portType="adtLog:auditLogService"
                             inputVariable="auditLogRequest"
                             outputVariable="auditLogResponse"/>
                <bpel:assign validate="yes" name="setPenaltyCounterStart">
                  <bpel:copy>
                    <bpel:from>
                      <bpel:literal>3</bpel:literal>
                    </bpel:from>
                    <bpel:to variable="authPenaltyResult">
                    </bpel:to>
                  </bpel:copy>
                </bpel:assign>
                <bpel:while name="PenaltyWait">
                  <bpel:condition expressionLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                    <![CDATA[$authPenaltyResult > 0]]>
                  </bpel:condition>
                  <bpel:sequence>
                    <bpel:wait name="WaitASecond">
                      <bpel:for><![CDATA['PT.1S']]></bpel:for>
                    </bpel:wait>
                    <bpel:assign validate="no" name="DecreaseCountDown">
                      <bpel:copy>
                        <bpel:from>$authPenaltyResult - 1</bpel:from>
                        <bpel:to variable="authPenaltyResult"/>
                      </bpel:copy>
                    </bpel:assign>
                  </bpel:sequence>

                </bpel:while>
                <bpel:empty name="FIX_ME-Add_Business_Logic_Here"/>
                <bpel:assign name="prepareGreetRequestForUnauthorized">
                  <bpel:copy>
                    <!--<bpel:from>$input.parameter/tns:username</bpel:from>-->
                    <bpel:from variable="input">
                      <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">tns:username
                      </bpel:query>
                    </bpel:from>
                    <bpel:to variable="greetRequest" part="parameter">
                      <bpel:query>name</bpel:query>
                    </bpel:to>
                  </bpel:copy>
                  <bpel:copy>
                    <bpel:from>
                      <bpel:literal>true</bpel:literal>
                    </bpel:from>
                    <bpel:to variable="greetRequest" part="parameter">
                      <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">authorized</bpel:query>
                    </bpel:to>
                  </bpel:copy>
                </bpel:assign>
                <bpel:invoke name="getGreetingTextFromGreetServiceForUnauthorized"
                             partnerLink="greetService"
                             operation="greetUnauthorized"
                             inputVariable="greetRequest"
                             outputVariable="greetResponse">
                </bpel:invoke>

              </bpel:sequence>
            </bpel:else>
          </bpel:if>

          <bpel:assign name="GreeterOutputToProcessOutput">
            <bpel:copy>
              <bpel:from variable="greetResponse" part="return">

              </bpel:from>
              <bpel:to variable="output" part="return">
                <bpel:query>tns:greeting</bpel:query>
              </bpel:to>
            </bpel:copy>
          </bpel:assign>
          <!-- Generate reply to synchronous request -->
          <bpel:reply
              name="replyToClient"
              partnerLink="client"
              portType="tns:authedGreeting"
              operation="process"
              variable="output"
          />
        </bpel:sequence>
      </bpel:else>
    </bpel:if>
  </bpel:sequence>
</bpel:process>
