package de.hu_berlin.informatik.svt.jbepl2ispl;

import de.hu_berlin.informatik.svt.jbepl2ispl.utils.Jb2iUtils;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

import java.util.List;
import java.util.Objects;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
public class IsEqualIspl extends BaseMatcher<List<String>> {
  private final List<String> expected;
  private final String       message;

  IsEqualIspl(List<String> operand) {
    expected = Jb2iUtils.normalizeISPL(operand);
    message = null;
  }

  IsEqualIspl(final List<String> expected, final String message) {
    this.expected = Jb2iUtils.normalizeISPL(expected);
    this.message = message;
  }

  @Factory
  public static Matcher<List<String>> equalsIspl(List<String> operand) {
    return new IsEqualIspl(operand);
  }

  @Override
  public void describeMismatch(final Object item, final Description description) {
    final List<String> l = Jb2iUtils.normalizeISPL((List<String>) item);

    for (int i = 0; i < Math.max(l.size(), expected.size()); i++) {
      description.appendText("\n" + StringUtils.leftPad(String.valueOf(i), 5) + ": ");
      final String s           = i < l.size() ? l.get(i) : "<null>";
      final String expectedStr = i < expected.size() ? expected.get(i) : "<null>";
      if (s.equals(expectedStr)) {
        description.appendText("  OK: ");
        description.appendValue(s);
      } else {
        description.appendText("DIFF:\n     expected: ");
        description.appendValue(expectedStr);
        description.appendText("\n     actual  : ");
        description.appendValue(s);
      }
    }
  }

  @Override
  public void describeTo(final Description description) {
    if (message != null) {
      description.appendText(message + " ");
    } else {
    }
    description.appendValueList("[", ", ", "]", expected);
  }

  @Override
  public boolean matches(final Object item) {
    return Objects.equals(expected, Jb2iUtils.normalizeISPL((List<String>) item));
  }
}
