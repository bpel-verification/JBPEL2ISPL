package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import javax.xml.xpath.XPathExpressionException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by Christoph Graupner on 2018-06-12.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
class XPathConditionStatementTest {

  private static Stream<Arguments> asConditionDataProvider() {
    return Stream.of(
        Arguments.of(
            "$x + 1",
            B2IExpression.arithmetic(
                B2IExpression.Type.NUM_ADD,
                B2IExpression.variableReference("x"),
                B2IExpression.term(1)
            )
        ),
        Arguments.of(
            "$x - 1",
            B2IExpression.arithmetic(
                B2IExpression.Type.NUM_SUB,
                B2IExpression.variableReference("x"),
                B2IExpression.term(1)
            )

        ),
        Arguments.of(
            "$x * 1",
            B2IExpression.arithmetic(
                B2IExpression.Type.NUM_MUL,
                B2IExpression.variableReference("x"),
                B2IExpression.term(1)
            )
        ),
        Arguments.of(
            "$x / 1",
            B2IExpression.arithmetic(
                B2IExpression.Type.NUM_DIV,
                B2IExpression.variableReference("x"),
                B2IExpression.term(1)
            )
        ),
        Arguments.of(
            "$x + 1 + $y",
            B2IExpression.arithmetic(
                B2IExpression.Type.NUM_ADD,
                B2IExpression.arithmetic(
                    B2IExpression.Type.NUM_ADD,
                    B2IExpression.variableReference("x"),
                    B2IExpression.term(1)
                ),
                B2IExpression.variableReference("y")
            )
        ),
        Arguments.of(
            "$x = 1",
            B2IExpression.equal(B2IExpression.variableReference("x"), B2IExpression.term(1))
        ),
        Arguments.of(
            "$x > $y",
            B2IExpression.greaterThan(B2IExpression.variableReference("x"), B2IExpression.variableReference("y"))
        ),
        Arguments.of(
            "$y < $z",
            B2IExpression.lessThan(B2IExpression.variableReference("y"), B2IExpression.variableReference("z"))
        ),
        Arguments.of(
            "$x >= $y",
            B2IExpression.greaterEqual(B2IExpression.variableReference("x"), B2IExpression.variableReference("y"))
        ),
        Arguments.of(
            "$x <= $y",
            B2IExpression.lessEqual(B2IExpression.variableReference("x"), B2IExpression.variableReference("y"))
        ),

        //TODO next steps
//        Arguments.of(
//            "$x = 1 and 10 < $x",
//            B2IExpression.and(
//                B2IExpression.equal(B2IExpression.variableReference("x"), B2IExpression.term(1)),
//                B2IExpression.lessThan(B2IExpression.term(10), B2IExpression.variableReference("x"))
//            )
//        ),
//        Arguments.of(
//            "$x = 1 or 10 < $x",
//            B2IExpression.and(
//                B2IExpression.equal(B2IExpression.variableReference("x"), B2IExpression.term(1)),
//                B2IExpression.lessThan(B2IExpression.term(10), B2IExpression.variableReference("x"))
//            )
//        ),
        Arguments.of("1 > 0", B2IExpression.term(true)),
        Arguments.of("1 = 1", B2IExpression.term(true)),
        Arguments.of("1 < 0", B2IExpression.term(false)),
        Arguments.of("1 = 0", B2IExpression.term(false))
    );
  }

  @MethodSource
  private static Stream<Arguments> conditionProvider() {
    return Stream.of(
        Arguments.of("1 > 0", Arrays.asList()),
        Arguments.of("$authSessionResult > 0", Arrays.asList("authSessionResult")),
        Arguments.of("$authSessionResult > $requestNo", Arrays.asList("authSessionResult", "requestNo")),
        Arguments.of("number($authSessionResult)", Arrays.asList("authSessionResult")),
        Arguments.of("$input.parameter/tns:username", Arrays.asList("input")),
//        Arguments.of("self_defined($authSessionResult)", Arrays.asList("authSessionResult")), //no custom functions
        Arguments.of(
            "($authSessionResult > $requestNo) = ($bla < $bla2)",
            Arrays.asList("authSessionResult", "requestNo", "bla", "bla2")
        )
    );
  }

  @ParameterizedTest
  @ValueSource(
      strings = {"$authSessionResult > 0", "$x = 10", "$x = $y", "$x < $y", "$x and $y", "$x or $y" /*, "$x = true"*/}
  )
  void ableToHandle(String condition) throws XPathExpressionException {
    final XPathConditionStatement sut = new XPathConditionStatement(condition);
    assertThat(condition, sut.ableToHandle(), is(true));
  }

  @ParameterizedTest
  @ValueSource(strings = {"$authSessionResult + 0", "$x - 10", "$x / $y", "$x * $y"})
  void ableToHandleNot(String condition) throws XPathExpressionException {
    final XPathConditionStatement sut = new XPathConditionStatement(condition);
    assertThat(condition, sut.ableToHandle(), is(false));
  }

  @ParameterizedTest
  @MethodSource("asConditionDataProvider")
  void asCondition(String expression, B2IExpression expectedCondition) throws XPathExpressionException {
    final XPathConditionStatement sut = new XPathConditionStatement(expression);
    assertThat(sut.asCondition(), is(equalTo(expectedCondition)));
  }

  @ParameterizedTest
  @ValueSource(strings = {"$authSessionResult > 0"})
  void getConditionRaw(String expression) throws XPathExpressionException {
    final XPathConditionStatement sut = new XPathConditionStatement(expression);
    assertThat(sut.getConditionRaw(), is(equalTo(expression)));
  }

  @ParameterizedTest
  @MethodSource("conditionProvider")
  void getVariables(String expression, List<String> expectedVariables) throws XPathExpressionException {
    final XPathConditionStatement sut       = new XPathConditionStatement(expression);
    Set<String>                   variables = sut.getVariables();
    assertThat(variables, Matchers.containsInAnyOrder(expectedVariables.toArray()));
    assertThat(expectedVariables, Matchers.containsInAnyOrder(variables.toArray()));
  }
}
