package de.hu_berlin.informatik.svt.jbepl2ispl.bpel;

import de.hu_berlin.informatik.svt.jbepl2ispl.bpel.BpelObjectStack;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by Christoph Graupner on 2018-02-27.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */
public class BpelObjectStackTest {

  @Test
  public void incCounter() {

  }

  @Test
  public void isEmpty() {
    BpelObjectStack sut = new BpelObjectStack();
    assertThat(sut.isEmpty(), is(true));
  }

  @Test
  public void peek() {
    BpelObjectStack sut = new BpelObjectStack();
    assertThat(sut.peek(), is(nullValue()));
  }

  @Test
  public void pop() {
    BpelObjectStack sut = new BpelObjectStack();
    assertThat(sut.pop(), is(nullValue()));
  }

  @Test
  public void push() {
  }

  @Test
  public void testToString() {
  }
}
