package de.hu_berlin.informatik.svt.jbepl2ispl.systemtest;

import de.hu_berlin.informatik.svt.jbepl2ispl.TransformationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.test.autoconfigure.data.neo4j.DataNeo4jTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@ExtendWith(SpringExtension.class)
@DataNeo4jTest(
    excludeFilters = @ComponentScan.Filter(
        type = FilterType.ASSIGNABLE_TYPE, value = ApplicationRunner.class
    )
)
@ComponentScan(basePackageClasses = {TransformationService.class})
@ActiveProfiles("test")
public class PrintGreetingCardSystemTest extends SystemTestBase {

  @Test
  public void testRun() throws Exception {
    testIsplGeneration(
        "GreetingCardPrinter/bpelContent/PrintGreetingCard.bpel",
        "GreetingCardPrinter/PrintGreetingCard.ispl"
    );
  }
}
