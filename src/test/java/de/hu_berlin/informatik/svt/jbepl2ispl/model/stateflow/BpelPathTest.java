package de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by Christoph Graupner on 2018-03-07.
 *
 * @author Christoph Graupner <christoph.graupner@informatik.hu-berlin.de>
 */

class BpelPathTest {

  @Test
  public void init() {
    BpelPath sut = new BpelPath(new String[]{"", "", "", "process[10.0]"});
    assertThat(sut.toString(), is("/process[10.0]"));
    sut = new BpelPath(new String[]{"process[10.0]"});
    assertThat(sut.toString(), is("/process[10.0]"));
    sut = new BpelPath(new String[]{"process[10.0]", "", ""});
    assertThat(sut.toString(), is("/process[10.0]"));
    sut = new BpelPath("/process[10.0]/sequence[13.10]/while[2.20]/sequence[8.15]/if[1.80]");
    assertThat(sut.toString(), is("/process[10.0]/sequence[13.10]/while[2.20]/sequence[8.15]/if[1.80]"));
    sut = new BpelPath("/bpel:process[0:11]/bpel:sequence[11:69]/bpel:if[11:140]");
    assertThat(sut.toString(), is("/bpel:process[0:11]/bpel:sequence[11:69]/bpel:if[11:140]"));
  }

  @Test
  protected void equalHashCode() {
    BpelPath sut  = new BpelPath("/process[10.0]/sequence[13.10]/while[2.20]/sequence[8.15]/if[1.80]");
    BpelPath sut2 = new BpelPath("/process[10.0]/sequence[13.10]/while[2.20]/sequence[8.15]/if[1.80]");
    assertThat(sut, is(equalTo(sut2)));
    assertThat(sut2, is(equalTo(sut)));
    Assertions.assertNotSame(sut, sut2);

    sut = new BpelPath("/bpel:process[0:11]/bpel:sequence[11:69]/bpel:if[11:140]");
    sut2 = new BpelPath("/bpel:process[0:11]/bpel:sequence[11:69]/bpel:if[11:140]");
    assertThat(sut, is(equalTo(sut2)));
    assertThat(sut2, is(equalTo(sut)));
    Assertions.assertNotSame(sut, sut2);

    sut = new BpelPath("/bpel:process[0:11]/bpel:sequence[11:69]/bpel:if[11:140]");
    sut2 = new BpelPath("/bpel:process[0:11]/bpel:sequence[11:69]/if[11:140]");
    assertThat(sut, is(not(equalTo(sut2))));
    assertThat(sut2, is(not(equalTo(sut))));
    Assertions.assertNotSame(sut, sut2);
  }

  @Test
  void getParent() {
    BpelPath sut = new BpelPath(
        "/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[3.40]/else[1.50]/sequence[1.60]/if[1.80]");
    BpelPath parent = sut.getParent();
    assertThat(
        parent.toString(),
        is("/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[3.40]/else[1.50]/sequence[1.60]")
    );
    sut = parent.getParent();
    assertThat(sut.toString(), is("/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[3.40]/else[1.50]"));
    sut = new BpelPath("/process[10.0]");
    parent = sut.getParent();
    assertThat(parent.toString(), is("/"));

    sut = new BpelPath("/bpel:process[0:11]/bpel:sequence[11:69]/bpel:if[11:140]");
    assertThat(
        sut.getParent().toString(),
        is("/bpel:process[0:11]/bpel:sequence[11:69]")
    );
  }

  @Test
  void isDirectChildOf() {
    BpelPath parent =
        new BpelPath("/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[3.40]/else[1.50]/sequence[1.60]");
    BpelPath child = new BpelPath(
        "/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[3.40]/else[1.50]/sequence[1.60]/if[1.80]");
    assertThat("parent can not be child of itself", parent.isDirectChildOf(parent), is(false));
    assertThat("parent can not be child of its child", parent.isDirectChildOf(child), is(false));
    assertThat(child.isDirectChildOf(parent), is(true));
    assertThat(parent.isSubElement(child), is(true));

    child = new BpelPath(
        "/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[3.40]/else[1.50]/sequence[1.60]/if[1.70/else[2.55]");
    assertThat(child.isDirectChildOf(parent), is(false));
    assertThat(parent.isSubElement(child), is(true));
    assertThat("parent can not be child of its child", parent.isDirectChildOf(child), is(false));
  }

  @Test
  void isSubElement() {
    BpelPath sut = new BpelPath("/process[10.0]/sequence[13.10]/while[2.20]/sequence[8.15]/if[1.80]");
    assertThat(
        sut.toString(),
        sut.isSubElement("/process[10.0]/sequence[13.10]/while[2.20]/sequence[8.15]/if[1.80]"),
        is(false)
    );
    assertThat(
        sut.toString(),
        sut.isSubElement("/process[10.0]/sequence[13.10]/while[2.20]/sequence[8.15]/if[1.80]/else[2.85]"),
        is(true)
    );
    assertThat(
        sut.toString(),
        sut.isSubElement(
            "/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[3.40]/else[1.50]/sequence[1.60]/if[2.70]/sequence[4.75]/if[1.80]"),
        is(false)
    );

    sut = new BpelPath("/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[1.80]");
    BpelPath otherPath
        = new BpelPath(
        "/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[1.80]/else[1.50]/sequence[1.60]/if[1.80]");

    assertThat(sut.isSubElement(otherPath), is(true));
    assertThat(otherPath.isSubElement(sut), is(false));
    assertThat(
        sut.isSubElement(
            "/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[1.80]/else[1.50]/sequence[1.60]/if[1.80]/sequence[4.75]/if[1.80]"),
        is(true)
    );
    assertThat(
        otherPath.isSubElement(
            "/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[1.80]/else[1.50]/sequence[1.60]/if[1.80]/sequence[4.75]/if[1.80]"
        ),
        is(true)
    );
    sut = new BpelPath("/bpel:process[0:11]/bpel:sequence[11:69]/bpel:if[11:140]");
    assertThat(
        sut.isSubElement(
            "/bpel:process[0:11]/bpel:sequence[11:69]/bpel:if[11:140]/else[22:150]"),
        is(true)
    );
    assertThat(
        sut.isSubElement(
            "/bpel:process[0:11]/bpel:sequence[11:69]/bpel:if[11:140]/bpel:else[22:150]"),
        is(true)
    );
    assertThat(
        sut.isSubElement(
            "/bpel:process[0:11]/bpel:sequence[11:69]/bpel:if[11:140]"),
        is(false)
    );
    assertThat(
        sut.isSubElement(
            "/bpel:process[0:11]/bpel:sequence[11:69]/if[11:140]"),
        is(false)
    );
  }

  @Test
  void nearestElement() {
    BpelPath sut = new BpelPath(
        "/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[3.40]/else[1.50]/sequence[1.60]/if[1.80]");
    assertThat(
        sut.getNearestElement("if").toString(),
        is("/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[3.40]/else[1.50]/sequence[1.60]/if[1.80]")
    );
    assertThat(
        sut.getNearestElement("while").toString(),
        is("/process[10.0]/sequence[13.10]/while[2.20]")
    );
    assertThat(
        sut.getNearestElement("sequence").toString(),
        is("/process[10.0]/sequence[13.10]/while[2.20]/sequence[9.30]/if[3.40]/else[1.50]/sequence[1.60]")
    );
    assertThat(
        sut.getNearestElement("process").toString(),
        is("/process[10.0]")
    );
    assertThat(
        sut.getNearestElement("unknown"),
        is(nullValue())
    );
    sut = new BpelPath("/bpel:process[0:11]/bpel:sequence[11:69]/bpel:if[11:140]");
    assertThat(
        sut.getNearestElement("if").toString(),
        is("/bpel:process[0:11]/bpel:sequence[11:69]/bpel:if[11:140]")
    );
  }
}
