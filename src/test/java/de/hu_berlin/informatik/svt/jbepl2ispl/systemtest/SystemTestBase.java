package de.hu_berlin.informatik.svt.jbepl2ispl.systemtest;

import de.hu_berlin.informatik.svt.jbepl2ispl.Config;
import de.hu_berlin.informatik.svt.jbepl2ispl.IsEqualIspl;
import de.hu_berlin.informatik.svt.jbepl2ispl.MainApp;
import de.hu_berlin.informatik.svt.jbepl2ispl.TransformationService;
import de.hu_berlin.informatik.svt.jbepl2ispl.model.stateflow.FlowModel;
import de.hu_berlin.informatik.svt.jbepl2ispl.utils.Jb2iUtils;
import org.junit.jupiter.api.BeforeEach;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
abstract class SystemTestBase {

  @Autowired ApplicationContext applicationContext;
  @Autowired FlowModel          model;
  @Autowired Session            session;

  void testIsplGeneration(String pathToFixture, String pathToIsplCompareFixture) throws Exception {
    pathToFixture = "./src/test/resources/fixtures/bpel/" + pathToFixture;
    pathToIsplCompareFixture = "./src/test/resources/fixtures/bpel/" + pathToIsplCompareFixture;
    TransformationService transformationService = applicationContext.getBean(TransformationService.class);

    final File canonicalFile = new File(pathToFixture).getCanonicalFile();
    transformationService.setInputBpel(canonicalFile);
    final Path tempFile = Files.createTempFile(
        "jbpel2ispl-" + com.google.common.io.Files.getNameWithoutExtension(canonicalFile.getName()), ".ispl");
    transformationService.setOutputIspl(tempFile.toFile());
    transformationService.call();

    final List<String> actual                     = Files.readAllLines(tempFile);
    final Path         pathToCompareFixtureAsPath = Paths.get(pathToIsplCompareFixture);
    Files.write(
        Paths.get("./build/" + pathToCompareFixtureAsPath.getFileName().toString()),
        Jb2iUtils.normalizeISPL(actual)
    );
    assertThat(
        actual,
        IsEqualIspl.equalsIspl(
            Files.readAllLines(pathToCompareFixtureAsPath)
        )

    );
  }

  @BeforeEach
  private void setUp() {
    MainApp.setApplicationContext(applicationContext);
    MainApp.setConfig(applicationContext.getBean(Config.class));
  }
}
