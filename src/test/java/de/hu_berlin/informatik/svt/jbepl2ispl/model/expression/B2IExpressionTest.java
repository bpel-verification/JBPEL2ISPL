package de.hu_berlin.informatik.svt.jbepl2ispl.model.expression;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
class B2IExpressionTest {

  private static Stream<Arguments> equalsDataProvider() {
    return Stream.of(
        Arguments.of(
            B2IExpression.term(1),
            B2IExpression.term(1)
        ),
        Arguments.of(
            B2IExpression.negate(B2IExpression.term(1)),
            B2IExpression.negate(B2IExpression.term(1))
        ),
        Arguments.of(
            B2IExpression.arithmetic(
                B2IExpression.Type.NUM_ADD,
                B2IExpression.variableReference("x"),
                B2IExpression.term(1)
            ),
            B2IExpression.arithmetic(
                B2IExpression.Type.NUM_ADD,
                B2IExpression.variableReference("x"),
                B2IExpression.term(1)
            )
        )
    );
  }

  @ParameterizedTest
  @MethodSource("equalsDataProvider")
  void testEquals(B2IExpression input, B2IExpression expected) {
    Assert.assertThat(input, Matchers.is(Matchers.equalTo(expected)));
  }
}
