package de.hu_berlin.informatik.svt.jbepl2ispl.systemtest;

import de.hu_berlin.informatik.svt.jbepl2ispl.TransformationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.test.autoconfigure.data.neo4j.DataNeo4jTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Created by Christoph Graupner on 2018-04-28.
 *
 * @author Christoph Graupner <ch.graupner@workingdeveloper.net>
 */
@ExtendWith(SpringExtension.class)
@DataNeo4jTest(
    excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = ApplicationRunner.class)
)
@ComponentScan(basePackageClasses = {TransformationService.class})
@ActiveProfiles("test")
public class SoftwareSaleExampleTest extends SystemTestBase {

  @Test
  public void testRun() throws Exception {
//    Files.write(
//        Paths.get("./build/" + "bpeltest.ispl"),
//        IsEqualIspl.normalizeISPL(Files.readAllLines(Paths.get("./src/test/resources/fixtures/bpel/bpeltest.ispl")))
//    );

    testIsplGeneration(
        "bpel/bpeltest.bpel",
        "bpel/bpeltest.ispl"
    );
  }
}
