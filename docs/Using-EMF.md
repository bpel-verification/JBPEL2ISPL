# Use EMF

https://wiki.eclipse.org/EMF/FAQ#How_do_I_use_EMF_in_standalone_applications_.28such_as_an_ordinary_main.29.3F

An EMF model can be used without change in a standalone application with couple of exceptions.

Default resource factories are not registered in the standalone EMF environment. Therefore, for each file extension or scheme your application wants to load or save, you need to register the corresponding resource factory. For example, to load and save XMI documents, add a line similar to the one below to your main program:

    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("library", new XMIResourceFactoryImpl());

To load and save XML documents, register the following factory:

    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xml", new XMLResourceFactoryImpl());

In the above examples, the model is registered using the global static resource factory registry instance. You can also register factories local to the ResourceSet being used, e.g:

    ResourceSet rs = new ResourceSetImpl();
    rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xml", new XMLResourceFactoryImpl());

In addition, you'll also need to register your package, which happens as a side effect of accessing `XyzPackage.eINSTANCE`. For example, to register the library model, add the following line to your main program:

    LibraryPackage.eINSTANCE.eClass();

You need to add to the `CLASSPATH` all the jars for the plugins (and their dependent plugins) that you want to use standalone. The list is dependent on exactly what plugins you want to use and is available by looking at the runtime libraries in their plugin.xml files. This includes, but is not limited to:

    org.eclipse.emf.common_2.3.0.v200706262000.jar
    org.eclipse.emf.ecore_2.3.0.v200706262000.jar
    org.eclipse.emf.ecore.xmi_2.3.0.v200706262000.jar

The jars are found under the plugins directory of your Eclipse installation. There will be a release number appended to the plugin name, e.g. `org.eclipse.emf.common_2.3.0.v200706262000`.

For `EMF.Edit` you would also have to add the following jar files to your classpath:

    org.eclipse.emf.common.ui_2.3.0.v200706262000.jar
    org.eclipse.emf.edit_2.3.0.v200706262000.jar

### I want to use EMF, SDO, or XSD in my standalone project, or include only a working subset of the code. What libraries (jar files) do I need in my CLASSPATH?

Eclipse Modeling Framework (EMF) provides the infrastructure to run the generated models and dynamic model based on existing ecore files. It also provides XMI/XML serialization and deserialization.

The following jars can be used in standalone mode. Jars are found under the plugins directory of your Eclipse installation, e.g. org.eclipse.emf.common_2.3.0.v200706262000.jar.

Add to the following jars to your CLASSPATH or copy them into your project. For EMF 2.3.0, this includes, but is not limited to:

    org.eclipse.emf.common_2.3.0.v200706262000.jar
    org.eclipse.emf.ecore_2.3.0.v200706262000.jar
    org.eclipse.emf.ecore.change_2.3.0.v200706262000.jar
    org.eclipse.emf.ecore.xmi_2.3.0.v200706262000.jar

Service Data Objects (SDO) is an API specification in the format of Java interfaces and the EMF implementation that requires the EMF jars above and the following jars to be used in standalone mode:

    org.eclipse.emf.commonj.sdo_2.3.0.v200706262000.jar
    org.eclipse.emf.ecore.sdo_2.3.0.v200706262000.jar

XML Schema Infoset Model (XSD) is a reference library for use with any code that manipulates XML schemas and requires the EMF jars above and the following jars to be used in standalone mode:

    org.eclipse.xsd_2.3.0.v200706262000.jar

For more information on working outside Eclipse, see the question How do I use EMF in standalone applications (such as an ordinary main)?. See also Standalone Zip removed in EMF 2.3. For EMF 2.1 standalone, see the archives.

### What's required to load a UML (.uml) resource from a standalone application?

In order to load a UML (.uml) resource, the package (schema), resource factory, and "pathmaps" for UML need to be registered. On the Eclipse platform, this is done through the org.eclipse.emf.ecore.generated_package, org.eclipse.emf.ecore.extension_parser, and org.eclipse.emf.ecore.uri_mapping extension points (see the plug-in manifests for the org.eclipse.uml2.uml and org.eclipse.uml2.uml.resources plug-ins). When loading resources externally from Eclipse, your application needs to perform these registrations programmatically as follows (given a variable resourceSet that points to an instance of ResourceSetImpl):

    resourceSet.getPackageRegistry().put(UMLPackage.eNS_URI, UMLPackage.eINSTANCE);

    resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
    Map uriMap = resourceSet.getURIConverter().getURIMap();
    URI uri = URI.createURI("jar:file:/C:/eclipse/plugins/org.eclipse.uml2.uml.resources_<version>.jar!/"); // for example
    uriMap.put(URI.createURI(UMLResource.LIBRARIES_PATHMAP), uri.appendSegment("libraries").appendSegment(""));
    uriMap.put(URI.createURI(UMLResource.METAMODELS_PATHMAP), uri.appendSegment("metamodels").appendSegment(""));
    uriMap.put(URI.createURI(UMLResource.PROFILES_PATHMAP), uri.appendSegment("profiles").appendSegment(""));

The last `appendSegment("")` appends a trailing slash, don't forget it when you create the URIs manually.
