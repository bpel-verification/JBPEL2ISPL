# Cypher

## Retrieve special `State`s

### Retrieve `State`s where a variable `x` is written

    MATCH (n:State)-[:VARIABLE_VALUE]->(:VariableValue)-[:VALUE_FOR]->(:Variable {name: "authSessionResult"}) return n

## Get a specific state

    MATCH (n:State {elementId: 9}) return n

### Get a specific 'IF' state following after a given state

    MATCH (n:State {elementId: 9})-[:FOLLOWED_BY*]->(s:State {causingActivity: "IF"}) return s

### Get a 'IF' state with a specific guard condition following after a given state

    MATCH (n:State {elementId: 9})-[:FOLLOWED_BY*]->(s:State {causingActivity: "IF"})-[g:FOLLOWED_BY]->() WHERE g.guard =~ '.*authSessionResult.*' return s,g

###

    MATCH (n:State {elementId: 9})-[:FOLLOWED_BY*]->(s:State {causingActivity: "IF",beginEndType: "BEGIN"})-[g:FOLLOWED_BY]->() WHERE g.guard =~ '.*authSessionResult.*' return s,g

###

    MATCH (n:State {elementId: 9})-[:FOLLOWED_BY*]->(s:State {beginEndType: "BEGIN"})-[g:FOLLOWED_BY]->() WHERE g.guard =~ '.*authSessionResult.*' AND s.causingActivity =~ '(IF|IF_CASE)' return s,g

###

    MATCH (n:State {elementId: 9})-[:FOLLOWED_BY*]->(s:State {beginEndType: "BEGIN"})-[g:FOLLOWED_BY]->() WHERE g.guard =~ '.*\\$authSessionResult\\b.*' AND s.causingActivity =~ '(IF|IF_CASE)' return s,g

###

    MATCH p=shortestPath((n:State {elementId: 88})-[:FOLLOWED_BY*0..200]->(x:State)) WHERE id(n)<>id(x) WITH p,x MATCH (x)-[:INPUT_VARIABLE]->(v:Variable {name: 'Software_components'}) RETURN p,v

###

    MATCH p=shortestPath((n:State {elementId: 88})-[:FOLLOWED_BY*0..200]->(x:State))
    ,(x)-[:INPUT_VARIABLE]->(v:Variable {name: 'Software_components'})
    WHERE id(n)<>id(x)
    RETURN p,v

### Retrieve variables belonging to agent

    MATCH p=((n:AgentVariableDefinition)-[:BELONGS_TO]->(:AgentAutomaton)) RETURN p
    MATCH p=((n:AgentVariableDefinition)<-[:VARIABLE]->(:AgentAutomaton)) RETURN p

### States talking to other

    WHERE id(s1)<>id(s2) and (s1.causingActivity ="ASSIGN" or  s1.causingActivity ="RECEIVE")
    WITH s1,s2,p
    MATCH (s1)-[:SOURCE_SERVICE|TARGET_SERVICE]->(srv:Service)
    MATCH (s2)-[:TARGET_SERVICE|SOURCE_SERVICE]->(srv2:Service)
    WHERE id(srv)<>id(srv2) and srv.name<>"Environment" and srv2.name<>"Environment"
    RETURN srv,srv2,p

    MATCH (s:Service)-[:FL_RECEIVER|FL_SENDER]-(m:Messaging)-[:FL_RECEIVER|FL_SENDER]-(st:Service)
    WHERE st.name={serviceName} AND id(st)<>id(s)
    RETURN DISTINCT s

### Variable READ-WRITE graph

    MATCH p=()-[r:FLOW_READS_FROM|FLOW_WRITE_TO]->() RETURN p

### find nearest INVOKE from VARIABLE starting at STATE x (not correct right now)

    MATCH p=allShortestPaths((s1:State)-[:FLOW_WRITE_TO|FLOW_READS_FROM*1..200]->(s2:State {causingActivity: "INVOKE"}))
    WHERE id(s1)<>id(s2) and (s1.causingActivity ="ASSIGN" or  s1.causingActivity ="RECEIVE")
    WITH s1,s2,p,relationships(p) as rl
    WHERE ALL(idx in range(0, size(rl)-2) WHERE (rl[idx]).elementId > 1 AND (rl[idx]).elementId < (rl[idx+1]).elementId)
    WITH s1,s2,p
    MATCH (s1)-[:SOURCE_SERVICE|TARGET_SERVICE]->(srv:Service)
    MATCH (s2)-[:TARGET_SERVICE|SOURCE_SERVICE]->(srv2:Service)
    WHERE id(srv)<>id(srv2) and srv.name<>"Environment" and srv2.name<>"Environment"
    RETURN srv,srv2,p

Get 1

    MATCH p=shortestPath((s1:State)-[:FLOW_WRITE_TO|FLOW_READS_FROM*1..200]->(s2:State))
    WHERE id(s2)=29 and id(s1)<>id(s2) and (s1.causingActivity ="ASSIGN" or  s1.causingActivity ="RECEIVE")
    WITH s1,s2,p,relationships(p) as rl
    WHERE ALL(idx in range(0, size(rl)-2) WHERE (rl[idx]).elementId > 1 AND (rl[idx]).elementId < (rl[idx+1]).elementId)
    WITH s1,s2,p
    MATCH (s1)-[:SOURCE_SERVICE|TARGET_SERVICE]->(srv1:Service)
    MATCH (s2)-[:TARGET_SERVICE|SOURCE_SERVICE]->(srv2:Service)
    WHERE id(srv1)<>id(srv2) and srv.name<>"Environment" and srv2.name<>"Environment"

    RETURN srv1,srv2,p
    ORDER BY s1.elementId DESC

### find backward INVOKE/RECEIVE, which writes variable

    MATCH (s1:State)-[:FLOW_WRITE_TO]->(v:Variable {name: {varName}})
    WHERE s1.causingActivity = "INVOKE" OR s1.causingActivity = "RECEIVE"
    WITH s1,v
    MATCH p=shortestPath((v)-[:FLOW_WRITE_TO|FLOW_READS_FROM*1..200]->(s2:State))
    WHERE id(s1)<>id(s2) and id(s2)={startingStateId}
    WITH s1,s2,relationships(p) as rl
    WHERE ALL(idx in range(0, size(rl)-2) WHERE (rl[idx]).elementId > 1 AND (rl[idx]).elementId < (rl[idx+1]).elementId)
    WITH s1,s2
    MATCH (s1)-[:SOURCE_SERVICE|TARGET_SERVICE]->(srv1:Service)
    MATCH (s2)-[:TARGET_SERVICE|SOURCE_SERVICE]->(srv2:Service)
    WHERE srv1.name<>"Environment" and srv2.name<>"Environment"
    RETURN srv1
    ORDER BY s1.elementId
    DESC LIMIT 1

### find forward INVOKE state, which reads variable

    MATCH (s1:State)-[r:FLOW_WRITE_TO]->(v:Variable {name: "auditLogRequest"})
    WHERE s1.elementId = 20
    WITH s1,v,r
    MATCH p=shortestPath((v)-[:FLOW_READS_FROM|FLOW_WRITE_TO*1..200]->(s2:State))
    WHERE id(s1)<>id(s2) AND s1.elementId < s2.elementId AND (s2.causingActivity IN [ "INVOKE", "REPLY"])
    WITH s1,s2,relationships(p) as rl,p,r
    WHERE
    NONE( n IN nodes(p)[2..length(p)-1] WHERE n.causingActivity IN [ "INVOKE", "REPLY"])
    AND ALL(idx in range(0, size(rl)-2) WHERE (rl[idx]).elementId > r.elementId AND (rl[idx]).elementId < (rl[idx+1]).elementId)
    return s2

## Variable Access Graph

    MATCH p=()-[r:FLOW_READS_FROM|FLOW_WRITE_TO]->() RETURN p

### Graph with Messaging connections

    MATCH (n:State)
     OPTIONAL MATCH p=()-[r:FLOW_READS_FROM|FLOW_WRITE_TO]->()
     OPTIONAL MATCH (m:Messaging)
     RETURN n,m,p

### Show FlowReadWrite with Variable Access Graph together

    MATCH p1=(s1)-[r:FLOW_READS_FROM|FLOW_WRITE_TO]->(s2)
    OPTIONAL MATCH (m:Messaging)
    RETURN p1,m

### Show FlowReadWrite with target services

    MATCH p2=(s1)-[r:FLOW_READS_FROM|FLOW_WRITE_TO]->(s2)
    OPTIONAL MATCH p3=(s1)-[sr1:SOURCE_SERVICE|TARGET_SERVICE]->(srv1)
    OPTIONAL MATCH p4=(s2)-[sr2:SOURCE_SERVICE|TARGET_SERVICE]->(srv2)
    RETURN p2,p3,p4

### States with are neighbors and read and write the same variables

    MATCH (s1:State)-[:FOLLOWED_BY]-(s2:State) WITH s1,s2
    MATCH (s1)-[:FLOW_WRITE_TO]->(v:Variable) WHERE (s2)-[:FLOW_WRITE_TO]->(v)
    WITH s1,s2,v
    MATCH (s1)<-[:FLOW_READS_FROM]-(v2:Variable) WHERE (s2)<-[:FLOW_READS_FROM]-(v2)
    RETURN s1,s2,v,v2

### States what are neighbors and read and write the same variables or one reads from ___LITERAL__

    MATCH (s1:State)-[:FLOW_WRITE_TO]->(varWrite:Variable)<-[:FLOW_WRITE_TO]-(s2:State)
     WHERE (s1)-[:FOLLOWED_BY]-(s2)
    WITH s1,s2,varWrite
    MATCH (s1)<-[:FLOW_READS_FROM]-(varRead:Variable)
     WHERE ((s2)<-[:FLOW_READS_FROM]-(varRead) OR (s2)<-[:FLOW_READS_FROM]-(:Variable {name: "___LITERAL__"}))
    RETURN s1,s2,varWrite,varRead

### Values for a specific variable

    MATCH (s:AgentState)
     WHERE any(k IN keys(s) WHERE k = {varName})
     RETURN s[{varName}]

## Commitment and Fulfillment

###

    MATCH (s:State)
    OPTIONAL MATCH (c:ScCommitment)
    OPTIONAL MATCH (f:ScFulfillment)
    OPTIONAL MATCH (t:ScTraceLabel)
    OPTIONAL MATCH  (m:Messaging)
    RETURN s,c,f,t,m

###

    MATCH p=(c:ScCommitment)-->(s:State)
    RETURN p


MATCH p=allShortestPaths((c1:State)-[:FOLLOWED_BY*1..200]->(f1:State)) WHERE c1.elementId = 8 AND f1.elementId = 52 RETURN p

## Paths that exclude a specific node

    MATCH p=allShortestPaths((c1:State)-[:FOLLOWED_BY*1..200]->(f1:State))
    WHERE c1.elementId = 1 AND f1.elementId = 52
    AND NONE(x IN NODES(p) WHERE x:State AND x.elementId IN [15] )
    RETURN p
    ORDER BY LENGTH(p)
    DESC LIMIT 25

MATCH (n:State) where n.name IN [1,52]
WITH collect(n) as nodes
UNWIND nodes as n
UNWIND nodes as m
WITH * WHERE id(n) < id(m)
MATCH path = allShortestPaths( (n)-[*..200]-(m) )
RETURN path


b__client__greetService