# Graph DBs

* <http://tinkerpop.apache.org>

* <https://github.com/lambdazen/bitsy>
* <https://github.com/MartinHaeusler/chronos>
* <https://neo4j.com/docs/operations-manual/current/installation/docker/>
* <http://infinispan.org/tutorials/simple/query/>
* <https://docs.spring.io/spring-data/jpa/docs/1.3.0.RELEASE/reference/html/jpa.repositories.html>

